﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="shopctrl.ascx.cs" Inherits="control_shopctrl" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>
<style type="text/css">
    .style1
    {
        font-size: smaller;
    }
    .style2
    {
        color: #FF3300;
    }
    .style3
    {
        color: #666666;
        font-size: smaller;
    }
</style>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
     <div class="title"></div>

      <div runat="server" id="TABLE2" >
       <a href="/product"> <span class="CartEmpty"></span></a> 
     <div class="title"></div>
    </div>
   <div > 
    <div runat="server" id="TABLE1" class="category_row">
    <div class="title"><span><h5>Following Item(s) are successfully added to your Cart.</h5> </div>
    <div class="title m10"><h5>Your Cart contains  <asp:Label ID="lbltitem" runat="server"></asp:Label> item(s). </h5></div>


<div class="m10 row">
         <asp:GridView id="GridView1" CssClass="table" runat="server" onrowdeleting="GridView1_RowDeleting" PageSize="20" DataKeyNames="pid" AutoGenerateColumns="False">
    <Columns>
        <asp:TemplateField HeaderText="Item(s)">
            <ItemStyle Font-Bold="False" HorizontalAlign="Center"  />
            <ItemTemplate>
                <asp:Label ID="lblitemid" runat="server" Text='<%# bind("itemid") %>' 
                    Visible="False"></asp:Label>
                      <asp:Label ID="lblPid" runat="server" Text='<%# bind("pid") %>' 
                    Visible="False"></asp:Label>
                <div class="cartImg">
                     <img src="/ProductImage/<%# GetImg(Eval("itemid")) %>" alt="Product image" />
                </div>
              <div class="text">
                            <asp:Label ID="lblitrm" runat="server" Text='<%# bind("item") %>'></asp:Label>
                   </div>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Unit Price ₹">
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Eval("price") %>'></asp:Label>
               
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Quantity">
            <ItemStyle Font-Bold="False" Font-Size="10px" HorizontalAlign="Center" 
                Width="150px"  />
            <ItemTemplate>
                <asp:TextBox ID="txtqty"  runat="server" Width="70px" AutoPostBack="true" CssClass="txt_1" 
                    MaxLength="5" OnTextChanged="txtqty_TextChanged" 
                    Text='<%# bind("quantity")  %>' ></asp:TextBox>
                        <a href="">Update</a>
                    
            </ItemTemplate>
        </asp:TemplateField>


<asp:TemplateField HeaderText="Total Price ₹">
            <ItemTemplate>
             <asp:Label ID="lbl_tprice" Text='<%# (Convert.ToDouble(Eval("quantity")) * Convert.ToDouble(Eval("price"))).ToString() %>' runat="server"></asp:Label>
            </ItemTemplate>
</asp:TemplateField>



        <asp:TemplateField>
            <ItemTemplate>
               
                    <asp:LinkButton ID="dellnk" runat="server" CausesValidation="false" 
                        CommandName="Delete" OnClientClick="return confirm('Do You Want To Delete?')" 
                        text="Delete" CssClass="btn backgroundbg border-radius"></asp:LinkButton>
              
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" Width="75px"  />
        </asp:TemplateField>
    </Columns>
     <HeaderStyle  ForeColor="Black" Height="30px" BorderColor="White" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
   <RowStyle HorizontalAlign="Center" BackColor="#E4E6E8" BorderColor="White" Height="30px" />
   <PagerStyle BackColor="#BDC3C7" ForeColor="White" HorizontalAlign="Center" />
 <HeaderStyle BackColor="#BDC3C7" Font-Bold="True"  
       Font-Size="13px" ForeColor="Black" HorizontalAlign="Center"  />
   <AlternatingRowStyle HorizontalAlign="Center" BackColor="#E4E6E8" ForeColor="Black" />
</asp:GridView> 
</div>



<div class="title"></div>
<div class="title">
<div style="float:left; font-size:smaller">
<asp:Panel ID="pnl_cont" runat="server">
<span class="txtedt">Shop for</span> <span class="pricet">₹ </span><asp:Label ID="lbl_lessprice" runat="server" CssClass="pricet"></asp:Label> 
<span class="txtedt">more and avail FREE SHIPPING…  *</span>
<a class="tnc" href="/termsandconditions">T &amp; C apply</a>
</asp:Panel>
<asp:Literal ID="ltrl_more" runat="server">
<span class="txtedt">Congrats! Shopping Amount is more than </span><span class="pricet">₹&nbsp;1004/-</span><span class="txtedt"> So Shipping is FREE.*</span>
    
</asp:Literal>
</div>
<br />
<span class="blk"> <strong style="color:Red;">Note:</strong> The delivery for Orders made post 5:30 PM will be scheduled to next day.</span>
</div>

<div class="column border-box">
            <div class="row m20">
             <table class="table1">
                    <tr>
                        <th>
                           Purchase Amount
                        </th>
                      

                          <th>
                            Shipping Charges
                        </th>

                        <th>
                            Total Amount
                        </th>
                    </tr>
                    <tr>
                        <td>
                            &#8377; <asp:Label ID="lblAmount" runat="server" Text="0"></asp:Label>
                        </td>
                     
                         <td>
                           &#8377; <asp:Label ID="lbl_shipcharge" runat="server" Text="0"></asp:Label>
                        </td>
                        <td>
                          &#8377; <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </table>
                </div>
        </div>

<div class="row m10">



<div class="right"><asp:Button ID="lnkshop" runat="server" Text="Continue Shopping"  OnClick="lnkshop_Click1"   CssClass="_button1 border-radius backgroundbg" /></div>
<div class="left"><asp:Button ID="imgbtn"  CssClass="_button1 border-radius backgroundbg"  runat="server" Text="Checkout" OnClick="imgbtn_Click" /></div>
</div>
    </div>
    </div>
  
      
    </ContentTemplate>
</asp:UpdatePanel>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary
{
    public class SubCategoryCS
    {
        public string Category { get; set;}
        public string SubCategory { get; set; }
        public string Image { get; set; }
        public string CatID { get; set; }
        public string SubCatID { get; set; }
        public string SortID { get; set; } 
        public string IsVisible { get; set; }
        public string Description { get; set; }
        public string Submitby { get; set; }
    }
}

﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using DAL;
//using BLL;

namespace BAL
{
   public class modifyData
    {


       ManageData insert = new ManageData();
       public bool insertcat(catProperty cat)
       {
          

           bool check = insert.Insertcategory(cat.m_Category, cat.m_image,cat.m_sortid,cat.m_Description,cat.m_Submitby);

           return check;
       }
       public DataSet Selectcategory()
       {
           //return ins
          return    insert.Selectcategory();
       }

       public void deletecategory(string catid)
       {
           insert.DelecteCategory(catid);

       }

       public bool updatecat(catProperty cat)
       {

           bool check = insert.updatecategory(cat.m_catid, cat.m_Category, cat.m_image, cat.m_sortid, cat.m_Description);
           return check;
       }


       /// <summary>
       /// //////Product
       /// </summary>
       /// <returns></returns>
       public DataSet CategoryForProduct()
       {
           //return ins
           return insert.selectCategoryForProduct();
       }

       public DataSet SelectProduct(string id)
       {
           //return ins
           return insert.SelectProduct(id);
       }
       public bool insertProduct(ProductProperty pro)
       {


           bool check = insert.insertproduct(pro.p_categoryname, pro.p_subcategoryId, pro.p_code, pro.p_name, pro.p_image, pro.P_price, pro.P_offerprice, pro.P_Detail, pro.P_productprice, pro.P_BestSellers, pro.P_DailyDeal, pro.isnew, pro.IsVisible, pro.P_IsFeatured, pro.P_Size, pro.P_Weight, pro.P_Submitby, pro.p_ShortDescripn, pro.p_buyingprice, pro.p_wholesaleprice);

           return check;
       }

       public void deleteProduct(string id)
       {
           insert.deleteProduct(id);

       }

       public bool updateItem(ProductProperty pro)
       {

           return insert.updateItem(pro.p_id, pro.p_code, pro.p_name, pro.p_image, pro.P_price, pro.P_offerprice, pro.P_Detail, pro.P_productprice, pro.P_BestSellers, pro.P_DailyDeal, pro.isnew, pro.P_IsVisible, pro.P_IsFeatured, pro.P_Size, pro.P_Weight,pro.p_ShortDescripn,pro.p_buyingprice,pro.p_wholesaleprice,pro.p_categoryname,pro.p_subcategoryId);
          
       }

       //user

       public DataSet selAllUsers()
       {
           return insert.selAllUsers();


       }

       public DataSet selSearchUsersdata(string srchtxt)
       {

           return insert.selSearchUsersdata(srchtxt);
         
       }

       public void datainsert(userproperty up)
       {

          insert.datainsert(up.U_uname, up.U_password, up.U_name, up.U_email, up.U_ph,up.U_altph, up.U_addr, up.U_city, up.U_state, up.country, up.U_zip);
                                    

       }

       public bool IsUsernameAvailable(string Username)
       {

           return insert.IsUsernameAvailable(Username);
       }


       public bool IsMobileAvailable(string Mobile)
       {

           return insert.IsMobileAvailable(Mobile);
       }



       public bool IsEmailAvailable(string email)
       {

           return insert.IsEmailAvailable(email);
       }
       public void deleteUser(string uname)
       {
           insert.deleteUser(uname);

       }


       //stock
       public DataSet SelectProductForstock()
       {
           return insert.SelectProductForstock();
       
       
       }

       public string SetQty(string itemid)
       {
           return insert.SetQty(itemid);
       }

       public void InserStkQty(string itemid, string qty)
       {
           insert.InserStkQty(itemid, qty);
       }

       public void updateuser(string UserName, string Password, string Name, string Email, string Phone, string Address, string City, string State, string Zipcode, string registerdate)
       {
           insert.updateuser(UserName,  Password,  Name,  Email, Phone, Address,City,  State,  Zipcode, registerdate);

       }

       public bool checkadmin(string uname, string password)
       {
           return insert.checkadmin(uname, password);
       }

       public bool checkUser(string uname, string password)
       {
           return insert.checkUser(uname, password);
       }
       public bool IsPrdctExistCart(string itemid, string buyer, string UserType)
       {
           return insert.IsPrdctExistCart(itemid, buyer, UserType);
       }
       public void UpdatePurchaseItem(string pid, string qty, string buyer)
       {
           insert.UpdatePurchaseItem(pid, qty, buyer);
       }

       public void inserProduct(string catid, string quantity, string buyer, string itemid, string Orderno, string item, string amount,string UserType)
       {
           insert.inserProduct(catid, quantity, buyer, itemid, Orderno, item, amount, UserType);
       }
       public Int32 SelPurItem(string uid)
       {

           return insert.SelPurItem(uid);
       }

       public DataSet SelPurProducts(string buyer, string Usertype)
       {
           return insert.SelPurProducts(buyer, Usertype);
       }

       //public string GetMaxOrderID()
       //{
       //    insert.GetMaxOrderID();
       //}
       public void UpdateQuantity(string qty, string itemid)
       {
           insert.UpdateQuantity(qty, itemid);

       }

       public DataSet registerdata(string uname)
       {
           return insert.registerdata(uname);
       }
       public DataSet registerdataForMobile(string Mobile)
       {
           return insert.registerdataForMobile(Mobile);
       }
       public string GetMaxOrderID()
       {
         return  insert.GetMaxOrderID();
       }
    
   

       public void UpdatePrchDtl(string sid, String pid, String ordno)
       {
           insert.UpdatePrchDtl(sid, pid, ordno);
       }
       public void InsertFinalPurchase(string ord, string uid, string addr, string amount, string Discount, string UsedLoyalty, string MainAmount)
       {
           insert.InsertFinalPurchase(ord, uid, addr, amount, Discount, UsedLoyalty, MainAmount);
       }

       public void InsertFinalPurchaseForCall(string ord, string uid, string addr, string amount, string Discount, string UsedLoyalty, string MainAmount, string CallExecutive, string SalesName, string UserType)
       {
           insert.InsertFinalPurchaseForCall(ord, uid, addr, amount, Discount, UsedLoyalty, MainAmount, CallExecutive, SalesName, UserType);
       }


       public void UpdatePrchflag(string ordno, string flag)
       {
           insert.UpdatePrchflag(ordno, flag);
       }

       public DataSet SelFinalProductsbydate(string dt1, string dt2)
       {
          return  insert.SelFinalProductsbydate(dt1, dt2);

       }
       public DataSet SelAllFinalProducts1()
       {
           return insert.SelAllFinalProducts1();
       }

       public string SelOrdProduct(string ord)
       {
           return insert.SelOrdProduct(ord);
       }
       public DataSet SelFinalOrdDtl(string ord)
       {
           return insert.SelFinalOrdDtl(ord);
       }
       public DataSet SelOrd(string ord)
       {
           return insert.SelOrd(ord);
       }
       public string selectaddress(string orderno)
       {
           return insert.selectaddress(orderno);
       }
       public void DeleteOrders(string orderID)
       {
           insert.DeleteOrders(orderID);
       }
       public string getcatname(string catid)
       {
          return insert.getcatname(catid);
       }
       public string getimagename(string itemid)
       {
           return insert.getimagename(itemid);
       }
       public void DeletePurchaseItem(string pid)
       {

           insert.DeletePurchaseItem(pid);
       }

       public string ChgUserPass(string Username, string pass, string newpass)
       {
         return  insert.ChgUserPass(Username,pass,newpass);
       }

       public string FindNameByUserName(string Username)
       {
         return  insert.FindNameByUserName(Username);
       }
       public string SelCatIDbycategory(string cat)
       {
           return insert.SelCatIDbycategory(cat);
       
       }

       public DataSet userSelCategory()
       {
           return insert.userSelCategory();
       }
       public DataSet userSelItemBycat(string cat)
       {
           return insert.userSelItemBycat(cat);
       }
       public void UpdateSessuid(string userid, string ses)
       {
           insert.UpdateSessuid(userid, ses);
       }
       public DataSet SearchbyPrice(string min, string max)
       {
           return insert.SearchbyPrice(min, max);
       }
       public DataSet SelFinalProducts(string uid)
       {
           return insert.SelFinalProducts(uid);
       }

       public void profileupdate(string Name, string Email, string Phone, string Address, string City, string State, string Zipcode, string Uid)
       {
           insert.profileupdate( Name,  Email,  Phone,  Address,  City,  State,  Zipcode,  Uid);
       }
       public void UpdtFnlDtl(string oid, String st, String dtl, String remark, string ClMsg)
       {
           insert.UpdtFnlDtl(oid, st, dtl, remark,ClMsg);
       }
       public DataSet SelFnlOrdDtl(string ord)
       {
           return insert.SelFnlOrdDtl(ord);
       }
       public string selectsmprice(string userid, string UsreType)
       {
           return insert.selectsmprice(userid, UsreType);
       }
       public DataSet SelectDispatchText()
       {
           return insert.SelectDispatchText();

       }
       public DataSet SelectProductByID(string id)
       {
           return insert.SelectProductByID(id);
       }
       public void InsertInWishlist(string Itemid, string Userid)
       {
            insert.InsertInWishlist(Itemid, Userid);
       }
       public void Updatewishlist(string userid, string ses)
       {
           insert.Updatewishlist(userid, ses);
       }
       public DataSet SelectWishListByID(string userid)
       {

           return insert.SelectWishListByID(userid);
       
       }
       public void Deletewishlist(string wid)
       {
           insert.Deletewishlist(wid);
       }
       public string getpercentage()
       {
           return insert.getpercentage();
       }
       public void InsertCheckout(string orderID, string uname, string BAddress, string SAddress, string Discount, string Promo, string paymentmode, string UsedLoyalty, string EarnLoyalty, string amount, string MainAmount)
       {
           insert.InsertCheckout( orderID,  uname,  BAddress,  SAddress,  Discount,  Promo,  paymentmode,  UsedLoyalty, EarnLoyalty, amount,  MainAmount);

       }
       public string getUserloyelty(string Userid)
       {
           return insert.getUserloyelty(Userid);
       }
       public DataSet SelectCheckOutFromOrderID(string orderid)
       {
           return insert.SelectCheckOutFromOrderID(orderid);
       }

  

       public void UserLoyaltyPoint(string Userid, string point)
       {
           insert.UserLoyaltyPoint(Userid, point);
       }
       public string UserLoyaltyPointbyUser(string Userid)
       {
           return insert.UserLoyaltyPointbyUser(Userid);
       }
       public void UpdateUserLoyaltyPoint(string Userid, string point)
       {
           insert.UpdateUserLoyaltyPoint(Userid, point);
       }
       public void InsertOrderHistory(string Loyalty, string ExpiryDate, string IssueDate, string Userid, string status, string Orderid)
       {
           insert.InsertOrderHistory(Loyalty, ExpiryDate, IssueDate, Userid, status, Orderid);
       }
       public string checExpiryLoyalty(string user)
       {
           return insert.checExpiryLoyalty(user);
       }
       public DataSet SelectloyatyHistory(string userid)
       {
           return insert.SelectloyatyHistory(userid);
       }
       public void InserPCode(string pmcd, string pab, string dis, string edt, string msg, string Ispracent)
       {
           insert.InserPCode(pmcd, pab, dis, edt, msg, Ispracent);
       }
       public bool CheckPCode(string CheckPCode)
       {
           return insert.CheckPCode(CheckPCode);
       }
       public DataSet SelAllPromo()
       {
           return insert.SelAllPromo();
       }
       public void UpdatePCode(string pmcd, string pab, string dis, string edt, string msg,  string Ispracent, string pid)
       {
           insert.UpdatePCode(pmcd, pab, dis, edt, msg, Ispracent, pid);
       }
       public void DelectePromo(string pid)
       {
           insert.DelectePromo(pid);
       }
       public DataSet SelAllPromoUser(string Promocode)
       {
           return insert.SelAllPromoUser(Promocode);
       }
       public string SelCatIDbycategoryForsearch(string cat)
       {
           return insert.SelCatIDbycategoryForsearch(cat);
       }
       public DataSet SelSearch(string searchtext)
       {
           return insert.SelSearch(searchtext);
       }
       public DataSet SelectProductRandam()
       {
           return insert.SelectProductRandam();
       }
       public string getreasonofcancellation(string orderid)
       {
           return insert.getreasonofcancellation(orderid);
       }
       public DataSet SelcancelProductsbydate(string dt1, string dt2)
       {
           return insert.SelcancelProductsbydate(dt1, dt2);

       }
       /// <summary>
       /// ////////Navin 2-5-14
       /// </summary>
       /// <returns></returns>
       public string SelectMaxForCat()
       {
           return insert.SelectMaxForCat();
       }

       public bool chkCatSortid(string sortid)
       {
           return insert.chkCatSortid(sortid);
       }

       public DataSet SelCatbysrtid(string sortid)
       {
           return insert.SelCatbysrtid(sortid);
       }

       public string UpdateSortid(string sortid, string catid)
       {
           return insert.UpdateSortid(sortid, catid);
       }

       public string updCatVisibility(string IsVisible, string catid)
       {
           return insert.updCatVisibility(IsVisible, catid);
       }

       public DataSet SelectProductforadmin(string cat,string scat)
       {
           return insert.SelectProductforadmin(cat,scat);
       }

       public DataSet SelCatforGroup()
       {
           return insert.SelCatforGroup();
       }

       public DataSet SelGrpProductforcat(string productID, string categoryname)
       {
           return insert.SelGrpProductforcat(productID, categoryname);
       }

       public void insertGroupProducts(string productid, string groupproductid)
       {
            insert.insertGroupProducts(productid, groupproductid);
       }


       public bool CheckGroupProducts(string productid, string groupproductid)
       {
         return   insert.CheckGroupProducts(productid, groupproductid);
       }


       public void deleteGroupProducts(string productID, string groupProductID)
       {
            insert.deleteGroupProducts(productID, groupProductID);
       }

       public DataSet SelGrpProductShow(string productID)
       {
           return insert.SelGrpProductShow(productID);
       }

       public DataSet SelIndexProduct()
       {
           return insert.SelIndexProduct();
       }

       public DataSet Selecttopproduts()
       {
           return insert.Selecttopproduts();
       }

       public string checkrole(string Username)
       {
           return insert.checkrole(Username);
       }

       public DataSet SelectCatdescription(string catid)
       {

           return insert.SelectCatdescription(catid);
       }

       public DataSet SelFinalProductsordandname(string username)
       {
           return insert.SelFinalProductsordandname(username);
       }

       public DataSet SelFinalProductsordandnameAndDate(string username, string dt1, string dt2)
       {
           return insert.SelFinalProductsordandnameAndDate(username, dt1, dt2);
       }

       public string findPid(string itemid, string buyer)
       {
           return insert.findPid(itemid,buyer);
       }

       public DataSet SelAllUserDtl()
       {
           return insert.SelAllUserDtl();
       }

       
     
    }
}

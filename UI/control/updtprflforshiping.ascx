<%@ Control Language="C#" AutoEventWireup="true" CodeFile="updtprflforshiping.ascx.cs" Inherits="controls_updtprflforshiping" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>

   <table id="Table1" width="100%" border="0" align="center"  cellpadding="0" cellspacing="0" >
            <tr><td>
              
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    
    
<asp:DataList ID="DataList1" runat="server" OnCancelCommand="DataList1_CancelCommand" OnEditCommand="DataList1_EditCommand" OnItemCommand="DataList1_ItemCommand" DataKeyField="uid" Width="100%">
    <ItemTemplate>
       <table id="Table1" width="60%" border="0" align="center"  cellpadding="2" cellspacing="2" class="form-tbl">
            <tr>
                <td width="35%" align="right">
                    Full  Name:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
                <td align="left" ><asp:Label ID="lbl1" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
        </td>
    </tr>
    <tr >
        <td width="35%" align="right" >
            Phone Number:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
        <td align="left" ><asp:Label ID="Label2" runat="server" Text='<%# Eval("Phone") %>'></asp:Label>
        </td>
    </tr>
            <tr >
                <td width="35%" align="right">
                    Email:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
                <td align="left"><asp:Label ID="Label1" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="35%" align="right">
                    Address 
                </td>    <td width="1%" class="lft-pad15">&nbsp;</td>
                <td align="left"><asp:Label ID="Label3" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="35%" align="right">
                    City:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
                <td align="left"><asp:Label ID="Label4" runat="server" Text='<%# Eval("City") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="35%" align="right">
                    State:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
                <td align="left"><asp:Label ID="Label5" runat="server" Text='<%# Eval("State") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="35%" align="right">
                    ZipCode:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
                <td align="left"><asp:Label ID="Label6" runat="server" Text='<%# Eval("Zipcode") %>'></asp:Label>
                </td>
            </tr>
        
            <tr> <td width="35%" align="right">
            </td>    <td class="lft-pad15">&nbsp;</td>
                <td   align="left" >
                   <asp:LinkButton ID="lnkbedit" runat="server" CommandName="Edit" >Edit</asp:LinkButton></td>
            </tr>
        </table>
    </ItemTemplate>
    <EditItemTemplate>
        <table id="Table2" runat="server" border="0" align="center" cellpadding="2" cellspacing="2" width="60%">
    <tr>
        <td width="35%" align="right">
            Full
            Name:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
        <td  align="left">
         <asp:TextBox id="txtname" runat="server"  Text='<%# Eval("Name") %>' CssClass="input"></asp:TextBox>
            <cc1:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" controltovalidate="txtname"
                cssclass="error" display="Dynamic" errormessage="Enter Name!" setfocusonerror="True" ValidationGroup="uprflie"></cc1:requiredfieldvalidator><span
                  > 
            <cc1:regularexpressionvalidator id="RegularExpressionValidator4" runat="server" controltovalidate="txtname"
                cssclass="error" display="Dynamic" errormessage="Invalid Name!" setfocusonerror="True"
                validationexpression="^[a-z A-Z . /']{0,50}$" ValidationGroup="uprflie"></cc1:regularexpressionvalidator></td>
    </tr>
            <tr >
                <td width="35%" align="right">
                    Phone Number:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
                <td align="left">
            <asp:TextBox id="txtpno" runat="server" Text='<%# Eval("Phone") %>' CssClass="input"></asp:TextBox><span style="font-size: 12pt; font-family: Times New Roman"> 
            <cc1:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" controltovalidate="txtpno" display="Dynamic" errormessage="Enter Phone no." setfocusonerror="True" ValidationGroup="uprflie"></cc1:requiredfieldvalidator>
            <cc1:regularexpressionvalidator id="RegularExpressionValidator3" runat="server" controltovalidate="txtpno"
                cssclass="error" display="Dynamic" errormessage="Invalid Phone No." setfocusonerror="True"
                validationexpression="^[0-9 -]{10,11}$" ValidationGroup="uprflie"></cc1:regularexpressionvalidator>
        </td>
    </tr>
    <tr >
        <td width="35%" align="right">
            Email:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
        <td align="left">
            <asp:TextBox id="txtemail" runat="server" Text='<%# Eval("Email") %>' CssClass="input"></asp:TextBox><cc1:requiredfieldvalidator id="RequiredFieldValidator1" runat="server"
                    controltovalidate="txtemail" cssclass="error" display="Dynamic" errormessage="Enter Email Id!"
                    setfocusonerror="True" validationgroup="uprflie"></cc1:requiredfieldvalidator><cc1:regularexpressionvalidator
                        id="RegularExpressionValidator1" runat="server" controltovalidate="txtemail"
                        cssclass="error" display="Dynamic" errormessage="Invalid Email ID" setfocusonerror="True"
                        validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" validationgroup="uprflie"></cc1:regularexpressionvalidator></td>
    </tr>
    <tr>
        <td width="35%" align="right">
            Address 
        </td>    <td width="1%" class="lft-pad15">&nbsp;</td>
        <td align="left">
            <asp:TextBox id="txtaddr1" runat="server" Text='<%# Eval("Address") %>' CssClass="input"></asp:TextBox>
            <cc1:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" controltovalidate="txtaddr1"
                cssclass="error" display="Dynamic" errormessage="Enter Address!" setfocusonerror="True" ValidationGroup="uprflie"></cc1:requiredfieldvalidator>
            <cc1:regularexpressionvalidator id="RegularExpressionValidator2" runat="server" controltovalidate="txtaddr1"
                cssclass="error" display="Dynamic" errormessage="Invalid Address!" validationexpression="^[^<>%]{0,100}$" ValidationGroup="uprflie"></cc1:regularexpressionvalidator>
        </td>
    </tr>
    <tr>
        <td width="35%" align="right">
            City:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
        <td align="left">
            <asp:TextBox id="txtcity" runat="server" Text='<%# Eval("City") %>' CssClass="input"></asp:TextBox>
            <cc1:requiredfieldvalidator id="RequiredFieldValidator5" runat="server" controltovalidate="txtcity"
                cssclass="error" display="Dynamic" errormessage="Enter City!" setfocusonerror="True" ValidationGroup="uprflie"></cc1:requiredfieldvalidator>
            <cc1:regularexpressionvalidator id="RegularExpressionValidator5" runat="server" controltovalidate="txtcity"
                cssclass="error" display="Dynamic" errormessage="Invalid City!" setfocusonerror="True"
                validationexpression="^[^<>%]{0,50}$" ValidationGroup="uprflie"></cc1:regularexpressionvalidator>
        </td>
    </tr>
    <tr>
        <td width="35%" align="right">
            State:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
        <td align="left">
            <asp:TextBox id="txtstate" runat="server" Text='<%# Eval("State") %>' CssClass="input"></asp:TextBox><cc1:requiredfieldvalidator
                id="RequiredFieldValidator6" runat="server" controltovalidate="txtstate" cssclass="error"
                display="Dynamic" errormessage="Enter State!" setfocusonerror="True" ValidationGroup="uprflie"></cc1:requiredfieldvalidator><cc1:regularexpressionvalidator
                    id="RegularExpressionValidator7" runat="server" controltovalidate="txtstate"
                    cssclass="error" display="Dynamic" errormessage="Invalid State" setfocusonerror="True"
                    validationexpression="^[^<>%]{0,50}$" ValidationGroup="uprflie"></cc1:regularexpressionvalidator></td>
    </tr>
    <tr>
        <td width="35%" align="right">
            ZipCode:</td>    <td width="1%" class="lft-pad15">&nbsp;</td>
        <td align="left">
            <asp:TextBox id="txtzip" runat="server" Text='<%# Eval("Zipcode") %>' CssClass="input"
            ></asp:TextBox>
            <cc1:regularexpressionvalidator id="RegularExpressionValidator6" runat="server" controltovalidate="txtzip"
                cssclass="error" display="Dynamic" errormessage="Invalid Postal Code!" validationexpression="^[^<>%]{0,50}$" ValidationGroup="uprflie"></cc1:regularexpressionvalidator>
        </td>
    </tr>
    <tr>
     <td width="35%" align="right">
            </td>    <td  >&nbsp;</td>
        <td align="left">
          <asp:LinkButton ID="lnkbupdate" runat="server" CommandName="Update" ValidationGroup="uprflie">Update</asp:LinkButton>&nbsp;
                                <asp:LinkButton ID="lnkbcancel" runat="server" CommandName="Cancel" CausesValidation="False" >Cancel</asp:LinkButton></td>
    </tr>
</table>
    </EditItemTemplate>
</asp:DataList>
    </ContentTemplate>
</asp:UpdatePanel>
</td></tr></table>
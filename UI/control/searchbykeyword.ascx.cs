﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ClassLibrary;
using BAL;
using DAL;
public partial class control_searchbykeyword : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    Search srch = new Search();
    dlsearch objsrch = new dlsearch();
   public Utility all = new Utility();
    dlSubCategory objscat = new dlSubCategory();
    string SCatID, CatId, txt;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            show();
        }
    }
    protected void ddlSortProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }

    public void bindAccordingSerach()
    {

        try
        {
            string category, SubCategory;
            if (Page.RouteData.Values["catname"] != null && Page.RouteData.Values["scatname"] != null)
            {

                category = all.RemoveUndeScore(Page.RouteData.Values["catname"].ToString());
                SubCategory = all.RemoveUndeScore(Page.RouteData.Values["scatname"].ToString());

                hl_cat.Text = category;
                hl_cat.NavigateUrl = "/product/" + Page.RouteData.Values["catname"].ToString() + "/All";

                hl_scat.Text = SubCategory;
                hl_scat.NavigateUrl = "/product/" + Page.RouteData.Values["catname"].ToString() + "/" + Page.RouteData.Values["scatname"].ToString();

                if (SubCategory != "all")
                {
                    SCatID = objscat.getSubCategoryID(SubCategory);
                }
                if (category != "all")
                {
                    CatId = objscat.getCategoryID(category);
                }


              
                srch.SearchBy = "";
                srch.CatID = CatId;
                srch.SubCatID = SCatID;

            }
            else if (Page.RouteData.Values["srch"] != null)
            {
                txt = all.RemoveUndeScore(Page.RouteData.Values["srch"].ToString());
                if (Session["srch"] != null)
                {
                    srch.SearchBy = Session["srch"].ToString();
                }
                else
                {
                    srch.SearchBy = txt;
                }
                srch.CatID = "";
                srch.SubCatID = "";
                hl_cat.Text = "<b>Search by:</b> " + txt;
                slash.Visible = false;
            }
            else
            {
                srch.SearchBy = "";
                // srch.CatID = "";
                // srch.SubCatID = "";
                slash.Visible = false;
            }
        }
        catch (Exception ex) { }

    }


    public void show()
    {
        bindAccordingSerach();

        //if (ddlSortProduct.SelectedItem.Value == "1")
        //{
        //    srch.Orderby = "2";
        //}
        //else if (ddlSortProduct.SelectedItem.Value == "2")
        //{
        //    srch.Orderby = "1";
        //}
        //else
        //{
        srch.Orderby = ddlSortProduct.SelectedItem.Value;
        

        DataSet ds = objsrch.itemSearchList(srch);
        if (ds.Tables[0].Rows.Count > 16)
        {
            string pg = Request.QueryString["Page"];
            if (!string.IsNullOrEmpty(pg))
            {

                if (pg != "1")
                {
                    Int32 cnt = default(Int32);
                    if (ds.Tables[0].Rows.Count > 16 * Convert.ToInt32(pg))
                    {
                        cnt = 16 * Convert.ToInt32(pg);
                    }
                    else
                    {
                        cnt = ds.Tables[0].Rows.Count;
                    }
                    //  lblcol1.Text = "Showing " + (16 * (Convert.ToInt32(pg) - 1) + 1) + " - " + cnt + " of " + ds.Tables[0].Rows.Count + " Product";
                    lblcol2.Text = "Showing " + (16 * (Convert.ToInt32(pg) - 1) + 1) + " - " + cnt + " of " + ds.Tables[0].Rows.Count + " Product";
                }
                else
                {
                    //  lblcol1.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product";
                    lblcol2.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product";
                }
            }
            else
            {
                //lblcol1.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product";
                lblcol2.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product";
            }
            CollectionPager1.Visible = true;
            CollectionPager1.DataSource = ds.Tables[0].DefaultView;
            CollectionPager1.BindToControl = DataList2;
            //Collectionpager2.Visible = true;
            //Collectionpager2.DataSource = ds.Tables[0].DefaultView;
            //Collectionpager2.BindToControl = DataList2;
            DataList2.DataSource = CollectionPager1.DataSourcePaged;
        }
        else
        {
            //lblcol1.Text = "Showing  " + ds.Tables[0].Rows.Count + " Product";
            lblcol2.Text = "Showing  " + ds.Tables[0].Rows.Count + " Product";
            CollectionPager1.Visible = false;
            //Collectionpager2.Visible = false;
            DataList2.DataSource = ds;
            DataList2.DataBind();
        }






    }
    public bool setVisiable(object obj)
    {
        if (obj.ToString() == "")
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("categories.aspx");
    }
    protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
    {

        if (e.CommandName == "book")
        {

            TextBox txtQuantity = ((TextBox)e.Item.FindControl("txtQuantity"));
            Label lblitemid = ((Label)e.Item.FindControl("lblid"));
            Label lblname = ((Label)e.Item.FindControl("lblname"));
            Label ablStock = ((Label)e.Item.FindControl("lbavlQty"));
            Label lblCode =((Label)e.Item.FindControl("lblCode"));
            Label price = ((Label)e.Item.FindControl("lblPrice"));
            Label offerprice = ((Label)e.Item.FindControl("lblOfferPrice"));
     

            if (ad.IsPrdctExistCart(lblitemid.Text, Session["IsUser"].ToString(),"1") == false)
            {
                string amount;
                if (offerprice.Text == "0.00")
                {
                    amount = price.Text;
                }
                else
                {
                    amount = offerprice.Text;
                }
                ad.inserProduct(lblitemid.Text, txtQuantity.Text, Session["IsUser"].ToString(), lblitemid.Text, "", lblname.Text, amount,"1");
                Session["Itemname"] = "Successfully Added " + lblname.Text + " in Cart";
            }
            else
            {
                ad.UpdatePurchaseItem(lblitemid.Text, txtQuantity.Text, Session["IsUser"].ToString());
              
                Session["Exist"] =lblname.Text+" Item is already Exist!";

            }
            show();
            Session["path"] = HttpContext.Current.Request.Url.AbsoluteUri;
            Response.Redirect(Session["path"].ToString());

        }
        if (e.CommandName == "Detail")
        {
            Label lblitemid = ((Label)e.Item.FindControl("lblid"));

            Response.Redirect("/ProductDetails/" + lblitemid.Text);

        }
        if (e.CommandName == "wishlist")
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                Label lblname = ((Label)e.Item.FindControl("lblname"));
                Label lblCode = ((Label)e.Item.FindControl("lblCode"));
                Label lblitemid = ((Label)e.Item.FindControl("lblid"));
                ad.InsertInWishlist(lblitemid.Text, HttpContext.Current.User.Identity.Name);
                JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, "Successfully Added " + lblname.Text + " in wishlist", 5000);
                show();
            }
            else
            {
                Session["URL"] = HttpContext.Current.Request.Url.AbsoluteUri;
                Response.Redirect("/login/Product");

            }
        }

    }
    public void viewitem()
    {
        try
        {
            chkoutctrl ctrlB = (chkoutctrl)Page.Master.FindControl("chkoutctrl1");
            LinkButton ddl = (LinkButton)ctrlB.FindControl("lnkbtn");
            int itm = ad.SelPurItem(HttpContext.Current.User.Identity.Name);
            ddl.Text = itm.ToString() + " Items";
            ddl.Enabled = true;
        }
        catch (Exception ex) { }
    }
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }

    public string getQTY(object obj)
    {

        return ad.SetQty(obj.ToString());

    }


    protected void lnkbtn_Click(object sender, EventArgs e)
    {
        Response.Redirect("view-cart.aspx");
    }
    protected void lnkChangPassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("change-password.aspx");
    }
    protected void DataList2_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem | e.Item.ItemType == ListItemType.Item)
        {
            Label price = ((Label)e.Item.FindControl("lblPrice"));
            Label offerprice = ((Label)e.Item.FindControl("lblOfferPrice"));
            if (offerprice.Text == "0" || offerprice.Text == "0.00")
            {
                price.Font.Strikeout = false;
                offerprice.Visible = false;

            }
            else
            {
                price.Font.Strikeout = true;
                offerprice.Visible = true;
            }
        }


    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BAL;
using ClassLibrary;
using System.IO;
using System.Data;
public partial class sitemap : System.Web.UI.Page
{
    public Utility objAll = new Utility();
    modifyData ad = new modifyData();
    dlSubCategory objscat = new dlSubCategory();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        bindCategory();
    }

    public void bindCategory()
    {
        DataSet ds = new DataSet();
        ds = ad.CategoryForProduct();
        if (ds.Tables[0].Rows.Count > 0)
        {
            r_cat.DataSource = ds;
            r_cat.DataBind();
        }
        else
        {
            r_cat.DataSource = null;
            r_cat.DataBind();
        }
    }

    public void bindSubCategory(Repeater subcat, string subcatID)
    {


        DataSet ds = objscat.getSubCategoryNameList(subcatID);

        if (ds.Tables[0].Rows.Count > 0)
        {
            subcat.DataSource = ds;
            subcat.DataBind();
        }
        else
        {
            subcat.DataSource = ds;
            subcat.DataBind();
        }
    }
    protected void r_cat_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        HiddenField CategoryId = (HiddenField)e.Item.FindControl("hf_catID");
        Label CategoryName = e.Item.FindControl("hf_category") as Label;
        Repeater r_Subcat = e.Item.FindControl("r_scat") as Repeater;
        bindSubCategory(r_Subcat, CategoryId.Value);

       // HiddenField Categoryname = (HiddenField)e.Item.FindControl("hf_catname");

    }
}
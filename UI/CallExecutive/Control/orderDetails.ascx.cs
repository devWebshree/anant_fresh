using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BAL;
using DAL;
using ClassLibrary;
public partial class control_orderdtl : System.Web.UI.UserControl
{
    modifyData md = new modifyData();

    Stock stk = new Stock();
    public dlStock objstk = new dlStock();

    CallExecutive call = new CallExecutive();
    dlCallExecutive objCall = new dlCallExecutive();
    string ord;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                binddata();
            }
             bcklnk.Visible = false;
        }
    }



    public void bindCostumerdata()
    {
        try
        {
            DataSet ds = objCall.selectFromPurchageTable(ord);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                     lblOrderDateTime.Text = dr["OrderDt"].ToString();
                     lblOrderNo.Text = dr["orderID"].ToString();
                    lblAmount.Text = dr["MainAmount"].ToString();
                    // lblBillTo.Text = dr["BAddress"].ToString();
                    lblAddress.Text = ds.Tables[1].Rows[0]["Address"].ToString();
                    lblOrderStatus.Text = GetDis(ds.Tables[1].Rows[0]["dispatch"]);
                    lblTotal.Text = dr["amount"].ToString();
                    lbl_shipcharge.Text = (Convert.ToInt32(dr["Amount"]) - Convert.ToInt32(dr["MainAmount"])).ToString();

                    lblQuantity.Text = objstk.getTotalQuantityByOrderId(ord);
                }
            }
        }
        catch(Exception ex)
        {}
    }

    public void binddata()
    {

        if (Request.QueryString["oid"] != null)
        {
            ord = Request.QueryString["oid"].ToString();
        }
        else
        {
            Response.Redirect("orderList.aspx");
        }
        bindCostumerdata();
        DataSet ds2 = objCall.selectOrderedDetails(ord);
        if (ds2.Tables[0].Rows.Count > 0)
        {
            grdViewOrdersOfCustomer.DataSource = ds2;
            grdViewOrdersOfCustomer.DataBind();
        }
        else
        {
            grdViewOrdersOfCustomer.DataSource = null;
            grdViewOrdersOfCustomer.DataBind();
            Response.Redirect("orderList.aspx");
        }
     //   grdViewOrdersOfCustomer.Enabled = false;
    }
    protected void bcklnk_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["oid"] != null)
        {
            Response.Redirect("genrateInvoice.aspx?orderno=" + Request.QueryString["oid"].ToString() + "");
        }
    }
    public string GetDis(object st)
    {


        if (st.ToString() == "3")
        {
            bcklnk.Visible = true;
            grdViewOrdersOfCustomer.Enabled = false;
            return "Order Dispatched";
        }
        else if (st.ToString() == "2")
        {
            grdViewOrdersOfCustomer.Enabled = false;
            return "Order Cancelled";
        }
        else if (st.ToString() == "1")
        {
            return "Cancellation Request";
        }
        else if (st.ToString() == "4")
        {
          //  bcklnk.Visible = true;
            return "Dispatch In Process";
        }
        else
        {
            return "Under Process";
        }
    }


    protected void txt_quantity_TextChanged(object sender, EventArgs e)
    {
        var txt = (TextBox)sender;
        var rptChild = txt.NamingContainer;//Child Repeater
        HiddenField pid = (HiddenField)rptChild.FindControl("hf_pid");
        TextBox txtQuantity = (TextBox)rptChild.FindControl("txt_quantity");

        string s = txtQuantity.Text;
        Int32 a;
        if (Int32.TryParse(s, out a))
        {
            stk.NewQuantity = Convert.ToInt32(txtQuantity.Text);
            stk.PurchageId = Convert.ToInt32(pid.Value);
            objstk.updateOrderedProduct(stk);
            // objCall.UpdatePurchaseItemCall(pid.Value, txtQuantity.Text);
        
        }

        binddata();
        
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        var imgbtn = (ImageButton)sender;
        var rptChild = imgbtn.NamingContainer;//Child Repeater
        HiddenField pid = (HiddenField)rptChild.FindControl("hf_pid");
        stk.NewQuantity = 0;
        stk.PurchageId = Convert.ToInt32(pid.Value);
        objstk.updateOrderedProduct(stk);
        binddata();
      

    }
}

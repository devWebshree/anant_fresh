﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using ClassLibrary;
public partial class admin_Feedback : System.Web.UI.Page
{
    dlFeedback feed = new dlFeedback();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) 
        {
            bindfeedback();
        }
    }

    public void bindfeedback() 
    {
        DataSet ds;

        ds = feed.listFeedBack();


        if (ds.Tables[0].Rows.Count > 0)
        {
            feedbackgrd.DataSource = ds;
            feedbackgrd.DataBind();
        }
        else
        {
            feedbackgrd.DataSource = null;
            feedbackgrd.DataBind();
        }
    }

}
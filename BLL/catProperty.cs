﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL
{
    public class catProperty
    {


        public string m_Category, m_image, m_catid, m_sortid, m_IsVisible, m_Description, m_Submitby;



        public string Category
        {
            get
            {
                return m_Category;
            }
            set
            {
                m_Category = value;
            }
        }


        public string image
        {
            get
            {
                return m_image;
            }
            set
            {
                m_image = value;
            }
        }

        public string catid
        {
            get
            {
                return m_catid;
            }
            set
            {
                m_catid = value;
            }
        }



        public string sortid
        {
            get
            {
                return m_sortid;
            }
            set
            {
                m_sortid = value;
            }
        }



        public string IsVisible
        {
            get
            {
                return m_IsVisible;
            }
            set
            {
                m_IsVisible = value;
            }
        }


        public string Description
        {
            get
            {
                return m_Description;
            }
            set
            {
                m_Description = value;
            }
        }


        public string Submitby
        {
            get
            {
                return m_Submitby;
            }
            set
            {
                m_Submitby = value;
            }
        }


    }


    
}

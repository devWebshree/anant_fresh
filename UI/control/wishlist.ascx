﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wishlist.ascx.cs" Inherits="control_wishlist" %>
<%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>
<%@ Register Assembly="ASPnetPagerV2netfx2_0" Namespace="CutePager" TagPrefix="cc2" %>
<%@ Register Assembly="msgBox" Namespace="BunnyBear" TagPrefix="cc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc4" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>
<%@ Reference VirtualPath="~/control/totalitems.ascx" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="row">
    <div class="title">
        <asp:Label ID="lblCatName" runat="server"></asp:Label>
    </div>
</div>
<div class="clear">
</div>
<div runat="server" id="TABLE2" class="row">
  <a href="/product"> <span class="WishlistEmpty"></span></a>  
</div>
<div class="row m10">
    <div class="title">
        <asp:Label ID="lblcol1" runat="server"></asp:Label>
    </div>
    <div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                Processing...<asp:Image ID="Image1" runat="server" ImageUrl="~/images/simple.gif" />
            </ProgressTemplate>
        </asp:UpdateProgress>
        <cc1:CollectionPager ID="Collectionpager2" runat="server" PageSize="16" SliderSize="5"
            QueryStringKey="Page" ResultsFormat="Products {0}-{1} (of {2})" ResultsLocation="None"
            ShowFirstLast="False" ShowLabel="False" BackText="« Back" ControlCssClass="page-detail"
            LabelText="Page : -" MaxPages="25" BackNextLinkSeparator="|" PageNumbersSeparator="|"
            UseSlider="True">
        </cc1:CollectionPager>
    </div>
    <div class="clear">
    </div>
    <div class="products m10">
        <asp:DataList ID="DataList2" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
            BorderWidth="0px" CellPadding="0" Width="100%" OnItemCommand="DataList2_ItemCommand"
            OnItemDataBound="DataList2_ItemDataBound" RepeatLayout="Flow">
            <ItemTemplate>
                <ul class="itemul">
                    <li class="border-box"><a href="<%# "/ProductDetails/" +  Eval("id")  %>">
                        <div class="catName" style="display:none;">
                            <h5>
                                <span class="left">Product code:</span>
                                <asp:Label ID="lblCode" runat="server" Text='<%# Bind("code") %>' CssClass="code"></asp:Label>
                            </h5>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="Pitem_img">
                            <div class="itemproduct">
                            <img src="<%# "/ProductImage/" + Eval("image") %>" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="ProductName">
                            <h5>

                             <asp:Label ID="Label1" runat="server" Text='<%# all.sortStringLength(Eval("name"),25) %>'></asp:Label>
                                <asp:Label ID="lblname" runat="server" Text='<%# Bind("name") %>' Visible="false"></asp:Label></h5>
                        </div>
                        <div class="clear">
                        </div>

                        <div class="Price clearfix">
                            <span>Price MRP </span>&#8377;<span></span><asp:Label ID="lblPrice" runat="server" Text='<%# Bind("price") %>'
                                CssClass="cross"></asp:Label>
                            <span>&#8377;</span><asp:Label ID="lblOfferPrice" CssClass="new" runat="server" Text='<%# Bind("offerprice") %>'
                                Visible="false"></asp:Label>
                        </div>


                    </a>
                        <div class="_button clearfix">
                            <div class="addtowishlist backgroundbg itembttn border-radius" style="width:31%;">
                              <asp:LinkButton ID="btn_delete" OnClientClick="javascript:return confirm('Are you sure!!!\nYou want to Delete this item from your wishlist?');"
                                    runat="server" ValidationGroup="dtle" CommandName="delete">
                                    <asp:Image ID="Image6" style="margin-left:10px;" ImageUrl="~/images/delete_icon.png" AlternateText=""
                                        runat="server" />Delete
                                </asp:LinkButton>
                                
                            </div>
                            <div class="addtocart backgroundbg itembttn border-radius">
                              <asp:LinkButton ID="btnBook" CommandName="book" ValidationGroup="dtle" runat="server">
                                    <asp:Image ID="Image7" ImageUrl="~/images/cart_btn_icon.png" runat="server" />Add
                                    to Cart</asp:LinkButton>
                            </div>
                        </div>

                        <div class="newFlag" id="NewFlag" runat="server" visible='<%# setVisiableNewBest(Eval("IsNew")) %>'>
                        </div>
                        <div class="bestFlag" id="BestSaler" runat="server" visible='<%# setVisiableNewBest(Eval("BestSellers")) %>'>
                        </div>
                        <asp:Label ID="lblWid" runat="server" Text='<%# Bind("wid") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblid" runat="server" Text='<%# Bind("id") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbavlQty" runat="server" Visible="false" Text='<%# getQTY(Eval("id")) %>'></asp:Label>
                        <asp:TextBox ID="txtQuantity" runat="server" Width="50px" Visible="false">1</asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtQuantity"
                            CssClass="error" Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999"
                            MinimumValue="1" SetFocusOnError="True" Type="Integer" ValidationGroup="dtle">                                                                                      
                        </asp:RangeValidator>

                    </li>
                </ul>
            </ItemTemplate>
        </asp:DataList>
        <asp:Label ID="lblcol2" runat="server" ForeColor="White"></asp:Label>
        <cc1:CollectionPager ID="CollectionPager1" runat="server" PageSize="16" SliderSize="5"
            QueryStringKey="Page" ResultsFormat="Products {0}-{1} (of {2})" ResultsLocation="None"
            ShowFirstLast="False" ShowLabel="False" BackText="« Back" ControlCssClass="page-detail"
            LabelText="Page : -" MaxPages="25" BackNextLinkSeparator="|" PageNumbersSeparator="|"
            UseSlider="True">
        </cc1:CollectionPager>
    </div>
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PressReleases.ascx.cs" Inherits="control_PressReleases" %>




<div class="row ">

    <div class="">
        <h2 class="title1 pd">Press Releases</h2>
    </div>
    <div class="clear"></div>

    <div class="outrdiv m10">
        <asp:Repeater ID="rptPress" runat="server">
            <ItemTemplate>
                <div class="pressitem">
                    <div class="pressthumb">
                    <div class="resizeimage">
                        <a href="/ProductImage/PressRelease/<%# Eval("ImagePath") %>" rel="lightbox" class="fancybox zoom1" alt="">
                            <img src="/ProductImage/PressRelease/<%# Eval("ImagePath") %>" alt="" data-maxWidth="200" data-maxHeight="200" />
                        </a>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <h2><%# Eval("name") %></h2>
                </div>

            </ItemTemplate>
        </asp:Repeater>

    </div>


</div>

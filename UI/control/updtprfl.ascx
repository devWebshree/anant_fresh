<%@ Control Language="C#" AutoEventWireup="true" CodeFile="updtprfl.ascx.cs" Inherits="controls_updtprfl" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>
<%@ Register Src="MenuMyAccount.ascx" TagName="MenuMyAccount" TagPrefix="uc1" %>
<div class="row">
    <uc1:MenuMyAccount ID="MenuMyAccount1" runat="server" />
</div>
<div class="row ">
    <div class="title">
        <h4>
            My Profile</h4>
    </div>
</div>
<div class="row m10">
    <div class="column border-box">
        <div class="center" style="width:50%; float:left;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:DataList ID="DataList1" runat="server" OnCancelCommand="DataList1_CancelCommand"
                        OnEditCommand="DataList1_EditCommand" OnItemCommand="DataList1_ItemCommand" DataKeyField="uid"
                        Width="100%" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <ItemTemplate>
                            <ul class="lgnul">
                                <li class="brdr">
                                <span class="lbl1">Full Name :</span> <span class="inputbox">
                                    <asp:Label ID="lbl1" runat="server" Text='<%# Eval("Name") %>' CssClass="lbl1"></asp:Label>
                                </li>
                                <li class="brdr">
                                <span class="lbl1">Phone Number :</span> <span class="inputbox">
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Phone") %>' CssClass="lbl1"></asp:Label>
                                </li>
                                <li class="brdr"><span class="lbl1">Email :</span> <span class="inputbox">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Email") %>' CssClass="lbl1"></asp:Label>
                                </li>
                                <li class="brdr"><span class="lbl1">Address :</span> <span class="inputbox">
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Address") %>' CssClass="lbl1"></asp:Label>
                                </li>
                                <li class="brdr"><span class="lbl1">City :</span> <span class="inputbox">
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("City") %>' CssClass="lbl1"></asp:Label>
                                </li>
                                <li class="brdr"><span class="lbl1">State :</span> <span class="inputbox">
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("State") %>' CssClass="lbl1"></asp:Label>
                                </li>
                                <li class="brdr">
                                 <span class="lbl1">Zip Code :</span> <span class="inputbox">
                                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("Zipcode") %>' CssClass="lbl1"></asp:Label>
                                </li>
                                <li>
                                <asp:LinkButton ID="lnkbedit" runat="server" CommandName="Edit" CssClass="_button1 backgroundbg border-radius" Width="20%" style="text-align:center; margin-top:10px;">Edit Profile</asp:LinkButton></button>
                                </li>
                            </ul>
                       
                        </ItemTemplate>
                        <EditItemTemplate>
                            <h4 class="h2h m10" style="margin-bottom: 10px; border-bottom:1px solid #ccc; padding-bottom:5px;">Edit Profile</h4>

                            <ul class="lgnul">
                            <li>
                            <span class="lbl">Full Name</span>
                            <div class="inputbox">
                             <asp:TextBox ID="txtname" runat="server" Text='<%# Eval("Name") %>' CssClass="input"></asp:TextBox>
                                        <cc1:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtname"
                                            CssClass="error1" Display="Dynamic" ErrorMessage="Enter Name!" SetFocusOnError="True"
                                            ValidationGroup="uprflie"></cc1:RequiredFieldValidator><span>
                                                <cc1:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtname"
                                                    CssClass="error" Display="Dynamic" ErrorMessage="Invalid Name!" SetFocusOnError="True"
                                                    ValidationExpression="^[a-z A-Z . /']{0,50}$" ValidationGroup="uprflie"></cc1:RegularExpressionValidator>
                            </div>
                            </li>
                             <li>
                            <span class="lbl">Phone Number</span>
                            <div class="inputbox"> 
                            <asp:TextBox ID="txtpno" runat="server" Text='<%# Eval("Phone") %>' CssClass="input"></asp:TextBox><span
                                            style="font-size: 12pt; font-family: Times New Roman">
                                            <cc1:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtpno"
                                                Display="Dynamic" ErrorMessage="Enter Phone no." SetFocusOnError="True" ValidationGroup="uprflie"></cc1:RequiredFieldValidator>
                                            <cc1:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtpno"
                                                CssClass="error1" Display="Dynamic" ErrorMessage="Invalid Phone No." SetFocusOnError="True"
                                                ValidationExpression="^[0-9 -]{10,11}$" ValidationGroup="uprflie"></cc1:RegularExpressionValidator></div>
                            </li>
                             <li>
                            <span class="lbl">Email</span>
                            <div class="inputbox">
                              <asp:TextBox ID="txtemail" runat="server" Text='<%# Eval("Email") %>' CssClass="input"></asp:TextBox><cc1:RequiredFieldValidator
                                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtemail" CssClass="error1"
                                            Display="Dynamic" ErrorMessage="Enter Email Id!" SetFocusOnError="True" ValidationGroup="uprflie"></cc1:RequiredFieldValidator><cc1:RegularExpressionValidator
                                                ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtemail"
                                                CssClass="error" Display="Dynamic" ErrorMessage="Invalid Email ID" SetFocusOnError="True"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="uprflie"></cc1:RegularExpressionValidator>
                            </div>
                            </li>
                             <li>
                            <span class="lbl">Address</span>
                            <div class="inputbox">
                             <asp:TextBox ID="txtaddr1" runat="server" Text='<%# Eval("Address") %>' CssClass="input"></asp:TextBox>
                                        <cc1:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtaddr1"
                                            CssClass="error1" Display="Dynamic" ErrorMessage="Enter Address!" SetFocusOnError="True"
                                            ValidationGroup="uprflie"></cc1:RequiredFieldValidator>
                                        <cc1:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtaddr1"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Invalid Address!" ValidationExpression="^[^<>%]{0,100}$"
                                            ValidationGroup="uprflie"></cc1:RegularExpressionValidator>
                            </div>
                            </li>
                             <li>
                            <span class="lbl">City</span>
                            <div class="inputbox">
                             <asp:TextBox ID="txtcity" runat="server" Text='<%# Eval("City") %>' CssClass="input"></asp:TextBox>
                                        <cc1:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtcity"
                                            CssClass="error1" Display="Dynamic" ErrorMessage="Enter City!" SetFocusOnError="True"
                                            ValidationGroup="uprflie"></cc1:RequiredFieldValidator>
                                        <cc1:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtcity"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Invalid City!" SetFocusOnError="True"
                                            ValidationExpression="^[^<>%]{0,50}$" ValidationGroup="uprflie"></cc1:RegularExpressionValidator>
                            </div>
                            </li>
                             <li>
                            <span class="lbl">State</span>
                            <div class="inputbox">
                             <asp:TextBox ID="txtstate" runat="server" Text='<%# Eval("State") %>' CssClass="input"></asp:TextBox><cc1:RequiredFieldValidator
                                            ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtstate" CssClass="error1"
                                            Display="Dynamic" ErrorMessage="Enter State!" SetFocusOnError="True" ValidationGroup="uprflie"></cc1:RequiredFieldValidator><cc1:RegularExpressionValidator
                                                ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtstate"
                                                CssClass="error" Display="Dynamic" ErrorMessage="Invalid State" SetFocusOnError="True"
                                                ValidationExpression="^[^<>%]{0,50}$" ValidationGroup="uprflie"></cc1:RegularExpressionValidator>
                            </div>
                            </li>
                             <li>
                            <span class="lbl">Zip Code</span>
                            <div class="inputbox">
                            
                              <asp:TextBox ID="txtzip" runat="server" Text='<%# Eval("Zipcode") %>' CssClass="input"></asp:TextBox>
                                        <cc1:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtzip"
                                            CssClass="error1" Display="Dynamic" ErrorMessage="Invalid Postal Code!" ValidationExpression="^[^<>%]{0,50}$"
                                            ValidationGroup="uprflie"></cc1:RegularExpressionValidator>
                            </div>
                            </li>

                            <li>
                               <asp:LinkButton ID="lnkbupdate" runat="server" CommandName="Update" ValidationGroup="uprflie"
                                            CssClass="_button1 backgroundbg border-radius left" style="width:15%; margin-right:10px; text-align:center;">Update</asp:LinkButton>&nbsp;
                                        <asp:LinkButton ID="lnkbcancel" runat="server" CommandName="Cancel" CausesValidation="False"
                                            CssClass="_button1 backgroundbg border-radius left" style="width:15%; text-align:center;">Cancel</asp:LinkButton>
                            </li>
                            </ul>

                            
                        </EditItemTemplate>
                    </asp:DataList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
<div id="container">
    <div class="row">
        <div class="span12">
            <div class="tab-content">
                <!-- Tab #two -->
                <div class="tab-pane active" id="two">
                    <!-- Second level tabs -->
                    <div class="tabbable tabs-left">
                        <div class="tab-content">
                            <br />
                        </div>
                    </div>
                </div>
                <!-- Second level tabs -->
            </div>
            <!-- /Tab #two -->
        </div>
    </div>
</div>
<div class="clear">
</div>

<%@ Page Title="Welcome to AnantFresh" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true"
    CodeFile="index.aspx.cs" Inherits="index" %>

<%@ Register Src="~/control/searchbykeyword.ascx" TagName="search" TagPrefix="uc1" %>

<%@ Register Src="~/control/Category.ascx" TagName="cat" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #hh {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <ul class="services">
            <li><a href="#"><span class="serviceicon">
                <asp:Image ID="Image1" ImageUrl="~/images/shoping_delevery.png" runat="server" /></span>
                <span class="serviceName">Easy to Start Shopping</span> </a></li>
            <li><a href="#"><span class="serviceicon">
                <asp:Image ID="Image2" ImageUrl="~/images/choose_Delevery_icon.png" runat="server" /></span>
                <span class="serviceName">Choose a Delivery Time</span> </a></li>
            <li><a href="#"><span class="serviceicon">
                <asp:Image ID="Image3" ImageUrl="~/images/time_icon.png" runat="server" /></span>
                <span class="serviceName">Place a order in minutes</span> </a></li>
            <li><a href="#"><span class="serviceicon">
                <asp:Image ID="Image4" ImageUrl="~/images/home_icon1.png" runat="server" /></span>
                <span class="serviceName">We Deliver to your home</span> </a></li>
        </ul>
    </div>
    <div class="row">
        <div class="banner border-box">
            <ul class="bxslider">
                <li>
                    <img src="/images/banner.jpg" alt="" /></li>
                <li>
                    <img src="/images/banner5.jpg" alt="" /></li>
                <li>
                    <img src="/images/banner2.jpg" alt="" /></li>
                <li>
                    <img src="/images/banner3.jpg" alt="" /></li>
                <li>
                    <img src="/images/banner4.jpg" alt="" /></li>             <%--   <li>
                    <img src="images/anantfresh_christmas_banner.jpg" alt="" /></li>
                <li>
                    <img src="images/anantfresh_happy-new-year_banner.jpg" alt="" /></li>--%>
                
 <li>
     <img src="images/special-offer-1.jpg" alt="special-offer" /></li>
            </ul>
        </div>

    </div>

    <div class="clear">
    </div>

    <div class="row m20">
        <div class="welcome">
            <div class="welcomeimgae border-radius">
                <asp:Image ID="Image5" ImageUrl="~/images/welcome.jpg" runat="server" />
            </div>
            <div class="welcometext">
                <h1>Welcome to Anant Fresh.</h1>
                <p>
                    <b>Anant Fresh Private Limited</b> is a distinguished retail sector brand that has assumed prominence within a short span of time on account of its quality and responsive services. The company was established in the JAN 2014 and since then, has developed a resurgent footprint along with a heavy base of satisfied customers to its credit! The company is a part of a diversified and well established conglomerate that includes four companies � Anant Fresh Private limited... <a title="Read More" style="float: right;" href="/whyAnantFresh">Read More</a>
                </p>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="row m10">
        <asp:Image ID="Image32" ImageUrl="~/images/category-sep.png" runat="server" />
    </div>
    <div class="row m20">
        <div class="productHeading border-box">
            <h4>Product at AnantFresh.com</h4>
        </div>
        <div class="products m5">

            <div class="row">
                <uc1:search ID="search1" runat="server" />
            </div>

        </div>
    </div>
</asp:Content>

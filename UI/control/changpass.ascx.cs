﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using BAL;

public partial class control_changpass : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.User.Identity.Name == "")
            {
                Response.Redirect("/login/changepassword");

            }
        }
    }
    protected void chdlnbk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            if (ad.ChgUserPass(Session["IsUser"].ToString(), txtoldpass.Text, txtcnfpass.Text) == "1")
            {
                txtoldpass.Text = "";
                txtnwpass.Text = "";
                txtcnfpass.Text = "";
                JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, "Password Changed Successfully.", 1000);
            }
            else
            {
                JqueryNotification.NotificationExtensions.ShowWarningNotification(this, "Old Password is wrong!", 1000);

            }



        }
    }
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("/product");
    }
    protected void lnkord_Click(object sender, EventArgs e)
    {
        Response.Redirect("my-account.aspx");

    }
    protected void mylnk_Click(object sender, EventArgs e)
    {
        Response.Redirect("reg-details.aspx");

    }
    protected void chglnk_Click(object sender, EventArgs e)
    {
        Response.Redirect("/changepassword");
       
    }
}

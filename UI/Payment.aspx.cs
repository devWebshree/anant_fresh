﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Payment : System.Web.UI.Page
{
    public string action1 = string.Empty;
    public string hash1 = string.Empty;
    public string txnid1 = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_pay_Click(object sender, EventArgs e)
    {
        MakeMyPayment();
    }

    public void MakeMyPayment()
    {
        Session["PaymentMode"] = rdobtnPOption.Text;

      //  if (!string.IsNullOrEmpty(Session["bankaddress"].ToString()))
       // {
            //  if (obj.chkorderid(Session["ord"]).ToString == "0") Product id is exist 
                MakePayment("1");

      //  }
    }



    public void MakePayment(string PaymentValue)
    {
            DataSet ds = null;
            string FName = "Arvind", PhoneNo = "8884320076", ProductInfo = "Bag", Amount = "1", Email = "arvind@webshree.in", User = "Arvind", SuccessURL = "", FailureURL = "";
           // Email = HttpContext.Current.User.Identity.Name;


       
            //  dsusrprofile = clsReg.selectMyProfile(HttpContext.Current.User.Identity.Name);

           // foreach (DataRow dr in ds.Tables[0].Rows)
           // {
              //  FName = dr["Name"].ToString();
              //  PhoneNo = dr["Phone"].ToString();
           // }
            // for Server
            //SuccessURL = "http://www.anantfresh.com/OrderComplete.aspx";
            //FailureURL = "http://wwww.anantfresh.com/PaymentFailed.aspx";

            // for local
            SuccessURL = "http://localhost:56441/OrderComplete";
            FailureURL = "http://localhost:56441/PaymentFailed.aspx";
            postBankData(FName, Amount, Email, PhoneNo, ProductInfo, SuccessURL, FailureURL);
    }
    public void postBankData(string fname, string amount, string email, string phone, string productinfo, string surl, string furl)
    {
        try
        {
            string[] hashVarsSeq = null;
            string hash_string = string.Empty;
            if (string.IsNullOrEmpty(Request.Form["txnid"]))
            {
                // generating txnid
                Random rnd = new Random();
                string strHash = Generatehash512(rnd.ToString() + DateTime.Now);
                txnid1 = strHash.ToString().Substring(0, 20);
            }
            else
            {
                txnid1 = Request.Form["txnid"];
            }
            if (string.IsNullOrEmpty(Request.Form["hash"]))
            {
                // generating hash value
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["MERCHANT_KEY"]) || string.IsNullOrEmpty(txnid1) || string.IsNullOrEmpty(amount) || string.IsNullOrEmpty(fname) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(phone) || string.IsNullOrEmpty(productinfo) || string.IsNullOrEmpty(surl) || string.IsNullOrEmpty(furl))
                {
                    //error

                    //frmError.Visible = True
                   // return;

                }
                else
                {
                    // frmError.Visible = False
                    hashVarsSeq = ConfigurationManager.AppSettings["hashSequence"].Split('|');
                    // spliting hash sequence from config
                    hash_string = "";
                    foreach (string hash_var in hashVarsSeq)
                    {
                        if (hash_var == "key")
                        {
                            hash_string = hash_string + ConfigurationManager.AppSettings["MERCHANT_KEY"];
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "txnid")
                        {
                            hash_string = hash_string + txnid1;
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "amount")
                        {
                            hash_string = hash_string + Convert.ToDecimal(amount).ToString("g29");
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "productinfo")
                        {
                            hash_string = hash_string + productinfo;
                            hash_string = hash_string + '|';

                        }
                        else if (hash_var == "firstname")
                        {
                            hash_string = hash_string + fname;
                            hash_string = hash_string + '|';

                        }
                        else if (hash_var == "email")
                        {
                            hash_string = hash_string + email;
                            hash_string = hash_string + '|';

                            //ElseIf hash_var = "phone" Then
                            //    hash_string = hash_string + phone.Text
                            //    hash_string = hash_string & "|"c

                            //ElseIf hash_var = "surl" Then
                            //    hash_string = hash_string + surl.Text
                            //    hash_string = hash_string & "|"c

                            //ElseIf hash_var = "furl" Then
                            //    hash_string = hash_string + furl.Text
                            //    hash_string = hash_string & "|"c


                        }
                        else
                        {
                            hash_string = hash_string + (Request.Form[hash_var] != null ? Request.Form[hash_var] : "");
                            // isset if else
                            hash_string = hash_string + '|';
                        }
                    }

                    hash_string += ConfigurationManager.AppSettings["SALT"];
                    // appending SALT
                    hash1 = Generatehash512(hash_string).ToLower();
                    //generating hash
                    // setting URL
                    action1 = ConfigurationManager.AppSettings["PAYU_BASE_URL"] + "/_payment";


                }

            }
            else if (!string.IsNullOrEmpty(Request.Form["hash"]))
            {
                hash1 = Request.Form["hash"];

                action1 = ConfigurationManager.AppSettings["PAYU_BASE_URL"] + "/_payment";
            }
            if (!string.IsNullOrEmpty(hash1))
            {
                hash.Value = hash1;
                txnid.Value = txnid1;
                key.Value = ConfigurationManager.AppSettings["MERCHANT_KEY"];
                System.Collections.Hashtable data = new System.Collections.Hashtable();
                // adding values in gash table for data post
                data.Add("hash", hash.Value);
                data.Add("key", key.Value);
                data.Add("txnid", txnid.Value);

                string AmountForm = Convert.ToDecimal(amount).ToString("g29");
                // eliminating trailing zeros
                amount = AmountForm;
                data.Add("amount", AmountForm);

                data.Add("firstname", fname.Trim());
                data.Add("email", email.Trim());
                data.Add("phone", phone.Trim());
                data.Add("productinfo", productinfo.Trim());
                data.Add("surl", surl.Trim());
                data.Add("furl", furl.Trim());
                // data.Add("lastname", lastname.Text.Trim())
                //  data.Add("curl", curl.Text.Trim())
                //  data.Add("address1", address1.Text.Trim())
                // data.Add("address2", address2.Text.Trim())
                // data.Add("city", city.Text.Trim())
                // data.Add("state", state.Text.Trim())
                // data.Add("country", country.Text.Trim())
                // data.Add("zipcode", zipcode.Text.Trim())
                //  data.Add("udf1", "")
                // data.Add("udf2", "")
                //  data.Add("udf3", "")
                // data.Add("udf4", "")
                //  data.Add("udf5", "")
                //data.Add("pg", "")


                string strForm = PreparePOSTForm(action1, data);
                Page.Controls.Add(new LiteralControl(strForm));

            }
        }
        catch (Exception ex)
        {

            Response.Write("<span style='color:red'>" + ex.Message + "</span>");
        }



    }
    private string PreparePOSTForm(string url, System.Collections.Hashtable data)
    {
        // post form
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" + formID + "\" action=\"" + url + "\" method=\"POST\">");


        foreach (System.Collections.DictionaryEntry key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + Convert.ToString(key.Key) + "\" value=\"" + Convert.ToString(key.Value) + "\">");
        }


        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." + formID + ";");
        strScript.Append("v" + formID + ".submit();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }
    public string Generatehash512(string text)
    {

        byte[] message = Encoding.UTF8.GetBytes(text);

        UnicodeEncoding UE = new UnicodeEncoding();
        byte[] hashValue = null;
        SHA512Managed hashString = new SHA512Managed();
        string hex = "";
        hashValue = hashString.ComputeHash(message);
        foreach (byte x in hashValue)
        {
            hex += String.Format("{0:x2}", x);
        }
        return hex;

    }







}
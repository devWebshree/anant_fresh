﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
 
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
 
using System.IO;

using BAL;

public partial class admin_controls_addcatgory : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    catProperty cat = new catProperty();

    string userid = HttpContext.Current.User.Identity.Name;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            

            show();
        }
    }

    protected void btnAddCategory_Click(object sender, EventArgs e)
    {
       

        cat.m_Category = txtcat.Text;

        cat.m_Description = txtDesc.Text;
        cat.m_Submitby = userid;
      
        cat.m_image = bindimg(flbnr);

       

            


        bool check = ad.insertcat(cat);

        if (check == true)
        {

            showmessage("Catalogue added successfully");
            cleardata();
            show();
        }
        else
        {

            showmessage("Catalogue already Exist!");
        }
       


    }

    void cleardata()
    {
        txtcat.Text="";
       
      
    }



    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }


    public string bindimg(FileUpload fl)
    {

        if (fl.HasFile)
        {
            string Type = Path.GetExtension(fl.PostedFile.FileName);


            if (Type.ToLower() == ".jpg" | Type.ToLower() == ".gif" | Type.ToLower() == ".jpeg")
            {
                string pt = null;
                string cpath = null;
                // Dim filmid As String = Request.QueryString("fid")
                Random r = new Random();
                string RandNo = Convert.ToString(r.Next(1000, 1000000));

                pt = Server.MapPath("~/CategoryImages/");
                string filename = null;

                filename = Path.GetFileName(fl.PostedFile.FileName);

                cpath = pt + RandNo + filename;
                if (File.Exists(cpath))
                {
                    File.Delete(cpath);
                }
                //File.Copy(file2.PostedFile.FileName, cpath)


                fl.PostedFile.SaveAs(cpath);

                return RandNo + filename;
            }
            else
            {
                // ShowMessage("Banner logo Images Should be in Jpg,Gif format")
                return "";
            }
        }
        else
        {
            return "";
        }
    }
    public void show()
    {

        gvrProduct.DataSource = ad.Selectcategory();
        gvrProduct.DataBind();
    }
 
    public string setimage(object obj)
    {
        if (obj != null)
        {
            return "~/ItemImages/" + obj.ToString();
            // string str = Server.MapPath("~/ItemImages/" + obj.ToString());


            //return Server.MapPath("~/ItemImages/" +obj.ToString());
        }
        else
        {

        }
        return obj.ToString();

    }
    protected void gvrProduct_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvrProduct.EditIndex = e.NewEditIndex;
        show();
    }
    protected void gvrProduct_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvrProduct.EditIndex = -1;
        show();
    }

    protected void gvrProduct_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        GridViewRow gv = gvrProduct.Rows[e.RowIndex];
        HiddenField hdnimage = ((HiddenField)gv.FindControl("HiddenField1Image"));
        Label lblid = ((Label)gv.FindControl("lblEditid"));
        TextBox txtcat = ((TextBox)gv.FindControl("txtcat1"));
      
        FileUpload imgupload = ((FileUpload)gv.FindControl("fileuploadUpdate"));

        Label prevsrt = ((Label)gv.FindControl("lblPrvsrt"));

        TextBox srt = ((TextBox)gv.FindControl("txtSrt"));

        //TextBox des = ((TextBox)gv.FindControl("txtDes"));


        Winthusiasm.HtmlEditor.HtmlEditor des = ((Winthusiasm.HtmlEditor.HtmlEditor)gv.FindControl("txtDes"));

        string imagename;
        string sm1 = bindimg(imgupload);
        if (sm1 == "")
        {
            imagename = hdnimage.Value;
        }
        else
        {
            imagename = sm1;
        }


        cat.m_Category = txtcat.Text;
       
        cat.m_image = imagename;
        cat.m_catid = lblid.Text;

        cat.m_sortid = srt.Text;
        cat.m_Description = des.Text;


        string srtid = srt.Text;
        if (srt.Text != prevsrt.Text)
        {
            srtid = UpdateSortid(srt.Text);
        }







        bool check = ad.updatecat(cat);

        if (check == true)
        {

            gvrProduct.EditIndex = -1;
            show();
            showmessage("successfully updated");
       

            
        }
        else
        {


            showmessage("Catalogue already Exist!");
        }
       

        
    }

    public string UpdateSortid(string srt)
    {
        try
        {
            if (ad.chkCatSortid(srt) == false)
            {
                System.Data.DataSet ds = ad.SelCatbysrtid(srt);
                int i = 0;
                int sn = 0;
                int srtid = 0;
                string itmid = null;
                i = ds.Tables[0].Rows.Count;
                if (i > 0)
                {
                    for (sn = 0; sn <= i - 1; sn++)
                    {
                        itmid = ds.Tables[0].Rows[sn]["catid"].ToString();
                        srtid = Convert.ToInt32(ds.Tables[0].Rows[sn]["sortid"]);
                        ad.UpdateSortid((srtid + 1).ToString(), itmid);
                    }
                }
            }
            return srt;
        }
        catch(Exception ex)
        {
            return srt;
        }
    }
    protected void gvrProduct_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow rw = gvrProduct.Rows[e.RowIndex];
        Label lbid = ((Label)rw.FindControl("lblid"));
        HiddenField hd = ((HiddenField)rw.FindControl("HiddenField2Image"));
        DataSet ds = ad.SelectProduct(lbid.Text);
        for (int i=0;i<ds.Tables[0].Rows.Count;i++)
        {

            string pt1 = "";
            pt1 = Server.MapPath("~/ProductImage/" + ds.Tables[0].Rows[i]["image"].ToString());
            if (File.Exists(pt1))
            {
                File.Delete(pt1);
            }
            ad.deleteProduct(ds.Tables[0].Rows[i]["id"].ToString());


        }

        string pt = "";
        pt = Server.MapPath("~/CategoryImages/" + hd.Value);
        if (File.Exists(pt))
        {
            File.Delete(pt);
        }
        ad.deletecategory(lbid.Text);

        show();

    }
    protected void gvrProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvrProduct.PageIndex = e.NewPageIndex;
        show();
    }

    public bool setVisiable(object obj)
    {
        if (obj.ToString() == "")
        {
            return false;
            
        }
        else
        {
            return true;
        }
       

    }

    //public string chkHide(object obj)
    //{
    //    if (obj.ToString() == "True")
    //    {
    //        return "Not Mentioned";
    //    }

    //    else
    //    {
    //        return obj.ToString();
    //    }
    //}

    protected void gvrProduct_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            int id = Convert.ToInt32(gvrProduct.DataKeys[e.Row.RowIndex].Value);

            Label lblgn = (Label)e.Row.FindControl("lblVisi");
            CheckBox chk = (CheckBox)e.Row.FindControl("chkHide");

            if (lblgn.Text == "True")
            {
                chk.Checked = true;

            }
            else
            {
                chk.Checked = false;
            }

        }
    }
    protected void chkHide_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (CheckBox)sender;

        GridViewRow gr = (GridViewRow)chk.Parent.Parent;
        Label lblgn = (Label)gr.FindControl("lblVisi");
        string id = gvrProduct.DataKeys[gr.RowIndex].Value.ToString();


        if(lblgn.Text=="True")
        {
            ad.updCatVisibility("False", id);
        }
        else
        {
            ad.updCatVisibility("True", id);
        }

        showmessage("successfully updated");

        show();
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data; 
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ClassLibrary;
using BAL;
using System.IO;
using DAL;
using System.Data;
using System.Data.SqlClient;

public partial class admin_controls_additem : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    dlSubCategory objscat = new dlSubCategory();
    SubCategoryCS scat = new SubCategoryCS();
    Search srch = new Search();
    dlsearch objsrch = new dlsearch();

    Stock stk = new Stock();
    dlStock objstk = new dlStock();
    string user = HttpContext.Current.User.Identity.Name;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindCategoryName();
            if (Request.QueryString["id"] != null)
            {
                getdata(Request.QueryString["id"].ToString());
                btn_update.Visible = true;
                btnAdd.Visible = false;
                pnl_visi.Visible = true;
            }
            else
            {
                btn_update.Visible = false;
                btnAdd.Visible = true;
                pnl_visi.Visible = false;

            }
        }

    }
    public void bindCategoryName()
    {
        DataSet ds = ad.CategoryForProduct();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCategoryName.DataSource = ds;
            ddlCategoryName.DataTextField = "category";
            ddlCategoryName.DataValueField = "catid";
            ddlCategoryName.DataBind();
            ddlCategoryName.Items.Insert(0, "All");
        }
        else
        {
            ddlCategoryName.DataSource = null;
            ddlCategoryName.DataBind();
        }
    }
    public void bindSubCategoryName()
    {
        DataSet ds = objscat.getSubCategoryNameList(ddlCategoryName.SelectedItem.Value);//Convert.ToInt32(ddl_subCat.SelectedIndex)
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddl_subCat.DataSource = ds;
            ddl_subCat.DataTextField = "SubCategory";
            ddl_subCat.DataValueField = "subcatid";
            ddl_subCat.DataBind();
            ddl_subCat.Items.Insert(0, "Select");
        }
        else
        {
            ddl_subCat.DataSource = null;
            ddl_subCat.DataBind();
        }
        
    }
    protected void ddlCategoryName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //show();
        bindSubCategoryName();

    }
    public void getdata(string id)
    {
        try
        {
           // srch.Visibilty = null;
            srch.SearchBy = "";
            srch.ProductID = id;
            DataSet ds = new DataSet();
            ds = objsrch.itemListForAdmin(srch);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {

                ddlCategoryName.Items.FindByValue(dr["categoryname"].ToString()).Selected = true;

                if (dr["IsVisible"].ToString() == "True")
                {
                    chkIsvisible.Checked = true;
                }
                else
                    chkIsvisible.Checked = false;


                txtOfferPrice.Text = dr["offerprice"].ToString();
                txtCode.Text = dr["code"].ToString();
                hf_path.Value = dr["image"].ToString();
                txtName.Text= dr["name"].ToString();
                txtDetail.Text = dr["detail"].ToString();
                txtPrice.Text = dr["price"].ToString();

                txtBuyingPrice.Text = dr["BuyingPrice"].ToString();
                txtWholesale.Text = dr["WholeSalePrice"].ToString();

                bindSubCategoryName();
                ddl_subCat.Items.FindByText(dr["Expr6"].ToString()).Selected = true;
                if (dr["qty"].ToString() != "")
                {
                    txtavailable.Text = dr["qty"].ToString();
                }
                txtAlertOnStock.Text = dr["alertonquantity"].ToString();
                
            }
        }
        catch (Exception ex){}
    }
    public void addInStock(int itemid) 
    {
        stk.AvailableQuantity = Convert.ToInt32(txtavailable.Text) + Convert.ToInt32(txtAddQuantity.Text);
        stk.AlertOnQuantity = Convert.ToInt32(txtAlertOnStock.Text);
        stk.ItemId = itemid;
        objstk.insertItemQuantity(stk);
    }
    public void updateInStock(int itemid) 
    {
        stk.AvailableQuantity = Convert.ToInt32(txtavailable.Text) + Convert.ToInt32(txtAddQuantity.Text);
        stk.AlertOnQuantity = Convert.ToInt32(txtAlertOnStock.Text);
  
        stk.ItemId = itemid;
        objstk.updateItemWithStock(stk);

        txtAddQuantity.Text = "0";
        txtavailable.Text = stk.AvailableQuantity.ToString();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {

        ProductProperty pd = new ProductProperty();
        pd.categoryname = ddlCategoryName.SelectedValue.ToString();
        pd.code = txtCode.Text;
        pd.name = txtName.Text;
        pd.image = bindimg(fuplImage);

        pd.price = Convert.ToDouble(txtPrice.Text);
        pd.offerprice = Convert.ToDouble(txtOfferPrice.Text);
        pd.buyingprice = Convert.ToDouble(txtBuyingPrice.Text);
        pd.wholesaleprice = Convert.ToDouble(txtWholesale.Text);

        pd.Detail = txtDetail.Text;
        pd.subcategoryname = ddl_subCat.SelectedItem.Value;
        pd.Submitby = user;


        //pd.p_ShortDescripn = txtshortDes.Text;
        string isnew, bestsaler, DeailyDeal, isVisible, featured;
        bestsaler = "true";
        DeailyDeal = "true";
        isnew = "true";
        isVisible = "true";
        featured = "true";
        
        pd.isnew = isnew;
        pd.BestSellers = bestsaler;
        pd.DailyDeal = DeailyDeal;
        pd.IsVisible = isVisible;
        pd.P_IsFeatured = featured;
        if (txtOfferPrice.Text == "")
        {
            pd.P_productprice = Convert.ToDouble(txtPrice.Text);
            pd.P_offerprice = 0;
        }
        else
        {
            pd.P_productprice = Convert.ToDouble(txtOfferPrice.Text);
        }
      bool ab= ad.insertProduct(pd);
      addInStock(objstk.getScopIdentityOfItem());
      if (ab == false)
      {
          showmessage("This Product is Already Exits");
      }
      else
      {
          showmessage("Successfully Added");
          //show();
          txtCode.Text = "";
          txtName.Text = "";
          txtDetail.Text = "";
          txtOfferPrice.Text = "";
          txtPrice.Text = "";
         
         
      }
        


    }
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    public string bindimg(FileUpload fl)
    {
        string Type = Path.GetExtension(fl.PostedFile.FileName);


        if (Type.ToLower() == ".jpg" | Type.ToLower() == ".gif" | Type.ToLower() == ".jpeg" | Type.ToLower() == ".png")
        {
            string pt = null;
            string cpath = null;
            // Dim filmid As String = Request.QueryString("fid")
            Random r = new Random();
            string RandNo = Convert.ToString(r.Next(1000, 1000000));

            pt = Server.MapPath("~/ProductImage/");
            string filename = null;

            filename = Path.GetFileName(fl.PostedFile.FileName);
            string dtime;
            DateTime dt = DateTime.Now;
            dtime = dt.ToString("ddMMyyyyhhmmssffftt");
            cpath = pt + RandNo + dtime+Type;
            //if (cpath != "NoImage.jpg")
            //{
            //    if (File.Exists(cpath))
            //    {
            //        File.Delete(cpath);
            //    }
            //}

            //File.Copy(file2.PostedFile.FileName, cpath)


            fl.PostedFile.SaveAs(cpath);

            return RandNo + dtime + Type;
        }
        else
        {
            // ShowMessage("Banner logo Images Should be in Jpg,Gif format")
            return "NoImage.jpg";
        }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        ProductProperty pd = new ProductProperty();
        string imagename;
        string sm1 = bindimg(fuplImage);
        if (sm1 == "NoImage.jpg")
        {
            imagename = hf_path.Value;
        }
        else
        {
            imagename = sm1;
        }
        string isvisible = "false";


        if (chkIsvisible.Checked)
        {
            isvisible = "true";
        }
        pd.p_code = txtCode.Text.Trim();
        pd.p_name = txtName.Text.Trim();
        pd.image = imagename;
        pd.P_price = Convert.ToDouble(txtPrice.Text.Trim());
        pd.P_offerprice = Convert.ToDouble(txtOfferPrice.Text.Trim());
        pd.Detail = txtDetail.Text.Trim();
        pd.p_id = Request.QueryString["id"].ToString();
        pd.IsVisible = isvisible;
        pd.wholesaleprice = Convert.ToDouble(txtWholesale.Text.Trim());
        pd.buyingprice = Convert.ToDouble(txtBuyingPrice.Text.Trim());
        pd.categoryname=ddlCategoryName.SelectedItem.Value;
        pd.subcategoryname = ddl_subCat.SelectedItem.Value;
        if (txtOfferPrice.Text == "")
        {
            pd.P_productprice = Convert.ToDouble(txtPrice.Text.Trim());
            pd.P_offerprice = 0;
        }
        else
        {
            pd.P_productprice = Convert.ToDouble(txtOfferPrice.Text.Trim());
        }
        updateInStock(Convert.ToInt32(pd.p_id));
        bool check = ad.updateItem(pd);



        if (check == false)
        {

            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, "This Product Code Already Exits!", 4500);
            showmessage("This Product Code Already Exits");
        }
        else
        {
            showmessage("successfully updated");
            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, "successfully updated.", 4500);
        }
    }
}

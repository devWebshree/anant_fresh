﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="viewcartctrl.ascx.cs" Inherits="control_viewcartctrl" %>
<%@ Register Assembly="msgBox" Namespace="BunnyBear" TagPrefix="cc2" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <cc2:msgBox ID="MSG" runat="server"></cc2:msgBox>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="row">
    <div class="title">
        <h4>
            Order No:
            <asp:Label ID="lblord" runat="server" Text="Label"></asp:Label>
            <p class="shopping-txt">
                Your Cart contains
                <asp:Label ID="lbltitem" runat="server"></asp:Label>
            item(s).</h4>
    </div>
</div>
<div class="clear">
</div>
<div class="row m10">
<div  style=" overflow:auto; max-height:500px;">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BorderStyle="None" 
        BorderWidth="0px" CellPadding="3" CssClass="table" DataKeyNames="pid" PageSize="20"
        Width="100%" OnRowDeleting="GridView1_RowDeleting">
        <Columns>
            <asp:TemplateField HeaderText="Item(S)">
                <ItemStyle Font-Bold="False" HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="lblitemid" runat="server" Text='<%# bind("itemid") %>' Visible="False"></asp:Label>
                    <asp:Label ID="lblPid" runat="server" Text='<%# bind("pid") %>' Visible="False"></asp:Label>
                  
                    <table class="intble" style="float:left;">
                        <tr>
                            <td align="center" width="90">
                                <asp:Image ID="sd" CssClass="cartImg" runat="server" alt="" ImageUrl='<%# "/ProductImage/"+ GetImg(Eval("itemid")) %>' />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblitrm" runat="server" Text='<%# bind("item") %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                  
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Unit Price &#8377;">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("price") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>


            <asp:TemplateField HeaderText="Quantity">
                <ItemStyle  />
                <ItemTemplate>
                    <asp:TextBox ID="txtqty" runat="server" Width="70px" AutoPostBack="true" CssClass="input1" MaxLength="5"
                        OnTextChanged="txtqty_TextChanged" Text='<%# bind("quantity")  %>'></asp:TextBox>

                        <asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="txtqty" CssClass="error"
                        Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="999" MinimumValue="1"
                        SetFocusOnError="True" Type="Integer" ValidationGroup="sp"></asp:RangeValidator>

                        <a>Update</a>
                </ItemTemplate>
            </asp:TemplateField>



            <asp:TemplateField HeaderText="Total Price &#8377;">
            <ItemTemplate>
             <asp:Label ID="lbl_tprice" Text='<%#  (Convert.ToDouble(Eval("quantity")) * Convert.ToDouble(Eval("price"))).ToString() %>' runat="server"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>



            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="dellnk" runat="server" CausesValidation="false" CommandName="Delete"
                        OnClientClick="return confirm('Do You Want To Delete?')" Text="Delete" CssClass="btn backgroundbg border-radius"></asp:LinkButton>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Width="75px" />
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BorderStyle="None" HorizontalAlign="Center" BackColor="#F2F3F4" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#BDC3C7" Font-Bold="True" Font-Names="verdana" Font-Size="13px"
            ForeColor="White" HorizontalAlign="Center" />
        <AlternatingRowStyle HorizontalAlign="Center" BackColor="White" ForeColor="Black" />
    </asp:GridView>
    </div>
    <div class="title"></div>
    <div class="title">
<div style="float:left; font-size:smaller">
<asp:Panel ID="pnl_cont" runat="server" style="">
   <span class="txtedt">Shop for</span> <span class="pricet"> ₹&nbsp;<asp:Label ID="lbl_lessprice" runat="server"></asp:Label> </span> <span class="txtedt">more and avail FREE SHIPPING…  *</span> <a style="float:right;" href="/whyAnantFresh">T &amp; C apply</a>
</asp:Panel>
<asp:Literal ID="ltrl_more" runat="server">
  <span class="txtedt">Congrats! Shopping Amount is more than</span><span class="pricet"> ₹&nbsp;1004/-</span> <span class="txtedt">So Shipping is FREE.*</span>
</asp:Literal>
</div>
<br />
<span class="blk"> <strong style="color:Red;">Note:</strong> The delivery for Orders made post 5:30 PM will be scheduled to next day.</span>
</div>

    <asp:Label ID="lblLoyalty" Visible="false" runat="server" Text="0"></asp:Label>
</div>
<div class="row m10">
    <div class="row m5">
        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                Processing...
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/simple.gif" />
            </ProgressTemplate>
        </asp:UpdateProgress>
        <div class="column border-box">
            <div class="row m20">
             <table class="table1">
                    <tr>
                        <th>
                           Purchase Amount
                        </th>
                        <th>
                            Discount
                        </th>

                          <th>
                            Shipping Charges
                        </th>

                        <th>
                            Total Amount
                        </th>
                    </tr>
                    <tr>
                        <td>
                            &#8377; <asp:Label ID="lblAmount" runat="server" Text="0"></asp:Label>
                        </td>
                        <td>
                           &#8377; <asp:Label ID="lblDiscount" runat="server" Text="0"></asp:Label>
                        </td>
                         <td>
                           &#8377; <asp:Label ID="lbl_shipcharge" runat="server" Text="0"></asp:Label>
                        </td>
                        <td>
                          &#8377; <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </table>
                </div>
        </div>

        <div class="clear">
        </div>
        <div class="row m10">
            <div class="column border-box">
                <div class="row right">
                    <asp:CheckBox ID="chksame" runat="server" AutoPostBack="True" OnCheckedChanged="chksame_CheckedChanged"
                        CssClass="check" />
                    <span class="left lbln top5">Same as Billing Address</span>
                </div>
                <div class="clear"></div>
                <div class="line m10"></div>
                <div class="clear">
                </div>
                <div class="row m10">
                    <div class="clft left">
                        <h4>
                            Billing Address</h4>
                        <div class="form-inline no-margin">
                        <ul class="lgnul m10">
                            <li>
                                <label class="lbl" for="input">
                                    Full Name :</label>
                                <div class="inputbox">
                                    <input id="txtname" class="input" runat="server" maxlength="64" name="ordName"
                                        size="30" type="text" />
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtname"
                                        Cssclass="error1" Display="Dynamic" ErrorMessage="Invalid Name!" SetFocusOnError="True"
                                        ValidationExpression="^[a-z A-Z]{0,250}$" ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtname"
                                        Cssclass="error1" Display="Dynamic" ErrorMessage="Enter Name!" SetFocusOnError="True"
                                        ValidationGroup="grpupdtbill"></cc1:RequiredFieldValidator></span>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="input">
                                    Mobile No. :</label>
                                <div class="inputbox">
                                    <input id="txtpno" runat="server" maxlength="32" name="ordPhoneNumber" size="30"
                                        type="text" class="input" /><br />
                                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtpno"
                                        Cssclass="error1" Display="Dynamic" ErrorMessage="Enter Mobile no." SetFocusOnError="True"
                                        ValidationGroup="grpupdtbill"></cc1:RequiredFieldValidator>
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtpno"
                                        Cssclass="error1" Display="Dynamic" ErrorMessage="Invalid Mobile No." SetFocusOnError="True"
                                        ValidationExpression="^[0-9 -]{10,11}$" ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="input">
                                    Email :</label>
                                <div class="inputbox">
                                    <input id="txtemail" class="input" runat="server" maxlength="64" name="ordEmailAddress"
                                        size="30" type="text" /><br />
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtemail"
                                        Cssclass="error1" Display="Dynamic" ErrorMessage="Invalid Email ID" SetFocusOnError="True"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="textarea">
                                    Address :</label>
                                <div class="inputbox">
                                    <input id="txtaddr1" class="input" runat="server" maxlength="99" name="ordAddress1"
                                        size="30" type="text" />
                                    <br />
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtaddr1"
                                        Cssclass="error1" Display="Dynamic" ErrorMessage="Please delete any of these characters(,<>#%;'`)."
                                        ValidationExpression="^[^&lt;&gt;(){}?&amp;*~`!#$%^=+|\\:'\;]{0,550}$" ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtaddr1"
                                        Cssclass="error1" Display="Dynamic" ErrorMessage="Enter Address!" SetFocusOnError="True"
                                        ValidationGroup="grpupdtbill"></cc1:RequiredFieldValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="input">
                                    City :</label>
                                <div class="inputbox">
                                    <input id="txtcity" runat="server" class="input" maxlength="32" name="ordCity"
                                        size="30" type="text" />
                                    <br />
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtcity"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Invalid City!" SetFocusOnError="True"
                                        ValidationExpression="^[^<>(){}?&*~`!#$%^=+|\\:'\&quot;,;]{0,250}$" ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtcity"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter City!" SetFocusOnError="True"
                                        ValidationGroup="grpupdtbill"></cc1:RequiredFieldValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="input">
                                    State :</label>
                                <div class="inputbox">
                                    <input id="txtstate" class="input" runat="server" maxlength="64" name="ordAddress2"
                                        size="30" type="text" />
                                    <br />
                                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtstate"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter State!" SetFocusOnError="True"
                                        Width="72px" ValidationGroup="grpupdtbill"></cc1:RequiredFieldValidator>
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtstate"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Invalid State" SetFocusOnError="True"
                                        ValidationExpression="^[^<>(){}?&*~`!#$%^=+|\\:'\&quot;,;]{0,50}$" ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="input">
                                    Zip Code :</label>
                                <div class="inputbox">
                                    <input id="txtzip" class="input" runat="server" maxlength="6" name="ordPostalCode"
                                        size="30" type="text" />
                                    <br />
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtzip"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Invalid Postal Code!" ValidationExpression="^[0-9 -]{0,50}$"
                                        ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtzip"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Specify Zipcode." ValidationGroup="grpupdtbill"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                        </ul>
                        </div>
                        <div class="m10 row">
                        </div>
                    </div>
                    <div class="clft right">
                        <h4>
                            Shipping Address</h4>
                        <div class="form-inline no-margin">
                        <ul class="lgnul m10">
                            <li>
                                <label class="lbl" for="input">
                                    Full Name :</label>
                                <div class="inputbox">
                                    <input id="txtSname" runat="server" class="input" maxlength="64" name="BordName"
                                        size="30" type="text" />
                                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtSname"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Name!" SetFocusOnError="True"
                                        ValidationGroup="grpupdtbill"></cc1:RequiredFieldValidator>
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtSname"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Invalid Name!" SetFocusOnError="True"
                                        ValidationExpression="^[a-z A-Z]{0,250}$" ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="input">
                                    Mobile No. :</label>
                                <div class="inputbox">
                                    <input id="txtSphone" class="input" runat="server" maxlength="32" name="BordPhoneNumber"
                                        size="30" type="text" /><br />
                                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtSphone"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Mobile no." SetFocusOnError="True"
                                        ValidationGroup="grpupdtbill"></cc1:RequiredFieldValidator>
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                        ControlToValidate="txtSphone" CssClass="error1" Display="Dynamic" ErrorMessage="Invalid Mobile No."
                                        SetFocusOnError="True" ValidationExpression="^[0-9 -]{10,11}$" ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="input">
                                    Email :</label>
                                <div class="inputbox">
                                    <input id="txtSemail" class="input" runat="server" maxlength="64" name="BordEmailAddress"
                                        size="30" type="text" /><br />
                               
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                        ControlToValidate="txtSemail" CssClass="error1" Display="Dynamic" ErrorMessage="Invalid Email ID"
                                        SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="textarea">
                                    Address :</label>
                                <div class="inputbox">
                                    <input id="txtSaddr" class="input" runat="server" maxlength="99" name="bordAddress1"
                                        size="30" type="text" /><br />
                                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtSaddr"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Address!" SetFocusOnError="True"
                                        ValidationGroup="grpupdtbill"></cc1:RequiredFieldValidator>
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                        ControlToValidate="txtSaddr" CssClass="error1" Display="Dynamic" ErrorMessage="Please delete any of these characters(,<>#%;'`)."
                                        ValidationExpression="^[^&lt;&gt;(){}?&amp;*~`!#$%^=+|\\:'\;]{0,550}$" ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="input">
                                    City :</label>
                                <div class="inputbox">
                                    <input id="txtScity" runat="server" class="input" maxlength="32" name="BordCity"
                                        size="30" type="text" /><br />
                                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtScity"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter City!" SetFocusOnError="True"
                                        ValidationGroup="grpupdtbill"></cc1:RequiredFieldValidator>
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                        ControlToValidate="txtScity" CssClass="error1" Display="Dynamic" ErrorMessage="Invalid City!"
                                        SetFocusOnError="True" ValidationExpression="^[^<>(){}?&*~`!#$%^=+|\\:'\&quot;,;]{0,250}$"
                                        ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="input">
                                    State :</label>
                                <div class="inputbox">
                                    <input id="txtSstate" runat="server" class="input" maxlength="64" name="bordAddress2"
                                        size="30" type="text" /><br />
                                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtSstate"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter State!" SetFocusOnError="True"
                                        ValidationGroup="grpupdtbill"></cc1:RequiredFieldValidator>
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                        ControlToValidate="txtSstate" CssClass="error1" Display="Dynamic" ErrorMessage="Invalid State"
                                        SetFocusOnError="True" ValidationExpression="^[^<>(){}?&*~`!#$%^=+|\\:'\&quot;,;]{0,50}$"
                                        ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                </div>
                            </li>
                            <li>
                                <label class="lbl" for="input">
                                    Zip Code :</label>
                                <div class="inputbox">
                                    <input id="txtSzip" runat="server" class="input" maxlength="6" name="bordPostalCode"
                                        size="30" type="text" /><br />
                                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                        ControlToValidate="txtSzip" CssClass="error1" Display="Dynamic" ErrorMessage="Invalid Postal Code!"
                                        ValidationExpression="^[0-9 -]{0,50}$" ValidationGroup="grpupdtbill"></cc1:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtSzip"
                                        CssClass="error1" Display="Dynamic" ErrorMessage="Specify Zipcode." ValidationGroup="grpupdtbill"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                        </ul>
                        </div>
                        
                    </div>
                </div>
                <asp:Button ID="bntConfirmOrder" runat="server" OnClick="bntConfirmOrder_Click" Text="Confirm Order" 
                    class="_button1 backgroundbg border-radius" ValidationGroup="grpupdtbill" />
            </div>
        </div>

    </div>

</div>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ClassLibrary;
using BAL;
using System.Web.Mail;
using DAL;
public partial class control_viewcartctrl : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    CrmManagement crm = new CrmManagement();
    SendMail sm = new SendMail();

    ManageData objmd = new ManageData();
    Stock stk = new Stock();
    dlStock objstk = new dlStock();
    Double OfferAfter = Convert.ToInt32(ConfigurationManager.AppSettings["OfferAfter"]);
    Double OfferValue = Convert.ToInt32(ConfigurationManager.AppSettings["OfferValue"]); 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["ShopUserId"] != "0")
            {

                binddata();
                lblord.Text = ad.GetMaxOrderID();
                FillAddress();
                total();
            }
            else
                Response.Redirect("ProdList.aspx");
        }

    }


    public string GetImg(object itemid)
    {
        if (itemid != null || itemid != null)
        {
            return ad.getimagename(itemid.ToString());
        }
        else
            return "";

    }
    public void total()
    {
        try
        {

            double i = Convert.ToDouble(ad.selectsmprice(Session["ShopUserId"].ToString(), Session["Usertype"].ToString()));
            lblAmount.Text = i.ToString();
            lblTotal.Text = i.ToString();
            if (i < OfferAfter)
            {
                pnl_cont.Visible = true;
                ltrl_more.Visible = false;
                lbl_lessprice.Text = (OfferAfter - Convert.ToDouble(lblTotal.Text)).ToString();
                lbl_shipcharge.Text = OfferValue.ToString();
                lblTotal.Text = (Convert.ToDouble(lblTotal.Text) + OfferValue).ToString();
            }
            else
            {
                pnl_cont.Visible = false;
                ltrl_more.Visible = true;
                lbl_shipcharge.Text = "0";

            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void txtqty_TextChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            TextBox txtQuantity = (TextBox)row.FindControl("txtqty");
            Label lblitrm = (Label)row.FindControl("lblitemid");
            Label pid = (Label)row.FindControl("lblPid");
            object obj = lblitrm.Text;
            ad.UpdatePurchaseItem(pid.Text, txtQuantity.Text, Session["ShopUserId"].ToString());

            stk.ReduceQuantity = Convert.ToInt32(txtQuantity.Text);
            stk.ItemId = Convert.ToInt32(lblitrm.Text);
            objstk.updateReduceItemQuantity(stk);

        }
        total();
        binddata();

    }
    public void binddata()
    {
        DataSet ds = ad.SelPurProducts(Session["ShopUserId"].ToString(), Session["UserType"].ToString());
        int i = ds.Tables[0].Rows.Count;
        if (ds.Tables[0].Rows.Count > 0)
        {

            GridView1.DataSource = ds;
            GridView1.DataBind();


        }
        else
        {
            Response.Redirect("ProdList.aspx");
        }
        lbltitem.Text = i.ToString();


    }

    protected void bntConfirmOrder_Click(object sender, EventArgs e)
    {
        total();
        binddata();
        foreach (GridViewRow row in GridView1.Rows)
        {
            TextBox txtQuantity = (TextBox)row.FindControl("txtqty");
            Label lblitrm = (Label)row.FindControl("lblitemid");
            stk.ReduceQuantity = Convert.ToInt32(txtQuantity.Text);
            stk.ItemId = Convert.ToInt32(lblitrm.Text);
            objstk.updateReduceItemQuantity(stk);
          //  Int32 stockQty = Convert.ToInt32(ad.SetQty(lblitrm.Text.ToString()));
          //  int qty = stockQty - Convert.ToInt32(txtQuantity.Text);
          //  ad.UpdateQuantity(qty.ToString(), lblitrm.Text);

        }
        // Update order id in purchse table
        sendbyuser();
        //
        Label lbldtl = new Label();
        Label lblBillingAddress = new Label();
            lbldtl.Text = "<font size=3><b>Shipping Address</b></font><br><table><tr><td><font size=2><b> Name :</b></font></td><td><font size=2>" + txtSname.Value + "</font></td></tr><tr><td><font size=2><b>Contact No.:</b></font></td><td><font size=2>" + txtSphone.Value + "</font></td></tr><tr><td><font size=2><b>Email:</b></font></td><td><font size=2>" + txtSemail.Value + "</font></td></tr><tr><td><font size=2><b>Address :</b></font></td><td><font size=2>" + txtSaddr.Value + "</font></td></tr><tr><td><font size=2><b>City:</b></font></td><td><font size=2>" + txtScity.Value + "</font></td></tr><tr><td><font size=2><b>State:</b></font></td><td><font size=2>" + txtSstate.Value + "</font></td></tr><tr><td><font size=2><b>Zip/Postal Code:</b></font></td><td><font size=2>" + txtSzip.Value + "</font></td></tr></table>";
            lblBillingAddress.Text = "<font size=3><b>Billing Address</b></font><br><table><tr><td><font size=2><b> Name :</b></font></td><td><font size=2>" + txtname.Value + "</font></td></tr><tr><td><font size=2><b>Contact No.:</b></font></td><td><font size=2>" + txtpno.Value + "</font></td></tr><tr><td><font size=2><b>Email:</b></font></td><td><font size=2>" + txtemail.Value + "</font></td></tr><tr><td><font size=2><b>Address :</b></font></td><td><font size=2>" + txtaddr1.Value + "</font></td></tr><tr><td><font size=2><b>City:</b></font></td><td><font size=2>" + txtcity.Value + "</font></td></tr><tr><td><font size=2><b>State:</b></font></td><td><font size=2>" + txtstate.Value + "</font></td></tr><tr><td><font size=2><b>Zip/Postal Code:</b></font></td><td><font size=2>" + txtzip.Value + "</font></td></tr></table>";
        if (Session["ShopUserId"] != "0")
        {
            ad.InsertCheckout(lblord.Text, Session["ShopUserId"].ToString(), lbldtl.Text, lblBillingAddress.Text, lblDiscount.Text, "", "CreditCard", "", lblLoyalty.Text, lblTotal.Text, lblAmount.Text);
            AfterPayment();
        }

    }

    private void ShowMessageLogout(string str)
    {
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "Login", ("alert('" + str + "'); window.parent.location.href='") + Page.ResolveUrl("/home") + "'", true);
    }



    public string sendbyuser()
    {
        //Dim user As String = HttpContext.Current.User.Identity.Name
        System.Data.DataSet ds = null;

        ds = ad.SelPurProducts(Session["ShopUserId"].ToString(), Session["UserType"].ToString());



        int @int = 0;
        int i = 0;
        string ord = null;
        @int = ds.Tables[0].Rows.Count;
        ord = lblord.Text;

        if (@int > 0)
        {
            for (i = 0; i <= @int - 1; i++)
            {
                ad.UpdatePrchDtl(Session["ShopUserId"].ToString(), ds.Tables[0].Rows[i]["pid"].ToString(), ord);
            }
        }
        return ord;
    }
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("/product");
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        Label name = (Label)row.FindControl("lblitrm");
        Label pid = (Label)row.FindControl("lblPid");
        ad.DeletePurchaseItem(pid.Text);
        binddata();
        total();
        Session["Itemname"] = "Selected item " + name.Text + " is successfully Remove from cart!";
        //JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, Session["Itemname"].ToString(), 1000);
        Session["path"] = HttpContext.Current.Request.Url.AbsoluteUri;
        Response.Redirect(Session["path"].ToString());



    }
    protected void chksame_CheckedChanged(object sender, EventArgs e)
    {
      

        if (chksame.Checked)
        {
            txtSname.Value = txtname.Value;
            txtSphone.Value = txtpno.Value;
            txtSemail.Value = txtemail.Value;
            txtSaddr.Value = txtaddr1.Value;
            txtSstate.Value = txtstate.Value;
            txtScity.Value = txtcity.Value;
            txtSzip.Value = txtzip.Value;
        }
        else
        {
            txtSname.Value = "";
            txtSphone.Value = "";
            txtSemail.Value = "";
            txtSaddr.Value = "";
            txtSstate.Value = "";
            txtScity.Value = "";
            txtSzip.Value = "";
        }
    }
    public void FillAddress()
    {
        DataSet ds = ad.registerdataForMobile(Session["ShopUserId"].ToString());

        if (ds.Tables[0].Rows.Count > 0)
        {

            txtname.Value = ds.Tables[0].Rows[0]["Name"].ToString();
            txtemail.Value = ds.Tables[0].Rows[0]["Email"].ToString();
            txtpno.Value = ds.Tables[0].Rows[0]["Phone"].ToString();
            txtaddr1.Value = ds.Tables[0].Rows[0]["Address"].ToString();
            txtcity.Value = ds.Tables[0].Rows[0]["City"].ToString();
            txtstate.Value = ds.Tables[0].Rows[0]["State"].ToString();
            txtzip.Value = ds.Tables[0].Rows[0]["Zipcode"].ToString();




        }
    }
    private void AfterPayment()
    {
        string SalesMan = "";
        if (Session["SalesName"] != "0")
        {
            SalesMan = Session["SalesName"].ToString();
        }
      
        Label lbldtl = new Label();
        lbldtl.Text = "<font size=3><b>Shipping Address</b></font><br><table><tr><td><font size=2><b> Name :</b></font></td><td><font size=2>" + txtSname.Value + "</font></td></tr><tr><td><font size=2><b>Contact No.:</b></font></td><td><font size=2>" + txtSphone.Value + "</font></td></tr><tr><td><font size=2><b>Email:</b></font></td><td><font size=2>" + txtSemail.Value + "</font></td></tr><tr><td><font size=2><b>Address :</b></font></td><td><font size=2>" + txtSaddr.Value + "</font></td></tr><tr><td><font size=2><b>City:</b></font></td><td><font size=2>" + txtScity.Value + "</font></td></tr><tr><td><font size=2><b>State:</b></font></td><td><font size=3>" + txtSstate.Value + "</font></td></tr><tr><td><font size=2><b>Zip/Postal Code:</b></font></td><td><font size=2>" + txtSzip.Value + "</font></td></tr></table>";
        ad.UpdatePrchflag(lblord.Text, "1");
        DataSet ds = objmd.SelectCheckOutFromOrderIDCallExecutive(lblord.Text); 
        if (ds.Tables[0].Rows.Count > 0)
        {
            ad.InsertFinalPurchaseForCall(lblord.Text, Session["ShopUserId"].ToString(), ds.Tables[0].Rows[0]["SAddress"].ToString(), ds.Tables[0].Rows[0]["amount"].ToString(), ds.Tables[0].Rows[0]["Discount"].ToString(), ds.Tables[0].Rows[0]["UsedLoyalty"].ToString(), ds.Tables[0].Rows[0]["MainAmount"].ToString(), HttpContext.Current.User.Identity.Name, SalesMan, Session["UserType"].ToString()); 
           //sm.sendViewcart(ds.Tables[0].Rows[0]["Email"].ToString(), lblord.Text, DateTime.Now.ToString(), ds.Tables[0].Rows[0]["Name"].ToString());
           //sm.sendmailTOAdmin(ds.Tables[0].Rows[0]["Email"].ToString(), lblord.Text, DateTime.Now.ToString(), "Admin");
        }
        Session["msgorderconfirm"] = lblord.Text;
        Response.Redirect("Ordermessage.aspx");
    }


}

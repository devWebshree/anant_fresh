﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GenrateInvoice.ascx.cs" Inherits="control_WebUserControl" %>




<table border="0" cellpadding="0" cellspacing="0" class="main-table" 
    style="color: rgb(0, 0, 0); font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; height: 638px; background-color: rgb(244, 246, 248);" 
    width="100%">
    <tr>
        <td align="center" 
            style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; width: 1600px; height: 638px;">
            <table border="0" cellpadding="0" cellspacing="0" 
                style="width: 602px; table-layout: fixed; margin: 24px 0px;">
                <tr>
                    <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; border: 1px solid rgb(230, 230, 230); margin: 0px auto; padding: 0px 44px 0px 46px; text-align: left; background-color: rgb(255, 255, 255);">
                        <table border="0" cellpadding="0" cellspacing="0" 
                            style="padding: 27px 0px 0px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(134, 134, 134); margin-bottom: 8px;" 
                            width="100%">
                            <tr>
                                <td align="left" 
                                    style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding-bottom: 3px;" 
                                    valign="middle">
                                    <h2>
                                    <img src="../images/smalllogo.png" alt="AnantFresh" /></h2>
                                </td>
                                <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: bold; font-size: 26px; line-height: normal; font-family: Arial; text-align: right; text-transform: uppercase; margin: 0px;" 
                                    valign="bottom" width="100%">
                                    ORDER DETAIL</td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr valign="top">
                                <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; width: 253px; padding: 14px 0px 0px 2px;">
                                    <h2 style="font-weight: bold; font-style: normal; font-variant: normal; font-size: 12px; line-height: normal; font-family: Arial; margin: 0px 0px 3px;">
                                        AnantFresh</h2>

Anant Groups <br />
A-307A, 3rd Floor, Roman Court, Ansal Sushant <br />
City Kundli, Haryana, 131028
<br />
                                    <table border="0" cellpadding="0" cellspacing="0">
                                    </table>
                                </td>
                                <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding-top: 14px;">
                                    <h2 style="font-weight: bold; font-style: normal; font-variant: normal; font-size: 17px; line-height: normal; font-family: Tahoma; margin: 0px;">
                                        Order&nbsp;<asp:Label ID="lblOrderNo" runat="server" ></asp:Label></h2>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr valign="top">
                                            <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: verdana, helvetica, arial, sans-serif; text-transform: uppercase; padding-right: 10px; white-space: nowrap;">
                                                ORDER DATE:</td>
                                            <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial;">
                                                <asp:Label ID="lblOrderDateTime" runat="server" ></asp:Label></td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: verdana, helvetica, arial, sans-serif; text-transform: uppercase; padding-right: 10px; white-space: nowrap;">
                                                PAYMENT METHOD:</td>
                                            <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial;">
                                                <asp:Label ID="lblPaymentMethod" Text="COD" runat="server" ></asp:Label></td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: verdana, helvetica, arial, sans-serif; text-transform: uppercase; padding-right: 10px; white-space: nowrap;">
                                                SHIPPING METHOD:</td>
                                            <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial;">
                                                Fulfilled by AnantFresh.com Velocity</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" 
                            style="padding: 32px 0px 24px;" width="100%">
                            <tr valign="top">
                                <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding-right: 10px; padding-left: 10px;" 
                                    width="34%">
                                  

                                        <p style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; margin: 2px 0px 3px;">
                                      
                                      <asp:Label ID="lblBillTo" runat="server"></asp:Label>
                                        </p>
                               </td>
 
                                
                                <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial;" 
                                    width="33%">
                                   


                                    <p style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; margin: 2px 0px 3px;">
                                      <asp:Label ID="lblShippingTo" runat="server"></asp:Label>

                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="1" 
                            style="background-color: rgb(221, 221, 221);" width="100%">
                         <tr><td>
                       
                            <asp:DataList ID="OrdersOfCustomer" runat="server">

                            <HeaderTemplate>
                               <tr>
                                <th style="padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial; background-color: rgb(238, 238, 238);" 
                                    width="70%">
                                    Product</th>
                                <th style="padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial; background-color: rgb(238, 238, 238);">
                                    Quantity</th>
                                <th style="padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial; background-color: rgb(238, 238, 238);">
                                    Unit price</th>
                                <th style="padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial; background-color: rgb(238, 238, 238);">
                                    Subtotal</th>
                            </tr>
                            
                            
                            </HeaderTemplate>


                            <ItemTemplate>
                            <tr>
                                <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding: 5px 10px; background-color: rgb(255, 255, 255);">
                                 
                                 <%# Eval("Item")%>
                                </td>
                                <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding: 5px 10px; text-align: center; background-color: rgb(255, 255, 255);">
                                   <%# Eval("Quantity") %>
                                    </td>
                                <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding: 5px 10px; text-align: right; background-color: rgb(255, 255, 255);">
                                     ₹ <%# Eval("price")%></td>
                                <td style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding: 5px 10px; text-align: right; white-space: nowrap; background-color: rgb(255, 255, 255);">
                                    <b>₹ <%# (Convert.ToInt16(Eval("Quantity"))*Convert.ToInt16(Eval("price"))).ToString() %>.00</b>&nbsp;</td>
                            </tr>

                             </ItemTemplate>
                            </asp:DataList>
                            </td></tr>

                        </table>

                        <div style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; width: 510px; border: none; float: right; display: inline; margin-top: 20px;">
                            <div style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; float: left; display: inline; width: 510px; margin-top: 7px;">
                                <div style="color: rgb(124, 126, 128); font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: normal; font-family: 'trebuchet ms'; display: inline; float: left; text-align: right; width: 351.890625px;">
                                    Subtotal :</div>
                                <div style="color: rgb(99, 101, 102); font-style: normal; font-variant: normal; font-weight: bold; font-size: 13px; line-height: normal; font-family: 'trebuchet ms'; float: right; display: inline; width: 147.890625px; text-align: right;">
                                     &#8377; <asp:Label ID="lblAmount" runat="server" Text="0"></asp:Label></div>
                            </div>
                            <div style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; float: left; display: inline; width: 510px; margin-top: 7px;">
                                <div style="color: rgb(124, 126, 128); font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: normal; font-family: 'trebuchet ms'; float: left; display: inline; width: 351.890625px; text-align: right;">
                                    Shipping Cost :</div>
                                <div style="color: rgb(99, 101, 102); font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: normal; font-family: 'trebuchet ms'; float: right; display: inline; width: 147.890625px; text-align: right;">
                                     &#8377; <asp:Label ID="lbl_shipcharge" runat="server"></asp:Label>.00</div>
                            </div>
                            
                            <div style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; float: left; display: inline; width: 510px; margin-top: 7px;">
                            </div>
                            <div style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; float: left; display: inline; width: 510px; margin-top: 7px;">
                                <div style="color: rgb(124, 126, 128); font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: normal; font-family: 'trebuchet ms'; float: left; display: inline; width: 351.890625px; text-align: right;">
                                    <span style="float: left; display: inline; width: 351.890625px; text-align: right; font-weight: bold; font-style: normal; font-variant: normal; font-size: 20px; line-height: normal; font-family: 'trebuchet ms'; color: rgb(153, 0, 42);">
                                    Total :</span></div>
                                <div style="color: rgb(99, 101, 102); font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: normal; font-family: 'trebuchet ms'; float: right; display: inline; width: 147.890625px; text-align: right;">
                                    <span style="float: left; display: inline; width: 147.890625px; text-align: right; font-weight: bold; font-style: normal; font-variant: normal; font-size: 20px; line-height: normal; font-family: 'trebuchet ms'; color: rgb(153, 0, 42);">
                                     &#8377; <asp:Label ID="lblTotal" runat="server"></asp:Label></span></div>
                                <div style="color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; clear: both;">
                                </div>
                                <div style="color: rgb(124, 126, 128); font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: normal; font-family: verdana; float: left; display: inline; width: 510px; text-align: right;">
                                    <span>Amount Is More Than ₹ 1004/- Shipping is Free</span><span 
                                        class="Apple-converted-space">&nbsp;</span><strong>.</strong></div>
                            </div>
                        </div>

                        <h3 style="color: rgb(0, 0, 0); font-family: Arial; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: right; margin: -7px 0px 7px;">
   
  
                     
    Powered by AnantFresh.com</h3>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>



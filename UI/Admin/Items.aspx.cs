﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ClassLibrary;
using BAL;
using System.IO;
using DAL;

public partial class Admin_Items : System.Web.UI.Page
{
    modifyData ad = new modifyData();
    dlSubCategory objscat = new dlSubCategory();
    SubCategoryCS scat = new SubCategoryCS();
    Utility all = new Utility();
    dlsearch objSrch = new dlsearch();
    Search srch = new Search();
    dlsearch objsrch = new dlsearch();
    static DataSet Export;
    Stock stk = new Stock();
    dlStock objstk = new dlStock();
    string user = HttpContext.Current.User.Identity.Name;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                pnl_itemEdit.Visible = true;
                pnl_product.Visible = false;
            }
            else 
            {
                pnl_itemEdit.Visible = false ;
                pnl_product.Visible = true;
            }

            bindCategoryName();
           
            show();
            hideShow();
        }
    }
    protected void ddlCategoryName_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindSubCategoryName();
        show();
    }
    protected void ddlSortProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }

    protected void ddl_subCat_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }
    public void bindCategoryName()
    {
        DataSet ds =ad.CategoryForProduct();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCategoryName.DataSource = ds;
            ddlCategoryName.DataTextField = "category";
            ddlCategoryName.DataValueField = "catid";
            ddlCategoryName.DataBind();
        }
        else
        {
            ddlCategoryName.DataSource = null;
            ddlCategoryName.DataBind();
        }
        ddlCategoryName.Items.Insert(0, "All");

    }
    public void bindSubCategoryName()
    {

        DataSet ds = objscat.getSubCategoryNameList(ddlCategoryName.SelectedItem.Value);//Convert.ToInt32(ddl_subCat.SelectedIndex)
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddl_subCat.DataSource = ds;
            ddl_subCat.DataTextField = "SubCategory";
            ddl_subCat.DataValueField = "subcatid";
            ddl_subCat.DataBind();
            ddl_subCat.Items.Insert(0, "All");
        }
        else
        {
            ddl_subCat.DataSource = null;
            ddl_subCat.DataBind();
        }

    }

    public string setimageForDelete(object obj)
    {
        if (obj == null || obj == System.DBNull.Value || obj.ToString() == "")
        {
            return "0.jpg";
        }
        else
        {

            return obj.ToString();
        }

    }


    protected void chkIsvisible_CheckedChanged(object sender, EventArgs e)
    {

    }
    public string isvisible(object obj)
    {
        bool bb;
        if (obj == null || obj == DBNull.Value)
        {
            bb = false;
        }
        else
        {
            bb = Convert.ToBoolean(obj);
        }


        if (bb == true)
        {
            return "~/images/tick.gif";
        }
        else
        {
            return "~/images/cross.gif";

        }

    }

    public bool setVisiable(object obj)
    {
        if (obj.ToString() == "")
        {
            return false;

        }
        else
        {
            return true;
        }


    }
    protected void btn_srch_Click(object sender, EventArgs e)
    {

        show();
    }
   
    public void show()
    {
        try
        {
            string scat = "", cat = "", visible;
            visible = ddl_Itemtype.SelectedItem.Value;
            if (visible == "0")
            {
                srch.Visibilty = false;
            }
            else
            {
                srch.Visibilty = true;
            }
            if (ddlCategoryName.SelectedItem.Value.ToString() != "All")
            {
                cat = ddlCategoryName.SelectedItem.Value.ToString();
            }
            if (ddl_subCat.SelectedItem.Value.ToString() != "All")
            {
                scat = ddl_subCat.SelectedItem.Value.ToString();
            }


            
            srch.Orderby = ddlSortProduct.SelectedItem.Value;
           

            srch.SearchBy = txt_search.Text;
            srch.CatID = cat;
            srch.SubCatID = scat;
            DataSet ds = null;
            if (ddlStock.SelectedItem.Value == "2")
            {
                ds = objsrch.itemStockSearchList(srch);
            }
            else if(ddlStock.SelectedItem.Value =="1")
            {
                ds = objsrch.itemStockInList(srch);
            }
            else
            {
                ds = objsrch.itemSearchList(srch);
            }
            gvrProduct.PageSize = Convert.ToInt32(ddlViewItem.SelectedItem.Text);
            int Count=ds.Tables[0].Rows.Count;
            if (Count > 0)
            {
                lblcount.Text = "Total Product(s) :" + Count.ToString();
                gvrProduct.DataSource = ds;
                Export = ds;
                gvrProduct.DataBind();
            }
            else
            {
                gvrProduct.DataSource = null;
                gvrProduct.DataBind();
                Export = null;
            }
        }
        catch { }
    }
    protected void gvrProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvrProduct.PageIndex = e.NewPageIndex;
        show();
    }
    protected void gvrProduct_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvrProduct.EditIndex = -1;
        show();
    }
    protected void gvrProduct_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow rw = gvrProduct.Rows[e.RowIndex];
        Label lbid = ((Label)rw.FindControl("lblid"));
        Label hd = ((Label)rw.FindControl("lblFordelete"));
        string pt = "";
        if (hd.Text != "NoImage.jpg")
        {
            pt = Server.MapPath("~/ProductImage/" + hd.Text);
            if (File.Exists(pt))
            {
                File.Delete(pt);
            }
        }
        ad.deleteProduct(lbid.Text);
        show();

    }
   
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    public string bindimg(FileUpload fl)
    {
        string Type = Path.GetExtension(fl.PostedFile.FileName);


        if (Type.ToLower() == ".jpg" | Type.ToLower() == ".gif" | Type.ToLower() == ".jpeg" | Type.ToLower() == ".png")
        {
            string pt = null;
            string cpath = null;
            // Dim filmid As String = Request.QueryString("fid")
            Random r = new Random();
            string RandNo = Convert.ToString(r.Next(1000, 1000000));

            pt = Server.MapPath("~/ProductImage/");
            string filename = null;

            filename = Path.GetFileName(fl.PostedFile.FileName);

            cpath = pt + RandNo + filename;
            if (cpath != "NoImage.jpg")
            {
                if (File.Exists(cpath))
                {
                    File.Delete(cpath);
                }
            }
            //File.Copy(file2.PostedFile.FileName, cpath)


            fl.PostedFile.SaveAs(cpath);

            return RandNo + filename;
        }
        else
        {
            // ShowMessage("Banner logo Images Should be in Jpg,Gif format")
            return "";
        }
    }
    

    protected void btn_visible_Click(object sender, EventArgs e)
    {
       string ids= all.getIds(gvrProduct, "lblid");
       objSrch.setIsVisibleItems(ids,1);
       show();
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
       string ids = all.getProductIds(gvrProduct, "lblid");
       objSrch.delItems(ids);
       show();
    }
    protected void btn_updateItems_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow x in gvrProduct.Rows)
        {
            CheckBox c1 = (CheckBox)x.FindControl("cb_select");
            if (c1.Checked == true)
            {
                Label id = (Label)x.FindControl("lblid");
                TextBox price = (TextBox)x.FindControl("lbledirprice");
                TextBox offerprice = (TextBox)x.FindControl("lblEditOfferprice");

                TextBox txtBuyingprice = (TextBox)x.FindControl("txtBuyingprice");
                TextBox txtWholeSalePrice = (TextBox)x.FindControl("txtWholeSalePrice");

                objSrch.updatePrice(id.Text, Convert.ToDouble(price.Text.Trim()), Convert.ToDouble(offerprice.Text.Trim()), Convert.ToDouble(txtWholeSalePrice.Text.Trim()), Convert.ToDouble(txtBuyingprice.Text.Trim()));
                Label lblStockAvailbility = (Label)x.FindControl("lblStockAvailbility");
                TextBox txtStockOnAlert = (TextBox)x.FindControl("txtStockOnAlert");
                TextBox txtAddQuantityInStock = (TextBox)x.FindControl("txtAddQuantityInStock");

                stk.AvailableQuantity = Convert.ToInt32(lblStockAvailbility.Text) + Convert.ToInt32(txtAddQuantityInStock.Text);
                stk.AlertOnQuantity = Convert.ToInt32(txtStockOnAlert.Text);
                stk.ItemId = Convert.ToInt32(id.Text);
                objstk.updateItemWithStock(stk);

                txtAddQuantityInStock.Text = "0";
                lblStockAvailbility.Text = stk.AvailableQuantity.ToString();
            }
        }
        show();
    }
    protected void btn_hide_Click(object sender, EventArgs e)
    {
        string ids = all.getIds(gvrProduct, "lblid");
        objSrch.setIsVisibleItems(ids, 0);
        show();
    }

    protected void ddl_Itemtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
        hideShow();
    }
    protected void ddlStock_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }

    public void hideShow()
    {
        string visible = ddl_Itemtype.SelectedItem.Value;
        if (visible != "0")
        {

            btn_hide.Visible = false;
            btn_visible.Visible = true;
        }
        else
        {
            btn_hide.Visible = true;
            btn_visible.Visible = false;
        }
    }


    //protected void btnUpdateStock_Click(object sender, EventArgs e) 
    //{
    //    foreach (GridViewRow x in gvrProduct.Rows)
    //    {
    //        CheckBox c1 = (CheckBox)x.FindControl("cb_select");
    //        if (c1.Checked == true)
    //        {
    //            Label itemid = (Label)x.FindControl("lblid");
    //            TextBox lblStockAvailbility = (TextBox)x.FindControl("lblStockAvailbility");
    //            TextBox txtStockOnAlert = (TextBox)x.FindControl("txtStockOnAlert");
    //            TextBox txtAddQuantityInStock = (TextBox)x.FindControl("txtAddQuantityInStock");
    //            stk.AvailableQuantity = Convert.ToInt32(lblStockAvailbility.Text) + Convert.ToInt32(txtAddQuantityInStock.Text);
    //            stk.AlertOnQuantity = Convert.ToInt32(txtStockOnAlert.Text);
    //            stk.ItemId =Convert.ToInt32(itemid.Text);
    //            objstk.updateItemWithStock(stk);

    //            txtAddQuantityInStock.Text = "0";
    //            lblStockAvailbility.Text = stk.AvailableQuantity.ToString();

    //        }
    //    }
    //    show();
    //}


    protected void ddlViewItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }
    protected void lbExport_Click(object sender, EventArgs e)
    {
        if (Export!=null)
        {
            // ds = ad.SelAllFinalProducts1();
            string flname = null;
            Random rnd = new Random();
            flname = "AF-ProductList [" + DateTime.Now.ToString("dd MMM yyyy hh:mm tt") + "].xls";

            //It represent name of column for which you want to select records
            string[] selectedColumns = new[] { "Code", "Name", "Category", "Sub Category", "Price", "offerPrice", "Detail", "Quantity In Stock", "AlertOnQuantity", "Productprice", "WholeSalePrice", "BuyingPrice", "IsVisible", "ProInsDt" };  
            DataTable table = Export.Tables[0];
            table.Columns["qty"].ColumnName = "Quantity In Stock";
            table.Columns["Expr6"].ColumnName = "Sub Category";
            //table.Columns["ProInsDt"].ColumnName = "Product Insert Date";
            DataTable tableWithOnlySelectedColumns = new DataView(table).ToTable(false, selectedColumns);
            ExportDataSetToExcel(AutoNumberedTable(tableWithOnlySelectedColumns), flname);
        }
    }
    private DataTable AutoNumberedTable(DataTable SourceTable)
    {

        DataTable ResultTable = new DataTable();
        DataColumn AutoNumberColumn = new DataColumn();
        AutoNumberColumn.ColumnName = "S.No.";
        AutoNumberColumn.DataType = typeof(int);
        AutoNumberColumn.AutoIncrement = true;
        AutoNumberColumn.AutoIncrementSeed = 1;
        AutoNumberColumn.AutoIncrementStep = 1;
        ResultTable.Columns.Add(AutoNumberColumn);
        ResultTable.Merge(SourceTable);
        return ResultTable;

    }
    public void ExportDataSetToExcel(DataTable dt, string filename)
    {
        HttpResponse response = HttpContext.Current.Response;
        response.Clear();
        response.Charset = "";
        response.ContentType = "application/vnd.ms-excel";
        response.AddHeader("Content-Disposition", "attachment;filename=\"" + filename + "\"");
        using (StringWriter sw = new StringWriter())
        {
            using (HtmlTextWriter htw = new HtmlTextWriter(sw))
            {
                DataGrid dg = new DataGrid();
                dg.DataSource = dt;
                dg.DataBind();
                dg.RenderControl(htw);
                response.Write(sw.ToString());
                response.End();
            }

        }
    }
}
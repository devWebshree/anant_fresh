﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
 
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
 

public partial class admin_controls_logoutAdmin : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //lblWelcome.Text += " " + HttpContext.Current.User.Identity.Name;
        }

    }
    protected void lnkLogout_Click(object sender, EventArgs e)
    {
 
        Response.Redirect("~/logout.aspx");
    }
    protected void lnkusr_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/logout.aspx");
    }
}

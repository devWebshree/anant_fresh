﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary
{
   public class Stock
    {
        public int ItemId { get; set; }
        public int AvailableQuantity { get; set; }
        public int AlertOnQuantity { get; set; }
        public int ReduceQuantity { get; set; }
        public int AddQuantity { get; set; }
        public int NewQuantity { get; set; }
        public int PurchageId { get; set; }  
        public string Action { get; set; } 
    }
}

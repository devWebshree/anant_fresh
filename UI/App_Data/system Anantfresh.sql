USE [AnantFresh]
GO
/****** Object:  Table [dbo].[Wishlist]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wishlist](
	[wid] [int] IDENTITY(1,1) NOT NULL,
	[Itemid] [int] NULL,
	[Userid] [nvarchar](500) NULL,
	[DOA] [datetime] NULL,
	[status] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserTable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTable](
	[uid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](200) NULL,
	[Password] [nvarchar](50) NULL,
	[Name] [nvarchar](200) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](2000) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Zipcode] [nvarchar](50) NULL,
	[Altphone] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[registerdate] [datetime] NULL,
 CONSTRAINT [PK_UserTable] PRIMARY KEY CLUSTERED 
(
	[uid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLoyaltyPoint]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLoyaltyPoint](
	[lid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Userid] [nvarchar](50) NULL,
	[Point] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblStockLog]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStockLog](
	[StockLogId] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [int] NULL,
	[AddedQuantity] [int] NULL,
	[AddedDateTime] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSalesPerson]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSalesPerson](
	[SalesPersonId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](2000) NULL,
	[registerdate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblGroupProducts]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGroupProducts](
	[gid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[productID] [nvarchar](50) NULL,
	[groupProductID] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCategory]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategory](
	[SubCatID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CategoryID] [nvarchar](100) NULL,
	[SubCategory] [nvarchar](100) NULL,
	[SortId] [numeric](18, 0) NOT NULL,
	[Image] [nvarchar](50) NULL,
	[IsVisible] [bit] NULL,
	[SubCatInsDt] [datetime] NULL,
	[Description] [nvarchar](2000) NULL,
	[Submitby] [nvarchar](50) NULL,
 CONSTRAINT [PK_SubCategoryTable] PRIMARY KEY CLUSTERED 
(
	[SubCatID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StockTable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StockTable](
	[sid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[itemid] [nvarchar](50) NULL,
	[qty] [numeric](18, 0) NULL,
	[AlertOnQuantity] [int] NULL,
	[date] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[finalordertable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[finalordertable](
	[pid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OrderNo] [nvarchar](50) NULL,
	[username] [nvarchar](50) NULL,
	[DOP] [datetime] NULL,
	[address] [text] NULL,
	[amount] [numeric](18, 2) NULL,
	[Dispatch] [int] NULL,
	[remark] [nvarchar](50) NULL,
	[dispatchno] [nvarchar](50) NULL,
	[Discount] [nvarchar](50) NULL,
	[UsedLoyalty] [nvarchar](50) NULL,
	[MainAmount] [numeric](18, 2) NULL,
	[ClMsg] [nvarchar](2000) NULL,
	[CallExecutive] [nvarchar](50) NULL,
	[SalesName] [nvarchar](50) NULL,
	[UserType] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[FeedBackID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[EmailID] [nvarchar](50) NULL,
	[PhoneNo] [nvarchar](50) NULL,
	[ProductName] [nvarchar](50) NULL,
	[ProductSubName] [nvarchar](50) NULL,
	[ProductQuality] [nvarchar](50) NULL,
	[Comment] [nvarchar](max) NULL,
	[FeedDate] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[Del] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FeedBackID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Enquiry]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enquiry](
	[EnquiryID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[EmailID] [nvarchar](50) NULL,
	[PhoneNo] [nvarchar](50) NULL,
	[ProductName] [nvarchar](50) NULL,
	[Comment] [nvarchar](max) NULL,
	[EnqDate] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[Del] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EnquiryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[crmlogin]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[crmlogin](
	[crmid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[crm_purchase]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[crm_purchase](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[orderno] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[crm_cancel]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[crm_cancel](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[req_id] [varchar](50) NULL,
	[orderno] [varchar](50) NULL,
	[user_id] [varchar](50) NULL,
	[dateofcancelreq] [datetime] NULL,
	[reason_cancel] [varchar](400) NULL,
	[status] [int] NULL,
	[crm_incharge_id] [varchar](50) NULL,
	[date_of_cancel] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Checkout]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Checkout](
	[Cid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[orderID] [nvarchar](50) NULL,
	[uname] [nvarchar](50) NULL,
	[BAddress] [text] NULL,
	[SAddress] [text] NULL,
	[Discount] [nvarchar](50) NULL,
	[Promo] [nvarchar](50) NULL,
	[paymentmode] [nvarchar](50) NULL,
	[UsedLoyalty] [nvarchar](50) NULL,
	[EarnLoyalty] [int] NULL,
	[amount] [numeric](18, 2) NULL,
	[MainAmount] [numeric](18, 2) NULL,
	[OrderDt] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryTable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryTable](
	[catid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](100) NULL,
	[sortid] [numeric](18, 0) NOT NULL,
	[image] [nvarchar](50) NULL,
	[IsVisible] [bit] NULL,
	[CatInsDt] [datetime] NULL,
	[Description] [nvarchar](2000) NULL,
	[Submitby] [nvarchar](50) NULL,
 CONSTRAINT [PK_CategoryTable] PRIMARY KEY CLUSTERED 
(
	[catid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[itemdetailtable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemdetailtable](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[categoryname] [varchar](50) NULL,
	[SubCategory] [nvarchar](50) NULL,
	[code] [varchar](50) NULL,
	[name] [varchar](50) NULL,
	[image] [varchar](50) NULL,
	[price] [numeric](18, 2) NULL,
	[offerPrice] [numeric](18, 2) NULL,
	[Detail] [text] NULL,
	[productprice] [numeric](18, 2) NULL,
	[BestSellers] [bit] NULL,
	[DailyDeal] [bit] NULL,
	[IsNew] [bit] NULL,
	[IsVisible] [bit] NULL,
	[IsFeatured] [bit] NULL,
	[Size] [nvarchar](50) NULL,
	[Weight] [nvarchar](50) NULL,
	[ShortDescripn] [nvarchar](200) NULL,
	[Submitby] [nvarchar](50) NULL,
	[ProInsDt] [datetime] NULL,
	[WholeSalePrice] [int] NULL,
	[BuyingPrice] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PurChaseTable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurChaseTable](
	[pid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[catid] [nvarchar](50) NULL,
	[quantity] [numeric](18, 0) NULL,
	[buyer] [nvarchar](200) NULL,
	[itemid] [nvarchar](200) NULL,
	[Orderno] [nvarchar](100) NULL,
	[flagstatus] [nvarchar](200) NULL,
	[item] [nvarchar](50) NULL,
	[price] [numeric](18, 2) NULL,
	[Submitdt] [datetime] NULL,
	[IsCancle] [bit] NULL,
	[UserType] [nvarchar](50) NULL,
 CONSTRAINT [PK_PurChaseTable] PRIMARY KEY CLUSTERED 
(
	[pid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[promotable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[promotable](
	[Pid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Promocode] [nvarchar](50) NULL,
	[PurchaseAbove] [numeric](18, 2) NULL,
	[Discount] [numeric](18, 2) NULL,
	[ExpireDate] [datetime] NULL,
	[Msg] [nvarchar](500) NULL,
	[senddate] [datetime] NULL,
	[Ispracent] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStausText]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStausText](
	[oid] [int] IDENTITY(1,1) NOT NULL,
	[StutasName] [nvarchar](500) NULL,
	[StutasIndex] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoyaltyPercentage]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoyaltyPercentage](
	[lid] [int] IDENTITY(1,1) NOT NULL,
	[percentage] [decimal](18, 1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoyaltyHistory]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoyaltyHistory](
	[lid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Loyalty] [int] NULL,
	[ExpiryDate] [datetime] NULL,
	[IssueDate] [datetime] NULL,
	[Userid] [nvarchar](50) NULL,
	[status] [nvarchar](50) NULL,
	[Orderid] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoyaltyHistory] PRIMARY KEY CLUSTERED 
(
	[lid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spProductListToCallExecutice]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spProductListToCallExecutice]
@SearchBy nvarchar(50) =null,

@CatID nvarchar(50)=null,

@SubCatID nvarchar(50)=null,
@Action nvarchar(50)=null,
@ProductID nvarchar(50)=null,
@RealPrice nvarchar(50)=null,
@OfferPrice nvarchar(50)=null,
@IDs nvarchar(max)=null,
@IsVisible bit =null,
@Visibilty bit =0,
@WholeSalePrice nvarchar (50)=null,
@BuyingPrice nvarchar (50)=null,
@orderby varchar(255)='proinsdt',
@SortPrefix varchar(255)='desc'

as
begin
if(@Action='ProductListToCallExecutice')
 begin
declare @temp nvarchar(max)
declare @srch nvarchar(50)
set @srch ='%' + @SearchBy + '%'
set @temp='SELECT * from ItemsDetail WHERE categoryname=isnull('+@CatID+',categoryname)and
				id=isnull('+@ProductID+',id)
				and
				SubCategory=isnull('+@SubCatID+',SubCategory)
				and
				(
				Expr6 LIKE '+@srch+'
				OR Category LIKE '+@srch+'
				OR code        LIKE '+@srch+'
				OR Detail      LIKE '+@srch+'
				OR name        LIKE '+@srch+' 
				)'
			  --  ORDER BY '+ @orderby+' '+ @SortPrefix
execute (@temp) 

end

end
--exec spProductListToCallExecutice @Action='ProductListToCallExecutice', @SearchBy='a'
GO
/****** Object:  UserDefinedFunction [dbo].[split]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[split](
    @delimited NVARCHAR(MAX),
    @delimiter NVARCHAR(100)
) RETURNS @t TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
    DECLARE @xml XML
    SET @xml = N'<t>' + REPLACE(@delimited,@delimiter,'</t><t>') + '</t>'
    INSERT INTO @t(val)
    SELECT  r.value('.','varchar(MAX)') as item
    FROM  @xml.nodes('/t') as records(r)
    RETURN
END
GO
/****** Object:  Table [dbo].[AdminTable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminTable](
	[AId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[UName] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[Role] [nvarchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_AdminTable] PRIMARY KEY CLUSTERED 
(
	[AId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spSalesPerson]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spSalesPerson]
@SalesPersonId int= NULL,
@Name nvarchar(200) = NULL,
@Email nvarchar(200) =NULL,
@Phone nvarchar(50)= NULL,
@Address nvarchar(2000)= NULL,
@registerdate datetime = NULL,
@Action int = NULL,
@Searchtxt nvarchar(50) =''

as begin
if(@Action=1)
begin
insert into tblSalesPerson (Name,Email,Phone,Address)values(@Name,@Email,@Phone,@Address)
end

if(@Action=2)
begin
update  tblSalesPerson set Name=@Name,Email=@Email,Phone=@Phone,Address=@Address where SalesPersonId=@SalesPersonId
end



if(@Action=3)
begin
select * from tblSalesPerson where SalesPersonId=isnull(@SalesPersonId,SalesPersonId) and (name like '%'+@Searchtxt+'%')
end


if(@Action=4)
begin
delete from tblSalesPerson where SalesPersonId=@SalesPersonId
end

if(@Action=5)
begin
select Name from tblSalesPerson where SalesPersonId=@SalesPersonId
end

end
GO
/****** Object:  StoredProcedure [dbo].[spRole]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spRole]
@aid int =null,
@UsreName nvarchar (50)=null,
@Password nvarchar (50)=null,
@Role nvarchar (50)=null,
@name nvarchar (50)=null,
@MobileNo nvarchar (50)=null,
@Action nvarchar (50)=null,
@SearchText nvarchar(50)=null
as
begin

if(@Action='Insert')
begin
insert into AdminTable (name,MobileNo,UName,password,role) values (@name,@MobileNo,@UsreName,@Password,@Role)
end

if(@Action='List')
begin
select * from AdminTable where Role=ISNULL(@Role,Role) order by aid Desc
end

if(@Action='getsrch')
begin
select * from AdminTable where  (Name like '%'+@SearchText+'%' or Role like '%'+@Role+'%' or MobileNo like '%'+@MobileNo+'%'  or UName like '%'+@UsreName+'%') order by aid Desc
end

if(@Action='delete')
begin
delete from AdminTable where AId=@aid
end

end
GO
/****** Object:  StoredProcedure [dbo].[spListFinalOrder]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spListFinalOrder]
@dt1 nvarchar (50)=null,
@dt2 nvarchar (50)=null,
@srchtxt nvarchar (50)=null,
@dispatch nvarchar (50)=null,
@Through nvarchar (50)=null,
@SalesName nvarchar (50)=null,
@PurchageType nvarchar (50)=null
as

begin
if(@dispatch='')
set @dispatch=null
end
begin
SELECT ut.Name, ft.*, (ft.amount-ft.MainAmount) as dif FROM finalordertable ft left join UserTable ut on ft.username= ut.UserName 
where  
dop between isnull(@dt1,dop) and isnull(@dt2,dop) and ft.CallExecutive=ISNULL(@Through,ft.CallExecutive) and ft.SalesName=ISNULL(@SalesName,ft.SalesName) 
and ft.UserType=ISNULL(@PurchageType,ft.UserType) and
 dispatch=isnull(@dispatch,dispatch)
and (ft.username LIKE '%' + @srchtxt + '%' OR  OrderNo LIKE '%' + @srchtxt + '%' or ut.Name LIKE '%' + @srchtxt + '%' ) order by dop desc
end
GO
/****** Object:  StoredProcedure [dbo].[spListDispatchFinalOrder]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spListDispatchFinalOrder]
@dt1 nvarchar (50)=null,
@dt2 nvarchar (50)=null,
@srchtxt nvarchar (50)=null,
@dispatch nvarchar (50)=null,
@Through nvarchar (50)=null,
@SalesName nvarchar (50)=null,
@PurchageType nvarchar (50)=null
as

begin
if(@dispatch='')
set @dispatch=null
end
begin
SELECT ut.Name, ft.*, (ft.amount-ft.MainAmount) as dif FROM finalordertable ft left join UserTable ut on ft.username= ut.UserName 
where  dispatch in(0,3,4) and
dop between isnull(@dt1,dop) and isnull(@dt2,dop) and ft.CallExecutive=ISNULL(@Through,ft.CallExecutive) 

and ft.SalesName=ISNULL(@SalesName,ft.SalesName) 
and ft.UserType=ISNULL(@PurchageType,ft.UserType)
and dispatch=isnull(@dispatch,dispatch)
and (ft.username LIKE '%' + @srchtxt + '%' OR  OrderNo LIKE '%' + @srchtxt + '%' or ut.Name LIKE '%' + @srchtxt + '%' ) order by dop desc
end
GO
/****** Object:  StoredProcedure [dbo].[spInsKeyPurChaseTable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInsKeyPurChaseTable]@catid            NVARCHAR(50)         = NULL,   @quantity         NUMERIC(18,0)        = NULL,   @buyer            NVARCHAR(200)        = NULL,   @itemid           NVARCHAR(200)        = NULL,   @Orderno          NVARCHAR(100)        = NULL,@item             NVARCHAR(50)		    = NULL,@price            int					= NULL ,       @UserType         NVARCHAR(50)			= NULL        asbeginINSERT INTO PurChaseTable(catid,quantity,buyer,itemid,Orderno,item,price,UserType)values(@catid,@quantity,@buyer,@itemid,@Orderno,@item,@price,@UserType)end
GO
/****** Object:  StoredProcedure [dbo].[spInsKeyLoyaltyHistory]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInsKeyLoyaltyHistory]/*	Created by: 	<<Company Name>>, <<Developers Name>>	Date: 		donderdag, 20 jun 2013	Purpose:	<<invullen>>	Input:		<<invullen>>	Output:		<<invullen>>*/	@Loyalty                                  INT                  = NULL,   	@ExpiryDate                               DATETIME             = NULL,   	@IssueDate                                DATETIME             = NULL,   	@Userid                                   NVARCHAR(50)         = NULL,	@status                                   NVARCHAR(50)         = NULL,	@Orderid                                   NVARCHAR(50)         = NULL     	AS 	DECLARE @ProcName 	VARCHAR(50)	DECLARE @Message  	VARCHAR(255)	DECLARE @Testlevel	INT /*_____________________________________________________________________________  Set @Testlevel, @ProcName, en Startingmessage  _____________________________________________________________________________*/ 	/*FILL OUT THIS SELECTSTATEMENT CORRECTLY !	  SELECT @Testlevel = <<Fieldname>> FROM <<SettingsTable>> 	  WHERE <<Keyvalue>> = 'SPTest'	  IF @@ERROR <> 0	  	GOTO ErrorHandling*/ 	SELECT @ProcName = OBJECT_NAME(@@PROCID) 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Start '				+ CONVERT(VARCHAR, GETDATE(), 113)		PRINT @Message	END /*_____________________________________________________________________________  Procedure starts here  _____________________________________________________________________________*/ 		INSERT INTO [LoyaltyHistory](			[Loyalty],			[ExpiryDate],			[IssueDate],			[Userid],[status],[Orderid])		SELECT			@Loyalty,			@ExpiryDate,			@IssueDate,			@Userid,@status,@Orderid		IF @@ERROR <> 0			GOTO ErrorHandling /*_____________________________________________________________________________  Closing  ___________________________________________________________________________*/ 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Einde '				+ CONVERT(VARCHAR, GETDATE(), 113) 		PRINT @Message	END 	RETURN 0 /*_____________________________________________________________________________  ErrorHandling  _____________________________________________________________________________*/ 	ErrorHandling:		SELECT @Message = '*** ' + @ProcName + ' - Error '				+ CONVERT(VARCHAR, GETDATE(), 113) 		IF @Testlevel > 0			PRINT @Message 		IF @@TRANCOUNT > 0			ROLLBACK TRAN 		RETURN 1
GO
/****** Object:  StoredProcedure [dbo].[spInsKeyCategoryTable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInsKeyCategoryTable]/*	Created by: 	<<Company Name>>, <<Developers Name>>	Date: 		woensdag, 21 dec 2011	Purpose:	<<invullen>>	Input:		<<invullen>>	Output:		<<invullen>>*/	@Category                                 NVARCHAR(100)        = NULL,   	@sortid                                   NUMERIC(18,0)        ,         	@image                                    NVARCHAR(50)         = NULL,  	@Description                              NVARCHAR(2000)         = NULL,  	@Submitby                                   NVARCHAR(50)         = NULL     	AS 	DECLARE @ProcName 	VARCHAR(50)	DECLARE @Message  	VARCHAR(255)	DECLARE @Testlevel	INT /*_____________________________________________________________________________  Set @Testlevel, @ProcName, en Startingmessage  _____________________________________________________________________________*/ 	/*FILL OUT THIS SELECTSTATEMENT CORRECTLY !	  SELECT @Testlevel = <<Fieldname>> FROM <<SettingsTable>> 	  WHERE <<Keyvalue>> = 'SPTest'	  IF @@ERROR <> 0	  	GOTO ErrorHandling*/ 	SELECT @ProcName = OBJECT_NAME(@@PROCID) 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Start '				+ CONVERT(VARCHAR, GETDATE(), 113)		PRINT @Message	END /*_____________________________________________________________________________  Procedure starts here  _____________________________________________________________________________*/ 		INSERT INTO [CategoryTable](			[Category],			[sortid],			[image],			[Description],			[Submitby])		SELECT			@Category,			@sortid,					@image,			@Description,			@Submitby		IF @@ERROR <> 0			GOTO ErrorHandling /*_____________________________________________________________________________  Closing  ___________________________________________________________________________*/ 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Einde '				+ CONVERT(VARCHAR, GETDATE(), 113) 		PRINT @Message	END 	RETURN 0 /*_____________________________________________________________________________  ErrorHandling  _____________________________________________________________________________*/ 	ErrorHandling:		SELECT @Message = '*** ' + @ProcName + ' - Error '				+ CONVERT(VARCHAR, GETDATE(), 113) 		IF @Testlevel > 0			PRINT @Message 		IF @@TRANCOUNT > 0			ROLLBACK TRAN 		RETURN 1
GO
/****** Object:  StoredProcedure [dbo].[SpgetTopProducts]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpgetTopProducts]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select  * FROM  dbo.itemdetailtable as tableNew   WHERE     (IsNew = '1') AND (IsVisible <> '0' )ORDER BY id DESC
select  * FROM  dbo.itemdetailtable  as tableDaily WHERE     (DailyDeal = '1') AND (IsVisible <> '0') ORDER BY id DESC
select  * FROM  dbo.itemdetailtable   as tableBest  WHERE   (BestSellers = '1') AND (IsVisible <> '0') ORDER BY id DESC
select  * FROM  dbo.itemdetailtable   as tableFeatured  WHERE   (IsFeatured = '1') AND (IsVisible <> '0') ORDER BY id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[SpGetIndex]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetIndex] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select top(5) * FROM  dbo.itemdetailtable as tableNew   WHERE     (IsNew = '1') AND (IsVisible <> '0' )ORDER BY id DESC
select top(5) * FROM  dbo.itemdetailtable  as tableDaily WHERE     (DailyDeal = '1') AND (IsVisible <> '0') ORDER BY id DESC
select top(3) * FROM  dbo.itemdetailtable   as tableBest  WHERE   (BestSellers = '1') AND (IsVisible <> '0') ORDER BY id DESC
select top(5) * FROM  dbo.itemdetailtable   as tableFeatured  WHERE   (IsFeatured = '1') AND (IsVisible <> '0') ORDER BY id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[spFeedBack]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spFeedBack]

	@Name				nvarchar(50)=null,  
	@EmailID			nvarchar(50)=null,  
	@PhoneNo			nvarchar(50)=null,  
	@ProductName		nvarchar(50)=null,  
	@ProductSubName	nvarchar(50)=null,  
	@ProductQuality		nvarchar(50)=null,  
	@Comment			nvarchar(Max)=null 

as 
begin
insert into Feedback(Name,EmailID,PhoneNo,ProductName,ProductSubName,ProductQuality,Comment) values
(@Name,@EmailID,@PhoneNo,@ProductName,@ProductSubName,@ProductQuality,@Comment)
end
GO
/****** Object:  StoredProcedure [dbo].[spEnquiry]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spEnquiry]

	@Name				nvarchar(50)=null,  
	@EmailID			nvarchar(50)=null,  
	@PhoneNo	     nvarchar(50)=null,  
	@ProductName		nvarchar(50)=null,  	
	@Comment			nvarchar(Max)=null 

as 
begin
insert into Enquiry(Name,EmailID,PhoneNo,ProductName,Comment) values
(@Name,@EmailID,@PhoneNo,@ProductName,@Comment)
end
GO
/****** Object:  View [dbo].[ItemsDetail]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ItemsDetail]
AS
SELECT     st.qty, st.AlertOnQuantity, ct.Category, sc.SubCategory AS Expr6, it.id, it.categoryname, it.SubCategory, it.code, it.name, it.image, it.price, it.offerPrice, it.Detail, 
                      it.productprice, it.BestSellers, it.DailyDeal, it.IsNew, it.IsVisible, it.IsFeatured, it.Size, it.Weight, it.ShortDescripn, it.Submitby, it.ProInsDt, it.WholeSalePrice, ct.sortid, 
                      it.BuyingPrice
FROM         dbo.itemdetailtable AS it INNER JOIN
                      dbo.CategoryTable AS ct ON it.categoryname = ct.catid INNER JOIN
                      dbo.SubCategory AS sc ON it.SubCategory = sc.SubCatID LEFT OUTER JOIN
                      dbo.StockTable AS st ON it.id = st.itemid
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[26] 4[7] 2[17] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "it"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 200
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ct"
            Begin Extent = 
               Top = 6
               Left = 238
               Bottom = 125
               Right = 398
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sc"
            Begin Extent = 
               Top = 6
               Left = 436
               Bottom = 125
               Right = 596
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "st"
            Begin Extent = 
               Top = 6
               Left = 634
               Bottom = 125
               Right = 802
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 27
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1335
         Width = 1170
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ItemsDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'    End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ItemsDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ItemsDetail'
GO
/****** Object:  StoredProcedure [dbo].[getOrderCallExeList]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getOrderCallExeList]

@Searchtxt nvarchar (50)= null,
@CallExecutive nvarchar (50)= null,
@Status nvarchar (50)= null
as 
begin
SELECT ut.Name, ut.Phone, ft.OrderNo,ft.pid,ft.Dispatch, ft.username, ft.DOP, ft.amount, ft.MainAmount,(ft.amount-ft.MainAmount) as dif
FROM finalordertable ft INNER JOIN UserTable ut ON ft.username = ut.UserName 
where ft.CallExecutive=@CallExecutive and ft.Dispatch=isnull(@Status,Dispatch) and( ft.username like '%'+@Searchtxt+'%' or ut.Name like '%'+@Searchtxt+'%' or ft.OrderNo like '%'+@Searchtxt+'%')
end
GO
/****** Object:  StoredProcedure [dbo].[GetIteamPageWise]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetIteamPageWise] 
--[dbo].[GetImagesPageWise] 2, 10, null
	@PageIndex INT 
   ,@PageSize INT 
   ,@PageCount INT OUTPUT
AS
BEGIN	
	SET NOCOUNT ON;

    SELECT ROW_NUMBER() OVER
            (
                  ORDER BY [Id] ASC
            )AS RowNumber
      ,itemdetailtable.*
    INTO #Results
    FROM itemdetailtable

	DECLARE @RecordCount INT
    SELECT @RecordCount = COUNT(*) FROM #Results

	SET @PageCount = CEILING(CAST(@RecordCount AS DECIMAL(10, 2)) / CAST(@PageSize AS DECIMAL(10, 2)))
    PRINT       @PageCount
           
    SELECT * FROM #Results
    WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
    DROP TABLE #Results
END
GO
/****** Object:  View [dbo].[catAndscatList]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[catAndscatList]
AS
SELECT     dbo.CategoryTable.Category, dbo.SubCategory.SubCategory, dbo.SubCategory.CategoryID, dbo.SubCategory.SubCatID
FROM         dbo.CategoryTable INNER JOIN
                      dbo.SubCategory ON dbo.CategoryTable.catid = dbo.SubCategory.CategoryID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CategoryTable"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SubCategory"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'catAndscatList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'catAndscatList'
GO
/****** Object:  StoredProcedure [dbo].[StockLog]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[StockLog]
@ProductID int= null,
@FromDate datetime =null,
@ToDate datetime =null,
@SubCatID int=null,
@CatID int=null,
@SearchBy nvarchar(50)=null
as
begin
select max(pt.itemid) as ItemId, pt.item ,max(idt.categoryname) as CategoryId, MAX(idt.SubCategory) 
as SubCategoryId,SUM(pt.quantity) as SumQuantity ,SUM(pt.quantity*pt.price)as SumPrice,MAX(st.qty) as AvailableQuantity from PurChaseTable pt

left join StockTable st on pt.itemid=st.itemid 
left join itemdetailtable idt on pt.itemid=idt.id 
where  pt.itemid=isnull(@ProductID,pt.itemid) and  idt.SubCategory=isnull(@SubCatID,idt.SubCategory) 
and pt.item like '%'+@SearchBy+'%' 
and  idt.categoryname=isnull(@CatID,idt.categoryname)
and pt.Submitdt between isnull(@FromDate,Submitdt)and isnull(@ToDate,Submitdt)

group by pt.item
end


--execute StockLog @ItemId=22 null
--select * from itemdetailtable where itemid=22
GO
/****** Object:  StoredProcedure [dbo].[spUpdUserTable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdUserTable]/*	Created by: 	<<Company Name>>, <<Developers Name>>	Date: 		vrijdag, 23 dec 2011	Purpose:	<<invullen>>	Input:		<<invullen>>	Output:		<<invullen>>*/		@UserName                                 NVARCHAR(200)        = NULL,   	@Password                                 NVARCHAR(50)         = NULL,   	@Name                                     NVARCHAR(200)        = NULL,   	@Email                                    NVARCHAR(200)        = NULL,   	@Phone                                    NVARCHAR(50)         = NULL,   	@Address                                  NVARCHAR(200)        = NULL,   	@City                                     NVARCHAR(50)         = NULL,   	@State                                    NVARCHAR(50)         = NULL,   	@Zipcode                                  NVARCHAR(50)         = NULL,   	@registerdate                             DATETIME             = NULL     	AS 	DECLARE @ProcName 	VARCHAR(50)	DECLARE @Message  	VARCHAR(255)	DECLARE @Testlevel	INT /*_____________________________________________________________________________  Set @Testlevel, @ProcName, en Startingmessage  _____________________________________________________________________________*/ 	/*FILL OUT THIS SELECTSTATEMENT CORRECTLY !	  SELECT @Testlevel = <<Fieldname>> FROM <<SettingsTable>> 	  WHERE <<Keyvalue>> = 'SPTest'	  IF @@ERROR <> 0	  	GOTO ErrorHandling*/ 	SELECT @ProcName = OBJECT_NAME(@@PROCID) 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Start '				+ CONVERT(VARCHAR, GETDATE(), 113)		PRINT @Message	END /*_____________________________________________________________________________  Procedure starts here  _____________________________________________________________________________*/ 		UPDATE 	[UserTable]		SET								[Password] = @Password, 				[Name] = @Name, 				[Email] = @Email, 				[Phone] = @Phone, 				[Address] = @Address, 				[City] = @City, 				[State] = @State, 				[Zipcode] = @Zipcode, 				[registerdate] = @registerdate		WHERE				[UserName] = @UserName		IF @@ERROR <> 0			GOTO ErrorHandling /*_____________________________________________________________________________  Closing  ___________________________________________________________________________*/ 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Einde '				+ CONVERT(VARCHAR, GETDATE(), 113) 		PRINT @Message	END 	RETURN 0 /*_____________________________________________________________________________  ErrorHandling  _____________________________________________________________________________*/ 	ErrorHandling:		SELECT @Message = '*** ' + @ProcName + ' - Error '				+ CONVERT(VARCHAR, GETDATE(), 113) 		IF @Testlevel > 0			PRINT @Message 		IF @@TRANCOUNT > 0			ROLLBACK TRAN 		RETURN 1
GO
/****** Object:  StoredProcedure [dbo].[spUpdCategoryTable]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdCategoryTable]/*	Created by: 	<<Company Name>>, <<Developers Name>>	Date: 		donderdag, 22 dec 2011	Purpose:	<<invullen>>	Input:		<<invullen>>	Output:		<<invullen>>*/	@catid                                    NUMERIC(18,0)        ,         	@Category                                 NVARCHAR(100)        = NULL,   	@sortid                                   NUMERIC(18,0)        ,        	@image                                    NVARCHAR(50)         = NULL,	@Description                              NVARCHAR(2000)         = NULL     	AS 	DECLARE @ProcName 	VARCHAR(50)	DECLARE @Message  	VARCHAR(255)	DECLARE @Testlevel	INT /*_____________________________________________________________________________  Set @Testlevel, @ProcName, en Startingmessage  _____________________________________________________________________________*/ 	/*FILL OUT THIS SELECTSTATEMENT CORRECTLY !	  SELECT @Testlevel = <<Fieldname>> FROM <<SettingsTable>> 	  WHERE <<Keyvalue>> = 'SPTest'	  IF @@ERROR <> 0	  	GOTO ErrorHandling*/ 	SELECT @ProcName = OBJECT_NAME(@@PROCID) 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Start '				+ CONVERT(VARCHAR, GETDATE(), 113)		PRINT @Message	END /*_____________________________________________________________________________  Procedure starts here  _____________________________________________________________________________*/ 		UPDATE 	[CategoryTable]		SET				[Category] = @Category, 				[sortid] = @sortid, 								[image] = @image,				[Description]=@Description		WHERE				[catid] = @catid		IF @@ERROR <> 0			GOTO ErrorHandling /*_____________________________________________________________________________  Closing  ___________________________________________________________________________*/ 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Einde '				+ CONVERT(VARCHAR, GETDATE(), 113) 		PRINT @Message	END 	RETURN 0 /*_____________________________________________________________________________  ErrorHandling  _____________________________________________________________________________*/ 	ErrorHandling:		SELECT @Message = '*** ' + @ProcName + ' - Error '				+ CONVERT(VARCHAR, GETDATE(), 113) 		IF @Testlevel > 0			PRINT @Message 		IF @@TRANCOUNT > 0			ROLLBACK TRAN 		RETURN 1
GO
/****** Object:  StoredProcedure [dbo].[spUpdateOrderedProduct]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spUpdateOrderedProduct]
@PurchageQuantityOfProduct int=0,
@PriceOfProduct int=0,
@TotalPriceOfProductOld int=0,
@TotalPriceOfProductNew int=0,
@TotalPriceOfProduct int=0,
@PurchageId int=null,
@Amount int=0,
@NewQuantity int=0,
@OrderNo nvarchar(50)=null,
@MainAmount int=0,
@ShippingAmount int=50,
@CheckAmount int=1004,
@ItemId int =null,


--for Stock
@PreQuantity int=0,
@DiffQuantity int=0,
@Quantity int=0
as 
begin
---------------------------------retrive price,orderno and quantity value of this pid from  PurChaseTable
select @PriceOfProduct=price,@ItemId=itemid,@OrderNo=orderno,@PurchageQuantityOfProduct=quantity from PurChaseTable where pid=@PurchageId  
---------------------------------retrive MainAmount and Amount value of this order no from finalordertable
select @MainAmount= MainAmount,@Amount=Amount from finalordertable where OrderNo= @OrderNo  
---------------------------------retrive qty of this itemid from StockTable
select @PreQuantity=qty from StockTable where itemid=@ItemId
---------------------------------set Total Price Of Product Old price of this pid
set @TotalPriceOfProductOld=@PurchageQuantityOfProduct*@PriceOfProduct  
---------------------------------set Total Price Of Product New price of this pid
set @TotalPriceOfProductNew =@NewQuantity*@PriceOfProduct    
---------------------------------update quantity of  PurChaseTable with New Quantity
update PurChaseTable set quantity=@NewQuantity where pid=@PurchageId 


---////////////////////  Start for updating finalordertable and checkout table  ///////////////////////---
--================================Validate @TotalPriceOfProductOld  is greater or less than 1004 for shipping charges     =============
if(@TotalPriceOfProductOld>@TotalPriceOfProductNew)
begin
	set @TotalPriceOfProduct =@TotalPriceOfProductOld-@TotalPriceOfProductNew-- this much have to minus from final order and checkout amount
	if(@MainAmount>=@CheckAmount)
		begin
			set @MainAmount-=@TotalPriceOfProduct
			set @Amount-=@TotalPriceOfProduct
			if(@MainAmount<@CheckAmount)
			begin 
			set @Amount+=@ShippingAmount
			end
		end
	else
		begin
			set @MainAmount-=@TotalPriceOfProduct
			set @Amount-=@TotalPriceOfProduct
		end
end
else
begin
	set @TotalPriceOfProduct=@TotalPriceOfProductNew-@TotalPriceOfProductOld    --- this much have to add from final order and checkout amount
	if(@MainAmount>=@CheckAmount)
		begin
			set @MainAmount+=@TotalPriceOfProduct
			set @Amount+=@TotalPriceOfProduct
		end
	else
		begin
			set @MainAmount+=@TotalPriceOfProduct
			set @Amount+=@TotalPriceOfProduct
			if(@MainAmount>=@CheckAmount)
			begin
			set @Amount-=@ShippingAmount
			end
		end		
end

update Checkout set MainAmount=@MainAmount,amount=@Amount where Orderid= @OrderNo        
update finalordertable set MainAmount=@MainAmount,amount=@Amount where OrderNo= @OrderNo	


if(@NewQuantity=0)
begin

delete PurChaseTable where pid=@PurchageId

select * from PurChaseTable where Orderno=@OrderNo
IF(@@ROWCOUNT=0)
BEGIN
delete from finalordertable where OrderNo=@OrderNo
delete from Checkout where  orderID=@OrderNo
END


--update PurChaseTable set Iscancle=1 where pid=@PurchageId
end
---////////////////////  End for updating finalordertable and checkout table  ///////////////////////---

---////////////////////  Start for updating stock management  ///////////////////////---

if(@PurchageQuantityOfProduct>@NewQuantity)
	begin
		set @DiffQuantity= @PurchageQuantityOfProduct-@NewQuantity
		set @Quantity=@PreQuantity+@DiffQuantity
		update StockTable set qty=@Quantity where itemid=@ItemId
	end
else
begin
	set @DiffQuantity= @NewQuantity-@PurchageQuantityOfProduct
	set @Quantity=@PreQuantity-@DiffQuantity
	--if(@Quantity<=0)
	--	begin
	--		set @Quantity=0
	--	end
	update StockTable set qty=@Quantity where itemid=@ItemId 

end
---////////////////////  end for updating stock management  ///////////////////////---

end


--execute spUpdateOrderedProduct  @NewQuantity=0,@PurchageId=245
GO
/****** Object:  StoredProcedure [dbo].[SpUpdateCategoryVisibility]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpUpdateCategoryVisibility]
	-- Add the parameters for the stored procedure here
	
    @IsVisible  varchar(15),
    @catid  int

AS
	DECLARE @intErrorCode INT
BEGIN TRAN
    UPDATE CategoryTable
    SET IsVisible = @IsVisible
    WHERE catid = @catid

    SELECT @intErrorCode = @@ERROR
    IF (@intErrorCode <> 0) GOTO PROBLEM

    UPDATE itemdetailtable
    SET itemdetailtable.IsVisible = @IsVisible
    WHERE itemdetailtable.categoryname = @catid

    SELECT @intErrorCode = @@ERROR
    IF (@intErrorCode <> 0) GOTO PROBLEM
COMMIT TRAN

PROBLEM:
IF (@intErrorCode <> 0) BEGIN
PRINT 'Unexpected error occurred!'
    ROLLBACK TRAN
END
GO
/****** Object:  StoredProcedure [dbo].[spSubCategory]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spSubCategory]

@SubCatID numeric(18, 0)= NULL,
@CategoryID nvarchar(100)= NULL,
@SubCategory nvarchar(100) =NULL,
@SortId numeric(18, 0)= NULL,
@Image nvarchar(50)= NULL,
@IsVisible bit =NULL,
@SubCatInsDt datetime =NULL,
@Description nvarchar(2000)= NULL,
@Submitby nvarchar(50)= NULL,
@Action nvarchar(50)= NULL
as
begin

if(@Action='Insert')
begin
insert into SubCategory (CategoryID,SubCategory,Image,Description,Submitby)values (@CategoryID,@SubCategory,@Image,@Description,@Submitby)
end

if(@Action='Update')
begin

Update  SubCategory set SortId=@SortId,SubCategory=@SubCategory,Image=@Image,Description=@Description where SubCatID=@SubCatID

end

if(@CategoryID='' or @CategoryID='0')
begin
set @CategoryID=null
end
if(@Action='getSubCategoryList')
begin
select c.Category,sc.* from SubCategory sc left join CategoryTable c on sc.CategoryID=c.catid where c.catid=isnull(@CategoryID,catid) order by sc.SubCatID DESC
end
end
GO
/****** Object:  StoredProcedure [dbo].[spStock]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spStock]
@ItemId numeric(18,0)=null,
@AvailableQuantity int =0,
@AlertOnQuantity int=null,
@Action nvarchar(50)=null,
@AddQuantity int=null,
@ReduceQuantity int=null
as
begin

if(@Action='insert')
begin
insert into StockTable(itemid,qty,AlertOnQuantity) values(@ItemId,@AvailableQuantity,@AlertOnQuantity)

insert into tblStockLog (ItemId,AddedQuantity )values (@ItemId,@AvailableQuantity)
end

if(@Action='updateAddItemQuantity')
begin
select @AvailableQuantity=qty from StockTable where itemid=@ItemId
set @AvailableQuantity=@AvailableQuantity+@AddQuantity
update StockTable set qty=@AvailableQuantity where itemid=@ItemId 

end


if(@Action='updateReduceItemQuantity')
begin
select @AvailableQuantity=qty from StockTable where itemid=@ItemId

	--if((@AvailableQuantity-@ReduceQuantity)<=0)
	--begin
	--	set @AvailableQuantity=0
	--end
	--else
	--begin
		set @AvailableQuantity=@AvailableQuantity-@ReduceQuantity
	--end
update StockTable set qty=@AvailableQuantity where itemid=@ItemId 
end

if(@Action='updateItemWithStock')
BEGIN


declare @forStockPreQuantity int
select @forStockPreQuantity=qty from StockTable where  itemid=@ItemId
if((@AvailableQuantity-@forStockPreQuantity)>=0)
begin
insert into tblStockLog (ItemId,AddedQuantity )values (@ItemId,(@AvailableQuantity-@forStockPreQuantity))
end
    SET NOCOUNT ON;
	update StockTable set qty=@AvailableQuantity,AlertOnQuantity=@AlertOnQuantity where itemid=@ItemId 

IF(@@ROWCOUNT=0)
BEGIN
    insert into StockTable (qty,AlertOnQuantity,itemid)values(@AvailableQuantity,@AlertOnQuantity,@ItemId)
    --for stock log
    insert into tblStockLog (ItemId,AddedQuantity )values (@ItemId,@AvailableQuantity)
    
END
END
end
GO
/****** Object:  StoredProcedure [dbo].[spSelectGroupproduct]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelectGroupproduct]
	-- Add the parameters for the stored procedure here
	@productID   int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     dbo.tblGroupProducts.gid, dbo.tblGroupProducts.productID, dbo.tblGroupProducts.groupProductID, dbo.itemdetailtable.categoryname, dbo.itemdetailtable.code, 
                      dbo.itemdetailtable.name, dbo.itemdetailtable.image, dbo.itemdetailtable.price, dbo.itemdetailtable.offerPrice, dbo.itemdetailtable.Detail, 
                      dbo.itemdetailtable.productprice, dbo.itemdetailtable.IsVisible, dbo.itemdetailtable.id
FROM         dbo.tblGroupProducts INNER JOIN
                      dbo.itemdetailtable ON dbo.tblGroupProducts.groupProductID = dbo.itemdetailtable.id
WHERE     (dbo.tblGroupProducts.productID = @productID) AND (dbo.itemdetailtable.IsVisible = 'True')
END
GO
/****** Object:  StoredProcedure [dbo].[spSearch]    Script Date: 09/26/2014 18:55:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spSearch]
@SearchBy nvarchar(50) =null,
@CatID nvarchar(50)=null,
@SubCatID nvarchar(50)=null,
@Action nvarchar(50)=null,
@ProductID nvarchar(50)=null,
@RealPrice nvarchar(50)=null,
@OfferPrice nvarchar(50)=null,
@IDs nvarchar(max)=null,
@IsVisible bit =null,
@Visibilty bit =0,
@WholeSalePrice nvarchar (50)=null,
@BuyingPrice nvarchar (50)=null, 
@Orderby int =3

as 
begin

	if(@CatID='' or @CatID='0')
	begin
				set @CatID=null
	end

	if(@ProductID='' or @ProductID='0')
	begin
				set @ProductID=null
	end

	if(@SubCatID='' or @SubCatID='0')
	begin
				set @SubCatID=null
	end
	if(@Action='Search')
	begin
	
	
				SELECT st.qty,st.AlertOnQuantity,ct.Category, sc.SubCategory AS Expr6, it.*
				FROM itemdetailtable it INNER JOIN
				CategoryTable ct ON it.categoryname = ct.catid INNER JOIN
				SubCategory sc ON it.SubCategory = sc.SubCatID 
				left join StockTable st on it.id=st.itemid 
				WHERE 
				it.categoryname=isnull(@CatID,it.categoryname)
				and
				it.id=isnull(@ProductID,it.id)
				and
				it.SubCategory=isnull(@SubCatID,it.SubCategory)
				and
				(
				sc.SubCategory LIKE '%' + @SearchBy + '%'
				OR ct.Category LIKE '%' + @SearchBy + '%'
				OR it.code        LIKE '%' + @SearchBy + '%'
				OR it.Detail      LIKE '%' + @SearchBy + '%'
				OR it.name        LIKE '%' + @SearchBy + '%' 
				)
				AND (it.IsVisible <> @Visibilty)  	
				ORDER BY			
CASE WHEN @Orderby=1
THEN it.offerPrice  END desc,
CASE WHEN @Orderby=2
THEN it.offerPrice END asc,
CASE WHEN @Orderby=3
THEN  ct.sortid  END desc
end
	
	if(@Action='getItemStockAlertList')
	begin
	
	
				SELECT st.qty,st.AlertOnQuantity,ct.Category, sc.SubCategory AS Expr6, it.*
				FROM itemdetailtable it INNER JOIN
				CategoryTable ct ON it.categoryname = ct.catid INNER JOIN
				SubCategory sc ON it.SubCategory = sc.SubCatID 
				left join StockTable st on it.id=st.itemid 
				WHERE 
				it.categoryname=isnull(@CatID,it.categoryname)
				and
				it.id=isnull(@ProductID,it.id)
				and
				it.SubCategory=isnull(@SubCatID,it.SubCategory)
				and
			    st.qty<=st.AlertOnQuantity
				and
				(
				sc.SubCategory LIKE '%' + @SearchBy + '%'
				OR ct.Category LIKE '%' + @SearchBy + '%'
				OR it.code        LIKE '%' + @SearchBy + '%'
				OR it.Detail      LIKE '%' + @SearchBy + '%'
				OR it.name        LIKE '%' + @SearchBy + '%' 
				)
				AND (it.IsVisible <> @Visibilty)  ORDER BY  ct.sortid desc

	end
	
	
	if(@Action='get')
	begin
				SELECT 
				st.qty,st.AlertOnQuantity,
				ct.Category, sc.SubCategory AS Expr6, it.*
				FROM itemdetailtable it INNER JOIN
				CategoryTable ct ON it.categoryname = ct.catid INNER JOIN
				SubCategory sc ON it.SubCategory = sc.SubCatID 
				left join StockTable st on it.id=st.itemid 
				WHERE 
				it.categoryname=isnull(@CatID,it.categoryname)
				and
				it.id=isnull(@ProductID,it.id)
				and
				it.SubCategory=isnull(@SubCatID,it.SubCategory)
				and
			    st.qty<=st.AlertOnQuantity
				and
				(
				sc.SubCategory LIKE '%' + @SearchBy + '%'
				OR ct.Category LIKE '%' + @SearchBy + '%'
				OR it.code        LIKE '%' + @SearchBy + '%'
				OR it.Detail      LIKE '%' + @SearchBy + '%'
				OR it.name        LIKE '%' + @SearchBy + '%' 
				)
				AND (it.IsVisible <> @Visibilty)  ORDER BY  ct.sortid desc

	end
	
	
	if(@Action='adminItemTable')
	begin
	
				SELECT * from ItemsDetail WHERE 
				categoryname=isnull(@CatID,categoryname)
				and
				id=isnull(@ProductID,id)
				and
				SubCategory=isnull(@SubCatID,SubCategory)
				and
				(
				Expr6 LIKE '%' + @SearchBy + '%'
				OR Category LIKE '%' + @SearchBy + '%'
				OR code        LIKE '%' + @SearchBy + '%'
				OR Detail      LIKE '%' + @SearchBy + '%'
				OR name        LIKE '%' + @SearchBy + '%' 
				)
			    ORDER BY  proinsdt  desc

	end
	
	if(@Action='Delete')
	begin 
	delete from itemdetailtable  where id in (select convert(int,val) from split(@IDs,','))
	end
	
	if(@Action='IsVisible')
	begin 
	update itemdetailtable set IsVisible=@IsVisible  where id in (select convert(int,val) from split(@IDs,','))
	end
	
	if(@Action='UpdatePrice')
	begin 
	update itemdetailtable set price=@RealPrice ,  offerPrice=@OfferPrice ,BuyingPrice=@BuyingPrice,WholeSalePrice=@WholeSalePrice  where id =@IDs
	end

	
end

--exec spSearch @Action='IsVisible',@IsVisible=0,@IDs="1,2"
GO
/****** Object:  Default [DF_Wishlist_status]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[Wishlist] ADD  CONSTRAINT [DF_Wishlist_status]  DEFAULT ((0)) FOR [status]
GO
/****** Object:  Default [DF__tblStockL__Added__7E37BEF6]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[tblStockLog] ADD  DEFAULT (getdate()) FOR [AddedDateTime]
GO
/****** Object:  Default [DF__tblSalesP__regis__02FC7413]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[tblSalesPerson] ADD  DEFAULT (getdate()) FOR [registerdate]
GO
/****** Object:  Default [DF__SubCatego__SortI__619B8048]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[SubCategory] ADD  DEFAULT ((0)) FOR [SortId]
GO
/****** Object:  Default [DF__SubCatego__IsVis__628FA481]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[SubCategory] ADD  DEFAULT ((1)) FOR [IsVisible]
GO
/****** Object:  Default [DF__SubCatego__SubCa__6383C8BA]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[SubCategory] ADD  DEFAULT (getdate()) FOR [SubCatInsDt]
GO
/****** Object:  Default [DF_finalordertable_Dispatch]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[finalordertable] ADD  CONSTRAINT [DF_finalordertable_Dispatch]  DEFAULT ((0)) FOR [Dispatch]
GO
/****** Object:  Default [DF_finalordertable_Discount]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[finalordertable] ADD  CONSTRAINT [DF_finalordertable_Discount]  DEFAULT ((0)) FOR [Discount]
GO
/****** Object:  Default [DF_finalordertable_UsedLoyalty]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[finalordertable] ADD  CONSTRAINT [DF_finalordertable_UsedLoyalty]  DEFAULT ((0)) FOR [UsedLoyalty]
GO
/****** Object:  Default [DF__Feedback__FeedDa__014935CB]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT (getdate()) FOR [FeedDate]
GO
/****** Object:  Default [DF__Feedback__Active__023D5A04]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT ((1)) FOR [Active]
GO
/****** Object:  Default [DF__Feedback__Del__03317E3D]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT ((0)) FOR [Del]
GO
/****** Object:  Default [DF__Enquiry__EnqDate__07F6335A]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[Enquiry] ADD  DEFAULT (getdate()) FOR [EnqDate]
GO
/****** Object:  Default [DF__Enquiry__Active__08EA5793]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[Enquiry] ADD  DEFAULT ((1)) FOR [Active]
GO
/****** Object:  Default [DF__Enquiry__Del__09DE7BCC]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[Enquiry] ADD  DEFAULT ((0)) FOR [Del]
GO
/****** Object:  Default [DF_Checkout_amount]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[Checkout] ADD  CONSTRAINT [DF_Checkout_amount]  DEFAULT ((0)) FOR [amount]
GO
/****** Object:  Default [DF_Checkout_OrderDt]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[Checkout] ADD  CONSTRAINT [DF_Checkout_OrderDt]  DEFAULT (getdate()) FOR [OrderDt]
GO
/****** Object:  Default [DF_CategoryTable_sortid]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[CategoryTable] ADD  CONSTRAINT [DF_CategoryTable_sortid]  DEFAULT ((0)) FOR [sortid]
GO
/****** Object:  Default [DF_CategoryTable_IsVisible]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[CategoryTable] ADD  CONSTRAINT [DF_CategoryTable_IsVisible]  DEFAULT ((1)) FOR [IsVisible]
GO
/****** Object:  Default [DF_CategoryTable_CatInsDt]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[CategoryTable] ADD  CONSTRAINT [DF_CategoryTable_CatInsDt]  DEFAULT (getdate()) FOR [CatInsDt]
GO
/****** Object:  Default [DF_itemdetailtable_offerPrice]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_offerPrice]  DEFAULT ((0)) FOR [offerPrice]
GO
/****** Object:  Default [DF_itemdetailtable_BestSellers]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_BestSellers]  DEFAULT ((0)) FOR [BestSellers]
GO
/****** Object:  Default [DF_itemdetailtable_DailyDeal]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_DailyDeal]  DEFAULT ((0)) FOR [DailyDeal]
GO
/****** Object:  Default [DF_itemdetailtable_IsNew]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_IsNew]  DEFAULT ((0)) FOR [IsNew]
GO
/****** Object:  Default [DF_itemdetailtable_IsVisible]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_IsVisible]  DEFAULT ((0)) FOR [IsVisible]
GO
/****** Object:  Default [DF_itemdetailtable_IsFeatured]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_IsFeatured]  DEFAULT ((0)) FOR [IsFeatured]
GO
/****** Object:  Default [DF_itemdetailtable_ProInsDt]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_ProInsDt]  DEFAULT (getdate()) FOR [ProInsDt]
GO
/****** Object:  Default [DF_PurChaseTable_flagstatus]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[PurChaseTable] ADD  CONSTRAINT [DF_PurChaseTable_flagstatus]  DEFAULT ((0)) FOR [flagstatus]
GO
/****** Object:  Default [DF_PurChaseTable_Submitdt]    Script Date: 09/26/2014 18:55:36 ******/
ALTER TABLE [dbo].[PurChaseTable] ADD  CONSTRAINT [DF_PurChaseTable_Submitdt]  DEFAULT (getdate()) FOR [Submitdt]
GO

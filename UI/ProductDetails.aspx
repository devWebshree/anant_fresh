﻿<%@ Page Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="ProductDetails.aspx.cs" Inherits="ProductDetails" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc4" %>
<%@ Reference VirtualPath="~/control/chkoutctrl.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<title id="t" runat="server"></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="row">

       <div class="directory">
            <span><asp:HyperLink ID="hl_cat" runat="server"></asp:HyperLink></span><span>/</span>
            <span><asp:HyperLink ID="hl_scat" runat="server"></asp:HyperLink></span><span>/</span>
            <span><asp:HyperLink ID="hf_pname" runat="server"></asp:HyperLink></span>
       </div>
<div class="clear">
</div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:DataList ID="dlstProductDeatal" runat="server" DataKeyField="id" BorderWidth="0px"
                CellPadding="0" CellSpacing="0" OnItemDataBound="dlstProductDeatal_ItemDataBound"
                OnItemCommand="dlstProductDeatal_ItemCommand">
                <ItemTemplate>
                    <div class="row clearfix m10">
                        <div class="pFinalimage border-box">
                            
                            <div class="itemproduct1">
                            <img src="<%# "/ProductImage/" + Eval("image") %>" />

                         </div>

                        </div>
                        <div class="productInfo">
                            <div class="itemTitle">
                                <h3>
                                    <asp:Label ID="lblname" runat="server" Text='<%# Bind("name") %>' Visible="true"></asp:Label></h3>
                            </div>
                            <div class="Clear">
                            </div>
                            <div class="itemTitle">
                                <p class="mclear">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Detail") %>'></asp:Label></p>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="itemTitle">
                          <ul class="prdct_ul_dis">
                                    <li>
                                    <span class="ptag">Price</span>
                                    <span class="pprice">&#8377;
                                        <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("price") %>' CssClass="mr"></asp:Label>
                                        <asp:Label ID="lblOfferPrice" runat="server" Text='<%# Bind("offerprice") %>'></asp:Label>
                                    </span></li>
                                    <li><span class="ptag">Product Code:</span>
                                        <asp:Label ID="lblCode" runat="server" Text='<%# Bind("code") %>' CssClass="pprice"></asp:Label>
                                    </li>
       
                                    <li><span class="ptag">Qty:</span> 
                                    <span class="pprice">
                                        <asp:TextBox ID="txtQuantity" runat="server" Width="60">1</asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtQuantity"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Enter Quantity!!" SetFocusOnError="True"
                                            ValidationGroup="AddTo"></asp:RequiredFieldValidator>
                                        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtQuantity"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999"
                                            MinimumValue="1" SetFocusOnError="True" Type="Integer" ValidationGroup="AddTo">                                                                                      
                                        </asp:RangeValidator></span> </li>
                              
                                    <li>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("image") %>'></asp:Label>
                                        <asp:Label ID="lblid" runat="server" Text='<%# Bind("id") %>' Visible="false"></asp:Label>
                                    </li>
                                </ul>
                            </div>
                        
                            <div class="row m20">

                                        <asp:Button ID="btnBook" runat="server" CssClass="_button1 backgroundbg border-radius" CommandName="book" Text="Buy Now"
                                            ValidationGroup="AddTo" />
                                        <asp:Button ID="btnWislist" runat="server" CommandName="wishlist" Text="Add to Wishlist"
                                            ValidationGroup="AddTo" CssClass="_button1 backgroundbg border-radius" />
                                     
                                        <asp:Button ID="cont" CommandName="continue" runat="server" class="_button1 backgroundbg border-radius" Text="Countinue Shopping" />
                            
                            </div>
                        </div>
                    
    
                    </div>
                </ItemTemplate>
            </asp:DataList>
        </ContentTemplate>
    </asp:UpdatePanel>
  
  </div>
</asp:Content>


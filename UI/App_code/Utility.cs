﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

using System.IO;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{
	public Utility()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string sortStringLength(object txt, int i)
    {
        if (txt != null)
        {
            string rettitle = txt.ToString();
            if (rettitle.Length > i)
                rettitle = rettitle.Substring(0, i) + "...";
            return rettitle;
        }
        else
        {
            return "";
        }
    }
    public string RemoveSpace(string txt)
    {
        txt = txt.Replace(' ', '-');
        txt = txt.Replace('.', '-');
        txt = txt.Replace("--", "-");
        txt = txt.TrimEnd('-');
       // txt = txt.Replace('&', '+');
        txt = txt.Replace("&", "And");
        return txt;
    }
    public string RemoveUndeScore(string txt)
    {
        txt = txt.Replace('-', ' ');
   
        txt = txt.Replace("And", "&");
        return txt;
    }

    public string getIds(GridView gv1, string ID) 
    {
        string s = "";
        foreach (GridViewRow x in gv1.Rows)
        {
            CheckBox c1 = (CheckBox)x.FindControl("cb_select");
            if (c1.Checked == true)
            {
                Label l1 = (Label)x.FindControl(ID);
                s += l1.Text + ", ";

            }

        }
        s = s.TrimEnd(',');
        return s;
    }


    public string getProductIds(GridView gv1, string ID)
    {
        string s = "";
        foreach (GridViewRow x in gv1.Rows)
        {
            CheckBox c1 = (CheckBox)x.FindControl("cb_select");
            if (c1.Checked == true)
            {
                Label l1 = (Label)x.FindControl(ID);
                Label hd = ((Label)x.FindControl("lblFordelete"));
                //for delete image of product start
                string pt = "";
                pt = HttpContext.Current.Server.MapPath("~/ProductImage/" + hd.Text);
                if (File.Exists(pt))
                {
                    if (hd.Text != "NoImage.jpg")
                    {
                        File.Delete(pt);
                    }
                }
                //for delete image of product  end
                s += l1.Text + ", ";

            }

        }
        s = s.TrimEnd(',');
        return s;
    }
    

}
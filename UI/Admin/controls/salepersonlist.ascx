﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="salepersonlist.ascx.cs"  Inherits="admin_controls_userlist" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc2" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      </ContentTemplate>
</asp:UpdatePanel>
 
 <style type="text/css">
 .close {
    display:none;
}
 </style>

<div class="row" id="tag">
 <div class="ProductTitle" style="width:200px">

  <img id="plumin" src="../images/plus.jpg" class="open" alt="Open"/>
      <img src="../images/minus.jpg" class="close" alt="Close" />

  <span style="float:right; padding:5px;"><h2>ADD SALESMAN</h2></span>  
 </div>
 </div>


 <div class="clear"></div>
 <div class="row" id="pnl">
 <div class="column">
  <ul class="productul">


         <li>
                <label for="input" class="lbl"> Name</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtname" runat="server" CssClass="input"></asp:TextBox>&nbsp;

                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtname"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Name."
                        ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>
                </div>
        </li>

        

                <li>

                  <br />
                <label for="input" class="lbl"> Mobile</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtMobile" runat="server" CssClass="input"></asp:TextBox>&nbsp;

                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMobile"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Mobile."
                        ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>

                    
                </div>
       </li>

      
         <li>
           <br />
                <label for="input" class="lbl"> Email</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="input"></asp:TextBox>&nbsp;

                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Email Id."
                        ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>

                    
                </div>
       </li>

  



        <li>
          <br />
                <label class="lbl" for="input">Address</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" CssClass="input"></asp:TextBox>&nbsp;
                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtAddress"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Address." ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>
                    
                </div>
        </li>

            <li>
              <div class="inputbox">
                           <br />
             <br />
                <asp:Button ID="btnRegister" runat="server"  Width="150px" Height="30px"
                    CssClass="submit right" Text="Submit" 
                    ValidationGroup="admingvreg" onclick="btnRegister_Click" />
                    </div>
            </li>
        </ul>
</div></div>








 <div class="clear"></div>

                        <div class="menu clearfix m10">
                        <ul class="parentul">
                          <li style="width:800px;">
<a>
<asp:TextBox ID="txtsearch" placeholder="Search with Name " CssClass="inputtxt" runat="server"></asp:TextBox>   

                                    </a></li>
                                <li><asp:LinkButton ID="btnsearch" runat="server" OnClick="btnsearch_Click"  Text="Search" ValidationGroup="grpvalusrsrch" /></li>
                        </ul>
                        </div>


 <div class="clear"></div>


 <div class="row m10">
 <div class="column">
 <div class="scroll">
 <asp:GridView ID="grdSalesPerson" runat="server" AutoGenerateColumns="False" 
                AllowPaging="True" 
                OnPageIndexChanging="grdSalesPerson_PageIndexChanging" PageSize="30" CssClass="table"
                OnRowDeleting="grdSalesPerson_RowDeleting" EmptyDataText="Data not available" 
                onrowcancelingedit="grdSalesPerson_RowCancelingEdit" 
                onrowediting="grdSalesPerson_RowEditing" onrowupdating="grdSalesPerson_RowUpdating">
                 <RowStyle HorizontalAlign="Center" Wrap="False" BackColor="White" />
    <Columns>



    <asp:TemplateField HeaderText="Serial No.">
                            <ItemTemplate>
                                <strong><%# Container.DataItemIndex + 1 %></strong>
                            </ItemTemplate>
    </asp:TemplateField>



       <asp:TemplateField HeaderText="Name">

       <ItemTemplate>
               <asp:Label ID="lblName" runat="server" Text='<%# Eval("name") %>'></asp:Label>
       </ItemTemplate>
              <EditItemTemplate>
                   <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("name") %>' CssClass="input"></asp:TextBox>
                   </br>
                <cc2:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtName" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Enter Name." ValidationGroup="Editval">
                </cc2:RequiredFieldValidator>
              </EditItemTemplate>

       </asp:TemplateField> 


        <asp:TemplateField HeaderText="Email" SortExpression="AEmail">
            <EditItemTemplate>
                <asp:TextBox ID="txtEmail" runat="server" Text='<%# Bind("Email") %>' CssClass="input wid"></asp:TextBox>
                <cc2:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                    ControlToValidate="txtEmail" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Invalid EmailId." 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    ValidationGroup="Editval">
                </cc2:RegularExpressionValidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>



        <asp:TemplateField HeaderText="Mobile No" SortExpression="Phone">
            <EditItemTemplate>
                <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("Phone") %>' CssClass="input wid"></asp:TextBox><br />
                <cc2:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                    ControlToValidate="txtPhone" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Enter Phone No." ValidationGroup="Editval">    
                </cc2:RequiredFieldValidator>
                <cc2:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" 
                    ControlToValidate="txtPhone" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Invalid Phone No." ValidationExpression="^[0-9 -]{10,11}$" 
                    ValidationGroup="Editval">
            
                </cc2:RegularExpressionValidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>



        <asp:TemplateField HeaderText="Address" SortExpression="Address">
            <EditItemTemplate>
                <asp:TextBox ID="txtAddress" runat="server" Text='<%# Bind("Address") %>' 
                     TextMode="MultiLine" CssClass="input wid"></asp:TextBox>
 
                <cc2:RequiredFieldValidator ID="RequiredFieldValidator401" runat="server"  CssClass="error" 
                    controltovalidate="txtAddress" display="Dynamic" 
                    errormessage="!" validationgroup="Editval">
            
                </cc2:RequiredFieldValidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

  
  
     
        <asp:TemplateField HeaderText="Registered date">
       <ItemTemplate>
       <asp:Label ID="lblregdate" runat="server" Text='<%# Eval("registerdate","{0:dd MMM yyy}") %>' ></asp:Label>
       </ItemTemplate>
       </asp:TemplateField>

          <asp:TemplateField HeaderText="Action" ShowHeader="False">
              <EditItemTemplate>
                  <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                      CommandName="Update" Text="Update" ValidationGroup="Editval" CssClass="button"></asp:LinkButton>
                  <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                      CommandName="Cancel" Text="Cancel" CssClass="button "></asp:LinkButton>
              </EditItemTemplate>
              <ItemTemplate>
                  <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                      CommandName="Edit" Text="Edit" CssClass="button "></asp:LinkButton>
              </ItemTemplate>
          </asp:TemplateField>

        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
                <asp:LinkButton ID="lnkdel" runat="server" CommandName="delete" OnClientClick="return confirm('Are you sure, you want to delete this Sales Person!')" CausesValidation="False" CssClass="button ">Delete</asp:LinkButton>
                     <asp:Label ID="lblSalesPersonId" runat="server" Text='<%# Eval("SalesPersonId") %>' Visible="False"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
     <HeaderStyle BackColor="#CCCCCC" />
     <AlternatingRowStyle BackColor="#CCCCCC" />
</asp:GridView>
</div>
</div>
 </div>

  
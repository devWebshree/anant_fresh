﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary
{
   public class CallExecutive
    {
       public string username { get; set; }
       public string OrderNo { get; set; }
       public string Phone { get; set; }
       public string UserId { get; set; }
       public string Searchtxt { get; set;}
       public string Status { get; set; }
       public string dt1 { get; set; }
       public string dt2 { get; set; }
       public string Through { get; set; } //
       public string Orderby { get; set; }
       public string SortPrefix { get; set; }
       public string SalesName { get; set; }
       public string PurchageType { get; set; }   


       //for payment

       public string TransactionId { get; set; }
       public string PaymentStatus { get; set; }   

    }
}

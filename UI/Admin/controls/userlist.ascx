﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="userlist.ascx.cs"  Inherits="admin_controls_userlist" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc2" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      </ContentTemplate>
</asp:UpdatePanel>

 <div class="row">
 <div class="ProductTitle"><h2>EXISTING CUSTOMER LIST</h2></div>
 </div>
 <div class="clear"></div>

                        <div class="menu clearfix m10">
                        <ul class="parentul">
                          <li style="width:800px;">
<a>
<asp:TextBox ID="txtsearch" placeholder="Search with Username / Name / Email / City / State" CssClass="inputtxt" runat="server"></asp:TextBox>   
<cc2:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtsearch"
                Display="Dynamic" CssClass="error1" ErrorMessage="Searching Text required!" ValidationGroup="grpvalusrsrch"></cc2:RequiredFieldValidator>
<asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
               <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/simple.gif" />
                </ProgressTemplate>
            </asp:UpdateProgress>  
                                    </a></li>
                                <li><asp:LinkButton ID="btnsearch" runat="server" OnClick="btnsearch_Click"  Text="Search" ValidationGroup="grpvalusrsrch" /></li>
                                <li><asp:LinkButton ID="lnkAll" runat="server" OnClick="lnkAll_Click" >All Users</asp:LinkButton> </li>
                                <li><asp:LinkButton ID="imgExel" runat="server" OnClick="imgExel_Click" Text="Download in Excel" ToolTip="Download in Excel" /> </li>
                        </ul>
                        </div>


 <div class="clear"></div>


 <div class="row m10">
 <div class="column">
 <div class="scroll">
 <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                AllowPaging="True" 
                OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="30" CssClass="table"
                OnRowDeleting="GridView1_RowDeleting" EmptyDataText="Data not available" 
                onrowcancelingedit="GridView1_RowCancelingEdit" 
                onrowediting="GridView1_RowEditing" onrowupdating="GridView1_RowUpdating">
                 <RowStyle HorizontalAlign="Center" Wrap="False" BackColor="White" />
    <Columns>



    <asp:TemplateField HeaderText="Serial No.">
                            <ItemTemplate>
                                <strong>
                                    <%# Container.DataItemIndex + 1 %></strong>
                            </ItemTemplate>
                        </asp:TemplateField>



          <asp:TemplateField HeaderText="UserName">
              <EditItemTemplate>
                  <asp:Label ID="Label9" runat="server" Text='<%# Eval("username") %>'></asp:Label>
              </EditItemTemplate>
       <ItemTemplate>
       
        <asp:Label ID="Label9" runat="server" Text='<%# Eval("username") %>'></asp:Label>
     
       </ItemTemplate>
       </asp:TemplateField> 
     <%--     <asp:TemplateField HeaderText="Password">
              <EditItemTemplate>
                  <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("Password") %>' CssClass="input"></asp:TextBox>
                  <cc2:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                      controltovalidate="TextBox7" display="Dynamic" 
                      errormessage="Please Enter Address!" validationgroup="Editval">        
                  </cc2:RequiredFieldValidator>
                  <cc2:RegularExpressionValidator ID="RegularExpressionValidator102" runat="server" 
                      controltovalidate="TextBox7" display="Dynamic" 
                      errormessage="Please delete any of these characters(,&lt;&gt;#%;'`)." 
                      validationexpression="^[^&lt;&gt;(){}?&amp;*~`!#$%^=+|\\:'\,;]{0,250}$" 
                      validationgroup="Editval"></cc2:RegularExpressionValidator>
              </EditItemTemplate>
              <ItemTemplate>
                  <asp:Label ID="Label7" runat="server" Text='<%# Bind("Password") %>'></asp:Label>
              </ItemTemplate>
          </asp:TemplateField>--%>
        <asp:TemplateField HeaderText="Name" SortExpression="AName">
            <EditItemTemplate>
             <asp:TextBox ID="txt_pass" runat="server" Text='<%# Bind("Password") %>' Visible="false" CssClass="input"></asp:TextBox>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Name") %>' CssClass="input wid"></asp:TextBox>
                <cc2:RegularExpressionValidator ID="Reglidator2" runat="server" 
                    ControlToValidate="TextBox1" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Name should contains only Alphabets." 
                    ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ValidationGroup="Editval">
                </cc2:RegularExpressionValidator>
                <cc2:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="TextBox1" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Please Specify your Name." ValidationGroup="Editval">
                </cc2:RequiredFieldValidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Email" SortExpression="AEmail">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Email") %>' CssClass="input wid"></asp:TextBox>
                <cc2:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                    ControlToValidate="TextBox2" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Invalid EmailId." 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    ValidationGroup="Editval">
               
                </cc2:RegularExpressionValidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Mobile No" SortExpression="Phone">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Phone") %>' CssClass="input wid"></asp:TextBox>
                <cc2:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                    ControlToValidate="TextBox3" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Please Enter Phone No." ValidationGroup="Editval">    
                </cc2:RequiredFieldValidator>
                <cc2:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" 
                    ControlToValidate="TextBox3" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Invalid Phone No." ValidationExpression="^[0-9 -]{10,11}$" 
                    ValidationGroup="Editval">
            
                </cc2:RegularExpressionValidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Address" SortExpression="Address">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Address") %>' 
                     TextMode="MultiLine" CssClass="input wid"></asp:TextBox>
                <cc2:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    controltovalidate="TextBox4" display="Dynamic" 
                    errormessage="Please delete any of these characters(,&lt;&gt;#%;'`)." 
                    validationexpression="^[^&lt;&gt;(){}?&amp;*~`!#$%^=+|\\:'\;]{0,2000}$" 
                    validationgroup="Editval"></cc2:RegularExpressionValidator>
                <cc2:RequiredFieldValidator ID="RequiredFieldValidator401" runat="server" 
                    controltovalidate="TextBox4" display="Dynamic" 
                    errormessage="Please Enter Address!" validationgroup="Editval">
            
                </cc2:RequiredFieldValidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="City" SortExpression="ACity">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("City") %>' CssClass="input wid"></asp:TextBox>
                <cc2:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" 
                    ControlToValidate="TextBox5" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Specify your City." ValidationGroup="Editval">
            
                </cc2:RequiredFieldValidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label5" runat="server" Text='<%# Bind("City") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="State" SortExpression="AState">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("State") %>' CssClass="input wid" ></asp:TextBox>
                <cc2:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                    controltovalidate="TextBox6" display="Dynamic" errormessage="Enter State!" 
                    setfocusonerror="True" validationgroup="Editval">
            
                </cc2:RequiredFieldValidator>
                <cc2:RegularExpressionValidator ID="RegularExpionValidator7" runat="server" 
                    controltovalidate="TextBox6" display="Dynamic" errormessage="Invalid State!" 
                    validationexpression="^[^&lt;&gt;%]{0,200}$" validationgroup="Editval">  
              
                </cc2:RegularExpressionValidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label6" runat="server" Text='<%# Bind("State") %>' ></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
     
        <asp:TemplateField HeaderText="Reg. date">
            <EditItemTemplate>
                <asp:Label ID="lblEditregdate" runat="server" 
                    Text='<%# Eval("registerdate","{0:dd MMM yyy}") %>' CssClass="input wid"></asp:Label>
            </EditItemTemplate>
       <ItemTemplate>
       <asp:Label ID="lblregdate" runat="server" Text='<%# Eval("registerdate","{0:dd MMM yyy}") %>' ></asp:Label>
       </ItemTemplate>
       </asp:TemplateField> 
        <asp:TemplateField HeaderText="Zipcode" SortExpression="AZipcode">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("Zipcode") %>' CssClass="input wid"></asp:TextBox>
                <cc2:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="TextBox8" CssClass="error" Display="Dynamic" 
                    ErrorMessage="Specify Zipcode." ValidationGroup="Editval">
               
                </cc2:RequiredFieldValidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label8" runat="server" Text='<%# Bind("Zipcode") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
          <asp:TemplateField HeaderText="Action" ShowHeader="False">
              <EditItemTemplate>
                  <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                      CommandName="Update" Text="Update" ValidationGroup="Editval" CssClass="button"></asp:LinkButton>
                  <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                      CommandName="Cancel" Text="Cancel" CssClass="button "></asp:LinkButton>
              </EditItemTemplate>
              <ItemTemplate>
                  <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                      CommandName="Edit" Text="Edit" CssClass="button "></asp:LinkButton>
              </ItemTemplate>
          </asp:TemplateField>
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
                <asp:LinkButton ID="lnkdel" runat="server" CommandName="delete" 
                    OnClientClick="return confirm('Are you sure, you want to delete this User!')" 
                     CausesValidation="False" CssClass="button ">Delete</asp:LinkButton><asp:Label
                    ID="lblusr" runat="server" Text='<%# Eval("username") %>' Visible="False"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
     <HeaderStyle BackColor="#CCCCCC" />
                         <AlternatingRowStyle BackColor="#CCCCCC" />
</asp:GridView>
</div>
</div>
 </div>

  
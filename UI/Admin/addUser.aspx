﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="addUser.aspx.cs" Inherits="Admin_addUser" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script src="../js/jquery-1.11.0.js" type="text/javascript"></script>

  
<script type="text/javascript">

    $(document).ready(function () {
        $("#addRole").addClass('active');

        $('#pnl').hide();
        $('.ProductTitle').on('click', function () {

            $(this).children('img').each(function () {
                $(this).toggle(0);
            });

            $('#pnl').slideToggle();
        });

    });
</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 <style type="text/css">
 .close {
    display:none;
}
 </style>

  <div class="row" id="tag">
 <div class="ProductTitle" style="width:200px">

  <img id="plumin" src="../images/plus.jpg" class="open" alt="Open"  />
      <img src="../images/minus.jpg" class="close" alt="Close" />

  <span style="float:right; padding:5px;"><h2>ADD EMPLOYEE </h2></span>  
 </div>
 </div>

 <div class="clear"></div>
 <div class="row">
 <div class="column"  id="pnl">
  <ul class="productul">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
     </ContentTemplate>
                    </asp:UpdatePanel>
            <div class="center">
        <ul class="lgnul m20">


         <li>
                <label for="input" class="lbl"> Name</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtname" runat="server" CssClass="input"></asp:TextBox>&nbsp;

                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtname"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Name Id."
                        ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>
                </div>
        </li>

        

                <li>

                  <br />
                <label for="input" class="lbl"> Mobile</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtMobile" runat="server" CssClass="input"></asp:TextBox>&nbsp;

                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMobile"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Mobile."
                        ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>

                    
                </div>
       </li>

      
         <li>
           <br />
                <label for="input" class="lbl"> User Name</label>
                <div class="inputbox">
                    <asp:TextBox ID="txt_username" runat="server" CssClass="input"></asp:TextBox>&nbsp;

                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_username"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Email Id."
                        ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>

                    
                </div>
       </li>

  



        <li>
          <br />
                <label class="lbl" for="input">Password</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtpass" runat="server" TextMode="Password" CssClass="input"></asp:TextBox>&nbsp;
                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtpass"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Password." ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>
                    
                </div>
        </li>

        
        <li>
         <br />
                <label class="lbl" for="input">Re Password</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtRePass" runat="server" TextMode="Password" CssClass="input"></asp:TextBox>&nbsp;
                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRePass"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter RePassword." ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>
                    <asp:CompareValidator CssClass="error1"  ID="CompareValidator1" runat="server" ControlToCompare="txtpass" ControlToValidate="txtRePass" ErrorMessage="Password is not match!"></asp:CompareValidator>

                    
                </div>
        </li>


            <li>

            

         <br />


                <label class="lbl" for="input">
                    Role</label>
                <div class="inputbox">
                  
                  <asp:DropDownList ID="ddl_role" runat="server" CssClass="input">
                  <asp:ListItem>Select</asp:ListItem>
                  <asp:ListItem Value="1">Admin</asp:ListItem>
                  <asp:ListItem Value="2">Call Executive</asp:ListItem>
                  <asp:ListItem Value="3">Dispatch Executive</asp:ListItem>
                  </asp:DropDownList>
                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" InitialValue="Select" ControlToValidate="ddl_role"
                        Display="Dynamic" ErrorMessage="Select Role!" SetFocusOnError="True" ValidationGroup="admingvreg"
                        CssClass="error1"></cc2:RequiredFieldValidator> 
                </div>
            </li>


   

            <li>
              <div class="inputbox">
                           <br />
             <br />
                <asp:Button ID="btnRegister" runat="server"  Width="150px" Height="30px"
                    CssClass="submit right" Text="Submit" 
                    ValidationGroup="admingvreg" onclick="btnRegister_Click" />
                    </div>
            </li>
        </ul>
</div>

 
   </div>                 


 <div class="clear"></div>

                        <div class="menu clearfix m10">
                        <ul class="parentul">
                          <li style="width:800px;">
<a>
<asp:TextBox ID="txtsearch" placeholder="Search with Name/Role/mobile/Username/" CssClass="inputtxt" runat="server"></asp:TextBox>   

                                    </a></li>
                                <li><asp:LinkButton ID="btnsearch" runat="server" OnClick="btnsearch_Click"  Text="Search" ValidationGroup="grpvalusrsrch" /></li>
                        </ul>
                        </div>


 <div class="clear"></div>


 <div class="column m10"  id="Div1">

                    
              <asp:GridView ID="listAdmin" CssClass="table" runat="server" AutoGenerateColumns="false">
              <Columns>

                        <asp:TemplateField HeaderText="Serial No.">
                        <ItemTemplate>

                    <strong> <%# Container.DataItemIndex + 1 %></strong>
              

                            </ItemTemplate>
                  </asp:TemplateField>


              <asp:TemplateField HeaderText="Name">
              <ItemTemplate>
              <%# Eval("name")%>
              <asp:HiddenField ID="fhId" runat="server" Value='<%# Eval("aid") %>' />
              </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField HeaderText="Mobile No">
              <ItemTemplate>
              <%# Eval("mobileno") %>              
              </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField HeaderText="User Name">
              <ItemTemplate>
              <%# Eval("uname") %>              
              </ItemTemplate>
              </asp:TemplateField>



              <asp:TemplateField HeaderText="Role">
              <ItemTemplate>
              <%# Eval("Role").ToString() == "dbadmin" ? "Admin" : Eval("Role")%>              
              </ItemTemplate>
              </asp:TemplateField>


              <asp:TemplateField HeaderText="Delete">
              <ItemTemplate>
             <asp:Button ID="btnDelete" runat="server"  Text="Delete" OnClientClick="return confirm('Are you sure?')" CssClass="button" OnClick="OnClick_btnDelete"/>            
              </ItemTemplate>
              </asp:TemplateField>


              </Columns>
              </asp:GridView>      
                    
                    
                    
                    
                    </div></div></div>

</asp:Content>


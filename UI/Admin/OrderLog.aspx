﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="OrderLog.aspx.cs" Inherits="Admin_OrderLog" %>

<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc2" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl" TagPrefix="rjs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#OrderLog").addClass('active');

        });
    </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">








    <div class="menubox">
        <div class="menu">
            <ul class="parentul">
                <%--<li><a href="#">Item Per Page<asp:DropDownList ID="ddlViewItem" CssClass="ddl width-1" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlViewItem_SelectedIndexChanged">
                                        <asp:ListItem>20</asp:ListItem>
                                        <asp:ListItem>40</asp:ListItem>
                                        <asp:ListItem>60</asp:ListItem>
                                        <asp:ListItem>80</asp:ListItem>
                                        <asp:ListItem>100</asp:ListItem>
                                        <asp:ListItem>200</asp:ListItem>
                                        <asp:ListItem>400</asp:ListItem>
                                        <asp:ListItem>800</asp:ListItem>
                                    </asp:DropDownList>
                                </a></li>--%>
                <%--        <li><a class="middle" href="#">Stock :<asp:DropDownList ID="ddlStock" runat="server" CssClass="ddl" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlStock_SelectedIndexChanged">
                                        <asp:ListItem Value="0">All</asp:ListItem>
                                      
                                        <asp:ListItem Value="2">Out Of Stock</asp:ListItem>
                                    </asp:DropDownList>
                                </a></li>--%>

                <li><a class="middle" href="#">Category :<asp:DropDownList ID="ddlCategoryName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryName_SelectedIndexChanged"
                    CssClass="ddl">
                </asp:DropDownList>
                </a></li>

                <li><a class="middle" href="#">Subcategory :<asp:DropDownList ID="ddl_subCat" CssClass="ddl" runat="server" AutoPostBack="True"
                    OnSelectedIndexChanged="ddl_subCat_SelectedIndexChanged">
                    <asp:ListItem>All</asp:ListItem>
                </asp:DropDownList>
                </a></li>
                <li><a>From :<asp:TextBox ID="txtsdt" runat="server" Width="100px" ReadOnly="True" CssClass="inputtxt"></asp:TextBox></a></li>
                <li><a>
                    <rjs:PopCalendar ID="PopCalendar2" runat="server" Control="txtsdt" Format="mm dd yyyy" From-Date="2009-01-01" To-Date="" To-Today="True" />
                </a></li>
                <li><a>To :<asp:TextBox ID="txtedt" runat="server" Width="100px" ReadOnly="True" CssClass="inputtxt"></asp:TextBox></a></li>
                <li><a>
                    <rjs:PopCalendar ID="PopCalendar1" runat="server" Control="txtedt" Format="mm dd yyyy" From-Date="2009-01-01" To-Date="" To-Today="True" />
                </a></li>

                <li style=""><a>
                    <asp:TextBox ID="txt_search" placeholder="Search by Code, name" CssClass="inputtxt" runat="server"></asp:TextBox></a></li>
                <li>
                    <asp:LinkButton ID="btn_srch" runat="server" Text="Search" OnClick="btn_srch_Click" /></li>
                <li>
                    <asp:LinkButton ID="LinkButton1" runat="server"
                        OnClick="LinkButton1_Click" CssClass="button right">Generate Excel</asp:LinkButton></li>
            </ul>
        </div>


    </div>












    <div class="row m10">
        <div class="column">
            <div class="scroll">


                <asp:GridView ID="gvrStockLog" runat="server" AllowPaging="True" PageSize="100" 
                    AutoGenerateColumns="False" CssClass="table"
                    OnPageIndexChanging="gvrStockLog_PageIndexChanging">

                    <Columns>
                        <asp:TemplateField HeaderText="Serial No.">
                            <ItemTemplate>
                                <strong><%# Container.DataItemIndex + 1 %></strong>
                                <asp:Label ID="lblCatid" Visible="false" runat="server" Text='<%# Bind("ItemId")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Product Name">
                            <ItemTemplate>
                                <asp:Label ID="lblCatNew" runat="server" Text='<%# Bind("Item")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Sold Out Quantity">
                            <ItemTemplate>
                                <asp:Label ID="lblCatNew" runat="server" Text='<%# Bind("SumQuantity")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Sold Price">
                            <ItemTemplate>
                                <asp:Label ID="lblCatNew" runat="server" Text='<%# Bind("Sumprice")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Available Quantity">
                            <ItemTemplate>
                                <asp:Label ID="lblCatNew" runat="server" Text='<%# Bind("availableQuantity")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                    <HeaderStyle ForeColor="Black" Height="30px" BorderColor="White" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle HorizontalAlign="Center" BackColor="#E4E6E8" BorderColor="White" Height="30px" />
                    <PagerStyle BackColor="#BDC3C7" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#BDC3C7" Font-Bold="True" Font-Size="13px" ForeColor="Black"
                        HorizontalAlign="Center" />
                    <AlternatingRowStyle HorizontalAlign="Center" BackColor="#E4E6E8" ForeColor="Black" />
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>


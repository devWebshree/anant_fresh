﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using BAL;

public partial class control_logout : System.Web.UI.UserControl
{
    modifyData md = new modifyData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblWelcome.Text += " " + md.FindNameByUserName(Session["IsUser"].ToString());
        }

    }
    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Session["IsUser"] = "0";
        Response.Redirect("/login");
    }
}

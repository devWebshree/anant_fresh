﻿<%@ Page Title="AnantFresh:upload product through ExelSheet" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="readExcel.aspx.cs" Inherits="admin_readExcel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
<script type="text/javascript"> 

    $(document).ready(function () {
        $("#uploadexcelActive").addClass('active');

    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="row">
<div class="ProductTitle"><h2>UPLOAD PRODUCT  </h2></div>
</div>
<div class="clear"></div>

<div class="row m10">
<div class="column">
 <asp:Panel ID="Panel1" runat="server">
 
        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="input_2 l mr5" style="width:86%;" />
        <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" CssClass="submit" />
  
        
     <asp:Label ID="Label1" runat="server"></asp:Label>
    </asp:Panel>
   </div>
</div>
</asp:Content>


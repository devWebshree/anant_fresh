﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebshreeCommonLibrary;
using DAL;
using ClassLibrary;
using System.Data;

public partial class Exam_Result : System.Web.UI.Page
{
    Result blResult = new Result();
    dalResult dlResult = new dalResult();
    Utility all = new Utility();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindResultDetails();
            lblMsg.Text = "";

        }
    }
    public void bindResultDetails()
    {
        blResult.Active = true;
        TransportationPacket Packet = new TransportationPacket();

        blResult.Name = txtName.Text;
        blResult.RollNumber = txtRollNo.Text;

        Packet.MessagePacket = blResult;
        Packet = dlResult.getResult(Packet);
        DataSet ds;
        if (Packet.MessageStatus == EStatus.Success)
        {
            ds = (DataSet)Packet.MessageResultset;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                lblName.Text = dr["NAME"].ToString();
                lblFatherName.Text=dr["FatherName"].ToString();
                lblSchoolName.Text = dr["SchoolName"].ToString();
                lblMarks.Text = dr["obtainMarks"].ToString();
                lblTotalmarks.Text = dr["Totalmarks"].ToString();
                lblRollNumber.Text = dr["RollNumber"].ToString();
                lblClass.Text = dr["Class"].ToString();
            }
            pnldetails.Visible = true;
            lblMsg.Text = "";
        }
        else 
        {
            lblMsg.Text = "Please enter valid Roll No.";
            lblMsg.ForeColor = System.Drawing.Color.Red;
            pnldetails.Visible = false;
        }
        
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bindResultDetails();
    }
}
﻿<%@ Page Title="Contact with us" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="contact.aspx.cs" Inherits="contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="row">
<div id="tab-container" class="tab-container">
  <ul class='etabs'>
    <li class='tab'><a href="#Contact-Us">Contact Us</a></li>
    <li class='tab'><a href="#Enquiry">Enquiry</a></li>
    <li class='tab'><a href="#Feedback">Feedback</a></li>
  </ul>
  <div class="line" style="margin-bottom:30px;"></div>
  <div id="Contact-Us">
   <h2 class="left">Contact Us</h2><span style="color:#f00; font-size:11px; float:right;">* All Fields are mandatory</span>
   <div class="about clearfix border-box">
   <div class="clft left">
     <ul class="lgnul">
     <li>
     <span class="left" style="width:50px;"><img src="/images/address_icon.png" /></span>
     <span class="left address" style=" text-transform:capitalize;">
       Anant Groups<br />
A-307A, 3rd Floor, </br>
Roman court,</br>
ansal sushant city,</br>
main sector road,</br>
kundli  haryana 131028 



     </span>
     </li>

     <li>
     <span class="left" style="width:50px;"><img src="/images/phone_icon.png" /></span>
     <span class="left lbln">
      0130-6057983
     </span>
     </li>
     <li>
     <span class="left" style="width:50px;"><img src="/images/messge_icon.png" /></span>

     <span class="left lbln" style="line-height:18px;">
    <a href="mailto:info@anantfresh.com" target="_blank" class="mailto">info@anantfresh.com</a>
   
     </span>


     <li>
     <span class="left" style="width:50px;"><img src="/images/web.png" /></span>
     <span class="left lbln">
      <a href="/home" class="mailto"> www.anantfresh.com </a>
     </span>
     </li>

     </li>


     </ul>

   </div>
   <div class="clft right">
   <ul class="lgnul">
                    <li>
                    <span class="lbln">Name</span>
                       <asp:TextBox ID="txt_sname" runat="server" placeholder="Name" CssClass="input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" Display="Dynamic" ValidationGroup="comment" ControlToValidate="txt_sname" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    
                    </li>
                    <li>
                    <span class="lbln">Phone</span>
                       <asp:TextBox ID="txt_sphone" runat="server" placeholder="Phone No" CssClass="input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" Display="Dynamic" ValidationGroup="comment" ControlToValidate="txt_sphone" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    
                    </li>
                    <li>
                    <span class="lbln">Email Id</span>
                     <asp:TextBox ID="txt_semail" runat="server" placeholder="Email ID" CssClass="input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" Display="Dynamic" ValidationGroup="comment" ControlToValidate="txt_semail" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    
                    </li>
            
                    <li>
                    <span class="lbln">Comment</span>
                     <asp:TextBox ID="txt_scomment" TextMode="MultiLine" runat="server" placeholder="Comment" CssClass="input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" Display="Dynamic" ValidationGroup="comment" ControlToValidate="txt_scomment" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    
                    </li>
                    <li>

                    <asp:Button ID="btn_sConatctus" runat="server" Text="Send Details" 
                            CssClass="_button1 border-radius backgroundbg" ValidationGroup="comment" 
                            onclick="btn_sConatctus_Click" /> <br />
                            <asp:Label ID="lbl_contact" runat="server"></asp:Label>

                    </li>
                    </ul>
   </div>
   
   </div>
</div>
  <div id="Enquiry">
    <h2 class="left">Bulk Enquiries about Products</h2><span style="color:#f00; font-size:11px; float:right;">* All Fields are mandatory</span>
   <div class="about clearfix border-box">
   <div class="clft left">
       <asp:Image ID="Image2" ImageUrl="../images/enquiry_image.jpg" AlternateText="AnantFresh Enquiry" runat="server" />
   </div>
   <div class="clft right">
   <ul class="lgnul">
                    <li>
                    <span class="lbln">Name</span>
                    <asp:TextBox ID="txt_eName" runat="server" placeholder="Name" CssClass="input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="enquiry" ControlToValidate="txt_eName" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                    <span class="lbln">Phone</span>
                    <asp:TextBox ID="txt_eMobile" runat="server" placeholder="Phone No" CssClass="input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ValidationGroup="enquiry" ControlToValidate="txt_eMobile" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                    <span class="lbln">Email Id</span>
                    <asp:TextBox ID="txt_eEmail" runat="server" placeholder="Email Id" CssClass="input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" ValidationGroup="enquiry" ControlToValidate="txt_eEmail" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                   
                    </li>
                    <li>
                    <span class="lbln">Product Name</span>
                  <asp:DropDownList ID="ddl_eProductName" CssClass="inputddn" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" ValidationGroup="enquiry" InitialValue="0" ControlToValidate="ddl_eProductName" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                    <span class="lbln">Comment</span>
                                        <asp:TextBox ID="txt_eComment" TextMode="MultiLine" CssClass="textarea" runat="server" placeholder="Comment" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" ValidationGroup="enquiry" ControlToValidate="txt_eComment" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                       <asp:Button ID="btn_eSendEnquiry" runat="server" Text="Send Enquiry" 
                            CssClass="_button1 backgroundbg border-radius" ValidationGroup="enquiry" OnClick="btn_eSendEnquiry_Click" /><br />
                            <asp:Label ID="lbl_eMsg" runat="server"></asp:Label>
          

                    </li>
                    </ul>
   </div>
   
   </div>
  </div>
  <div id="Feedback">
   <h2 class="left">Feedback</h2><span style="color:#f00; font-size:11px; float:right;">* All Fields are mandatory</span>
   <div class="about clearfix border-box">
   <div class="clft left">
       <asp:Image ID="Image1" ImageUrl="../images/feedback_image.jpg"  AlternateText="AnantFresh Feedback" style="margin-top:50px;" runat="server" />
   </div>
   <div class="clft right">
   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
      
                    
                    <ul class="lgnul">
                    <li>
                    <span class="lbln">Name</span>
                    <asp:TextBox ID="txt_name" runat="server" placeholder="Name" CssClass="input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="feedback" ControlToValidate="txt_name" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                    <span  class="lbln">Phone</span>
                     <asp:TextBox ID="txt_mobile" runat="server" placeholder="Phone" CssClass="input"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="feedback" CssClass="error1" ControlToValidate="txt_mobile" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>

                    </li>
                    <li>
                    <span  class="lbln">Email Id</span>
                  <asp:TextBox ID="txt_eamil" runat="server" placeholder="Email" CssClass="input"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" ValidationGroup="feedback" ControlToValidate="txt_eamil" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>

                    </li>
                    <li>
                    <span  class="lbln">Product Category</span>
                     <asp:DropDownList ID="ddl_productname" onselectedindexchanged="ddl_productname_SelectedIndexChanged" AutoPostBack="true" CssClass="inputddn" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" Display="Dynamic" ValidationGroup="feedback" InitialValue="Select" ControlToValidate="ddl_productname" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </li>

                     <li>
                     <span  class="lbln">Product Sub Category</span>
                     <asp:DropDownList ID="ddl_subcatl" CssClass="inputddn" AutoPostBack="true" 
                             runat="server" onselectedindexchanged="ddl_subcatl_SelectedIndexChanged" 
                             >
                        <asp:ListItem>Select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" Display="Dynamic" ValidationGroup="feedback" InitialValue="Select" ControlToValidate="ddl_subcatl" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </li>


                     <li>
                     <span  class="lbln">Product Name</span>
                     <asp:DropDownList ID="ddl_pname" CssClass="inputddn" runat="server" 
                             >
                        <asp:ListItem>Select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" Display="Dynamic" ValidationGroup="feedback" InitialValue="Select" ControlToValidate="ddl_pname" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </li>




                    <li>
                    <span class="lbln">Product Quality</span>
                  
                        <asp:DropDownList ID="ddl_productQuility" CssClass="inputddn" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>Very Good</asp:ListItem>
                        <asp:ListItem>Good</asp:ListItem>
                        <asp:ListItem>Bad</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" ValidationGroup="feedback" InitialValue="Select" ControlToValidate="ddl_productQuility" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                    <span class="lbln">Comment</span>
                     <asp:TextBox ID="txt_comment" TextMode="MultiLine" CssClass="input" runat="server" placeholder="Comment" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" ValidationGroup="feedback" ControlToValidate="txt_comment" CssClass="error1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                   
                    </li>
                    <li>
                  
                      <asp:Button ID="bt_Feedback" runat="server" Text="Send Feedback" 
                           CssClass="_button1 border-radius backgroundbg" ValidationGroup="feedback" 
                            onclick="bt_Feedback_Click" /><br />
                            <asp:Label ID="lbl_feedback" runat="server"></asp:Label>
                    </li>
                    </ul>
  
                </ContentTemplate>
                </asp:UpdatePanel>
   </div>
   </div>
  </div>
</div>

</div>































</asp:Content>


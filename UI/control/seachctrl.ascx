﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="seachctrl.ascx.cs" Inherits="control_seachctrl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc3" %>
<%@ Register Assembly="msgBox" Namespace="BunnyBear" TagPrefix="cc2" %>

<script type="text/javascript">
    $(document).ready(function () {

        $("#<%=txtsrch.ClientID %>").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<%=ResolveUrl("~/Service.asmx/GetProduct") %>',
                    data: "{ 'prefix': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('-')[0],
                                val: item.split('-')[1]
                            }
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (e, i) {
                $("#<%=hfCustomerId.ClientID %>").val(i.item.val);
            },
            minLength: 1
        });

    });
</script>


<asp:Panel ID="Panel1" runat="server" DefaultButton="lnkgo">

    <div class="_inputsearch">
        <asp:TextBox ID="txtsrch" runat="server" CssClass="srchbox"></asp:TextBox>
        <asp:HiddenField ID="hfCustomerId" runat="server" />


        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
            runat="server" ControlToValidate="txtsrch" CssClass="errorsrch" Display="Dynamic" ErrorMessage="!"
            ValidationGroup="srchingc"></asp:RequiredFieldValidator>

        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
            ControlToValidate="txtsrch" CssClass="error" Display="Dynamic" ErrorMessage="Invalid!"
            SetFocusOnError="True" ValidationExpression="^[^<>%]{0,100}$"
            ValidationGroup="srchingc"></asp:RegularExpressionValidator>
    </div>
    <cc2:msgBox ID="msg" runat="server" />
    <div class="backgroundbg _btnsearch">
        <asp:LinkButton ID="lnkgo" OnClick="lnkgo_Click" ValidationGroup="srchingc" runat="server">Search</asp:LinkButton>
    </div>

    <asp:HyperLink ID="asrch" runat="server" Text="Advance Search" NavigateUrl="~/search.aspx"
        Visible="False" />

</asp:Panel>
<cc3:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
    TargetControlID="txtsrch" WatermarkText="Search by Product Code/Product Name">
</cc3:TextBoxWatermarkExtender>

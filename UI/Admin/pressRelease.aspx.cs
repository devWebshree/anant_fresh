﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using ClassLibrary;
using System.IO;
public partial class Admin_addUser : System.Web.UI.Page
{
    PressRelease blpress = new PressRelease();
    dlPressRelease dlpress = new dlPressRelease();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindlist();
        }
    }

   
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        if (fuImage.HasFile)
        {
            string imagepath = "";
            imagepath = uploadimg(fuImage);
            if (imagepath != "")
            {
                blpress.Discription = "";
                blpress.ImagePath = imagepath;
                blpress.Name = txtname.Text;
                dlpress.insertPressRelease(blpress);
                bindlist();
                txtname.Text = "";
                showmessage("Successfully Created.");
                bindlist();
            }
            else
            {
                showmessage("Images Should be in Jpg,jpeg,png format.");
            }
        }
        else
        {
            showmessage("Please Select Images!");
        }
    }
    public string uploadimg(FileUpload fl)
    {
        string Type = Path.GetExtension(fl.PostedFile.FileName);
        if (Type.ToLower() == ".jpg" | Type.ToLower() == ".jpeg" | Type.ToLower() == ".png")
        {
            string pt = null;
            string cpath = null;
            Random r = new Random();
            string RandNo = Convert.ToString(r.Next(1000, 1000000));
            pt = Server.MapPath("~/ProductImage/PressRelease/");
            string filename = null;
            filename = Path.GetFileName(fl.PostedFile.FileName);
            string dtime;
            DateTime dt = DateTime.Now;
            dtime = dt.ToString("ddMMyyyyhhmmssffftt");
            cpath = pt + RandNo + dtime + Type;
            fl.PostedFile.SaveAs(cpath);
            return RandNo + dtime + Type;
        }
        else
        {
            return "";
        }
    }
    public void bindlist()
    {
        try
        {
            blpress.SearchText = txtsearch.Text;
            DataSet ds = dlpress.getPressRelease(blpress);
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdPress.DataSource = ds;
                grdPress.DataBind();
            }
            else
            {
                grdPress.DataSource = null;
                grdPress.DataBind();
            }

        }
        catch (Exception ex)
        {
            
        }
    }

    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel2, UpdatePanel2.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    protected void OnClick_btnDelete(object sender, EventArgs e)
    {
        var btnDelete = (Button)sender;
        var rptChild = btnDelete.NamingContainer;//Child Repeater
        HiddenField id = (HiddenField)rptChild.FindControl("fhId");
        blpress.Id =Convert.ToInt32(id.Value);
        dlpress.deletePressRelease(blpress);
        bindlist();
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bindlist();
    }
}
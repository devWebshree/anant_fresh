﻿<%@ Page Title="UserList" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true" EnableEventValidation = "false" CodeFile="userlist.aspx.cs" Inherits="admin_userlist" %>

<%@ Register src="controls/userlist.ascx" tagname="userlist" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {
       
        $("#custActive").addClass('active');

    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:userlist ID="userlist1" runat="server" />
</asp:Content>


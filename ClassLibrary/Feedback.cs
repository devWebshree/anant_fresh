﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary
{
   public class Feedback
    {
        public string EmailID { get; set; }
        public string Mystring { get; set; }
        public string Name { get; set; }
        public string UserID { get; set; }
        public string OrderNo { get; set; }
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Password { get; set; }
        public string MobileNo { get; set; }
        public string Comment { get; set; }
        public string ProductName { get; set; }
        public string ProductQuality { get; set; }
        public string Details { get; set; }
        public string ProductSubName { get; set; } 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class control_MenuMyAccount : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string currentpath = Request.RawUrl;
        if (currentpath.ToLower().Contains("my-account.aspx"))
        {
            h1.Attributes.Add("class", "active");
        }
        else if (currentpath.ToLower().Contains("reg-details.aspx"))
        {
            h2.Attributes.Add("class", "active");
        }
        else if (currentpath.ToLower().Contains("/changepassword"))
        {
            h3.Attributes.Add("class", "active");
        }
    }
}
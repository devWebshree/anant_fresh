﻿<%@ Page Title="Terms And Conditions" Language="C#" MasterPageFile="~/User.master"
    AutoEventWireup="true" CodeFile="termsAndConditions.aspx.cs" Inherits="termsAndConditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <h3 class="title1 pd">
           Terms & Conditions: AnantFresh.com</h3>
        <p class="m10">
          AnantFresh, provides all information on this site (together with all content and the underlying source 

HTML files that implement the hypertext features, collectively this "Site") exclusively under the 

following Terms and Conditions and all applicable laws:
        </p>
        <h4 class="title2 pd">
            Ownership Restriction Use
        </h4>
        <p class="m5">
            All right, title and interest including all copyrights, trademarks and other intellectual
            property rights in this Site belongs to Anantfresh.com. In addition, the names,
            images, pictures, logos and icons identifying Anantfresh.com products and services
            are proprietary marks of us and/or its subsidiaries or affiliates. Except as expressly
            provided below, nothing contained herein shall be construed as conferring any license
            or right, by implication or otherwise, under copyright, trademark or other intellectual
            property rights.
        </p>
        <h4 class="title2 pd">
            Your Account and Registration Obligations
        </h4>
        <p class="m5">
            If you use the Website, You shall be responsible for maintaining the confidentiality
            of your Display Name and Password and you shall be responsible for all activities
            that occur under your Display Name and Password. If You provide any information
            that is untrue, inaccurate, not current or incomplete or We have reasonable grounds
            to suspect that such information is untrue, inaccurate, not current or incomplete,
            or not in accordance with this Terms of Use, We shall have the right to indefinitely
            suspend or terminate or block access of your membership on the Website and refuse
            to provide You with access to the Website.
        </p>
        <h4 class="title2 pd">
            Links to Third Party Sites
        </h4>
        <p class="m5">
            Anantfresh may provide links to third party web sites from time to time for your
            convenience. Anantfresh does not assume any responsibility for the content, technology
            implementation, privacy practices of third party sites and all use is at your own
            risk. You should review the privacy policy and terms of use for each third party
            site and confirm they are acceptable prior to registration on or use of the site.
            Links to third party sites do not imply endorsement of the sites by Anantfresh in
            any way.
        </p>
        <h4 class="title2 pd">
            Disclaimers
        </h4>
        <p class="m5">
            Anantfresh disclaims all responsibility for any loss, injury, claim, liability,
            or damage of any kind resulting from, arising out of, or in any way related to <br />
            (a) Any errors in or omissions from this Site, including but not limited to technical
            inaccuracies and typographical errors, <br />(b) Any third party websites or content therein
            directly or indirectly accessed through links in this Site, including but not limited
            to any errors in or omissions there from <br /> (c) The unavailability of this Site
        </p>
        <h4 class="title2 pd">
            Limitation Of Liability
        </h4>
        <p class="m5">
            A covered party shall not be liable for any direct, indirect, incidental, special
            or consequential damages of any kind of whatsoever. In other way Limitation of liability
            generally refers to a knowing all of risk. Liability is generally a term that refers
            to a debt or obligation.</p>
    </div>
</asp:Content>

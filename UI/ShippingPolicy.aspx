﻿<%@ Page Title="Shipping Policy" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="ShippingPolicy.aspx.cs" Inherits="RefundPolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="row">
<h4 class="title1 pd">
Shipping Policy
</h4>
<p>
At Anant Fresh, we are very thankful to you and appreciate your purchase with us. Please read the policies, conditions and process time to time as we will update you for important information and guidelines. We make every effort to service the orders placed with us as per specifications and timelines mentioned against each product. 

Anant Fresh guarantees full satisfaction for its products.  However,  in  case  of  any  inconsistency  or  otherwise,  the company  maintains  a  responsive  refund  mechanism.  We provide refund for any purchase within 24 hours and that too without any deductions! Free door step pick up is also arranged for the same.
</p>

<ol class="ol">

<li>At Anantfresh.com, we provide standard shipping to all our customers that includes inspection,
fast & on-time delivery, quality packaging, insurance for the package, and timely communication 
at each stage of shipping.</li>
<li>Free shipping charge on purchase of Rs.1004/- or above. Rs.50 shipping charge on less than 1004 purchasing..</li>
<li>Well at Anantfresh.com, we address that worry by promising you "No Anxiety Shipping". This
means that once you have bought a product at our site, we take care of everything so you don't 
have to be anxious about anything.</li>
<li>Same day delivery if customer makes an order before 5:30pm otherwise next day delivery i.e.
- when the order is placed, processed, shipped, and delivered. And of course, if any questions 
along the way just contact our customer support and we will resolve your problem right there.</li>

</ol>

<div class="clear"></div>
<p>
Please Note: The delivery timeframe mentioned on product details as is estimated. Actual delivery time
depends upon availability of the product, location where the order needs to be delivered and courier 
issues and other circumstances that might affect delivery. You can rest assured that you are completely 
protected as a shopper at Anantfresh.com. Once the order is shipped, No cancellation only exchange 
with 50 rupees shipping charge.
</p>
</div>
</asp:Content>


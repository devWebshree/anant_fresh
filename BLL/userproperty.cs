﻿using System;
using System.Collections.Generic;
using DAL;
using System.Text;
using System.Data;

namespace BAL
{
 public   class userproperty
    {
     ManageData md = new ManageData();
     public string U_uname, U_password, U_name, U_email, U_ph, U_altph, U_addr, U_city, U_state, U_country, U_zip, U_UserType;
     public string uname
     {
         get
         {
             return U_uname;
         }
         set
         {
             U_uname = value;
         }
     }


     public string password
     {
         get
         {
             return U_password;
         }
         set
         {
             U_password = value;
         }
     }
     public string name
     {
         get
         {
             return U_name;
         }
         set
         {
             U_name = value;
         }
     }
     public string email
     {
         get
         {
             return U_email;
         }
         set
         {
             U_email = value;
         }
     }
     public string ph
     {
         get
         {
             return U_ph;
         }
         set
         {
             U_ph = value;
         }
     }
     public string altph
     {
         get
         {
             return U_altph;
         }
         set
         {
             U_altph = value;
         }
     }
     public string addr
     {
         get
         {
             return U_addr;
         }
         set
         {
             U_addr = value;
         }
     }
     public string city
     {
         get
         {
             return U_city;
         }
         set
         {
             U_city = value;
         }
     }
     public string state
     {
         get
         {
             return U_state;
         }
         set
         {
             U_state = value;
         }
     }
     public string country
     {
         get
         {
             return U_country;
         }
         set
         {
             U_country = value;
         }
     }
     public string zip
     {
         get
         {
             return U_zip;
         }
         set
         {
             U_zip = value;
         }
     }


     public DataSet getAccountInfo(string ID)
     {
         return md.getAccountInfo(ID);
     }


    }
}

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="additem.ascx.cs" Inherits="admin_controls_additem" %>
<%@ Register TagPrefix="cc" Namespace="Winthusiasm.HtmlEditor" Assembly="Winthusiasm.HtmlEditor" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
</asp:UpdatePanel>


 <div class="clear"></div>
 <div class="row">
 <div class="column border-box">
 <div class="center">
 <ul class="productul">
 <li>
 <span class="lbl">Category</span>
 <span class="inputbox"> <asp:DropDownList ID="ddlCategoryName" runat="server" 
                AutoPostBack="True" 
                onselectedindexchanged="ddlCategoryName_SelectedIndexChanged" 
                CssClass="inputddn" >
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="ddlCategoryName" Display="Dynamic" CssClass="error" InitialValue="Select"
                ErrorMessage="Select Category" ValidationGroup="a1"></asp:RequiredFieldValidator></span>
 </li>






  <li>
 <span class="lbl">Sub Category</span>
 <span class="inputbox"> 

     <asp:DropDownList ID="ddl_subCat" runat="server" AutoPostBack="True" CssClass="inputddn" >
     <asp:ListItem>Select</asp:ListItem>
     </asp:DropDownList>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" CssClass="error" runat="server" InitialValue="Select" ControlToValidate="ddl_subCat" Display="Dynamic" 
    ErrorMessage="Select Sub Category" ValidationGroup="a1"></asp:RequiredFieldValidator>

</span>
 </li>

 <li>
  <span class="lbl">Code</span>
  <span class="inputbox"><asp:TextBox ID="txtCode" runat="server" CssClass="input"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                ControlToValidate="txtCode" Display="Dynamic" ErrorMessage="Enter a Code" CssClass="error"
                ValidationGroup="a1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" CssClass="error" 
                ControlToValidate="txtCode" Display="Dynamic" 
                ErrorMessage="Please delete any of these characters(,&lt;&gt;#%;'`)." 
                ValidationExpression="^[^&lt;&gt;{}?;*~`!#$%^=+|\\:'\;]{0,250}$" 
                ValidationGroup="a1"></asp:RegularExpressionValidator></span>
 </li>

 <li>
  <span class="lbl">Name</span>
  <span class="inputbox"><asp:TextBox ID="txtName" runat="server"  CssClass="input"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="error"
                ControlToValidate="txtName" Display="Dynamic" 
                ErrorMessage="Please delete any of these characters(,&lt;&gt;#%;'`)." 
                ValidationExpression="^[^&lt;&gt;{}?&amp;*~`!#$%^=|\\:'\;]{0,250}$" 
                ValidationGroup="a1"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="error"
                ControlToValidate="txtName" Display="Dynamic" ErrorMessage="Enter Name" 
                ValidationGroup="a1"></asp:RequiredFieldValidator></span>
 </li>
<%--
<li>
  <span class="lbl">Short Description</span><span class="inputbox"> <asp:TextBox ID="txtshortDes" CssClass="textarea" runat="server" TextMode="MultiLine"></asp:TextBox></span>
 </li>--%>



 <li>
<span class="lbl">Buying Price &#8377;</span>
<span class="inputbox">
<asp:TextBox ID="txtBuyingPrice"  Text="0" runat="server" CssClass="input" ></asp:TextBox>


<asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                runat="server" ControlToValidate="txtBuyingPrice" CssClass="error" Display="Dynamic"
                ErrorMessage="Enter Price!" SetFocusOnError="True" ValidationGroup="a1"></asp:RequiredFieldValidator>

<asp:RangeValidator ID="Rangevalidator6" runat="server" ControlToValidate="txtBuyingPrice" CssClass="error"
                    Display="Dynamic" ErrorMessage="Invalid Price!" MaximumValue="9999" MinimumValue="0"
                    SetFocusOnError="True" Type="Double" ValidationGroup="a1"></asp:RangeValidator>
</span>
</li>




 <li>
  <span class="lbl">MRP Price &#8377;</span><span class="inputbox"> <asp:TextBox ID="txtPrice" runat="server" CssClass="input" 
                ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                    runat="server" ControlToValidate="txtPrice" CssClass="error" Display="Dynamic"
                    ErrorMessage="Enter Price !!" SetFocusOnError="True" ValidationGroup="a1"></asp:RequiredFieldValidator>

                    <asp:rangevalidator id="RangeValidator2" runat="server" controltovalidate="txtPrice" cssclass="error"
                        display="Dynamic" errormessage="Invalid price!" maximumvalue="9999" minimumvalue="1"
                        setfocusonerror="True" type="Double" validationgroup="a1"></asp:rangevalidator></span>
 </li>



 <li>
  <span class="lbl">Retail Price &#8377;</span>
  <span class="inputbox">
  <asp:TextBox ID="txtOfferPrice" runat="server" CssClass="input" ></asp:TextBox>
  <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtOfferPrice" CssClass="error"
                        Display="Dynamic" ErrorMessage="Invalid Price!" MaximumValue="9999" MinimumValue="0"
                        SetFocusOnError="True" Type="Double" ValidationGroup="a1"></asp:RangeValidator></span>
 </li>

<li>
<span class="lbl">Wholesale Price &#8377;</span>
<span class="inputbox">
<asp:TextBox ID="txtWholesale"  Text="0" runat="server" CssClass="input" ></asp:TextBox>


<asp:RequiredFieldValidator ID="RequiredFieldValidator9"
            runat="server" ControlToValidate="txtWholesale" CssClass="error" Display="Dynamic"
            ErrorMessage="Enter Price!" SetFocusOnError="True" ValidationGroup="a1"></asp:RequiredFieldValidator>

<asp:RangeValidator ID="Rangevalidator7" runat="server" ControlToValidate="txtWholesale" CssClass="error"
                Display="Dynamic" ErrorMessage="Invalid Price!" MaximumValue="9999" MinimumValue="0"
                SetFocusOnError="True" Type="Double" ValidationGroup="a1"></asp:RangeValidator>
</span>
</li>




 <li>
  <span class="lbl">Product Detail</span>
  <span class="inputbox">
  <cc:HtmlEditor 
                ID="txtDetail" runat="server" Height="400px" 
                Width="400px" 
                Toolbars="Select#Format,Bold,Italic,Underline:Left,Center,Right,Justify" />
  </span>
 </li>


  <li>
  <span class="lbl">Available Quantity In Stock</span>
  <span class="inputbox">
  <asp:TextBox ID="txtavailable" Text="0"  ReadOnly="true" AutoCompleteType="None" runat="server" CssClass="input" ></asp:TextBox>
<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                    runat="server" ControlToValidate="txtavailable" CssClass="error" Display="Dynamic"
                    ErrorMessage="Enter Quantity!" SetFocusOnError="True" ValidationGroup="a1"></asp:RequiredFieldValidator>--%>



  <%--<asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="txtavailable" CssClass="error"
                        Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999" MinimumValue="0"
                        SetFocusOnError="True" Type="Integer" ValidationGroup="a1"></asp:RangeValidator>--%>
  </span>
 </li>


  <li>
  <span class="lbl">Stock Alert On Quantity</span>
  <span class="inputbox">
  <asp:TextBox ID="txtAlertOnStock" Text="0" runat="server" CssClass="input" ></asp:TextBox>


    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                    runat="server" ControlToValidate="txtAlertOnStock" CssClass="error" Display="Dynamic"
                    ErrorMessage="Enter Quantity!" SetFocusOnError="True" ValidationGroup="a1"></asp:RequiredFieldValidator>


  <asp:RangeValidator ID="Rangevalidator4" runat="server" ControlToValidate="txtAlertOnStock" CssClass="error"
                        Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999" MinimumValue="0"
                        SetFocusOnError="True" Type="Integer" ValidationGroup="a1"></asp:RangeValidator>
  </span>
 </li>




  <li>
  <span class="lbl">Add Quantity In Stock</span>
  <span class="inputbox">
  <asp:TextBox ID="txtAddQuantity"  Text="0" runat="server" CssClass="input" ></asp:TextBox>


    <asp:RequiredFieldValidator ID="RequiredFieldValidator8"
                    runat="server" ControlToValidate="txtAddQuantity" CssClass="error" Display="Dynamic"
                    ErrorMessage="Enter Quantity!" SetFocusOnError="True" ValidationGroup="a1"></asp:RequiredFieldValidator>


  <asp:RangeValidator ID="Rangevalidator5" runat="server" ControlToValidate="txtAddQuantity" CssClass="error"
                        Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999" MinimumValue="0"
                        SetFocusOnError="True" Type="Integer" ValidationGroup="a1"></asp:RangeValidator>
  </span>
 </li>







 <asp:Panel runat="server" ID="pnl_visi">
 <li>
  <span class="lbl">Is Visible</span><span class="inputbox">
      <asp:CheckBox ID="chkIsvisible" runat="server" /></span>
 </li>
 </asp:Panel>
 <li>
 <span class="lbl">Upload Product Image</span><span class="inputbox">
 <asp:HiddenField ID="hf_path" runat="server" />
 <asp:FileUpload ID="fuplImage" runat="server" CssClass="cursor"/></span>
 </li>
 <li class="ctr">
  <asp:Button ID="btnAdd" runat="server" Text="Add Product" onclick="btnAdd_Click" ValidationGroup="a1" CssClass="submit"  />
  <asp:Button ID="btn_update" runat="server" Text="Update Product" 
         ValidationGroup="a1" CssClass="submit" onclick="btn_update_Click"  />
 </li>
 </ul>
 </div>
 </div>
 
 </div>

    

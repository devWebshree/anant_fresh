﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="Feedback.aspx.cs" Inherits="admin_Feedback" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
<script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#feedActive").addClass('active');

    });
</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="row">
<div class="ProductTitle"><h2>FEEDBACK</h2></div>
</div>
<div class="clear"></div>
<div class="row m10">
<div class="column">
<div class="scroll">


<asp:GridView ID="feedbackgrd" runat="server" CssClass="table"  AutoGenerateColumns="False">

                <Columns>
                    <asp:TemplateField HeaderText="Sr.No."  HeaderStyle-CssClass="tblhd">
                        <ItemTemplate>
                            <asp:Label ID="lblSN" ForeColor="black" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                   <asp:TemplateField  HeaderText="Name"  HeaderStyle-CssClass="tblhd">
                        <ItemTemplate>
                            <asp:Label ID="lbl_name" ForeColor="black"  runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                            <asp:HiddenField ID="feedId" Value='<%# Eval("FeedBackID") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Email ID"  HeaderStyle-CssClass="tblhd">
                        <ItemTemplate>
                            <asp:Label ID="lbl_email" ForeColor="black"  runat="server" Text='<%# Eval("EmailID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Phone No"  HeaderStyle-CssClass="tblhd">
                        <ItemTemplate>
                            <asp:Label ID="lbl_mobile" ForeColor="Black" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Product Name"  HeaderStyle-CssClass="tblhd">
                        <ItemTemplate>
                            <asp:Label ID="lbl_pname" ForeColor="black"  runat="server" Text='<%# Eval("ProductName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Product Sub Category"  HeaderStyle-CssClass="tblhd">
                        <ItemTemplate>
                            <asp:Label ID="lbl_pname" ForeColor="black"  runat="server" Text='<%# Eval("ProductSubName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Product Quality"  HeaderStyle-CssClass="tblhd">
                        <ItemTemplate>
                            <asp:Label ID="lbl_Quality" ForeColor="black"  runat="server" Text='<%# Eval("ProductQuality") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                   <asp:TemplateField HeaderText="Comment"  HeaderStyle-CssClass="tblhd">
                        <ItemTemplate>
                            <asp:Label ID="lbl_comment" ForeColor="black"  runat="server" Text='<%# Eval("Comment") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Date"  HeaderStyle-CssClass="tblhd">
                        <ItemTemplate>
                            <asp:Label ID="lbl_feedtime" ForeColor="black"  runat="server" Text='<%# Eval("FeedDate","{0:dd-MMM-yy hh:mm tt}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
               
               
               </asp:GridView>
             </div>
</div>
</div>
</asp:Content>


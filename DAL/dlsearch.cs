﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data.Common;
using ClassLibrary;

namespace DAL
{
   public class dlsearch
    {

       Database db = GetDatabase(AppDatabaseName);
       private static string AppDatabaseName;
       private static Database GetDatabase(string dbName)
       {
           try
           {
               Database db = null;
               if (dbName == null)
               {
                   return db = DatabaseFactory.CreateDatabase();
               }
               else
               {
                   return db = DatabaseFactory.CreateDatabase(dbName);
               }
               return db;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       static dlsearch()
       {
           AppDatabaseName = ConfigurationSettings.AppSettings["AppDatabaseName"];
       }

       public DataSet itemSearchList(Search srch) 
        {
            DbCommand cmd = db.GetStoredProcCommand("spSearch");
            db.AddInParameter(cmd, "@SearchBy", DbType.String, srch.SearchBy);
            db.AddInParameter(cmd, "@SubCatID", DbType.String, srch.SubCatID);
            db.AddInParameter(cmd, "@CatID", DbType.String, srch.CatID);
            db.AddInParameter(cmd, "@ProductID", DbType.String, srch.ProductID);
            db.AddInParameter(cmd, "@Visibilty", DbType.String, srch.Visibilty);
            db.AddInParameter(cmd, "@Orderby", DbType.String, srch.Orderby);
            db.AddInParameter(cmd, "@Action", DbType.String, "Search");
           
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;

        }

       public DataSet itemStockSearchList(Search srch) //For out of stock alert 
       {
           DbCommand cmd = db.GetStoredProcCommand("spSearch");
           db.AddInParameter(cmd, "@SearchBy", DbType.String, srch.SearchBy);
           db.AddInParameter(cmd, "@SubCatID", DbType.String, srch.SubCatID);
           db.AddInParameter(cmd, "@CatID", DbType.String, srch.CatID);
           db.AddInParameter(cmd, "@ProductID", DbType.String, srch.ProductID);
           db.AddInParameter(cmd, "@Visibilty", DbType.String, srch.Visibilty);
           db.AddInParameter(cmd, "@Action", DbType.String, "getItemStockAlertList");

           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;

       }


       public DataSet itemStockInList(Search srch) //For stock In Product list getItemStockInList
       {
           DbCommand cmd = db.GetStoredProcCommand("spSearch");
           db.AddInParameter(cmd, "@SearchBy", DbType.String, srch.SearchBy);
           db.AddInParameter(cmd, "@SubCatID", DbType.String, srch.SubCatID);
           db.AddInParameter(cmd, "@CatID", DbType.String, srch.CatID);
           db.AddInParameter(cmd, "@ProductID", DbType.String, srch.ProductID);
           db.AddInParameter(cmd, "@Visibilty", DbType.String, srch.Visibilty);
           db.AddInParameter(cmd, "@Action", DbType.String, "getItemStockInList");

           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;

       }

       public DataSet itemListForAdmin(Search srch)
       {
           DbCommand cmd = db.GetStoredProcCommand("spSearch");
           db.AddInParameter(cmd, "@SearchBy", DbType.String, srch.SearchBy);
           db.AddInParameter(cmd, "@SubCatID", DbType.String, srch.SubCatID);
           db.AddInParameter(cmd, "@CatID", DbType.String, srch.CatID);
           db.AddInParameter(cmd, "@ProductID", DbType.String, srch.ProductID);
           db.AddInParameter(cmd, "@Visibilty", DbType.String, srch.Visibilty);
           db.AddInParameter(cmd, "@Action", DbType.String, "adminItemTable");

           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;

       }

       public DataSet getAllCartItem()
       {

           DbCommand cmd = db.GetSqlStringCommand("SELECT * FROM PurChaseTable WHERE flagstatus=0");
          // db.AddInParameter(cmd, "@buyer", DbType.String, buyer);
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;


       }

       public void delItems(string IDs)  
       {
           DbCommand cmd = db.GetStoredProcCommand("spSearch");
           db.AddInParameter(cmd, "@IDs", DbType.String, IDs);
           db.AddInParameter(cmd, "@Action", DbType.String, "Delete");
           db.ExecuteNonQuery(cmd);
       }
       public void setIsVisibleItems(string IDs, int IsVisible)
       {
           DbCommand cmd = db.GetStoredProcCommand("spSearch");
           db.AddInParameter(cmd, "@IDs", DbType.String, IDs);
           db.AddInParameter(cmd, "@IsVisible", DbType.String, IsVisible);
           db.AddInParameter(cmd, "@Action", DbType.String, "IsVisible");
           db.ExecuteNonQuery(cmd);
       }

       public void updatePrice(string IDs, Double RealPrice, Double OfferPrice, Double WholeSalePrice, Double BuyingPrice)
       {
           DbCommand cmd = db.GetStoredProcCommand("spSearch");
           db.AddInParameter(cmd, "@IDs", DbType.String, IDs);
           db.AddInParameter(cmd, "@RealPrice", DbType.Double, RealPrice);
           db.AddInParameter(cmd, "@OfferPrice", DbType.Double, OfferPrice);

           db.AddInParameter(cmd, "@BuyingPrice", DbType.Double, BuyingPrice);
           db.AddInParameter(cmd, "@WholeSalePrice", DbType.Double, WholeSalePrice);

           db.AddInParameter(cmd, "@Action", DbType.String, "UpdatePrice");
           db.ExecuteNonQuery(cmd);
       }
    }
}

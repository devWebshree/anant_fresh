﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubCategory.ascx.cs" Inherits="control_SubCategory" %>

<%@ Register src="Category.ascx" tagname="Category" tagprefix="uc1" %>

<div class="Categ">
<ul>
 <li><a href="/home" class="cat">Categories</a>
 <ul class="mainul">
             <asp:Repeater ID="rpt_cat" runat="server">
                <ItemTemplate>
                    <li class="border-box"><a href="/product/<%# all.RemoveSpace(Eval("Category").ToString()) %>/all" data-name='<%# all.RemoveSpace(Eval("Category").ToString()) %>' data-id='<%# Eval("catid") %>'
                        rel="innermenu">
                        <%# Eval("Category") %></a>
            
                        </li>
                </ItemTemplate>
            </asp:Repeater>
 </ul>
 </li>
</ul>
</div>
<div class="clear">
   
</div>
  <div class="subcat">
  <asp:HyperLink ID="hf_cat" runat="server" CssClass="_subcategoryName"></asp:HyperLink>
  <img src="/images/dropdown.png" />
  </div>
  <ul class="_categoryul">

<asp:Repeater ID="rpt_subcat" runat="server">
<ItemTemplate>
<li class="border-box"><a href="<%# "/product/"+all.RemoveSpace(Page.RouteData.Values["catname"].ToString())+"/"+all.RemoveSpace(Eval("SubCategory").ToString()) %>" ><%# Eval("SubCategory") %></a></li>
</ItemTemplate>
</asp:Repeater>

 </ul>
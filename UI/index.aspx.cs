﻿using System;
using System.Collections.Generic;
using DAL;
using BAL;
using System.Data;
using ClassLibrary;
using System.Web.Services;
using System.Web.Script.Services;
using WebshreeCommonLibrary;

public partial class index : System.Web.UI.Page
{
    public Utility all = new Utility();
    modifyData ad = new modifyData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            insertVendorView();
        }
    }



   



    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<SubCategoryCS> getSubCategory(string catId)
    {
        dlSubCategory objSc = new dlSubCategory();
        DataSet ds = objSc.getSubCategoryNameList(catId);


        List<SubCategoryCS> lsc = new List<SubCategoryCS>();
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                SubCategoryCS sc1 = new SubCategoryCS();
                sc1.SubCatID = ds.Tables[0].Rows[i]["subcatid"].ToString();
                sc1.SubCategory = ds.Tables[0].Rows[i]["SubCategory"].ToString();
                lsc.Add(sc1);
            }
        }

        return lsc;
    }

    public void insertVendorView()
    {
        try
        {
            dalResult dlViews = new dalResult();
            TransportationPacket Packet = new TransportationPacket();
            Packet = dlViews.insertViewCounter(Packet);
        }
        catch (Exception ex)
        { }
    }
}
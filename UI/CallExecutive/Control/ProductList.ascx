﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductList.ascx.cs"
    Inherits="control_searchbykeyword" %>
<%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>
<%@ Register Assembly="ASPnetPagerV2netfx2_0" Namespace="CutePager" TagPrefix="cc2" %>
<%@ Register Assembly="msgBox" Namespace="BunnyBear" TagPrefix="cc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc4" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>
<%@ Reference VirtualPath="~/control/chkoutctrl.ascx" %>
    <script type="text/javascript">

        function checkList() {

            $(document).ready(function () {

               
                $('.cbAll input').addClass('shiftCheck');
                $('.cbSelect input').addClass('cbSelects');

                $(".shiftCheck").change(function () {
                    if ($(this).is(':checked')) {

                        $('.cbSelects').each(function () {
                            $(this).prop('checked', true);
                        });

                    }
                    else {
                        $('.cbSelects').each(function () {
                            $(this).prop('checked', false);
                        });
                    }
                });
            });

        }

    </script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    </ContentTemplate>
</asp:UpdatePanel>


            <script type="text/javascript">
                Sys.Application.add_load(checkList);
           </script>
<div class="row m10">
    <div class="registerdiv clearfix" style="width: 93%;">



<div class="menu clearfix m10">

<asp:Panel ID="pnlTab" DefaultButton="btn_search" runat="server">
<ul class="parentul">
<li style="width:300px;"><a>
    <asp:TextBox ID="txtsrch" runat="server" placeholder="Search by Product Code/Product Name"  CssClass="inputtxt" ></asp:TextBox>
    
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtsrch" CssClass="error" Display="Dynamic" ErrorMessage="Invalid!" SetFocusOnError="True" ValidationExpression="^[^<>%]{0,100}$" ValidationGroup="srchingc"></asp:RegularExpressionValidator>
</a></li>

<li><a>Category :<asp:DropDownList ID="ddlCategoryName" runat="server" AutoPostBack="True" CssClass="ddl width-1"  onselectedindexchanged="ddlCategoryName_SelectedIndexChanged"></asp:DropDownList></a></li>
<li><a> Subcategory :<asp:DropDownList ID="ddl_subCat" runat="server" AutoPostBack="True" CssClass="ddl width-1" onselectedindexchanged="ddl_subCat_SelectedIndexChanged" > <asp:ListItem>All</asp:ListItem></asp:DropDownList></a></li>
<li><a>Sort By :<asp:DropDownList ID="ddlSortProduct" CssClass="ddl width-1" runat="server" AutoPostBack="true" onselectedindexchanged="ddlSortProduct_SelectedIndexChanged" >               
                               <asp:ListItem Value="3">Newest First</asp:ListItem>             
                                    <asp:ListItem Value="1">Price--Low to High</asp:ListItem>
                             <asp:ListItem Value="2">Price--High to Low</asp:ListItem>
                          </asp:DropDownList>
                          </a>
                         </li>
<li><asp:LinkButton ID="btn_search" runat="server" onclick="lnkgo_Click" ValidationGroup="srchingc" Text="Search" /></li>
</ul>
</asp:Panel>
</div>



<div class="title"></div>
        <div class="directory">
            <span style="float:left"  class="left"><asp:HyperLink ID="hl_cat" runat="server"></asp:HyperLink></span> <span style="float:right" class="right"><asp:Label ID="lblcol2" runat="server"></asp:Label></span> 
</div>

    <div class="products m10">
      
            <asp:DataList ID="DataList2" runat="server"  RepeatDirection="Vertical" 
                BorderWidth="0px" CellPadding="0" Width="100%" OnItemCommand="DataList2_ItemCommand"
                OnItemDataBound="DataList2_ItemDataBound" >

                <HeaderTemplate>
                <table class="table">
                <tr>
                <td style="width:0.7%;">
                  <div class="menu">
                            <ul class="parentul">
                    <li><a>
                                    <asp:CheckBox ID="cb_all" runat="server" CssClass="cbAll" /><span class="arrow"></span></a>
                                    <ul class="childul">
                                       
                                        <li>
                                            <asp:LinkButton ID="lbParentAddToCart" 
                                                OnClientClick="return confirm('Are you sure? You want to Add to cart.')" runat="server"
                                                Text="AddToCart" onclick="lbParentAddToCart_Click" /></li>
                                   
                                   
                                    </ul>
                                </li>
                                </ul>
                                </div>
                </td>
                <td class="grdtd grdtdwidth">Image</td>
                <td class="grdtd grdtdwidth">Name</td>
                <td class="grdtd grdtdwidth">Price</td>
                <td class="grdtd grdtdwidth">Quantity</td>
                <td class="grdtd grdtdwidth">Add To Cart</td>
                </tr></table>
                </HeaderTemplate>
             
                <ItemTemplate>
                 
                   


                   <table class="table" >
                   <tr>
                      <td style="width:3.4%;">
                      
                           <asp:CheckBox ID="cb_select" runat="server" CssClass="cbSelect" />
                      </td>
                   <td class="grdtd gdwd">
                   <asp:Image ID="Image1" Width="100px" Height="100px" runat="server" ImageUrl='<%# "http://www.anantfresh.com//ProductImage/" + Eval("image") %>' />
                   </td>

                   <td class="grdtd gdwd">
                   
                            <div class="ProductName">
                                <h5>

                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("name")%>'></asp:Label>
                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("name") %>' Visible="false"></asp:Label>
                                </h5>
                            </div>
                   
                   </td>


                   <td class="grdtd gdwd">
                   
                          <div class="Price clearfix">

                                <asp:Panel ID="pnlretailPrice" runat="server">
                                <span>Retail Price</span> <span>&#8377;&nbsp;</span><asp:Label ID="lblPrice" runat="server" CssClass="cross"
                                    Text='<%# Bind("price") %>'></asp:Label>
                                <span>&#8377; </span><asp:Label ID="lblOfferPrice" runat="server" CssClass="new" Text='<%# Bind("offerprice") %>'></asp:Label>

                                </asp:Panel>
                                <asp:Panel ID="pnlWholeSalePrice" runat="server">
                                 <span>WholeSale Price</span> <span>&#8377;&nbsp;</span><asp:Label ID="lblWholeSalePrice" runat="server" CssClass="cross"
                                    Text='<%# Bind("WholeSalePrice") %>'></asp:Label>

                                </asp:Panel>
                            </div>
                   
                   </td>
<td class="grdtd gdwd">
      <div class="_button clearfix" >
      <h4 class="code"><asp:TextBox ID="txtQuantity" runat="server" Width="40">1</asp:TextBox>
      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtQuantity" CssClass="error" Display="Dynamic" ErrorMessage="!" SetFocusOnError="True" ValidationGroup='<%# Eval("id") %>'></asp:RequiredFieldValidator>
      <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtQuantity" CssClass="error" Display="Dynamic" ErrorMessage="!" MaximumValue="9999" MinimumValue="1" SetFocusOnError="True" Type="Integer" ValidationGroup='<%# Eval("id") %>'></asp:RangeValidator>
      </h4>
      </div>
</td>
<td class="grdtd gdwd">
         <div class="addtocart backgroundbg itembttn border-radius" style="width: auto;">
         <asp:LinkButton ID="btnBook" ValidationGroup='<%# Eval("id") %>' runat="server" CommandName="book"><asp:Image ID="Image7" ImageUrl="~/images/cart_btn_icon.png" runat="server" />Add to Cart</asp:LinkButton>
         </div>               
</td>
    <div class="catName" style="display:none"><h5><span class="left">Code Product</span><asp:Label ID="lblCode" runat="server" Text='<%# Eval("code") %>' CssClass="code"></asp:Label></h5></div>                        
    <div class="Price" style="display:none;"><span id="hh" >Available Quantity <asp:Label ID="lbavlQty" runat="server" Text='<%# Eval("qty") %>'></asp:Label></span></div>
    <div class="row"><span class="code_bg"><asp:Label ID="lblid" runat="server" Text='<%# Bind("id") %>' Visible="false"></asp:Label></span></div>

                   </tr>
                   </table>










               
                </ItemTemplate>
            </asp:DataList>
        
    </div>
    <tr><td>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
    <td style="padding-left:10px;"> <asp:Image ID="imglg" runat="server" />
    </td><td class="process"><asp:UpdateProgress ID="UpdateProgress1" runat="server"><ProgressTemplate>
    Processing...<asp:Image ID="Image1" runat="server" ImageUrl="~/images/bx_loader.gif" />
    </ProgressTemplate></asp:UpdateProgress>
    </td><td align="right" style="padding-right:10px;">
       </td></tr></table>
       
       </td>
  </tr>
  <div class="clear"></div>
    <div class="pager m10 clearfix">
        <cc1:CollectionPager ID="CollectionPager1" runat="server" PageSize="16" SliderSize="5"
            QueryStringKey="Page" ResultsFormat="Products {0}-{1} (of {2})" ResultsLocation="None"
            ShowFirstLast="False" ShowLabel="False" BackText="« Back" ControlCssClass=""
            LabelText="Page : -" MaxPages="25" BackNextLinkSeparator="" PageNumbersSeparator=""
            UseSlider="True" BackNextLocation="Split" BackNextDisplay="HyperLinks">
        </cc1:CollectionPager>
    </div>
</div>
</div>
</div>

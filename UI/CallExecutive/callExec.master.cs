﻿using System;
using System.Web;
using System.Web.Security;
using System.Data;
using ClassLibrary;
using BAL;
using DAL;
public partial class CallExecutive_callExec : System.Web.UI.MasterPage
{
    modifyData ad = new modifyData();
    dlSubCategory objscat = new dlSubCategory();
    SubCategoryCS scat = new SubCategoryCS();
    dlsearch objSrch = new dlsearch();
    Search srch = new Search();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                lbl_name.Text = HttpContext.Current.User.Identity.Name;
                
            }
            else
            {
                Response.Redirect("~/login");
            }
        }

        try
        {

            if (Session["ShopUser"] != "0")
            {
                lbl_shopfor.Text = Session["ShopUser"].ToString();
                lbClosed.ToolTip = "Closed Shoping for: " + Session["ShopUser"].ToString();
                lbClosed.Visible = true;
                lblMenuChange.Text = "Product";
            }
            else
            {
                lblMenuChange.Text = "Register";
                lbl_shopfor.Text = "";
            }
            if (Session["Exist"] != null)
            {
                JqueryNotification.NotificationExtensions.ShowWarningNotification(this, Session["Exist"].ToString(), 5000);
                Session["Exist"] = null;

            }

            if (Session["UserType"] != "0")
            {
                string PurchaseType;
                PurchaseType = Session["UserType"].ToString();
                if (PurchaseType == "1")
                    lblPurchaseType.Text = " : <font color='red'>Retail Shopping</font>";
                else
                    lblPurchaseType.Text = " : <font color='red'>Wholesale Shopping</font>";

            }
        }
        catch (Exception ex)
        {
            
        }
    }

    protected void lbClosed_Click(object sender, EventArgs e)
    {
        Session["ShopUser"] = "0";
        Session["SalesName"] = "0";
        Session["UserType"] = "0";
        Session["order"] = "0";
     
        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);


    }
    protected void lb_logout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Buffer = true;
        Response.Expires = -1500;
        Response.AddHeader("pragma", "noCache");
        Response.CacheControl = "no-cache";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Session.Clear();
        Session["URL"] = "0";
        Session["IsUser"] = "0";
        Session["min"] = "0";
        Session["max"] = "0";
        Session["SalesName"] = "0";
        Session["UserType"] = "0";
        FormsAuthentication.SignOut();
        Response.Redirect("/login");

    }
}

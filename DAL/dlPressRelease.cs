﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data.Common;
using ClassLibrary;

namespace DAL
{
    public class dlPressRelease
    {
       Database db = GetDatabase(AppDatabaseName);
       private static string AppDatabaseName;
       private static Database GetDatabase(string dbName)
       {
           try
           {
               Database db = null;
               if (dbName == null)
               {
                   return db = DatabaseFactory.CreateDatabase();
               }
               else
               {
                   return db = DatabaseFactory.CreateDatabase(dbName);
               }
               return db;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       static dlPressRelease()
       {
           AppDatabaseName = ConfigurationSettings.AppSettings["AppDatabaseName"];
       }

       public bool insertPressRelease(PressRelease obj)     
        {
            try
            {
                DbCommand cmd = db.GetStoredProcCommand("spPressRelease");
              
                db.AddInParameter(cmd, "@Name", DbType.String, obj.Name);
                db.AddInParameter(cmd, "@Discription", DbType.String, obj.Discription);
                db.AddInParameter(cmd, "@ImagePath", DbType.String, obj.ImagePath);
                db.AddInParameter(cmd, "@Action", DbType.String, "insert");
                int i = db.ExecuteNonQuery(cmd);

                if (i > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public bool updatePressRelease(PressRelease obj)   
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spPressRelease");
               db.AddInParameter(cmd, "@Id", DbType.String, obj.Id);
               db.AddInParameter(cmd, "@Name", DbType.String, obj.Name);
               db.AddInParameter(cmd, "@Discription", DbType.String, obj.Discription);
               db.AddInParameter(cmd, "@ImagePath", DbType.String, obj.ImagePath);
               db.AddInParameter(cmd, "@Action", DbType.String, "update");
               int i = db.ExecuteNonQuery(cmd);

               if (i > 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }




       public bool deletePressRelease(PressRelease obj)
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spPressRelease");
               db.AddInParameter(cmd, "@Id", DbType.String, obj.Id);
               db.AddInParameter(cmd, "@Action", DbType.String, "delete");
               int i = db.ExecuteNonQuery(cmd);

               if (i > 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }



       public DataSet getPressRelease(PressRelease obj) 
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spPressRelease"); 
               db.AddInParameter(cmd, "@Id", DbType.String, obj.Id);
               db.AddInParameter(cmd, "@SearchText", DbType.String, obj.SearchText);
               db.AddInParameter(cmd, "@Action", DbType.String, "getList");
               return db.ExecuteDataSet(cmd);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

    }
}

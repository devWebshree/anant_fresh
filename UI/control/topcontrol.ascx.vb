﻿Imports BAL

Partial Class topcontrol
    Inherits System.Web.UI.UserControl
    Dim ab As New modifyData
    Dim itm As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("IsUser") = "0" Then
            Session("IsUser") = extra.BuyerUniqueID()
        End If
        If HttpContext.Current.User.Identity.Name <> "" Then

            lnkusr.Text = "My Account"
            lnkusr.ToolTip = HttpContext.Current.User.Identity.Name
            liLogout.Visible = True
            liLogin.Visible = False
            lnkregs.Visible = False

        Else
            lnkusr.Text = "Welcome Guest"
            lnkregs.Visible = True
            lnklgns.Text = "Log in"
            'lval.Visible = True
            liLogout.Visible = False
            liLogin.Visible = True

        End If
    End Sub

    Protected Sub lnkusr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkusr.Click
        If HttpContext.Current.User.Identity.Name <> "" Then
            Response.Redirect("/OrderHistory")
        Else
            Response.Redirect("/login")
        End If
    End Sub

    Protected Sub lnkregs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkregs.Click
        Response.Redirect("/register")
    End Sub

    Protected Sub lnklgns_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnklgns.Click
     
        Response.Redirect("/login")

    End Sub

   

    Protected Sub btnWISHLIST_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWISHLIST.Click
        If HttpContext.Current.User.Identity.Name = "" Then

            Response.Redirect("/login/wishlist")


        Else

            Response.Redirect("/wishlist")

        End If

    End Sub

    Protected Sub btnShopingcart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShopingcart.Click
        If Session("IsUser") <> "" Then
            Response.Redirect("/cart")
        Else
            Response.Redirect("/login/cart")
        End If
    End Sub


    Protected Sub lnlLogout_Click(sender As Object, e As System.EventArgs) Handles lnlLogout.Click
        Session.Abandon()

        Response.Buffer = True
        Response.Expires = -1500
        Response.AddHeader("pragma", "noCache")
        Response.CacheControl = "no-cache"
        'Response.ExpiresAbsolute = DateTime.Now.AddDays(-1D)
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Session.Clear()
        Response.ExpiresAbsolute = Now().Subtract(New TimeSpan(1, 0, 0, 0))
        ' Response.Expires = 0
        ' Response.CacheControl = "no-cache"
        Session("URL") = "0"
        Session("IsUser") = "0"
        Session("min") = "0"
        Session("max") = "0"
        FormsAuthentication.SignOut()
        Response.Redirect("/login")
    End Sub
End Class

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

using System.Configuration;

namespace DAL
{
   public class ManageData
    {
        Database db = GetDatabase(AppDatabaseName);
       private static string AppDatabaseName;
       private static Database GetDatabase(string dbName)
       {
           try
           {
               Database db = null;
               if (dbName == null)
               {
                   return db = DatabaseFactory.CreateDatabase();
               }
               else
               {
                   return db = DatabaseFactory.CreateDatabase(dbName);
               }
               return db;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       static ManageData()
        {
            AppDatabaseName = ConfigurationSettings.AppSettings["AppDatabaseName"];
        }
       public bool Insertcategory(string Category, string image, string sortid, string Description, string Submitby)
        {
            try
            {

                bool check = Checkcategory(Category);
                if (check == true)
                {
                    int sort =Convert.ToInt32( SelectMaxForCat());
                    sort = sort + 1;


                    db = GetDatabase(AppDatabaseName);
                    DbCommand cmd = db.GetStoredProcCommand("spInsKeyCategoryTable");
                    db.AddInParameter(cmd, "@Category", DbType.String, Category);
                    db.AddInParameter(cmd, "@sortid", DbType.String, sort.ToString());                 
                    db.AddInParameter(cmd, "@image", DbType.String, image);
                    db.AddInParameter(cmd, "@Description", DbType.String, Description);
                    db.AddInParameter(cmd, "@Submitby", DbType.String, Submitby);
                    db.ExecuteNonQuery(cmd);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw ex;

            }


        }

       public DataSet getAccountInfo( string ID)
       {
           try
           {
               DbCommand cmd = db.GetSqlStringCommand("select * from usertable where username=@uname");
               db.AddInParameter(cmd, "@uname", DbType.String, ID);
               DataSet ds = db.ExecuteDataSet(cmd);
               return ds;
           }
           catch (Exception ex)
           {

               throw ex;

           }
       }
        public bool Checkcategory(string Category)
        {
            try
            {


                DbCommand cmd = db.GetSqlStringCommand("Select Count (*) from CategoryTable where category=@Category");
                db.AddInParameter(cmd, "@Category", DbType.String, Category);
                int i = Convert.ToInt32(db.ExecuteScalar(cmd));

                if (i == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch (Exception ex)
            {

                throw ex;

            }


        }



        public DataSet Selectcategory()
        {
            try
            {



                DbCommand cmd = db.GetSqlStringCommand("Select * from categorytable order by catid desc");

                DataSet ds = db.ExecuteDataSet(cmd);

                return ds;





            }
            catch (Exception ex)
            {

                throw ex;

            }


        }

        public void DelecteCategory(string catid)
        {
            try
            {



                DbCommand cmd = db.GetSqlStringCommand("Delete from categorytable where catid=@catid");
                db.AddInParameter(cmd, "@catid", DbType.String, catid);
                db.ExecuteNonQuery(cmd);


            }
            catch (Exception ex)
            {

                throw ex;

            }


        }



        public bool updatecategory(string Catid, string Category, string image, string sortid,string Description)
        {
            try
            {


                bool check = UpdateCheckcategory(Category, Catid);
                if (check == true)
                {


                    DbCommand cmd = db.GetStoredProcCommand("spUpdCategoryTable");
                    db.AddInParameter(cmd, "@Catid", DbType.Int32, Catid);
                    db.AddInParameter(cmd, "@Category", DbType.String, Category);
                    db.AddInParameter(cmd, "@sortid", DbType.String, sortid);                
                    db.AddInParameter(cmd, "@image", DbType.String, image);
                    db.AddInParameter(cmd, "@Description", DbType.String, Description);
                    db.ExecuteNonQuery(cmd);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw ex;

            }


        }


        public bool UpdateCheckcategory(string Category, string catid)
        {
            try
            {




                DbCommand cmd = db.GetSqlStringCommand("Select Count (*) from CategoryTable where category=@Category and catid<>@catid");
                db.AddInParameter(cmd, "@Category", DbType.String, Category);
                db.AddInParameter(cmd, "@catid", DbType.Int32, catid);
                int i = Convert.ToInt32(db.ExecuteScalar(cmd));

                if (i == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch (Exception ex)
            {

                throw ex;

            }


        }

        /// <summary>
        /// //////////Product
        /// </summary>
        /// <returns></returns>

        public DataSet selectCategoryForProduct()
        {


            DbCommand cmd = db.GetSqlStringCommand("select catid,category from CategoryTable where IsVisible=1 order by sortid asc");

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;






        }


        public DataSet SelectProduct(string cname)
        {


            DbCommand cmd = db.GetSqlStringCommand("select * from itemdetailtable where categoryname=@cname and  IsVisible='True' order by id desc");
            db.AddInParameter(cmd, "@cname", DbType.String, cname);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;


        }

        public DataSet SelectProductforadmin(string cat,string scat)
        {
            if (scat == "" || scat == "0")
                scat=null;
            if (cat == "" || cat == "0")
                cat=null;
            DbCommand cmd = db.GetSqlStringCommand("select * from itemdetailtable where categoryname=isnull(@categoryname,categoryname) and subcategory=isnull(@subcategory,subcategory)  order by id desc");
            db.AddInParameter(cmd, "@categoryname", DbType.String, cat);
            db.AddInParameter(cmd, "@subcategory", DbType.String, scat);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;


        }



        public bool insertproduct(string catname, string subcatname, string code, string name, string imag, Double price, Double offerprice, string detail, Double productprice, string BestSellers, string DailyDeal, string isnew, string IsVisible, string IsFeatured, string Size, string Weight, string Submiyby, string ShortDescripn, Double buyingprice, Double wholesaleprice)
        {



            bool a = false;
            DbCommand cmd = db.GetSqlStringCommand("select count(*) from itemdetailtable where code=@code");
            db.AddInParameter(cmd, "@code", DbType.String, code);
            int abc = Convert.ToInt32(db.ExecuteScalar(cmd));

            if (abc == 0)
            {

                DbCommand cmd1 = db.GetSqlStringCommand("insert into itemdetailtable (categoryname,subcategory,code,name,image,price,offerprice,detail,productprice,BestSellers,DailyDeal,isnew,IsVisible,IsFeatured,Size,Weight,Submitby,ShortDescripn,buyingprice,wholesaleprice) values (@catname,@subcategory,@code,@name,@image,@price,@offerprice,@detail,@productprice,@BestSellers,@DailyDeal,@isnew,@IsVisible,@IsFeatured,@Size,@Weight,@Submiyby,@ShortDescripn,@buyingprice,@wholesaleprice)");
                db.AddInParameter(cmd1, "@catname", DbType.String, catname);
                db.AddInParameter(cmd1, "@subcategory", DbType.String, subcatname);
                db.AddInParameter(cmd1, "@code", DbType.String, code);
                db.AddInParameter(cmd1, "@name", DbType.String, name);
                db.AddInParameter(cmd1, "@image", DbType.String, imag);


                db.AddInParameter(cmd1, "@price", DbType.Double, price);
                db.AddInParameter(cmd1, "@offerprice", DbType.Double, offerprice);
                db.AddInParameter(cmd1, "@buyingprice", DbType.Double, buyingprice);
                db.AddInParameter(cmd1, "@wholesaleprice", DbType.Double, wholesaleprice);
                db.AddInParameter(cmd1, "@productprice", DbType.Double, productprice);


                db.AddInParameter(cmd1, "@detail", DbType.String, detail);
                db.AddInParameter(cmd1, "@BestSellers", DbType.String, BestSellers);
                db.AddInParameter(cmd1, "@DailyDeal", DbType.String, DailyDeal);
                db.AddInParameter(cmd1, "@isnew", DbType.String, isnew);
                db.AddInParameter(cmd1, "@IsVisible", DbType.String, IsVisible);
                db.AddInParameter(cmd1, "@IsFeatured", DbType.String, IsFeatured);

                db.AddInParameter(cmd1, "@Size", DbType.String, Size);
                db.AddInParameter(cmd1, "@Weight", DbType.String, Weight);
                db.AddInParameter(cmd1, "@Submiyby", DbType.String, Submiyby);
                db.AddInParameter(cmd1, "@ShortDescripn", DbType.String, ShortDescripn);
                db.ExecuteNonQuery(cmd1);
                a = true;

            }
            else
            {
                a = false;
            }
            return a;
        }

        public void deleteProduct(string id)
        {
            DbCommand cmd = db.GetSqlStringCommand("delete from itemdetailtable where id=@id");
            db.AddInParameter(cmd, "@id", DbType.Int32, id);
            db.ExecuteNonQuery(cmd);

            //DbCommand cmd1 = db.GetSqlStringCommand("delete from stocktable where itemid=@id");
            //db.AddInParameter(cmd1, "@id", DbType.Int32, id);
            //db.ExecuteNonQuery(cmd1);

        }

        public bool updateItem(string id, string code, string name, string imag, Double price, Double offerprice, string detail, Double productprice, string BestSellers, string DailyDeal, string isnew, string IsVisible, string IsFeatured, string Size, string Weight, string ShortDescripn, Double BuyingPrice, Double WholeSalePrice, string categoryname, string SubCategory)
        {


            bool a = false;
            DbCommand cmd = db.GetSqlStringCommand("select count(*) from itemdetailtable where code=@code and id<>@id");
            db.AddInParameter(cmd, "@code", DbType.String, code);
            db.AddInParameter(cmd, "@id", DbType.Int32, id);
            int abc = Convert.ToInt32(db.ExecuteScalar(cmd));

            if (abc == 0)
            {

                string str = "update itemdetailtable set categoryname=@categoryname, SubCategory=@SubCategory,code=@code,name=@name,image=@image,price=@price,offerprice=@offerprice,detail=@detail,productprice=@productprice,BestSellers=@BestSellers,DailyDeal=@DailyDeal, isnew=@isnew,IsVisible=@IsVisible,IsFeatured=@IsFeatured,Size=@Size,Weight=@Weight,ShortDescripn=@ShortDescripn,BuyingPrice=@BuyingPrice,WholeSalePrice=@WholeSalePrice where id=@id";
                DbCommand cmd1 = db.GetSqlStringCommand(str);

                db.AddInParameter(cmd1, "@id", DbType.Int32, id);
                db.AddInParameter(cmd1, "@code", DbType.String, code);
                db.AddInParameter(cmd1, "@name", DbType.String, name);
                db.AddInParameter(cmd1, "@image", DbType.String, imag);
                db.AddInParameter(cmd1, "@price", DbType.Double, price);
                db.AddInParameter(cmd1, "@offerprice", DbType.Double, offerprice);
                db.AddInParameter(cmd1, "@detail", DbType.String, detail);
                db.AddInParameter(cmd1, "@productprice", DbType.Double, productprice);
                db.AddInParameter(cmd1, "@BestSellers", DbType.String, BestSellers);
                db.AddInParameter(cmd1, "@DailyDeal", DbType.String, DailyDeal);
                db.AddInParameter(cmd1, "@isnew", DbType.String, isnew);
                db.AddInParameter(cmd1, "@IsVisible", DbType.String, IsVisible);
                db.AddInParameter(cmd1, "@IsFeatured", DbType.String, IsFeatured);
                db.AddInParameter(cmd1, "@Size", DbType.String, Size);
                db.AddInParameter(cmd1, "@Weight", DbType.String, Weight);
                db.AddInParameter(cmd1, "@ShortDescripn", DbType.String, ShortDescripn);
                db.AddInParameter(cmd1, "@BuyingPrice", DbType.Double, BuyingPrice);
                db.AddInParameter(cmd1, "@WholeSalePrice", DbType.Double, WholeSalePrice);
                db.AddInParameter(cmd1, "@categoryname", DbType.String, categoryname);
                db.AddInParameter(cmd1, "@SubCategory", DbType.String, SubCategory);
          
                db.ExecuteNonQuery(cmd1);
                return true;
            }
            else
            {
                return false;
            }
        }



        // user

        public void datainsert(string uname, string password, string name, string email, string ph, string altph, string addr, string city, string state,string count, string zip)
        {

            DateTime dt;

            dt = DateTime.Now;

            string str = System.DateTime.Now.ToString();
            string[] sr = str.Split(' ');
            str = sr[0];

            DbCommand cmd1 = db.GetSqlStringCommand("insert into UserTable (Username,Password,Name,Email,Phone,Altphone,Address,City,State,Country,Zipcode,registerdate) values (@Username,@Password,@Name,@Email,@pno,@altph,@addr,@city,@st,@count,@zip,@regdate)");


            db.AddInParameter(cmd1, "@Username", DbType.String, uname);
            db.AddInParameter(cmd1, "@Password", DbType.String, password);
            db.AddInParameter(cmd1, "@Name", DbType.String, name);
            db.AddInParameter(cmd1, "@Email", DbType.String, email);
            db.AddInParameter(cmd1, "@pno", DbType.String, ph);
            db.AddInParameter(cmd1, "@altph", DbType.String, altph);
            db.AddInParameter(cmd1, "@addr", DbType.String, addr);
            db.AddInParameter(cmd1, "@city", DbType.String, city);
            db.AddInParameter(cmd1, "@st", DbType.String, state);
            db.AddInParameter(cmd1, "@count", DbType.String, count);
            db.AddInParameter(cmd1, "@zip", DbType.String, zip);
            db.AddInParameter(cmd1, "@regdate", DbType.String, dt.ToString());
   

            db.ExecuteNonQuery(cmd1);


        }

        public DataSet selAllUsers()
        {
            string qury = "SELECT * FROM UserTable order by uid desc";
            DbCommand cmd = db.GetSqlStringCommand(qury);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;



        }

        public DataSet selSearchUsersdata(string srchtxt)
        {


            string qury = "select * from UserTable where Username like '" + srchtxt + "%' or email like '" + srchtxt + "%' or city like '" + srchtxt + "%' or name like '" + srchtxt + "%' or state like '" + srchtxt + "%'";
            DbCommand cmd = db.GetSqlStringCommand(qury);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public bool IsUsernameAvailable(string Username)
        {

            DbCommand cmd = db.GetSqlStringCommand("Select count(*) from UserTable where Username=@uname");
            db.AddInParameter(cmd, "@uname", DbType.String, Username);


            int i = Convert.ToInt32(db.ExecuteScalar(cmd));

            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool IsEmailAvailable(string email) 
        {

            DbCommand cmd = db.GetSqlStringCommand("Select count(*) from UserTable where email=@email");
            db.AddInParameter(cmd, "@email", DbType.String, email);


            int i = Convert.ToInt32(db.ExecuteScalar(cmd));

            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsMobileAvailable(string Mobile)
        {

            DbCommand cmd = db.GetSqlStringCommand("Select count(*) from UserTable where Phone=@Mobile");
            db.AddInParameter(cmd, "@Mobile", DbType.String, Mobile);


            int i = Convert.ToInt32(db.ExecuteScalar(cmd));

            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public void deleteUser(string uname)
        {
            DbCommand cmd = db.GetSqlStringCommand("delete from UserTable where Username=@uname");
            db.AddInParameter(cmd, "@uname", DbType.String, uname);
            db.ExecuteNonQuery(cmd);

        }

        // stock


        public DataSet SelectProductForstock()
        {


            DbCommand cmd = db.GetSqlStringCommand("select * from itemdetailtable ");

            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;


        }

        public string SetQty(string itemid)
        {

            DbCommand cmd = db.GetSqlStringCommand("select qty from stocktable where itemid=@itemid");
            db.AddInParameter(cmd, "@itemid", DbType.String, itemid);
            object obj = db.ExecuteScalar(cmd);

            if (obj == null || obj == System.DBNull.Value)
            {
                return "0";
            }
            else
            {
                return obj.ToString();
            }
        }


        public void InserStkQty(string itemid, string qty)
        {



            DbCommand cmd1 = db.GetSqlStringCommand("select count(*) from StockTable where itemid=@itemid");
            db.AddInParameter(cmd1, "@itemid", DbType.String, itemid);
            int abc = Convert.ToInt32(db.ExecuteScalar(cmd1));
            string dt = DateTime.Now.ToString();
            DbCommand cmd;
            if (abc == 0)
            {


                cmd = db.GetSqlStringCommand("Insert into StockTable(itemid,qty,date)values(@itemid,@qty,@dt)");

            }
            else
            {
                cmd = db.GetSqlStringCommand("update StockTable set qty=@qty,date=@dt where itemid=@itemid");

            }
            db.AddInParameter(cmd, "@itemid", DbType.String, itemid);
            db.AddInParameter(cmd, "@qty", DbType.Int32, qty);
            db.AddInParameter(cmd, "@dt", DbType.String, dt.ToString());
            db.ExecuteNonQuery(cmd);

        }
        public void updateuser(string UserName, string Password, string Name, string Email, string Phone, string Address, string City, string State, string Zipcode, string registerdate)
        {



            db = GetDatabase(AppDatabaseName);
            DbCommand cmd = db.GetStoredProcCommand("spUpdUserTable");
            db.AddInParameter(cmd, "@UserName", DbType.String, UserName);
            db.AddInParameter(cmd, "@Password", DbType.String, Password);
            db.AddInParameter(cmd, "@Name", DbType.String, Name);
            db.AddInParameter(cmd, "@Phone", DbType.String, Phone);
            db.AddInParameter(cmd, "@Address", DbType.String, Address);
            db.AddInParameter(cmd, "@City", DbType.String, City);
            db.AddInParameter(cmd, "@State", DbType.String, State);
            db.AddInParameter(cmd, "@Zipcode", DbType.String, Zipcode);
            db.AddInParameter(cmd, "@Email", DbType.String, Email);
            db.AddInParameter(cmd, "@registerdate", DbType.String, registerdate);

            db.ExecuteNonQuery(cmd);






        }
        //Insert Product

        public void inserProduct(string catid, string quantity, string buyer, string itemid, string Orderno, string item, string price, string UserType)
        {

            db = GetDatabase(AppDatabaseName);
            DbCommand cmd = db.GetStoredProcCommand("spInsKeyPurChaseTable");
            db.AddInParameter(cmd, "@catid", DbType.String, catid);
            db.AddInParameter(cmd, "@quantity", DbType.String, quantity);
            db.AddInParameter(cmd, "@buyer", DbType.String, buyer);
            db.AddInParameter(cmd, "@itemid", DbType.String, itemid);
            db.AddInParameter(cmd, "@Orderno", DbType.String, Orderno);
            db.AddInParameter(cmd, "@item", DbType.String, item);
            db.AddInParameter(cmd, "@price", DbType.Decimal, price);
            db.AddInParameter(cmd, "@UserType", DbType.String, UserType);
            db.ExecuteNonQuery(cmd);

        }

        public bool IsPrdctExistCart(string itemid, string buyer,string UserType)
        {
            string str = "Select quantity from purchasetable where itemid=@itemid and buyer=@buyer and UserType=@UserType and flagstatus=0";
            DbCommand cmd = db.GetSqlStringCommand(str);
            db.AddInParameter(cmd, "@itemid", DbType.String, itemid);
            db.AddInParameter(cmd, "@buyer", DbType.String, buyer);
            db.AddInParameter(cmd, "@UserType", DbType.String, UserType);
            object obj = db.ExecuteScalar(cmd);

            if (obj == null || obj == System.DBNull.Value)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public string findPid(string itemid, string buyer)
        {
            string str = "Select pid from purchasetable where itemid=@itemid and buyer=@buyer and flagstatus=0";
            DbCommand cmd = db.GetSqlStringCommand(str);
            db.AddInParameter(cmd, "@itemid", DbType.String, itemid);
            db.AddInParameter(cmd, "@buyer", DbType.String, buyer);
            object obj = db.ExecuteScalar(cmd);

            if (obj == null || obj == System.DBNull.Value)
            {
                return "0";
            }
            else
            {
                return obj.ToString();
            }

        }

        public void UpdatePurchaseItem(string pid, string qty, string buyer)
        {
            string str = "update purchasetable set quantity=@qty where (pid=@pid) and (buyer=@buyer) and (flagstatus='0')";
            DbCommand cmd = db.GetSqlStringCommand(str);
            db.AddInParameter(cmd, "@pid", DbType.String, pid);
            db.AddInParameter(cmd, "@qty", DbType.String, qty);
            db.AddInParameter(cmd, "@buyer", DbType.String, buyer);
            db.ExecuteNonQuery(cmd);


        }



        public bool checkadmin(string uname, string password)
        {

            DbCommand cmd = db.GetSqlStringCommand("select uname from admintable where uname=@uname and password=@password");
            db.AddInParameter(cmd, "@uname", DbType.String, uname);
            db.AddInParameter(cmd, "@password", DbType.String, password);
            object obj = db.ExecuteScalar(cmd);

            if (obj == null || obj == System.DBNull.Value)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool checkcrm(string uname, string password)
        {

            DbCommand cmd = db.GetSqlStringCommand("select username from crmlogin where username=@uname and password=@password");
            db.AddInParameter(cmd, "@uname", DbType.String, uname);
            db.AddInParameter(cmd, "@password", DbType.String, password);
            object obj = db.ExecuteScalar(cmd);

            if (obj == null || obj == System.DBNull.Value)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool checkUser(string uname, string password)
        {

            DbCommand cmd = db.GetSqlStringCommand("select username from usertable where username=@uname and password=@password");
            db.AddInParameter(cmd, "@uname", DbType.String, uname);
            db.AddInParameter(cmd, "@password", DbType.String, password);
            object obj = db.ExecuteScalar(cmd);

            if (obj == null || obj == System.DBNull.Value)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public Int32 SelPurItem(string buyer)
        {
            object a;
            DbCommand cmd = db.GetSqlStringCommand("select count(*) from purchasetable where buyer=@buyer and   flagstatus='0'");
            db.AddInParameter(cmd, "@buyer", DbType.String, buyer);
            a = db.ExecuteScalar(cmd);
            if (a == null || a.ToString() == "0")
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(a);
            }
        }




        public DataSet SelPurProducts(string buyer,string Usertype)
        {

            DbCommand cmd = db.GetSqlStringCommand("SELECT * FROM PurChaseTable WHERE flagstatus=0 and  buyer=@buyer and Usertype=@Usertype");
            db.AddInParameter(cmd, "@buyer", DbType.String, buyer);
            db.AddInParameter(cmd, "@Usertype", DbType.String, Usertype);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;


        }

        public void UpdateQuantity(string qty, string itemid)
        {
            DbCommand cmd = db.GetSqlStringCommand("update StockTable set qty=@qty where itemid=@itemid");
            db.AddInParameter(cmd, "@itemid", DbType.String, itemid);
            db.AddInParameter(cmd, "@qty", DbType.Int32, qty);
            db.ExecuteNonQuery(cmd);

        }


        public DataSet registerdata(string uname)
        {
            DbCommand cmd = db.GetSqlStringCommand("select * from UserTable where username=@uname");
            db.AddInParameter(cmd, "@uname", DbType.String, uname);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet registerdataForMobile(string username)
        {
            DbCommand cmd = db.GetSqlStringCommand("select * from UserTable where phone=@username or email=@username");
            db.AddInParameter(cmd, "@username", DbType.String, username); 
     
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;
        }



        public string GetMaxOrderID()
        {
            object a;
            string orderno;
            DbCommand cmd = db.GetSqlStringCommand("Select max(pid) from PurchaseTable");
            a = db.ExecuteScalar(cmd);
            if (a == null || a.ToString() == "0")
            {
                orderno = "1";
            }
            else
            {
                orderno = a.ToString();
            }
            Random rnd = new Random();
            return "AF-" + orderno + rnd.Next(1111, 999999);

        }

        public string GetMaxCancelID()
        {
            object a;
            string cancel_id;
            DbCommand cmd = db.GetSqlStringCommand("Select max(id) from crm_cancel");
            a = db.ExecuteScalar(cmd);
            if (a == null || a.ToString() == "0")
            {
                cancel_id = "1";
            }
            else
            {
                cancel_id = a.ToString();
            }
            Random rnd = new Random();
            return "CR-" + cancel_id + rnd.Next(1111, 999999);

        }

        public void UpdatePrchDtl(string sid, String pid, String ordno)
        {
            DbCommand cmd = db.GetSqlStringCommand("Update purchasetable set flagstatus=0,OrderNo='" + ordno + "' where pid='" + pid + "' and buyer='" + sid + "'");
            db.ExecuteNonQuery(cmd);
        }

        public void InsertFinalPurchase(string ord, string uid, string addr, string amount, string Discount, string UsedLoyalty, string MainAmount)
        {
            DbCommand cmd = db.GetSqlStringCommand("Insert into finalordertable (OrderNo,username,DOP,address,amount,Discount,UsedLoyalty,MainAmount)values(@OrderNo,@username,@DOP,@addr,@amount,@Discount,@UsedLoyalty,@MainAmount)");
            db.AddInParameter(cmd, "@OrderNo", DbType.String, ord);
            db.AddInParameter(cmd, "@username", DbType.String, uid);
            db.AddInParameter(cmd, "@DOP", DbType.String, DateTime.Now.ToString());
            db.AddInParameter(cmd, "@addr", DbType.String, addr);
            db.AddInParameter(cmd, "@amount", DbType.Decimal, amount);
            db.AddInParameter(cmd, "@Discount", DbType.String, Discount);
            db.AddInParameter(cmd, "@UsedLoyalty", DbType.String, UsedLoyalty);
            db.AddInParameter(cmd, "@MainAmount", DbType.Decimal, MainAmount);

            
            db.ExecuteNonQuery(cmd);
        }

        public void InsertFinalPurchaseForCall(string ord, string uid, string addr, string amount, string Discount, string UsedLoyalty, string MainAmount, string CallExecutive, string SalesName, string UserType)
        {
            DbCommand cmd = db.GetSqlStringCommand("Insert into finalordertable (OrderNo,username,DOP,address,amount,Discount,UsedLoyalty,MainAmount,CallExecutive,SalesName,UserType)values(@OrderNo,@username,@DOP,@addr,@amount,@Discount,@UsedLoyalty,@MainAmount,@CallExecutive,@SalesName,@UserType)");
            db.AddInParameter(cmd, "@OrderNo", DbType.String, ord);
            db.AddInParameter(cmd, "@username", DbType.String, uid);
            db.AddInParameter(cmd, "@DOP", DbType.String, DateTime.Now.ToString());
            db.AddInParameter(cmd, "@addr", DbType.String, addr);
            db.AddInParameter(cmd, "@amount", DbType.Decimal, amount);
            db.AddInParameter(cmd, "@Discount", DbType.String, Discount);
            db.AddInParameter(cmd, "@UsedLoyalty", DbType.String, UsedLoyalty);
            db.AddInParameter(cmd, "@MainAmount", DbType.Decimal, MainAmount);
            db.AddInParameter(cmd, "@CallExecutive", DbType.String, CallExecutive);
            db.AddInParameter(cmd, "@SalesName", DbType.String, SalesName);
            db.AddInParameter(cmd, "@UserType", DbType.String, UserType);
            db.ExecuteNonQuery(cmd);
        }

        public void UpdatePrchflag(string ordno, string flag)
        {
            DbCommand cmd = db.GetSqlStringCommand("Update purchasetable set flagstatus='" + flag + "' where orderno='" + ordno + "'");
            db.ExecuteNonQuery(cmd);

        }

        public DataSet SelFinalProductsbydate(string dt1, string dt2)
        {
            DbCommand cmd = db.GetSqlStringCommand("SELECT  * FROM finalordertable where dop between @dt1 and @dt2  order by dop desc");
            db.AddInParameter(cmd, "@dt1", DbType.String, dt1);
            db.AddInParameter(cmd, "@dt2", DbType.String, dt2);
            return db.ExecuteDataSet(cmd);

        }


        public DataSet SelFinalProductsordandname(string username)
        {
            DbCommand cmd = db.GetSqlStringCommand("SELECT  * FROM finalordertable WHERE     (username LIKE '" + username + "%') OR   (OrderNo LIKE '" + username + "%') ORDER BY DOP DESC");
            db.AddInParameter(cmd, "@username", DbType.String, username);
          
            return db.ExecuteDataSet(cmd);

        }

        public DataSet SelFinalProductsordandnameAndDate(string username, string dt1, string dt2)
        {
            DbCommand cmd = db.GetSqlStringCommand("SELECT  * FROM finalordertable WHERE   (DOP BETWEEN @dt1 AND @dt2) and ( (username LIKE '" + username + "%') OR  (OrderNo LIKE '" + username + "%'))  ORDER BY DOP DESC");
            db.AddInParameter(cmd, "@username", DbType.String, username);
            db.AddInParameter(cmd, "@dt1", DbType.String, dt1);
            db.AddInParameter(cmd, "@dt2", DbType.String, dt2);
            return db.ExecuteDataSet(cmd);

        }


        public DataSet SelAllFinalProducts1()
        {
            DbCommand cmd = db.GetSqlStringCommand("SELECT  * FROM finalordertable  order by dop desc");

            return db.ExecuteDataSet(cmd);
        }


        public string SelOrdProduct(string ord)
        {

            DbCommand cmd = db.GetSqlStringCommand("DECLARE @Pro VARCHAR(8000) SELECT @Pro = COALESCE (@Pro + '<br> ', '') + item FROM (SELECT (itmdet.name + ' - ' + itmdet.code  + ' Qty ' + CONVERT(nvarchar, ptbl.quantity))  AS item, ptbl.Orderno FROM itemdetailtable AS itmdet INNER JOIN PurChaseTable AS ptbl ON itmdet.id = ptbl.catid WHERE (ptbl.Orderno = @ord)) AS tbl SELECT products = @Pro");
            db.AddInParameter(cmd, "@ord", DbType.String, ord);
            return db.ExecuteScalar(cmd).ToString();


        }


        public bool selorderfromcrm(string ord)
        {

            DbCommand cmd = db.GetSqlStringCommand("select * from crm_purchase where orderno=@ord");
            db.AddInParameter(cmd, "@ord", DbType.String, ord);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(cmd);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

       public void Insertcrm_puchase(string order_id)
       {
           DbCommand cmd = db.GetSqlStringCommand("insert into crm_purchase values(@ord)");
           db.AddInParameter(cmd, "@ord", DbType.String, order_id);
           db.ExecuteNonQuery(cmd);
       }

        public DataSet SelFinalOrdDtl(string ord)
        {
            DbCommand cmd = db.GetSqlStringCommand("SELECT * FROM finalordertable left join Checkout on Checkout.orderID=finalordertable.orderno where finalordertable.orderno=@ord");
            db.AddInParameter(cmd, "@ord", DbType.String, ord);
            return db.ExecuteDataSet(cmd);

        }

        public DataSet SelOrd(string ord)
        {

            // DbCommand cmd = db.GetSqlStringCommand("SELECT   pur.quantity,  pur.pid, pur.catid, pur.buyer, pur.Orderno,itm.id FROM itemdetailtable AS itm INNER JOIN PurChaseTable AS pur ON itm.id = pur.catid AND pur.Orderno = @ord");

            DbCommand cmd = db.GetSqlStringCommand("SELECT pur.quantity, pur.pid,pur.price, pur.catid, pur.buyer, pur.item, pur.Orderno,itm.code FROM itemdetailtable AS itm INNER JOIN PurChaseTable AS pur ON itm.id = pur.catid AND pur.Orderno = @ord");
            db.AddInParameter(cmd, "@ord", DbType.String, ord);
            return db.ExecuteDataSet(cmd);

        }

        public string selectaddress(string orderno)
        {

            DbCommand cmd = db.GetSqlStringCommand("SELECT address FROM finalordertable WHERE   orderno=@orderno");
            db.AddInParameter(cmd, "@orderno", DbType.String, orderno);
            return db.ExecuteScalar(cmd).ToString();


        }

        public void DeleteOrders(string orderID)
        {
            DbCommand cmd2 = db.GetSqlStringCommand("select itemid, quantity from purchasetable where orderno=@ord");
            db.AddInParameter(cmd2, "@ord", DbType.String, orderID);
            DataSet ds = new DataSet();
            ds=db.ExecuteDataSet(cmd2);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                DbCommand cmd3= db.GetSqlStringCommand("update stocktable set qty=qty+@qt where itemid=@id");
                db.AddInParameter(cmd3, "@id", DbType.String,ds.Tables[0].Rows[i]["itemid"].ToString());
                db.AddInParameter(cmd3, "@qt", DbType.String, ds.Tables[0].Rows[i]["quantity"].ToString());
                db.ExecuteNonQuery(cmd3);
            }

            DbCommand cmd4 = db.GetSqlStringCommand("select userid, loyalty from loyaltyhistory where orderid=@ord");
            db.AddInParameter(cmd4, "@ord", DbType.String, orderID);
            DataSet ds2 = new DataSet();
            ds2 = db.ExecuteDataSet(cmd4);
            for (int i = 0; i < ds2.Tables[0].Rows.Count; i++)
            {
                DbCommand cmd5 = db.GetSqlStringCommand("update UserLoyaltyPoint set point=point-@qt where userid=@id");
                db.AddInParameter(cmd5, "@id", DbType.String, ds2.Tables[0].Rows[i]["userid"].ToString());
                db.AddInParameter(cmd5, "@qt", DbType.String, ds2.Tables[0].Rows[i]["loyalty"].ToString());
                db.ExecuteNonQuery(cmd5);
            }           



            string qry = null;

            //qry = "delete from finalordertable where orderno =@ord ";
            qry =  "delete from purchasetable where orderno =@ord ";
            qry = qry + "delete from loyaltyhistory where orderid=@ord ";
            qry= qry +"delete from checkout where orderid=@ord ";
            DbCommand cmd = db.GetSqlStringCommand(qry);
            db.AddInParameter(cmd, "@ord", DbType.String, orderID);

            db.ExecuteNonQuery(cmd);


            

            string qry2 = null;
            qry2 = "UPDATE [finalordertable]   SET      [Dispatch] =2 WHERE orderno=@order ";
            qry2 = qry2 + "update crm_cancel  set status=2, date_of_cancel=@dt where orderno=@order ";
            DbCommand cmdupd = db.GetSqlStringCommand(qry2);
            db.AddInParameter(cmdupd, "@order", DbType.String, orderID);
            db.AddInParameter(cmdupd, "@dt", DbType.String, DateTime.Now.ToString());
            db.ExecuteNonQuery(cmdupd);
            



        }

        public string getreasonofcancellation(string orderid)
        {
            DbCommand cmd = db.GetSqlStringCommand("Select reason_cancel from crm_cancel where orderno=@order");
            db.AddInParameter(cmd, "@order", DbType.String, orderid);
            object ob = db.ExecuteScalar(cmd);
            if (ob == null || ob == System.DBNull.Value || ob.ToString() == "")
            {
                return "not mention";
            }
            else
            {
                return ob.ToString();
            }
        }

        public string getcatname(string catid)
        {

            DbCommand cmd = db.GetSqlStringCommand("Select category from categorytable where catid=@catid");
            db.AddInParameter(cmd, "@catid", DbType.String, catid);

            return db.ExecuteScalar(cmd).ToString();

        }

        public string getimagename(string itemid)
        {
            try
            {
                DbCommand cmd = db.GetSqlStringCommand("SELECT image FROM itemdetailtable WHERE id=@itemid");
                db.AddInParameter(cmd, "@itemid", DbType.String, itemid);
                return db.ExecuteScalar(cmd).ToString();

            }
            catch(Exception ex) 
            {
                return "";
            }

        }

        public void DeletePurchaseItem(string pid)
        {
            DbCommand cmd = db.GetSqlStringCommand("delete from purchasetable where pid=@pid");
            db.AddInParameter(cmd, "@pid", DbType.String, pid);
            db.ExecuteNonQuery(cmd).ToString();
        }

        public string ChgUserPass(string Username, string pass, string newpass)
        {
            // Username = "shyam@gmail.com";
            DbCommand cmd = db.GetSqlStringCommand("Select Password from UserTable where Username=@Username");

            db.AddInParameter(cmd, "@Username", DbType.String, Username);

            string str = db.ExecuteScalar(cmd).ToString();


            if (pass == str)
            {
                DbCommand cmd1 = db.GetSqlStringCommand("update UserTable set Password=@pass where Username=@Username");

                db.AddInParameter(cmd1, "@Username", DbType.String, Username);
                db.AddInParameter(cmd1, "@pass", DbType.String, newpass);
                db.ExecuteNonQuery(cmd1);

                return "1";
            }
            else
            {
                return "Old Password is wrong!";
            }
        }



        public string FindNameByUserName(string Username)
        {
            // Username = "shyam@gmail.com";
            DbCommand cmd = db.GetSqlStringCommand("Select Name from UserTable where Username=@Username");

            db.AddInParameter(cmd, "@Username", DbType.String, Username);

            string str = db.ExecuteScalar(cmd).ToString();

            return str;

        }

       public  string SelCatIDbycategory(string cat)
       {
          DbCommand cmd = db.GetSqlStringCommand("select catid from  CategoryTable where category=@cat");
          db.AddInParameter(cmd, "@cat", DbType.String, cat);

          string str = db.ExecuteScalar(cmd).ToString();

          return str;
       }
         public DataSet userSelCategory()
        {



            DbCommand cmd = db.GetSqlStringCommand("select * from categorytable where IsVisible = 'True'  order by sortid");
           
            return db.ExecuteDataSet(cmd);

        }
       public DataSet userSelItemBycat(string cat)
       {
           DbCommand cmd = db.GetSqlStringCommand("SELECT itm.itemid,itm.isdailydeals,itm.isbestsellers, itm.iswatchonsale, itm.category, itm.itemcode, itm.description, itm.itemimage, itm.sortid, itm.product, itm.price, itm.oldprice, itm.newflag, itm.prodtype, itm.strap, itm.PFlag, itm.Isprcent, itm.Promocode, itm.offer FROM itemdetailtable AS itm INNER JOIN CategoryTable AS ct ON itm.category = ct.catid AND ct.isCallingVisible = '0' WHERE (itm.category = @cat) AND (itm.isvisible = 'YES') ORDER BY itm.sortid");
           db.AddInParameter(cmd, "@cat", DbType.String, cat);

           return db.ExecuteDataSet(cmd);

          

       }
       public void UpdateSessuid(string userid, string ses)
       {
           DbCommand cmd = db.GetSqlStringCommand("update purchasetable set buyer=@userid where buyer =@ses");
           db.AddInParameter(cmd, "@userid", DbType.String, userid);
           db.AddInParameter(cmd, "@ses", DbType.String, ses);
         
           db.ExecuteNonQuery(cmd);
          
       }
       public DataSet SearchbyPrice(string min, string max)
       {


           DbCommand cmd = db.GetSqlStringCommand("select * from itemdetailtable where productprice between @min and @max order by productprice DESC");
           db.AddInParameter(cmd, "@min", DbType.String, min);
           db.AddInParameter(cmd, "@max", DbType.String, max);
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;
           //

       }
       public DataSet SelFinalProducts(string uid)
       {

           DbCommand cmd = db.GetSqlStringCommand("SELECT * FROM finalordertable  WHERE  (username = @uid) order by dop desc");
           db.AddInParameter(cmd, "@uid", DbType.String, uid);          
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;


       }
       public DataSet SelFnlOrdDtl(string ord)
       {

           DbCommand cmd = db.GetSqlStringCommand("Select * from finalordertable where orderno=@ord");
           db.AddInParameter(cmd, "@ord", DbType.String, ord);          
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;


       }


       

       public void profileupdate(string Name, string Email, string Phone, string Address, string City, string State, string Zipcode, string Uid)
       {



           db = GetDatabase(AppDatabaseName);
           DbCommand cmd = db.GetSqlStringCommand("update UserTable set Name=@Name,Phone=@Phone,Email=@Email,Address=@Address,City=@City,State=@State,ZipCode=@Zipcode where Uid=@Uid");
        
           db.AddInParameter(cmd, "@Name", DbType.String, Name);
           db.AddInParameter(cmd, "@Phone", DbType.String, Phone);
           db.AddInParameter(cmd, "@Address", DbType.String, Address);
           db.AddInParameter(cmd, "@City", DbType.String, City);
           db.AddInParameter(cmd, "@State", DbType.String, State);
           db.AddInParameter(cmd, "@Zipcode", DbType.String, Zipcode);
           db.AddInParameter(cmd, "@Email", DbType.String, Email);
           db.AddInParameter(cmd, "@Uid", DbType.String, Uid);
         

           db.ExecuteNonQuery(cmd);






       }
       public void UpdtFnlDtl(string oid, String st, String dtl, String remark, string ClMsg)
       {

           DbCommand cmd = db.GetSqlStringCommand("Update finalordertable set Dispatch=@st,dispatchno=@dtl,remark=@remark,ClMsg=@ClMsg where pid=@oid");
           db.AddInParameter(cmd, "@oid", DbType.String, oid);
           db.AddInParameter(cmd, "@st", DbType.String, st);
           db.AddInParameter(cmd, "@dtl", DbType.String, dtl);
           db.AddInParameter(cmd, "@remark", DbType.String, remark);
           db.AddInParameter(cmd, "@ClMsg", DbType.String, ClMsg);
         db.ExecuteNonQuery(cmd);
      


       }

       public string selectsmprice(string userid, string UserType)
       {

           DbCommand cmd = db.GetSqlStringCommand("SELECT SUM(price * quantity) AS total FROM PurChaseTable WHERE (buyer = @userid) AND (flagstatus = 0) and UserType=@UserType");
           db.AddInParameter(cmd, "@userid", DbType.String, userid);
           db.AddInParameter(cmd, "@UserType", DbType.String, UserType);
           return db.ExecuteScalar(cmd).ToString();


       }
       public DataSet SelectDispatchText()
       {


           DbCommand cmd = db.GetSqlStringCommand("select * from OrderStausText");          
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;


       }

       public DataSet SelectProductByID(string id)
       {


           DbCommand cmd = db.GetSqlStringCommand("select * from itemdetailtable where id=@id and IsVisible <> 'False'");
           db.AddInParameter(cmd, "@id", DbType.String, id);
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;


       }
       public void InsertInWishlist(string Itemid, string Userid)
       {
             bool a = false;
             DbCommand cmd1 = db.GetSqlStringCommand("select count(*) from Wishlist where Itemid=@Itemid");
            db.AddInParameter(cmd1, "@Itemid", DbType.String, Itemid);
            int abc = Convert.ToInt32(db.ExecuteScalar(cmd1));

            if (abc == 0)
            {
                DbCommand cmd = db.GetSqlStringCommand("Insert into Wishlist (Itemid,Userid,DOA)values(@Itemid,@Userid,@DOA)");
                db.AddInParameter(cmd, "@Itemid", DbType.String, Itemid);
                db.AddInParameter(cmd, "@Userid", DbType.String, Userid);
                db.AddInParameter(cmd, "@DOA", DbType.String, DateTime.UtcNow.Date.ToString());
                db.ExecuteNonQuery(cmd);
            }
       }
       public void Updatewishlist(string userid, string ses)
       {



           DbCommand cmd = db.GetSqlStringCommand("update wishlist set userid=@userid, status=1 where userid=@ses");
           db.AddInParameter(cmd, "@userid", DbType.String, userid);
           db.AddInParameter(cmd, "@ses", DbType.String, ses);
           db.ExecuteNonQuery(cmd);

       }

       public DataSet SelectWishListByID(string userid)
       {


           DbCommand cmd = db.GetSqlStringCommand("SELECT Wishlist.Itemid, itemdetailtable.*, Wishlist.wid FROM Wishlist INNER JOIN  itemdetailtable ON Wishlist.Itemid =itemdetailtable.id WHERE (Wishlist.Userid = @userid)");
           db.AddInParameter(cmd, "@userid", DbType.String, userid);
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;


       }

       public void Deletewishlist(string wid)
       {
           DbCommand cmd = db.GetSqlStringCommand("delete from wishlist where wid=@wid");
           db.AddInParameter(cmd, "@wid", DbType.String, wid);
           db.ExecuteNonQuery(cmd).ToString();
       }
       public string getpercentage()
       {

           DbCommand cmd = db.GetSqlStringCommand("select percentage from LoyaltyPercentage where lid=1");
         
           object obj = db.ExecuteScalar(cmd);

           if (obj == null || obj == System.DBNull.Value)
           {
               return "0";
           }
           else
           {
               return obj.ToString();
           }
       }
       public void InsertCheckout(string orderID, string uname, string BAddress, string SAddress, string Discount, string Promo, string paymentmode, string UsedLoyalty, string EarnLoyalty, string amount, string MainAmount)
       {
           DbCommand cmd = db.GetSqlStringCommand("Insert into Checkout (orderID,uname,BAddress,SAddress,Discount,Promo,paymentmode,UsedLoyalty,EarnLoyalty,amount,MainAmount)values(@orderID,@uname,@BAddress,@SAddress,@Discount,@Promo,@paymentmode,@UsedLoyalty,@EarnLoyalty,@amount,@MainAmount)");
           db.AddInParameter(cmd, "@orderID", DbType.String, orderID);
           db.AddInParameter(cmd, "@uname", DbType.String, uname);       
           db.AddInParameter(cmd, "@BAddress", DbType.String, BAddress);
           db.AddInParameter(cmd, "@SAddress", DbType.String, SAddress);
           db.AddInParameter(cmd, "@Discount", DbType.String, Discount);
           db.AddInParameter(cmd, "@Promo", DbType.String, Promo);
           db.AddInParameter(cmd, "@paymentmode", DbType.String, paymentmode);
           db.AddInParameter(cmd, "@UsedLoyalty", DbType.String, UsedLoyalty);
           db.AddInParameter(cmd, "@EarnLoyalty", DbType.String, EarnLoyalty);
           db.AddInParameter(cmd, "@amount", DbType.Decimal, amount);
           db.AddInParameter(cmd, "@MainAmount", DbType.Decimal, MainAmount);     
      
      
           
           db.ExecuteNonQuery(cmd);
       }

       public string getUserloyelty(string Userid)
       {

           DbCommand cmd = db.GetSqlStringCommand("select Point from UserLoyaltyPoint where Userid=@Userid");
           db.AddInParameter(cmd, "@Userid", DbType.String, Userid);
           object obj = db.ExecuteScalar(cmd);

           if (obj == null || obj == System.DBNull.Value)
           {
               return "0";
           }
           else
           {
               return obj.ToString();
           }
       }
       public DataSet SelectCheckOutFromOrderID(string orderid)
       {


           DbCommand cmd = db.GetSqlStringCommand("SELECT Checkout.Cid, Checkout.orderID, Checkout.amount, Checkout.uname, Checkout.BAddress,Checkout.MainAmount, Checkout.SAddress, Checkout.Discount, Checkout.Promo, Checkout.paymentmode, Checkout.UsedLoyalty, Checkout.EarnLoyalty, UserTable.Email, UserTable.Name FROM Checkout INNER JOIN UserTable ON Checkout.uname = UserTable.UserName WHERE (Checkout.orderID = @orderid)");
           db.AddInParameter(cmd, "@orderid", DbType.String, orderid);
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;


       }

       public DataSet SelectCheckOutFromOrderIDCallExecutive(string orderid) 
       {


           DbCommand cmd = db.GetSqlStringCommand("SELECT Checkout.Cid, Checkout.orderID, Checkout.amount, Checkout.uname, Checkout.BAddress,Checkout.MainAmount, Checkout.SAddress, Checkout.Discount, Checkout.Promo, Checkout.paymentmode, Checkout.UsedLoyalty, Checkout.EarnLoyalty, UserTable.Email, UserTable.Name FROM Checkout INNER JOIN UserTable ON Checkout.uname = UserTable.Phone WHERE (Checkout.orderID = @orderid)");
           db.AddInParameter(cmd, "@orderid", DbType.String, orderid);
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;


       }

       public void UserLoyaltyPoint( string Userid, string point)
       {
          
           DbCommand cmd1 = db.GetSqlStringCommand("select point from UserLoyaltyPoint where Userid=@Userid"); 
           db.AddInParameter(cmd1, "@Userid", DbType.String, Userid);
           object obj = db.ExecuteScalar(cmd1);

           if (obj == null || obj == System.DBNull.Value)
           {
               DbCommand cmd = db.GetSqlStringCommand("Insert into UserLoyaltyPoint (Userid,point)values(@Userid,@point)");
               db.AddInParameter(cmd, "@Userid", DbType.String, Userid);
               db.AddInParameter(cmd, "@point", DbType.String, point);
               db.ExecuteNonQuery(cmd);
             
           }
           else
           {

               int i = Convert.ToInt32(obj) + Convert.ToInt32(point);
               DbCommand cmd = db.GetSqlStringCommand("Update UserLoyaltyPoint set point=@point where Userid=@Userid");
       
               db.AddInParameter(cmd, "@Userid", DbType.String, Userid);
               db.AddInParameter(cmd, "@point", DbType.String, i.ToString());
              
               db.ExecuteNonQuery(cmd);

           }
       }

       public string  UserLoyaltyPointbyUser(string Userid)
       {

           DbCommand cmd1 = db.GetSqlStringCommand("select point from UserLoyaltyPoint where Userid=@Userid");
           db.AddInParameter(cmd1, "@Userid", DbType.String, Userid);
           object obj = db.ExecuteScalar(cmd1);
           if (obj == null || obj == System.DBNull.Value)
           {
               return "0";
           }
           else
           {
               return obj.ToString();
           }

       }

       public void UpdateUserLoyaltyPoint(string Userid, string point)
       {

           DbCommand cmd1 = db.GetSqlStringCommand("select point from UserLoyaltyPoint where Userid=@Userid");
           db.AddInParameter(cmd1, "@Userid", DbType.String, Userid);
           object obj = db.ExecuteScalar(cmd1);

           if (obj == null || obj == System.DBNull.Value)
           {
               DbCommand cmd = db.GetSqlStringCommand("Insert into UserLoyaltyPoint (Userid,point)values(@Userid,@point)");
               db.AddInParameter(cmd, "@Userid", DbType.String, Userid);
               db.AddInParameter(cmd, "@point", DbType.String, point);
               db.ExecuteNonQuery(cmd);

           }
           else
           {

              
               DbCommand cmd = db.GetSqlStringCommand("Update UserLoyaltyPoint set point=@point where Userid=@Userid");

               db.AddInParameter(cmd, "@Userid", DbType.String, Userid);
               db.AddInParameter(cmd, "@point", DbType.String, point);

               db.ExecuteNonQuery(cmd);

           }
       }
       public void InsertOrderHistory(string Loyalty, string ExpiryDate, string IssueDate, string Userid, string status, string Orderid)
       {
           DbCommand cmd = db.GetStoredProcCommand("spInsKeyLoyaltyHistory");
           db.AddInParameter(cmd, "@Loyalty", DbType.Int32, Loyalty);
           db.AddInParameter(cmd, "@ExpiryDate", DbType.String, ExpiryDate);
           db.AddInParameter(cmd, "@IssueDate", DbType.String, IssueDate);
           db.AddInParameter(cmd, "@Userid", DbType.String, Userid);
           db.AddInParameter(cmd, "@status", DbType.String, status);
           db.AddInParameter(cmd, "@Orderid", DbType.String, Orderid); 
           db.ExecuteNonQuery(cmd);
       }

       public string  checExpiryLoyalty(string user)
       {
           int loyalty = 0;
           int usedloyalty = 0;

           DbCommand cmd = db.GetSqlStringCommand("SELECT SUM(Loyalty) AS Expr1  FROM LoyaltyHistory WHERE  (ExpiryDate <= @dt1) AND (status <> 'Used') and (status<>'Expiry') and userid=@user1");
           db.AddInParameter(cmd, "@dt1", DbType.String, DateTime.Now.ToShortDateString());
           db.AddInParameter(cmd, "@user1", DbType.String, user);
           object obj = db.ExecuteScalar(cmd);

           if (obj == null || obj == System.DBNull.Value)
           {
              
           }
           else
           {
               loyalty = Convert.ToInt32(obj.ToString());
           }

           DbCommand cmd1 = db.GetSqlStringCommand("SELECT SUM(Loyalty) AS Expr1 FROM LoyaltyHistory WHERE  (ExpiryDate <= @dt) AND (status = 'Used') OR (status='Expiry')  and userid=@user");
           db.AddInParameter(cmd1, "@dt", DbType.String, DateTime.Now.ToShortDateString());
           db.AddInParameter(cmd1, "@user", DbType.String, user);
           object obj1 = db.ExecuteScalar(cmd1);
           if (obj1 == null || obj1 == System.DBNull.Value)
           {
              
           }
           else
           {
               usedloyalty = Convert.ToInt32(obj1.ToString());
           }

           if (loyalty > usedloyalty)
           {
               loyalty = loyalty - usedloyalty;
               return loyalty.ToString();
           }
           else
           {
               return "0";
           }


         
       }
       public DataSet SelectloyatyHistory(string userid)
       {

           DbCommand cmd = db.GetSqlStringCommand("SELECT * from LoyaltyHistory where userid=@userid");
           db.AddInParameter(cmd, "@userid", DbType.String, userid);
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;
           
       }
       public void InserPCode(string pmcd, string pab, string dis, string edt, string msg, string Ispracent)
       {
           string dt = DateTime.UtcNow.Date.ToString();
           DbCommand cmd = db.GetSqlStringCommand("Insert into PromoTable(Promocode,PurchaseAbove,Discount,Expiredate,Msg,Ispracent,senddate)values(@pmcd,@pab,@dis,@edt,@msg,@Ispracent,@senddate)");
            db.AddInParameter(cmd, "@pmcd", DbType.String, pmcd);
            db.AddInParameter(cmd, "@pab", DbType.String, pab);
            db.AddInParameter(cmd, "@dis", DbType.String, dis);
            db.AddInParameter(cmd, "@edt", DbType.String, edt);
            db.AddInParameter(cmd, "@msg", DbType.String, msg);
            db.AddInParameter(cmd, "@Ispracent", DbType.String, Ispracent);
            db.AddInParameter(cmd, "@senddate", DbType.String, DateTime.Now.ToShortDateString());
            db.ExecuteDataSet(cmd);
       }
       public void UpdatePCode(string pmcd, string pab, string dis, string edt, string msg, string Ispracent, string pid)
       {
           string dt = DateTime.UtcNow.Date.ToString();
           DbCommand cmd = db.GetSqlStringCommand("Update Promotable set PromoCode =@pmcd,purchaseAbove=@pab,discount=@dis,ExpireDate=@edt,msg=@msg, Ispracent=@Ispracent  where pid =@pid");
           db.AddInParameter(cmd, "@pmcd", DbType.String, pmcd);
           db.AddInParameter(cmd, "@pab", DbType.String, pab);
           db.AddInParameter(cmd, "@dis", DbType.String, dis);
           db.AddInParameter(cmd, "@edt", DbType.String, edt);
           db.AddInParameter(cmd, "@msg", DbType.String, msg);
           db.AddInParameter(cmd, "@Ispracent", DbType.String, Ispracent);
           db.AddInParameter(cmd, "@pid", DbType.String, pid);
           db.ExecuteDataSet(cmd);
       }


       

       public bool CheckPCode(string pmcd)
       {

           DbCommand cmd1 = db.GetSqlStringCommand("select pid from PromoTable where Promocode=@pmcd");
           db.AddInParameter(cmd1, "@pmcd", DbType.String, pmcd);
           object obj = db.ExecuteScalar(cmd1);
           if (obj == null || obj == System.DBNull.Value)
           {
               return false;
           }
           else
           {
               return true;
           }

       }

       public DataSet SelAllPromo()
       {

           DbCommand cmd = db.GetSqlStringCommand("Select * from Promotable order by pid desc");

           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;

       }

       public void DelectePromo(string pid)
        {
            try
            {



                DbCommand cmd = db.GetSqlStringCommand("delete from Promotable where pid =@pid");
                db.AddInParameter(cmd, "@pid", DbType.String, pid);
                db.ExecuteNonQuery(cmd);


            }
            catch (Exception ex)
            {

                throw ex;

            }


        }
       public DataSet SelAllPromoUser(string Promocode)
       {

           DbCommand cmd = db.GetSqlStringCommand("Select * from Promotable where Promocode=@Promocode");
           db.AddInParameter(cmd, "@Promocode", DbType.String, Promocode);
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;

       }

       public string SelCatIDbycategoryForsearch(string cat)
       {
           DbCommand cmd = db.GetSqlStringCommand("select catid from  CategoryTable where category=@cat");
           db.AddInParameter(cmd, "@cat", DbType.String, cat);

           object obj = db.ExecuteScalar(cmd);

           if (obj == null || System.DBNull.Value == obj)
           {
               return "0";
           }
           else
           {
               return obj.ToString();
           }

         
       }
       public DataSet SelSearch(string searchtext)
       {

          // DbCommand cmd = db.GetSqlStringCommand("select * from itemdetailtable where ([categoryname] LIKE '%" + searchtext + "%' OR [code] LIKE '%" + searchtext + "%' OR [detail] LIKE '%" + searchtext + "%' OR [name] LIKE '%" + searchtext + "%') ORDER BY  CONVERT(int, price) - CONVERT(int, offerPrice)");

           DbCommand cmd = db.GetSqlStringCommand("select *  from itemdetailtable WHERE (categoryname LIKE '%" + searchtext + "%') AND (IsVisible <> 'False') OR  (code LIKE '%" + searchtext + "%') AND (IsVisible <> 'False') OR  (Detail LIKE '%" + searchtext + "%') AND (IsVisible <> 'False') OR (name LIKE '%" + searchtext + "%') AND (IsVisible <> 'False')ORDER BY CONVERT(int, price) - CONVERT(int, offerPrice)");
           db.AddInParameter(cmd, "@searchtext", DbType.String, searchtext);
           DataSet ds = db.ExecuteDataSet(cmd);
           return ds;

       }

        public DataSet SelectProductRandam()
        {
            DbCommand cmd = db.GetSqlStringCommand("SELECT TOP (12) id, categoryname, code, name, image, price, offerPrice, Detail, productprice, BestSellers, DailyDeal, IsNew FROM  itemdetailtable ORDER BY NEWID()");
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;
        }
       
          public DataSet getuserorderbyid(string userid)
        {
            DbCommand cmd = db.GetSqlStringCommand("SELECT  OrderNo, DOP, amount, Discount, UsedLoyalty, MainAmount FROM    dbo.finalordertable where username=@ss order by dop desc");
            db.AddInParameter(cmd, "@ss", DbType.String, userid);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;
        }
          public DataSet orderSearch(string id,string searchtext)
          {

              DbCommand cmd = db.GetSqlStringCommand("SELECT  OrderNo, DOP, amount, Discount, UsedLoyalty, MainAmount FROM    dbo.finalordertable where username=@ss and ([orderno] LIKE '%" + searchtext + "%') order by dop desc");
              db.AddInParameter(cmd, "@ss", DbType.String, id);
              DataSet ds = db.ExecuteDataSet(cmd);
              return ds;

          }

          public DataSet getorderdetailbyorderno(string uid, string orderno)
          {
              DbCommand cmd = db.GetSqlStringCommand("SELECT     dbo.PurChaseTable.quantity, dbo.PurChaseTable.buyer, dbo.PurChaseTable.price, dbo.itemdetailtable.name, dbo.itemdetailtable.categoryname, dbo.PurChaseTable.Orderno FROM         dbo.PurChaseTable INNER JOIN     dbo.itemdetailtable ON dbo.PurChaseTable.itemid = dbo.itemdetailtable.id WHERE     (dbo.PurChaseTable.Orderno = @ord) AND (dbo.PurChaseTable.buyer = @uid)");
              db.AddInParameter(cmd, "@uid", DbType.String, uid);
              db.AddInParameter(cmd, "@ord", DbType.String, orderno);
              DataSet ds = db.ExecuteDataSet(cmd);
              return ds;
          }

          public int getorderstatus(string orderid)
          {
              DbCommand cmd = db.GetSqlStringCommand("select dispatch from finalordertable where orderno=@ord");
              db.AddInParameter(cmd, "@ord", DbType.String, orderid);
             object i=db.ExecuteScalar(cmd);
             if (i == null || System.DBNull.Value == i)
             {
                 return 0;
             }
             else
             {
                 return Convert.ToInt32(i);
             }
          }

          public string setorder_as_cancel(string orderid, string user_id, string res,string crm_incharge)
          {
              string cancelid=GetMaxCancelID();
             
              string currentdate = DateTime.Now.ToString();
              DbCommand cmd1 = db.GetSqlStringCommand("INSERT INTO [crm_cancel] ([req_id]   ,[orderno]    ,[user_id]   ,[dateofcancelreq]   ,[reason_cancel]  ,[status]  ,[crm_incharge_id]  ,[date_of_cancel])     VALUES(@req_id,@orderno,@user_id,@dateofcancelreq,@reason_cancel,'1',@crm_incharge_id,'')");
              db.AddInParameter(cmd1, "@req_id", DbType.String, cancelid);
              db.AddInParameter(cmd1, "@orderno", DbType.String, orderid);
              db.AddInParameter(cmd1, "@user_id", DbType.String, user_id);
              db.AddInParameter(cmd1, "@dateofcancelreq", DbType.String, currentdate);
              db.AddInParameter(cmd1, "@reason_cancel", DbType.String, res);
              db.AddInParameter(cmd1, "@crm_incharge_id", DbType.String, crm_incharge);
              db.ExecuteNonQuery(cmd1);
           

              DbCommand cmdupd = db.GetSqlStringCommand("UPDATE [finalordertable]   SET      [Dispatch] =1 WHERE orderno=@order");
              db.AddInParameter(cmdupd, "@order", DbType.String, orderid);
              db.ExecuteNonQuery(cmdupd);

              return cancelid;


          }

          public DataSet SelcancelProductsbydate(string dt1, string dt2)
          {
              DbCommand cmd = db.GetSqlStringCommand("SELECT  * FROM finalordertable where (dop between @dt1 and @dt2) and dispatch=1  order by dop desc");
              db.AddInParameter(cmd, "@dt1", DbType.String, dt1);
              db.AddInParameter(cmd, "@dt2", DbType.String, dt2);
              return db.ExecuteDataSet(cmd);

          }

       /// <summary>
       /// ///////Navin 2-5-14
       /// </summary>
       /// <returns></returns>
          public string SelectMaxForCat()
          {
              DbCommand cmd = db.GetSqlStringCommand("select max(sortid) from CategoryTable");
              string str = db.ExecuteScalar(cmd).ToString();
             

              if (string.IsNullOrEmpty(str) | str == "0")
              {
                  return "0";
              }
              else
              {
                  return str;
              }


           

          }


          public bool chkCatSortid(string   sortid)
          {
              string str = null;

              DbCommand cmd = db.GetSqlStringCommand("Select catid from CategoryTable where sortid=@sortid");
              db.AddInParameter(cmd, "@sortid", DbType.String, sortid);
              str = db.ExecuteScalar(cmd).ToString();

            //  str = SqlHelper.ExecuteScalar(System.Data.CommandType.Text, "select catid from CategoryTable where sortid=@sortid", new SqlParameter("@sortid", srtid));
              if (string.IsNullOrEmpty(str) | str == "0")
              {
                  return true;
              }
              else
              {
                  return false;
              }
          }


          public DataSet SelCatbysrtid(string sortid)
          {
              DbCommand cmd = db.GetSqlStringCommand("select * from CategoryTable where sortid >=@sortid order by sortid");
              db.AddInParameter(cmd, "@sortid", DbType.Decimal, sortid);
              return db.ExecuteDataSet(cmd);

              
          }

          public string UpdateSortid(string sortid, string catid)
          {
              DbCommand cmd = db.GetSqlStringCommand("Update CategoryTable set sortid =@sortid where catid =@catid");
              db.AddInParameter(cmd, "@sortid", DbType.String, sortid);
              db.AddInParameter(cmd, "@catid", DbType.String, catid);

              return db.ExecuteNonQuery(cmd).ToString();



              
          }

          public string updCatVisibility(string IsVisible, string catid)
          {
              DbCommand cmdupd = db.GetStoredProcCommand("SpUpdateCategoryVisibility");
              db.AddInParameter(cmdupd, "@catid", DbType.String, catid);
              db.AddInParameter(cmdupd, "@IsVisible", DbType.Boolean, IsVisible);

              try
              {
                  return db.ExecuteNonQuery(cmdupd).ToString();
              }
              catch (Exception err)
              {

                  return err.ToString();
              }

            



          }

          public DataSet SelCatforGroup()
          {
              DbCommand cmd = db.GetSqlStringCommand("select * from CategoryTable");
          
              return db.ExecuteDataSet(cmd);


          }

          public DataSet SelGrpProductforcat(string productID, string categoryname)
          {
              DbCommand cmd = db.GetSqlStringCommand("select * from itemdetailtable where categoryname=@categoryname and id <> @productID");
              db.AddInParameter(cmd, "@categoryname", DbType.String, categoryname);
              db.AddInParameter(cmd, "@productID", DbType.String, productID);

              return db.ExecuteDataSet(cmd);


          }

          public void insertGroupProducts(string productid, string groupproductid)
          {

              if (CheckGroupProducts(productid, groupproductid) == false)
              {
                  DbCommand cmd = db.GetSqlStringCommand("Insert into tblgroupproducts(productID,groupProductID) Values(@productid,@groupproductid) Insert into tblgroupproducts(productID,groupProductID) Values(@groupproductid,@productid)");
                    db.AddInParameter(cmd, "@productid", DbType.String, productid);
                      db.AddInParameter(cmd, "@groupproductid", DbType.String, groupproductid);
                      db.ExecuteNonQuery(cmd).ToString();
                  

              }

          }



          public bool CheckGroupProducts(string productid, string groupproductid)
          {

              DbCommand cmd = db.GetSqlStringCommand("Select count(*) from tblgroupproducts where productID=@productid and groupProductID=@groupproductid");
              db.AddInParameter(cmd, "@productid", DbType.String, productid);
              db.AddInParameter(cmd, "@groupproductid", DbType.String, groupproductid);
              object obj = db.ExecuteScalar(cmd);

              if (obj.ToString() == "0" || obj == null)
              {
                  return false;
              }
              else
              {
                  return true;
              }

          }


          public void deleteGroupProducts(string productID, string groupProductID)
          {

              DbCommand cmd = db.GetSqlStringCommand("delete from tblgroupproducts where productid=@productID and groupproductid=@groupProductID delete from tblgroupproducts where productid=@groupProductID and groupproductid=@productID");
              db.AddInParameter(cmd, "@productID", DbType.String, productID);
              db.AddInParameter(cmd, "@groupProductID", DbType.String, groupProductID);

              db.ExecuteNonQuery(cmd).ToString();

         

          }

          public DataSet SelGrpProductShow(string productID)
          {
              DbCommand cmd = db.GetStoredProcCommand("spSelectGroupproduct");

              db.AddInParameter(cmd, "@productID", DbType.String, productID);//

              return db.ExecuteDataSet(cmd);


          }


          public DataSet SelIndexProduct()
          {
              DbCommand cmd = db.GetStoredProcCommand("SpGetIndex");

             

              return db.ExecuteDataSet(cmd);


          }

          public DataSet Selecttopproduts()
          {
              DbCommand cmd = db.GetStoredProcCommand("SpgetTopProducts");
                            
              return db.ExecuteDataSet(cmd);


          }

          public string checkrole(string Username)
          {

              DbCommand cmd = db.GetSqlStringCommand("Select role from admintable where uname=@Username");

              db.AddInParameter(cmd, "@Username", DbType.String, Username);//
              object obj =  db.ExecuteScalar(cmd);   

              if (obj == null || obj.ToString() == "0")
              {
                  return "0";
              }
              else
              {
                  return obj.ToString();
              }
          }


          public DataSet SelectCatdescription(string catid)
          {


              DbCommand cmd = db.GetSqlStringCommand("select * from CategoryTable where catid=@catid and  IsVisible='True'");
              db.AddInParameter(cmd, "@catid", DbType.String, catid);
              DataSet ds = db.ExecuteDataSet(cmd);
              return ds;


          }

          public DataSet SelAllUserDtl()
          {
              DbCommand cmd = db.GetSqlStringCommand("SELECT  UserName,Name,Email,Phone ,Address,City,State,ZipCode,AltPhone as AlternateNo,Country,registerdate as DateOfRegistration FROM UserTable  order by uid desc");

              return db.ExecuteDataSet(cmd);
          }
      

    }
}

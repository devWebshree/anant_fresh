﻿<%@ Page Title="Cancellation Policy" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="CancellationPolicy.aspx.cs" Inherits="RefundPolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="row">
<h4 class="title1 pd">
Cancellations Policy
</h4>

<h4 class="title2 m10">Cancellations by AnantFresh:</h4>

<p>
While, we try to ship out every order that has been successfully placed with anantfresh.com, there are

some situations in which we may have to cancel orders. <br /><br />

Some of the situations that may result in your order being cancelled include (but not limited to):</p>




<ol class="ol">

<li>Product No Longer Available With Our Merchant Or Temporarily Out Of Stock</li>
<li>Limitations On Quantities Available For Purchase</li>
<li>Restrictions On Quantity Or Number Or Orders As Per T&C</li>
<li>Established defect in the product which the seller is not able to either replace or repair.</li>
<li>Problems Identified By Our Credit And Fraud Avoidance Department</li>
<li>Inaccurate Or Insufficient Address</li>
<li>Non-Serviceable Locations</li>
</ol>
<div class="clear"></div>
<p>

At times we may also require additional verifications or information before accepting any order. We

will contact you if all or any portion of your order is cancelled or if additional information is required to 

accept your order.
</p>
    
<div class="clear"></div>
<h4 class="title2 m10">Cancellations by the Customer:</h4>
<p>You can cancel your order online before the product has been shipped. We will be more than happy
to cancel the Order. We will not be able to cancel those orders that have already been processed and 
shipped out by us. 100% refund if delivered product has expired and damage within 24 hours in refund 
policy. After 24 hours no refund; No cancellation & only exchange with 50 rupees shipping charge.
</p>
<div class="clear"></div>
<h4 class="title2 m10">How long will it take to process my Cancellation Request?</h4>
<p>
Once you request the cancellation of item(s) in your order, it will take us a maximum of 1-2 business 

days to cancel the order and initiate a refund. You will be notified of the same by contact details as with 

us.</p>
</div>
</asp:Content>


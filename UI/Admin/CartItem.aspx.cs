﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using System.Data;
using BAL;
using DAL;
public partial class Admin_CartItem : System.Web.UI.Page
{
    dlsearch objsrch = new dlsearch();
    modifyData ad = new modifyData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddata();
        }
    }

    public void binddata()
    {
        DataSet ds = objsrch.getAllCartItem();
        if (ds.Tables[0].Rows.Count > 0)
        {
            GridView1.DataSource = ds;
            GridView1.DataBind();
        
            //imgbtn.Visible = true;
            //TABLE1.Visible = true;
            //TABLE2.Visible = false;
        }
        else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
        }

    }
    public string GetImg(object itemid)
    {
        return "~/imgform.aspx?img=";// +ad.getimagename(itemid.ToString()) + "&itmid=1";

    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        Label pid = (Label)row.FindControl("lblPid");
        ad.DeletePurchaseItem(pid.Text);
        binddata();
    }

    protected void txtqty_TextChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            TextBox txtQuantity = (TextBox)row.FindControl("txtqty");
            Label pid = (Label)row.FindControl("lblPid");
            Label lblitrm = (Label)row.FindControl("lblitemid");
            Label lblstock = (Label)row.FindControl("lblstock");
            object obj = lblitrm.Text;
            lblstock.Text = GetStock(obj, txtQuantity.Text.ToString());

            ad.UpdatePurchaseItem(pid.Text, txtQuantity.Text, Session["IsUser"].ToString());

        }
    }

    public string GetStock(object itm, object qty)
    {


        Int32 @int = Convert.ToInt32(ad.SetQty(itm.ToString()));
        if (@int >= Convert.ToInt32(qty))
        {
            return "In Stock";
        }
        else
        {
            if (@int > 0)
            {
                return "Stock Available (" + @int + ")";
            }
            else
            {
                return "Sold Out";
            }
        }



    }
}
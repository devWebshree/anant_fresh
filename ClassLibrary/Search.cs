﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary
{
   public class Search
    {
        public string CatID { get; set; }
        public string SubCatID { get; set; }
        public string SearchBy { get; set; }
        public string Action { get; set; }
        public string ProductID { get; set; }
        public string Orderby { get; set; } 
        public string SortPrefix { get; set; }
        public string ToDate { get; set; } 
        public string FromDate { get; set; }
        public bool Visibilty { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BAL
{
   public class CrmManagement
   {
       ManageData insert = new ManageData();
       
       public bool checkadmin(string uname, string password)
       {
           return insert.checkcrm(uname, password);
       }

       public DataSet getuserorderbyid(string id)
       {
           return insert.getuserorderbyid(id);
       }

       public DataSet registerdata(string id)
       {
           return insert.registerdata(id);
       }
       public DataSet orderSearch(string id,string serc)
       {
           return insert.orderSearch(id,serc);
       }

       public DataSet getorderdetailbyorderno(string uid,string orderno)
       {
           return insert.getorderdetailbyorderno(uid, orderno);
       }

       public int getorderstatus(string orderid)
       {
           return insert.getorderstatus(orderid);
       }
       public string GetMaxCancelID()
       {
           return insert.GetMaxCancelID();
       }
       public string setorder_as_cancel(string id, string user_id, string res, string crm_incharge)
       {
           return insert.setorder_as_cancel(id, user_id, res, crm_incharge);
       }

       public void Insertcrm_puchase(string order_id)
       {
           insert.Insertcrm_puchase(order_id);
       }

       public bool selorderfromcrm(string order)
       {
         return   insert.selorderfromcrm(order);
       }
    }
}

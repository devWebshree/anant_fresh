﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ClassLibrary;
using BAL;
using DAL;
public partial class control_searchbykeyword : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    Search srch = new Search();
    dlsearch objsrch = new dlsearch();
    public Utility all = new Utility();
    dlSubCategory objscat = new dlSubCategory();
    string SCatID, CatId, txt,currentShopUser;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Session["ShopUser"] != "0")
            {
                currentShopUser = Session["ShopUser"].ToString();
            }
            bindCategoryName();
            show();
            if (Session["searchText"] != null)
            {
                txtsrch.Text = Session["searchText"].ToString();
                show();
            }


        }
      

       
    }




    protected void ddlCategoryName_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindSubCategoryName();
        show();
    }

    protected void ddl_subCat_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }

    protected void ddlSortProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }


    public void bindCategoryName()
    {
        DataSet ds = ad.CategoryForProduct();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCategoryName.DataSource = ds;
            ddlCategoryName.DataTextField = "category";
            ddlCategoryName.DataValueField = "catid";
            ddlCategoryName.DataBind();
            ddlCategoryName.Items.Insert(0, "All");
        }
        else
        {
            ddlCategoryName.DataSource = null;
            ddlCategoryName.DataBind();
        }
    }
    public void bindSubCategoryName()
    {

        DataSet ds = objscat.getSubCategoryNameList(ddlCategoryName.SelectedItem.Value);//Convert.ToInt32(ddl_subCat.SelectedIndex)
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddl_subCat.DataSource = ds;
            ddl_subCat.DataTextField = "SubCategory";
            ddl_subCat.DataValueField = "subcatid";
            ddl_subCat.DataBind();
            ddl_subCat.Items.Insert(0, "All");
        }
        else
        {
            ddl_subCat.DataSource = null;
            ddl_subCat.DataBind();
        }

    }





    protected void lnkgo_Click(object sender, EventArgs e)
    {
        Session["searchText"] = txtsrch.Text;
        show();
    }

    public void bindAccordingSerach()
    {

        try
        {

            srch.SearchBy = txtsrch.Text.Trim();
            txt = "<b>Search by:</b> " + srch.SearchBy;
            if (ddlCategoryName.SelectedItem.Value == "All")
            {
                srch.CatID = "0";
            }
            else
            {
                srch.CatID = ddlCategoryName.SelectedItem.Value;
            }

            if (ddl_subCat.SelectedItem.Value == "All")
            {
                srch.SubCatID = "0";
            }
            else
            {
                srch.SubCatID = ddl_subCat.SelectedItem.Value;
            }



            srch.Orderby = ddlSortProduct.SelectedItem.Value;
            


            hl_cat.Text = txt;
        }
        catch (Exception ex) { }

    }


    public void show()
    {
        bindAccordingSerach();
        DataSet ds = objsrch.itemSearchList(srch);
        if (ds.Tables[0].Rows.Count > 16)
        {
            string pg = Request.QueryString["Page"];
            if (!string.IsNullOrEmpty(pg))
            {

                if (pg != "1")
                {
                    Int32 cnt = default(Int32);
                    if (ds.Tables[0].Rows.Count > 16 * Convert.ToInt32(pg))
                    {
                        cnt = 16 * Convert.ToInt32(pg);
                    }
                    else
                    {
                        cnt = ds.Tables[0].Rows.Count;
                    }
                    //  lblcol1.Text = "Showing " + (16 * (Convert.ToInt32(pg) - 1) + 1) + " - " + cnt + " of " + ds.Tables[0].Rows.Count + " Product";
                    lblcol2.Text = "Showing " + (16 * (Convert.ToInt32(pg) - 1) + 1) + " - " + cnt + " of " + ds.Tables[0].Rows.Count + " Product(s)";
                }
                else
                {
                    //  lblcol1.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product";
                    lblcol2.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product(s)";
                }
            }
            else
            {
                //lblcol1.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product";
                lblcol2.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product(s)";
            }
            CollectionPager1.Visible = true;
            CollectionPager1.DataSource = ds.Tables[0].DefaultView;
            CollectionPager1.BindToControl = DataList2;
            //Collectionpager2.Visible = true;
            //Collectionpager2.DataSource = ds.Tables[0].DefaultView;
            //Collectionpager2.BindToControl = DataList2;
            DataList2.DataSource = CollectionPager1.DataSourcePaged;
        }
        else
        {
            //lblcol1.Text = "Showing  " + ds.Tables[0].Rows.Count + " Product";
            lblcol2.Text = "Showing  " + ds.Tables[0].Rows.Count + " Product(s)";
            CollectionPager1.Visible = false;
            //Collectionpager2.Visible = false;
            DataList2.DataSource = ds;
            DataList2.DataBind();
        }

    }
    public bool setVisiable(object obj)
    {
        if (obj.ToString() == "")
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("categories.aspx");
    }


    protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
    {

        if (e.CommandName == "book")
        {
            TextBox txtQuantity = ((TextBox)e.Item.FindControl("txtQuantity"));
            Label lblitemid = ((Label)e.Item.FindControl("lblid"));
            Label lblname = ((Label)e.Item.FindControl("lblname"));
            Label ablStock = ((Label)e.Item.FindControl("lbavlQty"));
            Label lblCode =((Label)e.Item.FindControl("lblCode"));
            Label price = ((Label)e.Item.FindControl("lblPrice"));
            Label offerprice = ((Label)e.Item.FindControl("lblOfferPrice"));
            Label lblWholeSalePrice = ((Label)e.Item.FindControl("lblWholeSalePrice"));
            string amount = "0";
            if (Session["UserType"] != "0")
            {
                if (Session["UserType"].ToString() == "2")
                {
                        amount = lblWholeSalePrice.Text;
                }
                if (Session["UserType"].ToString() == "1")
                {
                   
                    if (offerprice.Text == "0.00")
                    {
                        amount = price.Text;
                    }
                    else
                    {
                        amount = offerprice.Text;
                    }
                }
                if (amount != "" || amount != "" || amount == null)
                {
                    if (ad.IsPrdctExistCart(lblitemid.Text, Session["ShopUserId"].ToString(), Session["UserType"].ToString()) == false)
                    {

                        ad.inserProduct(lblitemid.Text, txtQuantity.Text, Session["ShopUserId"].ToString(), lblitemid.Text, "", lblname.Text, amount, Session["UserType"].ToString());
                        Session["Itemname"] = "Successfully Added " + lblname.Text + " in Cart";
                    }
                    else
                    {
                        ad.UpdatePurchaseItem(lblitemid.Text, txtQuantity.Text, Session["ShopUserId"].ToString());
                        Session["Exist"] = lblname.Text + " Item is already Exist!";
                    }
                   
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
                    JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, "Successfully Added " + lblname.Text + " in cart", 5000);
                }
                show();


            }
        }
      
       

    }
    public string getQTY(object obj)
    {

        return ad.SetQty(obj.ToString());

    }
    protected void lnkbtn_Click(object sender, EventArgs e)
    {
        Response.Redirect("view-cart.aspx");
    }
    protected void lnkChangPassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("change-password.aspx");
    }
    protected void DataList2_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem | e.Item.ItemType == ListItemType.Item)
        {

            Panel pnlretailPrice = ((Panel)e.Item.FindControl("pnlretailPrice"));
            Panel pnlWholeSalePrice = ((Panel)e.Item.FindControl("pnlWholeSalePrice"));
            Label lblWholeSalePrice = ((Label)e.Item.FindControl("lblWholeSalePrice"));
            Label price = ((Label)e.Item.FindControl("lblPrice"));
            Label offerprice = ((Label)e.Item.FindControl("lblOfferPrice"));

          

            if (Session["UserType"] != "0")
            {
                if (Session["UserType"].ToString() == "2")
                {
                    pnlretailPrice.Visible = false;
                    pnlWholeSalePrice.Visible = true;
                }
                if (Session["UserType"].ToString() == "1")
                {

                    pnlretailPrice.Visible = true;
                    pnlWholeSalePrice.Visible =false;
                    if (offerprice.Text == "0" || offerprice.Text == "0.00")
                    {
                        price.Font.Strikeout = false;
                        offerprice.Visible = false;

                    }
                    else
                    {
                        price.Font.Strikeout = true;
                        offerprice.Visible = true;
                    }
                }

                if (price.Text == offerprice.Text)
                {
                    price.Font.Strikeout = false;
                    offerprice.Visible = false;

                }
            }
        }


    }


    protected void lbParentAddToCart_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["UserType"] != "0")
            {
                foreach (DataListItem x in DataList2.Items)
                {
                    CheckBox c1 = (CheckBox)x.FindControl("cb_select");
                    if (c1.Checked)
                    {
                        Label itemid = (Label)x.FindControl("lblid");
                        TextBox lblStockAvailbility = (TextBox)x.FindControl("lblStockAvailbility");
                        TextBox txtQuantity = ((TextBox)x.FindControl("txtQuantity"));
                        Label lblitemid = ((Label)x.FindControl("lblid"));
                        Label lblname = ((Label)x.FindControl("lblname"));
                        Label ablStock = ((Label)x.FindControl("lbavlQty"));
                        Label lblCode = ((Label)x.FindControl("lblCode"));
                        Label price = ((Label)x.FindControl("lblPrice"));
                        Label offerprice = ((Label)x.FindControl("lblOfferPrice"));
                        Label lblWholeSalePrice = ((Label)x.FindControl("lblWholeSalePrice"));
                        string amount = "0";

                        if (Session["UserType"].ToString() == "2")
                        {
                            amount = lblWholeSalePrice.Text;
                        }
                        if (Session["UserType"].ToString() == "1")
                        {

                            if (offerprice.Text == "0.00")
                            {
                                amount = price.Text;
                            }
                            else
                            {
                                amount = offerprice.Text;
                            }
                        }

                        if (amount != "" || amount != "0" || amount == null)
                        {
                            if (ad.IsPrdctExistCart(lblitemid.Text, Session["ShopUserId"].ToString(), Session["UserType"].ToString()) == false)
                            {
                                ad.inserProduct(lblitemid.Text, txtQuantity.Text, Session["ShopUserId"].ToString(), lblitemid.Text, "", lblname.Text, amount, Session["UserType"].ToString());
                            }
                            else
                            {
                                ad.UpdatePurchaseItem(lblitemid.Text, txtQuantity.Text, Session["ShopUserId"].ToString());
                            }
                        }

                    }

                }
            }
            show();
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
            JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, "Successfully Added in cart", 5000);
        }
        catch (Exception ex)
        {
            show();
        }
    }
}

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="orderList.ascx.cs" Inherits="admin_controls_orderDetail" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc2" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"    TagPrefix="rjs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>




<div class="row m10">
    <div class="registerdiv clearfix" style="width: 93%;">




<div class="row">
<div class="ProductTitle"><h2 class="left">ORDER LIST</h2> 

<asp:LinkButton ID="lb_print" runat="server" OnClientClick="window.print();" CssClass="button right">Print</asp:LinkButton>


<asp:LinkButton ID="LinkButton1" runat="server" 
  onclick="LinkButton1_Click" Visible="False" CssClass="button right">Generate Excel</asp:LinkButton></div>
</div>

<div class="clear"></div>
<div class="row">

<asp:DataList ID="dlst" runat="server" CssClass="_admintbl1" 
        RepeatDirection="Horizontal" RepeatLayout="Flow">
             <ItemTemplate>
             <div class="lft">
             <asp:Label ID="lbladd" CssClass="spanhd" runat="server" Text='<%# selectaddress(Eval("orderno")) %>'></asp:Label>/
            
             </div>
             <div class="lft">
             <ul class="ordrul">
             <li>
             Order Details
             </li>
             <li>
             <span class="txt">Order Date</span>
             <span class="info"><asp:Label ID="lblodt" runat="server" Text='<%# Eval("DOP","{0:dd-MMM-yy hh:mm tt}") %>'></asp:Label></span>
             </li>
             <li>
             <span class="txt">Order No.</span>
             <span class="info"><asp:Label ID="lblord" runat="server" Text='<%# Eval("OrderNo") %>'></asp:Label></span>
             </li>
             </ul>
             </div>
      
</ItemTemplate>
</asp:DataList>
<div class="clear"></div>

<asp:DataGrid ID="dgrd" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="20" CssClass="table">
            <Columns>
                <asp:TemplateColumn HeaderText="Item" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemStyle HorizontalAlign="Center"  />
                    <ItemTemplate>
                        <asp:Label ID="lbsalpr" runat="server" Text='<%# bind("item") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>

                  <asp:TemplateColumn HeaderText="Item Code" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemStyle HorizontalAlign="Center"  />
                    <ItemTemplate>
                        <asp:Label ID="lbitmcd" runat="server" Text='<%# bind("code") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>

      <asp:TemplateColumn HeaderText="Price" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
            <ItemStyle HorizontalAlign="Center" Width="75px" />
            <ItemTemplate>
                <asp:Label ID="lblpr" runat="server" Text='<%# bind("price") %>' Width="50px"></asp:Label>
            </ItemTemplate>
        </asp:TemplateColumn>
             
                <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemStyle HorizontalAlign="Center"  />
                    <ItemTemplate>
                        <asp:Label ID="txtqty" runat="server" MaxLength="4" Text='<%# bind("quantity")  %>'
                            Width="50px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditItemStyle BackColor="#2461BF" />
            <SelectedItemStyle BackColor="#FFDEE9" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <AlternatingItemStyle BackColor="#CCCCCC" />
            <ItemStyle BackColor="White" />
            <HeaderStyle BackColor="#CCCCCC" Font-Bold="True" ForeColor="Black" 
                HorizontalAlign="Center" />
        </asp:DataGrid>

</div>
<div class="clear"></div>
<div class="row m10">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
  
</asp:UpdatePanel>
<div class="row">
<div class="column border-box">
    <table width="100%" id="tabsrch" runat="server" class="table">
     <tr>
      <td>
          <asp:TextBox ID="txtSrc" runat="server" CssClass="input" Width="90%"></asp:TextBox>
          <cc1:TextBoxWatermarkExtender ID="txtSrc_TextBoxWatermarkExtender" 
              runat="server" Enabled="True" TargetControlID="txtSrc" WatermarkText="Username / Order No.">
          </cc1:TextBoxWatermarkExtender>
         </td>
      <td><span class="l mr">From Date</span><span class="l"> <asp:TextBox ID="txtsdt" runat="server" ReadOnly="True" CssClass="input" Width="50%"></asp:TextBox></span>

      <rjs:PopCalendar ID="PopCalendar2"  runat="server" Control="txtsdt" Format="mm dd yyyy" From-Date="2009-01-01" To-Date="" To-Today="True" />

      </td>
      <td class="register-txt"><span class="l mr">To</span>
      <span class="l"><asp:TextBox ID="txtedt" runat="server" ReadOnly="True" CssClass="input" Width="60%"></asp:TextBox></span>
      <rjs:PopCalendar ID="PopCalendar1" runat="server" Control="txtedt" Format="mm dd yyyy"  From-Date="2009-01-01" To-Date="" To-Today="True" />
      </td>

          <td valign="middle">
      
         <asp:DropDownList ID="ddlThroughCall" runat="server" AutoPostBack="true" 
              CssClass="input" onselectedindexchanged="ddlThroughCall_SelectedIndexChanged" >

         </asp:DropDownList>

      
      </td>


      <td valign="middle">
      
         <asp:DropDownList ID="ddl_searchOrderStatus" runat="server" AutoPostBack="true" 
              CssClass="input" 
              onselectedindexchanged="ddl_searchOrderStatus_SelectedIndexChanged"  >
                     <asp:ListItem Value="">All Order</asp:ListItem>                    
                     <asp:ListItem Value="0">Under Process</asp:ListItem>                    
                     <asp:ListItem Value="1">Cancellation Request</asp:ListItem>
                     <asp:ListItem Value="2">Cancellation Confirmed</asp:ListItem>
                     <asp:ListItem Value="3">Order Dispatched</asp:ListItem>
         </asp:DropDownList>

      
      </td>
       <td valign="middle">
         <asp:Button ID="lnksearch" runat="server" onclick="lnksearch_Click" Text="Search" CssClass="button" style="text-align:center" />       
       </td>
    </tr>
</table>
</div>
</div>
<asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                Processing...
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/simple.gif" />
            </ProgressTemplate>
        </asp:UpdateProgress>
     
<div class="clear"> </div>
    <div class="row m20" style="display:none;">
         <asp:Label ID="lblMessage" runat="server" Text="Data Not Available to this Duration." Font-Bold="True" Visible="False"></asp:Label>
             <div class="clear"> </div>
         <asp:Label ID="lblNote" style="font-size:13px; color:#333;" runat="server" Text="Note: To open Delivery Note, please Disable Pop-up Blocker of your explorer (if any)."></asp:Label>
    </div>

<div class="clear"></div>
</div>

<div class="clear"></div>
<div class="row m20">
<div class="column border-box">
            <asp:DataGrid ID="ordgrd" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" CssClass="table" DataKeyField="pid" 
                OnItemCommand="ordgrd_ItemCommand" OnItemDataBound="ordgrd_ItemDataBound" 
                PageSize="100" onpageindexchanged="ordgrd_PageIndexChanged">
                <AlternatingItemStyle />
                <Columns>

               <asp:TemplateColumn HeaderText="Serial No." HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                        <ItemTemplate>
                          <strong> <%# Container.DataSetIndex + 1 %></strong>

                        </ItemTemplate>
                        </asp:TemplateColumn>


                    <asp:TemplateColumn HeaderText="Ord. Date" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                        <ItemTemplate>
                            <asp:Label ID="pdt" runat="server" Text='<%# Eval("dop","{0:dd-MMM-yy hh:mm tt}") %>'></asp:Label>
                            <br />
                             <asp:HyperLink ID="lnkord" runat="server" 
                                NavigateUrl='<%# "~/DispatchExecutive/orderDetails.aspx?oid=" + Eval("orderno") %>' 
                                Target="_blank" Text='<%# Eval("orderno") %>'></asp:HyperLink>


   
                        </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbladd" runat="server" 
                                Text='<%# selectaddress(Eval("orderno")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>


      <asp:TemplateColumn HeaderText="Name" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
           
            <ItemTemplate>
                <asp:Label ID="lbl_name" runat="server" Text='<%# bind("name") %>' ></asp:Label>
            </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>


                    <asp:TemplateColumn HeaderText="User" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                        <ItemTemplate>
                            <asp:Label ID="urname" runat="server" Text='<%# Eval("username") %>'></asp:Label>
                        </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Order By" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="hh" runat="server" Text='<%# checkorderby(Eval("orderno")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn Visible="false" HeaderText="Order No" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                        <ItemTemplate>
                            <asp:Label ID="lblpid" runat="server" Visible="false" Text='<%# Eval("pid") %>'></asp:Label>
                            <asp:Label ID="itemlbl" runat="server" Visible="false"></asp:Label>
                        </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateColumn>
                    
        <asp:TemplateColumn HeaderText="Order Summary" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
           
            <ItemTemplate>
 <asp:GridView ID="grdViewOrdersOfCustomer" runat="server" AutoGenerateColumns="false" 
                    DataKeyNames="orderno" CssClass="ChildGrid" Width="100%" 
                    onrowdatabound="grdViewOrdersOfCustomer_RowDataBound">
                <columns>


               <asp:TemplateField HeaderText="Serial No.">
                        <ItemTemplate>

                    <strong> <%# Container.DataItemIndex + 1 %></strong>
              

                            </ItemTemplate>
               </asp:TemplateField>



                  <asp:TemplateField  HeaderText="Available Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_iteggmCode" runat="server" Text='<%# objstk.selectQuantity(Eval("itemid").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>



                  <asp:TemplateField  HeaderText="Code" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_itemCode" runat="server" Text='<%# bind("code") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>
                  <asp:TemplateField  HeaderText="Product" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_item" runat="server" Text='<%# bind("Item") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>

                  <%--<asp:TemplateField  HeaderText="Available Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_itemQuantity" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>--%>

                  <asp:TemplateField  HeaderText="Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                       <%--     --%>
                         


                           <asp:TextBox ValidationGroup="xtx" ID="txt_quantity" ReadOnly='<%# Convert.ToInt32(Eval("flagstatus"))!=1 ? true :false  %>' runat="server" MaxLength="5" Width="50px" Text='<%# bind("Quantity") %>'  ontextchanged="txt_quantity_TextChanged" AutoPostBack="true"></asp:TextBox>
                    
                      <asp:RangeValidator ID="Rangevalidator5" runat="server" ControlToValidate="txt_quantity" CssClass="error"
                        Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999" MinimumValue="0"
                        SetFocusOnError="True" Type="Integer" ValidationGroup="xtx"></asp:RangeValidator>

                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>
                  <asp:TemplateField  HeaderText="Product Price  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                        <asp:HiddenField ID="hf_pid" runat="server" Value='<%# bind("pid") %>' />
                       <asp:Label ID="lbl_price" runat="server" Text='<%# bind("price") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>


                  <asp:TemplateField  HeaderText="Total Price  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                           <asp:Label ID="lbl_tprice" runat="server"  Text='<%# (Convert.ToInt16(Eval("Quantity"))*Convert.ToInt16(Eval("price"))).ToString() %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                 </asp:TemplateField>

                  <asp:TemplateField  HeaderText="Delete" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/Redcross.png" 
                            runat="server" onclick="ImageButton1_Click" ValidationGroup="Cancle" /> 
                    </ItemTemplate>
                    <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>



           </columns>
</asp:GridView>
            </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>


        <asp:TemplateColumn HeaderText="Purchase Amount  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
           
            <ItemTemplate>
                <asp:Label ID="lbl_pamount" runat="server" Text='<%# bind("mainamount") %>' Width="50px"></asp:Label>
            </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>


                <asp:TemplateColumn HeaderText="Shipping Amount  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
           
            <ItemTemplate>
                <asp:Label ID="lbl_ship" runat="server" Text='<%# bind("dif") %>' Width="50px"></asp:Label>
            </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText="Total Amount  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
           
            <ItemTemplate>
                <asp:Label ID="lblpr" runat="server" Text='<%# bind("amount") %>' Width="50px"></asp:Label>
            </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>


                    <asp:TemplateColumn HeaderText="Remark" Visible="false">
                        <EditItemTemplate>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtRemark" runat="server" Text='<%# Eval("remark") %>' TextMode="MultiLine"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Message for Client" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff"  Visible="false">
                    <ItemTemplate>
                        <asp:TextBox ID="txtMsgcl" runat="server" CssClass="msg_input" TextMode="MultiLine"  Text='<%# Eval("ClMsg") %>'></asp:TextBox>
                  
                    </ItemTemplate>
                    <EditItemTemplate>
                       
                    </EditItemTemplate>
                    
<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
                    
                    </asp:TemplateColumn>

               
                    <asp:TemplateColumn HeaderText="Order Status" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                     <ItemTemplate>
                    
                     <asp:DropDownList ID="ddst" runat="server" CssClass="input"  >
                         
                     <asp:ListItem Selected="True">Under Process</asp:ListItem>                    
                     <asp:ListItem  >Cancellation Request</asp:ListItem>
                     <asp:ListItem >Cancellation Confirmed</asp:ListItem>
                     <asp:ListItem>Order Dispatched</asp:ListItem>
                     </asp:DropDownList>

                     <br><br>

<asp:Label ID="lblDispatch2" runat="server" Visible="false" ></asp:Label><asp:Label ID="lblDispatch" runat="server" Text='<%# Eval("Dispatch") %>' Visible="false" ></asp:Label></span>
<asp:TextBox  placeholder="Dispatch Number"  Width="48%"  CssClass="input_2" ID="txtdno" Visible="false" runat="server" Text='<%# Eval("Dispatchno") %>' ></asp:TextBox><!--Used when courier service needed -->                      
<asp:LinkButton ID="lnkdsbt" runat="server" CommandName="UpdtSt" ValidationGroup="stupdt" CssClass="submit">Update  </asp:LinkButton>                  
</ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
                    </asp:TemplateColumn>



                    <asp:TemplateColumn HeaderText="Action" Visible="false">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkdelorders" runat="server" CommandName="deleteorder" 
                                OnClientClick="return confirm('Do you want to cancel this order?')">Order Cancel</asp:LinkButton>
                            <asp:Label ID="dis" Visible="false" runat="server" Width="50%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                                              


  <asp:TemplateColumn HeaderText="Through" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                       <asp:Label ID="lblThrough" runat="server" Text='<%# Eval("callexecutive") %>'></asp:Label>
                    </ItemTemplate>    
<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
 </asp:TemplateColumn>

   <asp:TemplateColumn HeaderText="Genrate Invoice" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>

                    
  <a href="genrateInvoice.aspx?OrderNo=<%# Eval("orderno") %>" target="_blank" Class="submit" title="Genrate Invoice">Genrate Invoice</a><br>

    <asp:HyperLink ID="lnkVieword" runat="server" CssClass="submit"
                                NavigateUrl='<%# "~/DispatchExecutive/orderDetails.aspx?oid=" + Eval("orderno") %>' 
                                Target="_blank" Text="View Details"></asp:HyperLink>

                    </ItemTemplate>    
<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
 </asp:TemplateColumn>



                </Columns>
                <HeaderStyle BackColor="#CCCCCC" BorderStyle="None" Font-Bold="True" 
                    ForeColor="#666666" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" BackColor="White" />
                <PagerStyle Mode="NumericPages" NextPageText="Next" PrevPageText="Prev" />
            </asp:DataGrid>
</div>
</div>
        
 <asp:Button ID="btnupdateselected"  CssClass="submit" runat="server" 
                Text="Update Selected Rows" Visible="False" onclick="btnupdateselected_Click" />

                </div></div>

         
         




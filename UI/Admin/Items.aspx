﻿<%@ Page Title="" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true"
    CodeFile="Items.aspx.cs" Inherits="Admin_Items" %>

<%@ Register TagPrefix="cc" Namespace="Winthusiasm.HtmlEditor" Assembly="Winthusiasm.HtmlEditor" %>
<%@ Register Src="~/Admin/controls/additem.ascx" TagName="AddItem" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
    <script type="text/javascript">

        function checkList() {

            $(document).ready(function () {
                $('.cbAll input').addClass('shiftCheck');
                $('.cbSelect input').addClass('cbSelects');

                $(".shiftCheck").change(function () {
                    if ($(this).is(':checked')) {

                        $('.cbSelects').each(function () {
                            $(this).prop('checked', true);
                        });

                    }
                    else {
                        $('.cbSelects').each(function () {
                            $(this).prop('checked', false);
                        });
                    }
                });

                $("#prodActive").addClass('active');


                $('#showSearchBox').click(showSearchBox);

                function showSearchBox() {
                    var height = 0,
        $searchBox = $('#gmailSearchBox');

                    $searchBox.css({ display: 'block' });

                    //get the height of the search box
                    height = $searchBox.height();

                    //set the offset equal to the height
                    $searchBox.css({ bottom: -height + 'px' });
                }

            });


        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    
     <asp:Panel ID="pnl_product" runat="server">


            <script type="text/javascript">
                Sys.Application.add_load(checkList);
           </script>
                <div class="clear">
                </div>
                <div class="row m20">
                    <div class="ProductTitle">
                        <h2>
                            PRODUCT LIST
                        </h2>
                       
                    </div>
                    <div style="float:right;">
                     <asp:LinkButton ID="lbExport" CssClass="button" runat="server" Text="Export Excel" OnClick="lbExport_Click" />
                    </div>
                </div>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate> 

                <div class="clear">
                </div>



                <div class="menubox">
              <div class="menu">
                            <ul class="parentul">
                                <li><a href="#">
                                    <asp:CheckBox ID="cb_all" runat="server" CssClass="cbAll" /><span class="arrow"></span></a>
                                    <ul class="childul">
                                        <li>
                                            <asp:LinkButton ID="btn_visible" runat="server" OnClick="btn_visible_Click" OnClientClick="return confirm('Are you sure?')">Show</asp:LinkButton>
                                            <asp:LinkButton ID="btn_hide" OnClientClick="return confirm('Are you sure? You want to Hide.')" runat="server"
                                                Text="Hide" OnClick="btn_hide_Click" />
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="btn_delete" OnClientClick="return confirm('Are you sure? You want to delete.')" runat="server"
                                                Text="Delete" OnClick="btn_delete_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="btn_updateItems" OnClientClick="return confirm('Are you sure? You want to update.')"
                                                runat="server" Text="Update" OnClick="btn_updateItems_Click" /></li>
                                   
                                    </ul>
                                </li>
                        
                             


                                <li><a class="middle" href="#">Visibility :<asp:DropDownList ID="ddl_Itemtype" CssClass="ddl" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddl_Itemtype_SelectedIndexChanged">
                                        <asp:ListItem Value="0">Active</asp:ListItem>
                                        <asp:ListItem Value="1">In Active</asp:ListItem>
                                    </asp:DropDownList>
                                </a></li>


                                <li><a class="middle" href="#">Stock :<asp:DropDownList ID="ddlStock" runat="server" CssClass="ddl" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlStock_SelectedIndexChanged">
                                        <asp:ListItem Value="0">All</asp:ListItem>
                                        <asp:ListItem Value="1">In Stock</asp:ListItem> 
                                        <asp:ListItem Value="2">Out Of Stock</asp:ListItem>
                                    </asp:DropDownList>
                                </a></li>



                                <li><a class="middle" href="#">Category :<asp:DropDownList ID="ddlCategoryName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryName_SelectedIndexChanged"
                                        CssClass="ddl">
                                    </asp:DropDownList>
                                </a></li>
                                <li><a class="middle" href="#">Subcategory :<asp:DropDownList ID="ddl_subCat" CssClass="ddl" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddl_subCat_SelectedIndexChanged">
                                        <asp:ListItem>All</asp:ListItem>
                                    </asp:DropDownList>
                                </a></li>

                        <li><a>Sort By :<asp:DropDownList ID="ddlSortProduct" CssClass="ddl width-1" runat="server" AutoPostBack="true" onselectedindexchanged="ddlSortProduct_SelectedIndexChanged" >               
                                 <asp:ListItem Value="3">Newest First</asp:ListItem>                   
                                 <asp:ListItem Value="1">Price--Low to High</asp:ListItem>
                                 <asp:ListItem Value="2">Price--High to Low</asp:ListItem>
                          </asp:DropDownList>
                          </a>
                         </li>

                              
                            </ul>
                        </div>
                        <div class="clear"></div>
                        <div class="menu clearfix m10">
                        <ul class="parentul">

                           <li><a href="#">Item Per Page<asp:DropDownList ID="ddlViewItem" CssClass="ddl width-1" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlViewItem_SelectedIndexChanged">
                                        <asp:ListItem>20</asp:ListItem>
                                        <asp:ListItem>40</asp:ListItem>
                                        <asp:ListItem>60</asp:ListItem>
                                        <asp:ListItem>80</asp:ListItem>
                                        <asp:ListItem>100</asp:ListItem>
                                        <asp:ListItem>200</asp:ListItem>
                                        <asp:ListItem>400</asp:ListItem>
                                        <asp:ListItem>800</asp:ListItem>
                                    </asp:DropDownList>
                                </a></li>
<asp:Panel ID="pnlTab" DefaultButton="btn_srch" runat="server">
                          <li style="width:400px;"><a >
                                    <asp:TextBox ID="txt_search" placeholder="Search by Code, name" CssClass="inputtxt" runat="server"></asp:TextBox></a></li>
                                <li>
                                    <asp:LinkButton ID="btn_srch" runat="server" Text="Search" OnClick="btn_srch_Click" /></li>
     <li><a><asp:Label ID="lblcount" runat="server" Text="Total Product(s) : 0"  Font-Bold="True"></asp:Label></a>
                                    </li>
</asp:Panel>
                           
                        </ul>
                        </div>
                </div>

        <div class="clear"></div>
                <asp:GridView ID="gvrProduct" runat="server" AutoGenerateColumns="False" CssClass="table"
                    OnRowDeleting="gvrProduct_RowDeleting" AllowPaging="True" OnPageIndexChanging="gvrProduct_PageIndexChanging"
                    DataKeyNames="id" EmptyDataText=" No item is available!!!">
                    <RowStyle HorizontalAlign="Center" Wrap="False" BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:CheckBox ID="cb_select" runat="server" CssClass="cbSelect" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Serial No.">
                            <ItemTemplate>
                                <strong>
                                    <%# Container.DataItemIndex + 1 %></strong>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Code">
                            <ItemTemplate>
                                <asp:Label ID="lblCode" runat="server" Text='<%# Bind("code") %>'></asp:Label>
                                <asp:Label ID="lblid" Visible="false" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblname" runat="server" Text='<%# Bind("name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>



                         <asp:TemplateField HeaderText="Buying Price &#8377;">
                            <ItemTemplate>
                                <asp:TextBox ID="txtBuyingprice" Width="50px" Text='<%# Eval("BuyingPrice").ToString()==""?"0":Eval("BuyingPrice")  %>' runat="server"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="rfvp1Buyingprice" runat="server" ControlToValidate="txtBuyingprice"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Enter price!!" SetFocusOnError="True"
                                    ValidationGroup="e1"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RangeValidator ID="rvp2Buyingprice" runat="server" ControlToValidate="txtBuyingprice" CssClass="error"
                                    Display="Dynamic" ErrorMessage="Invalid price!" MaximumValue="9999" MinimumValue="1"
                                    SetFocusOnError="True" Type="Double" ValidationGroup="a1"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>




                        <asp:TemplateField HeaderText="MRP Price &#8377;">
                            <ItemTemplate>
                                <asp:TextBox ID="lbledirprice" Width="50px" Text='<%# Bind("price") %>' runat="server"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="rfvp1" runat="server" ControlToValidate="lbledirprice"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Enter price!!" SetFocusOnError="True"
                                    ValidationGroup="e1"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RangeValidator ID="rvp2" runat="server" ControlToValidate="lbledirprice" CssClass="error"
                                    Display="Dynamic" ErrorMessage="Invalid price!" MaximumValue="9999" MinimumValue="1"
                                    SetFocusOnError="True" Type="Double" ValidationGroup="a1"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Retail Price &#8377; ">
                            <ItemTemplate>
                                <asp:TextBox ID="lblEditOfferprice" Width="50px" Text='<%# Bind("Offerprice") %>'
                                    runat="server"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="rfvp4" runat="server" ControlToValidate="lblEditOfferprice"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Enter offer price!!" SetFocusOnError="True"
                                    ValidationGroup="e1"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RangeValidator ID="rvp3" runat="server" ControlToValidate="lblEditOfferprice"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Invalid offer price!" MaximumValue="9999"
                                    MinimumValue="1" SetFocusOnError="True" Type="Double" ValidationGroup="a1"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>

                            <asp:TemplateField HeaderText="Wholesale Price &#8377;">
                            <ItemTemplate>
                                <asp:TextBox ID="txtWholeSalePrice" Width="50px" Text='<%# Eval("WholeSalePrice").ToString()==""?"0":Eval("WholeSalePrice") %>' runat="server"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="rfvp1WholeSalePrice" runat="server" ControlToValidate="txtWholeSalePrice"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Enter price!!" SetFocusOnError="True"
                                    ValidationGroup="e1"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RangeValidator ID="rvp2WholeSalePrice" runat="server" ControlToValidate="txtWholeSalePrice" CssClass="error"
                                    Display="Dynamic" ErrorMessage="Invalid price!" MaximumValue="9999" MinimumValue="1"
                                    SetFocusOnError="True" Type="Double" ValidationGroup="a1"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="Available Stock">
                            <ItemTemplate>
                                <asp:Label ID="lblStockAvailbility" Text='<%# Eval("Qty").ToString()==""?"0":Eval("Qty") %>' runat="server"></asp:Label>
                            
                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="Stock Alert On Availbility">
                            <ItemTemplate>
                                <asp:TextBox ID="txtStockOnAlert" Width="50px" Text='<%# Eval("alertonQuantity").ToString()==""?"0":Eval("alertonQuantity")  %>'
                                    runat="server"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="rfvStockOnAlert" runat="server" ControlToValidate="txtStockOnAlert"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Enter Quantity!!" SetFocusOnError="True"
                                    ValidationGroup="e1"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RangeValidator ID="rvStockOnAlert" runat="server" ControlToValidate="txtStockOnAlert"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999"
                                    MinimumValue="0" SetFocusOnError="True" Type="Double" ValidationGroup="a1"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Add Quantity in Stock">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAddQuantityInStock" Width="50px" Text="0" runat="server"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="rfvAddStockOnAlert" runat="server" ControlToValidate="txtAddQuantityInStock"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Enter Quantity!!" SetFocusOnError="True"
                                    ValidationGroup="e1"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RangeValidator ID="rvAddStockOnAlert" runat="server" ControlToValidate="txtAddQuantityInStock"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999"
                                    MinimumValue="0" SetFocusOnError="True" Type="Double" ValidationGroup="a1"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Image">
                            <ItemTemplate>
                                <asp:Image ID="Image1" CssClass="cartImg" runat="server" ImageUrl='<%# "~/ProductImage/" + Eval("image") %>' />
                                <asp:Label ID="lblFordelete" runat="server" Visible="false" Text='<%# setimageForDelete(Eval("image")) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Is Visible">
                            <ItemTemplate>
                                <asp:Image ID="Image3" runat="server" ImageUrl='<%# isvisible(Eval("IsVisible")) %>' />
                                <asp:Label ID="lblvisi" Visible="false" runat="server" Text='<%# Bind("IsVisible") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="Delete"
                                    Text="Delete" CssClass="button" OnClientClick="return confirm('Are you sure?')"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <a href='<%# "Items.aspx?id="+Eval("id") %>' class="button">E<br />
                                    D<br />
                                    I<br>T</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#CCCCCC" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>







      <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="UpdatePanel1"
        runat="server">
            <ProgressTemplate>  
            <div class="progress_bg">         
                              <img src="../images/loading_car.gif" />
               </div>       
            </ProgressTemplate>
        </asp:UpdateProgress>


 </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <asp:Panel ID="pnl_itemEdit" runat="server" Visible="false">
        <div class="row">
            <div class="ProductTitle">
                <span style="float: left;">
                    <h2>
                        Edit Products</h2>
                </span><span style="float: right;"><a href="#" class="button" onclick="history.go(-1);return false;">
                    Go BackK</a> </span>
            </div>
        </div>
        <uc:AddItem ID="ctrl_EditItem" runat="server" />
    </asp:Panel>
</asp:Content>

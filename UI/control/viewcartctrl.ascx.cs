﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using BAL;
using System.Web.Mail;
using WebshreeCommonLibrary;
using ClassLibrary;
using DAL;
using System.Text;
using System.Security.Cryptography;
public partial class control_viewcartctrl : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    CrmManagement crm = new CrmManagement();
    SendMail sm = new SendMail();
    Stock stk = new Stock();
    dlStock objstk = new dlStock();
    Double OfferAfter = Convert.ToDouble(ConfigurationManager.AppSettings["OfferAfter"]);
    Double OfferValue = Convert.ToDouble(ConfigurationManager.AppSettings["OfferValue"]); 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (HttpContext.Current.User.Identity.Name != "")
                {
                    binddata();
                    lblord.Text = ad.GetMaxOrderID();
                    FillAddress();
                    total();
                }
                else
                {
                    Response.Redirect("/login/cartCheckOut");
                }
            }
            catch (Exception ex)
            {
                HandleExceptions.ExceptionLogging(ex.Source, ex.Message, false);
            }
        }

    }
    public string GetImg(object itemid)
    {
        return ad.getimagename(itemid.ToString());
    }
    public void total()
    {
        try
        {
            double i = Convert.ToDouble(ad.selectsmprice(Session["IsUser"].ToString(), "1"));
            lblAmount.Text = i.ToString();
            lblTotal.Text = i.ToString();
            if (i < OfferAfter)
            {
                pnl_cont.Visible = true;
                ltrl_more.Visible = false;
                lbl_lessprice.Text = (OfferAfter - Convert.ToDouble(lblTotal.Text)).ToString();
                lbl_shipcharge.Text = OfferValue.ToString();
                lblTotal.Text = (Convert.ToDouble(lblTotal.Text) + OfferValue).ToString();
            }
            else
            {
                pnl_cont.Visible = false;
                ltrl_more.Visible = true;
                lbl_shipcharge.Text = "0";

            }

        }
        catch (Exception ex)
        {
        }
    }
    protected void txtqty_TextChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            TextBox txtQuantity = (TextBox)row.FindControl("txtqty");
            Label lblitrm = (Label)row.FindControl("lblitemid");
            Label pid = (Label)row.FindControl("lblPid");
            object obj = lblitrm.Text;
            ad.UpdatePurchaseItem(pid.Text, txtQuantity.Text, Session["IsUser"].ToString());

        }
        total();
        binddata();

    }
    public void binddata()
    {
        DataSet ds = ad.SelPurProducts(Session["IsUser"].ToString(),"1");
        int i = ds.Tables[0].Rows.Count;
        if (ds.Tables[0].Rows.Count > 0)
        {

            GridView1.DataSource = ds;
            GridView1.DataBind();


        }
        else
        {
            Response.Redirect("/home");
        }
        lbltitem.Text = i.ToString();


    }
    protected void bntConfirmOrder_Click(object sender, EventArgs e)
    {
        try
        {
            total();
            binddata();
            foreach (GridViewRow row in GridView1.Rows)
            {
               
                TextBox txtQuantity = (TextBox)row.FindControl("txtqty");
                Label lblitrm = (Label)row.FindControl("lblitemid");
                stk.ReduceQuantity = Convert.ToInt32(txtQuantity.Text);
                stk.ItemId = Convert.ToInt32(lblitrm.Text);
                objstk.updateReduceItemQuantity(stk);

            }

            // Update order id in purchse table
            sendbyuser();
            //
            Label lbldtl = new Label();
            Label lblBillingAddress = new Label();
            DataSet ds = ad.registerdata(Session["IsUser"].ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Rows[0]["Name"].ToString();
                ds.Tables[0].Rows[0]["Email"].ToString();
                ds.Tables[0].Rows[0]["Phone"].ToString();
                ds.Tables[0].Rows[0]["Address"].ToString();

                ds.Tables[0].Rows[0]["City"].ToString();
                ds.Tables[0].Rows[0]["State"].ToString();
                ds.Tables[0].Rows[0]["Zipcode"].ToString();


                lbldtl.Text = "<font size=3><b>Shipping Address</b></font><br><table><tr><td><font size=2><b> Name :</b></font></td><td><font size=2>" + txtSname.Value + "</font></td></tr><tr><td><font size=2><b>Contact No.:</b></font></td><td><font size=2>" + txtSphone.Value + "</font></td></tr><tr><td><font size=2><b>Email:</b></font></td><td><font size=2>" + txtSemail.Value + "</font></td></tr><tr><td><font size=2><b>Address :</b></font></td><td><font size=2>" + txtSaddr.Value + "</font></td></tr><tr><td><font size=2><b>City:</b></font></td><td><font size=2>" + txtScity.Value + "</font></td></tr><tr><td><font size=2><b>State:</b></font></td><td><font size=2>" + txtSstate.Value + "</font></td></tr><tr><td><font size=2><b>Zip/Postal Code:</b></font></td><td><font size=2>" + txtSzip.Value + "</font></td></tr></table>";
                lblBillingAddress.Text = "<font size=3><b>Billing Address</b></font><br><table><tr><td><font size=2><b> Name :</b></font></td><td><font size=2>" + txtname.Value + "</font></td></tr><tr><td><font size=2><b>Contact No.:</b></font></td><td><font size=2>" + txtpno.Value + "</font></td></tr><tr><td><font size=2><b>Email:</b></font></td><td><font size=2>" + txtemail.Value + "</font></td></tr><tr><td><font size=2><b>Address :</b></font></td><td><font size=2>" + txtaddr1.Value + "</font></td></tr><tr><td><font size=2><b>City:</b></font></td><td><font size=2>" + txtcity.Value + "</font></td></tr><tr><td><font size=2><b>State:</b></font></td><td><font size=2>" + txtstate.Value + "</font></td></tr><tr><td><font size=2><b>Zip/Postal Code:</b></font></td><td><font size=2>" + txtzip.Value + "</font></td></tr></table>";
            }


            if (HttpContext.Current.User.Identity.Name != "")
            {
                if (rdobtnPOption.SelectedItem.Value == "1")
                {
                    
                    ad.InsertCheckout(lblord.Text, HttpContext.Current.User.Identity.Name, lbldtl.Text, lblBillingAddress.Text, lblDiscount.Text, "", "Online", "", lblLoyalty.Text, lblTotal.Text, lblAmount.Text);
                    AfterOnlinePayment();
                    MakePayment();
                }
                else
                {
                    ad.InsertCheckout(lblord.Text, HttpContext.Current.User.Identity.Name, lbldtl.Text, lblBillingAddress.Text, lblDiscount.Text, "", "COD", "", lblLoyalty.Text, lblTotal.Text, lblAmount.Text);
                    AfterCODPayment();
                   
                }
            }
        }
        catch (Exception ex) 
        {
            HandleExceptions.ExceptionLogging(ex.Source, ex.Message, false);
        }
    }
    private void ShowMessageLogout(string str)
    {
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "Login", ("alert('" + str + "'); window.parent.location.href='") + Page.ResolveUrl("/home") + "'", true);
    }
    public string sendbyuser()
    {
        //Dim user As String = HttpContext.Current.User.Identity.Name
        System.Data.DataSet ds = null;

        ds = ad.SelPurProducts(Session["IsUser"].ToString(),"1");



        int @int = 0;
        int i = 0;
        string ord = null;
        @int = ds.Tables[0].Rows.Count;
        ord = lblord.Text;

        if (@int > 0)
        {
            for (i = 0; i <= @int - 1; i++)
            {
                ad.UpdatePrchDtl(Session["IsUser"].ToString(), ds.Tables[0].Rows[i]["pid"].ToString(), ord);
            }
        }
        return ord;
    }
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("/product");
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        Label name = (Label)row.FindControl("lblitrm");
        Label pid = (Label)row.FindControl("lblPid");
        ad.DeletePurchaseItem(pid.Text);
        binddata();
        total();
        Session["Itemname"] = "Selected item " + name.Text + " is successfully Remove from cart!";
        //JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, Session["Itemname"].ToString(), 1000);
        Session["path"] = HttpContext.Current.Request.Url.AbsoluteUri;
        Response.Redirect(Session["path"].ToString());



    }
    protected void chksame_CheckedChanged(object sender, EventArgs e)
    {
        FillAddress();
    }
    public void FillAddress()
    {
        DataSet ds = ad.registerdata(HttpContext.Current.User.Identity.Name);

        if (ds.Tables[0].Rows.Count > 0)
        {

            txtname.Value = ds.Tables[0].Rows[0]["Name"].ToString();
            txtemail.Value = ds.Tables[0].Rows[0]["Email"].ToString();
            txtpno.Value = ds.Tables[0].Rows[0]["Phone"].ToString();
            txtaddr1.Value = ds.Tables[0].Rows[0]["Address"].ToString();
            txtcity.Value = ds.Tables[0].Rows[0]["City"].ToString();
            txtstate.Value = ds.Tables[0].Rows[0]["State"].ToString();
            txtzip.Value = ds.Tables[0].Rows[0]["Zipcode"].ToString();
            if (chksame.Checked)
            {
                txtSname.Value = txtname.Value;
                txtSphone.Value = txtpno.Value;
                txtSemail.Value = txtemail.Value;
                txtSaddr.Value = txtaddr1.Value;
                txtSstate.Value = txtstate.Value;
                txtScity.Value = txtcity.Value;
                txtSzip.Value = txtzip.Value;
            }
            else
            {
                txtSname.Value = "";
                txtSphone.Value = "";
                txtSemail.Value = "";
                txtSaddr.Value = "";
                txtSstate.Value = "";
                txtScity.Value = "";
                txtSzip.Value = "";
            }
        }



    }
    private void AfterOnlinePayment()
    {
        Label lbldtl = new Label();
        lbldtl.Text = "<font size=3><b>Shipping Address</b></font><br><table><tr><td><font size=2><b> Name :</b></font></td><td><font size=2>" + txtSname.Value +
            "</font></td></tr><tr><td><font size=2><b>Contact No.:</b></font></td><td><font size=2>" + txtSphone.Value +
            "</font></td></tr><tr><td><font size=2><b>Email:</b></font></td><td><font size=2>" + txtSemail.Value +
            "</font></td></tr><tr><td><font size=2><b>Address :</b></font></td><td><font size=2>" + txtSaddr.Value +
            "</font></td></tr><tr><td><font size=2><b>City:</b></font></td><td><font size=2>" + txtScity.Value +
            "</font></td></tr><tr><td><font size=2><b>State:</b></font></td><td><font size=3>" + txtSstate.Value +
            "</font></td></tr><tr><td><font size=2><b>Zip/Postal Code:</b></font></td><td><font size=2>" + txtSzip.Value +
            "</font></td></tr></table>";
        ad.UpdatePrchflag(lblord.Text, "1");
        DataSet ds = ad.SelectCheckOutFromOrderID(lblord.Text);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ad.InsertFinalPurchaseForCall(lblord.Text, ds.Tables[0].Rows[0]["uname"].ToString(), ds.Tables[0].Rows[0]["SAddress"].ToString(), ds.Tables[0].Rows[0]["amount"].ToString(), ds.Tables[0].Rows[0]["Discount"].ToString(), ds.Tables[0].Rows[0]["UsedLoyalty"].ToString(), ds.Tables[0].Rows[0]["MainAmount"].ToString(), "Web", "Retail","1");
          //  sm.sendViewcart(ds.Tables[0].Rows[0]["Email"].ToString(), lblord.Text, DateTime.Now.ToString(), ds.Tables[0].Rows[0]["Name"].ToString());
           // sm.sendmailTOAdmin(ds.Tables[0].Rows[0]["Email"].ToString(), lblord.Text, DateTime.Now.ToString(), "Admin");
        }
        Session["msgorderconfirm"] = lblord.Text;
       
    }


    private void AfterCODPayment() 
    {
        Label lbldtl = new Label();
        lbldtl.Text = "<font size=3><b>Shipping Address</b></font><br><table><tr><td><font size=2><b> Name :</b></font></td><td><font size=2>" + txtSname.Value +
            "</font></td></tr><tr><td><font size=2><b>Contact No.:</b></font></td><td><font size=2>" + txtSphone.Value +
            "</font></td></tr><tr><td><font size=2><b>Email:</b></font></td><td><font size=2>" + txtSemail.Value +
            "</font></td></tr><tr><td><font size=2><b>Address :</b></font></td><td><font size=2>" + txtSaddr.Value +
            "</font></td></tr><tr><td><font size=2><b>City:</b></font></td><td><font size=2>" + txtScity.Value +
            "</font></td></tr><tr><td><font size=2><b>State:</b></font></td><td><font size=3>" + txtSstate.Value +
            "</font></td></tr><tr><td><font size=2><b>Zip/Postal Code:</b></font></td><td><font size=2>" + txtSzip.Value +
            "</font></td></tr></table>";
        ad.UpdatePrchflag(lblord.Text, "1");
        DataSet ds = ad.SelectCheckOutFromOrderID(lblord.Text);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ad.InsertFinalPurchaseForCall(lblord.Text, ds.Tables[0].Rows[0]["uname"].ToString(), ds.Tables[0].Rows[0]["SAddress"].ToString(), ds.Tables[0].Rows[0]["amount"].ToString(), ds.Tables[0].Rows[0]["Discount"].ToString(), ds.Tables[0].Rows[0]["UsedLoyalty"].ToString(), ds.Tables[0].Rows[0]["MainAmount"].ToString(), "Web", "Retail", "1");
              sm.sendViewcart(ds.Tables[0].Rows[0]["Email"].ToString(), lblord.Text, DateTime.Now.ToString(), ds.Tables[0].Rows[0]["Name"].ToString());
              sm.sendmailTOAdmin(ds.Tables[0].Rows[0]["Email"].ToString(), lblord.Text, DateTime.Now.ToString(), "Admin");
        }
        Session["msgorderconfirm"] = lblord.Text;
        Response.Redirect("/OrderComplete");
    }



    public string action1 = string.Empty;
    public string hash1 = string.Empty;
    public string txnid1 = string.Empty;

    public void MakePayment()
    {
        DataSet ds = null;
        string FName = txtname.Value, PhoneNo = txtpno.Value, ProductInfo = lblord.Text, Amount = lblTotal.Text, Email, User = txtname.Value, SuccessURL = "", FailureURL = "";
        Email = HttpContext.Current.User.Identity.Name;
        // for Server
        SuccessURL = "http://www.anantfresh.com/PaymentSuccess.aspx";
        FailureURL = "http://wwww.anantfresh.com/PaymentFailed.aspx";

        // for local
        //SuccessURL = "http://localhost:56441/PaymentSuccess.aspx";
        //FailureURL = "http://localhost:56441/PaymentFailed.aspx";
        postBankData(FName, Amount, Email, PhoneNo, ProductInfo, SuccessURL, FailureURL);
    }
    public void postBankData(string fname, string amount, string email, string phone, string productinfo, string surl, string furl)
    {
        try
        {
            string[] hashVarsSeq = null;
            string hash_string = string.Empty;
            if (string.IsNullOrEmpty(Request.Form["txnid"]))
            {
                // generating txnid
                Random rnd = new Random();
                string strHash = Generatehash512(rnd.ToString() + DateTime.Now);
                txnid1 = strHash.ToString().Substring(0, 20);
            }
            else
            {
                txnid1 = Request.Form["txnid"];
            }
            if (string.IsNullOrEmpty(Request.Form["hash"]))
            {
                // generating hash value
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["MERCHANT_KEY"]) || string.IsNullOrEmpty(txnid1) || string.IsNullOrEmpty(amount) || string.IsNullOrEmpty(fname) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(phone) || string.IsNullOrEmpty(productinfo) || string.IsNullOrEmpty(surl) || string.IsNullOrEmpty(furl))
                {
                    //error

                    //frmError.Visible = True
                    // return;

                }
                else
                {
                    // frmError.Visible = False
                    hashVarsSeq = ConfigurationManager.AppSettings["hashSequence"].Split('|');
                    // spliting hash sequence from config
                    hash_string = "";
                    foreach (string hash_var in hashVarsSeq)
                    {
                        if (hash_var == "key")
                        {
                            hash_string = hash_string + ConfigurationManager.AppSettings["MERCHANT_KEY"];
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "txnid")
                        {
                            hash_string = hash_string + txnid1;
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "amount")
                        {
                            hash_string = hash_string + Convert.ToDecimal(amount).ToString("g29");
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "productinfo")
                        {
                            hash_string = hash_string + productinfo;
                            hash_string = hash_string + '|';

                        }
                        else if (hash_var == "firstname")
                        {
                            hash_string = hash_string + fname;
                            hash_string = hash_string + '|';

                        }
                        else if (hash_var == "email")
                        {
                            hash_string = hash_string + email;
                            hash_string = hash_string + '|';

                            //ElseIf hash_var = "phone" Then
                            //    hash_string = hash_string + phone.Text
                            //    hash_string = hash_string & "|"c

                            //ElseIf hash_var = "surl" Then
                            //    hash_string = hash_string + surl.Text
                            //    hash_string = hash_string & "|"c

                            //ElseIf hash_var = "furl" Then
                            //    hash_string = hash_string + furl.Text
                            //    hash_string = hash_string & "|"c


                        }
                        else
                        {
                            hash_string = hash_string + (Request.Form[hash_var] != null ? Request.Form[hash_var] : "");
                            // isset if else
                            hash_string = hash_string + '|';
                        }
                    }

                    hash_string += ConfigurationManager.AppSettings["SALT"];
                    // appending SALT
                    hash1 = Generatehash512(hash_string).ToLower();
                    //generating hash
                    // setting URL
                    action1 = ConfigurationManager.AppSettings["PAYU_BASE_URL"] + "/_payment";


                }

            }
            else if (!string.IsNullOrEmpty(Request.Form["hash"]))
            {
                hash1 = Request.Form["hash"];

                action1 = ConfigurationManager.AppSettings["PAYU_BASE_URL"] + "/_payment";
            }
            if (!string.IsNullOrEmpty(hash1))
            {
                hash.Value = hash1;
                txnid.Value = txnid1;
                key.Value = ConfigurationManager.AppSettings["MERCHANT_KEY"];
                System.Collections.Hashtable data = new System.Collections.Hashtable();
                // adding values in gash table for data post
                data.Add("hash", hash.Value);
                data.Add("key", key.Value);
                data.Add("txnid", txnid.Value);

                string AmountForm = Convert.ToDecimal(amount).ToString("g29");
                // eliminating trailing zeros
                amount = AmountForm;
                data.Add("amount", AmountForm);

                data.Add("firstname", fname.Trim());
                data.Add("email", email.Trim());
                data.Add("phone", phone.Trim());
                data.Add("productinfo", productinfo.Trim());
                data.Add("surl", surl.Trim());
                data.Add("furl", furl.Trim());
                // data.Add("lastname", lastname.Text.Trim())
                //  data.Add("curl", curl.Text.Trim())
                //  data.Add("address1", address1.Text.Trim())
                // data.Add("address2", address2.Text.Trim())
                // data.Add("city", city.Text.Trim())
                // data.Add("state", state.Text.Trim())
                // data.Add("country", country.Text.Trim())
                // data.Add("zipcode", zipcode.Text.Trim())
                //  data.Add("udf1", "")
                // data.Add("udf2", "")
                //  data.Add("udf3", "")
                // data.Add("udf4", "")
                //  data.Add("udf5", "")
                //data.Add("pg", "")


                string strForm = PreparePOSTForm(action1, data);
                Page.Controls.Add(new LiteralControl(strForm));

            }
        }
        catch (Exception ex)
        {

            Response.Write("<span style='color:red'>" + ex.Message + "</span>");
        }



    }
    private string PreparePOSTForm(string url, System.Collections.Hashtable data)
    {
        // post form
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" + formID + "\" action=\"" + url + "\" method=\"POST\">");


        foreach (System.Collections.DictionaryEntry key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + Convert.ToString(key.Key) + "\" value=\"" + Convert.ToString(key.Value) + "\">");
        }


        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." + formID + ";");
        strScript.Append("v" + formID + ".submit();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }
    public string Generatehash512(string text)
    {

        byte[] message = Encoding.UTF8.GetBytes(text);

        UnicodeEncoding UE = new UnicodeEncoding();
        byte[] hashValue = null;
        SHA512Managed hashString = new SHA512Managed();
        string hex = "";
        hashValue = hashString.ComputeHash(message);
        foreach (byte x in hashValue)
        {
            hex += String.Format("{0:x2}", x);
        }
        return hex;

    }

}

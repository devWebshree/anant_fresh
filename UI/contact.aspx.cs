﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using BLL;
using DAL;
using ClassLibrary;
using BAL;
public partial class contact : System.Web.UI.Page
{
    Contact cn = new Contact();
    SendMail sm = new SendMail();
    Enquiry enq = new Enquiry();
    Feedback feed = new Feedback();
    dlEnquiry objenq = new dlEnquiry();
    dlFeedback objfeed = new dlFeedback(); 
    dlSubCategory objscat = new dlSubCategory();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindCategories(ddl_productname);
            bindCategories(ddl_eProductName);
        }
    }

    protected void btn_sConatctus_Click(object sender, EventArgs e)
    {
        cn.Comment = txt_scomment.Text;
        cn.EmailID = txt_semail.Text;
        cn.MobileNo = txt_sphone.Text;
        cn.Name = txt_sname.Text;
        sm.sendContactMail(cn);
        JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, "Thank you for Contacting us. We will get back to you shortly. Enjoy Shopping at AnantFresh.", 5000);

     
    }

    protected void btn_eSendEnquiry_Click(object sender, EventArgs e)
    {
        
            enq.Name = txt_eName.Text;
            enq.EmailID = txt_eEmail.Text;
            enq.MobileNo = txt_eMobile.Text;
            enq.ProductName = ddl_eProductName.SelectedItem.Text;
            enq.Comment = txt_eComment.Text;
            if (objenq.insertEnquiry(enq))
            {
                sm.sendUserEnquiry(enq);
              //  lbl_eMsg.Text = "Your Enquiry successfully sent.";
                JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, "Thank you for your Enquiry on " + enq.ProductName + " We will get back to you shortly. Enjoy Shopping at AnantFresh.", 5000);

                txt_eName.Text = "";
                txt_eEmail.Text = "";
                txt_eMobile.Text = "";
                txt_eComment.Text = "";
            }
            else
            {
                //lbl_eMsg.Text = "Something wrong here, Your Enquiry is not send.";

            }

    }


    protected void bt_Feedback_Click(object sender, EventArgs e)
    {
      
            feed.Name = txt_name.Text;
            feed.EmailID = txt_eamil.Text;
            feed.MobileNo = txt_mobile.Text;
            feed.ProductName = ddl_pname.SelectedItem.Text;
            feed.ProductSubName = ddl_subcatl.SelectedItem.Text;
            feed.ProductQuality = ddl_productQuility.SelectedItem.Text;
            feed.Comment = txt_comment.Text;
            if (objfeed.insertFeedback(feed))
            {
                sm.sendFeedback(feed);
                JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, "Thank you for your feedback on " + feed.ProductName + " & your valuable time. Enjoy Shopping at AnantFresh.", 5000);
              //  lbl_feedback.Text = "Your Enquiry successfully sent.";
                txt_name.Text = "";
                txt_eamil.Text = "";
                txt_mobile.Text = "";
                txt_comment.Text = "";
            }
            else
            {
               // lbl_feedback.Text = "Something wrong here, Your Enquiry is not send.";

            }
    }


    public void bindSubCategoryName(DropDownList ddl_subCat,DropDownList ddl_catid)
    {
        DataSet ds = objscat.getSubCategoryNameList(ddl_catid.SelectedItem.Value);//Convert.ToInt32(ddl_subCat.SelectedIndex)
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddl_subCat.DataSource = ds;
            ddl_subCat.DataTextField = "SubCategory";
            ddl_subCat.DataValueField = "subcatid";
            ddl_subCat.DataBind();
            ddl_subCat.Items.Insert(0, "Select");
        }
        else
        {
            ddl_subCat.DataSource = null;
            ddl_subCat.DataBind();
        }

    }
    public void bindCategories(DropDownList productname)
    {
        modifyData ad = new modifyData();
        DataSet ds = ad.CategoryForProduct();
        if (ds.Tables[0].Rows.Count > 0)
        {
            productname.DataSource = ds;
            productname.DataTextField = "category";
            productname.DataValueField = "catid";
            productname.DataBind();
            productname.Items.Insert(0, new ListItem("Select", "0"));
        }
        else
        {
            productname.DataSource = null;
            productname.DataBind();
        }

    }


    public void bindproductname() 
    {
        dlsearch objsrch=new dlsearch();
        Search srch = new Search();
        srch.SubCatID = ddl_subcatl.SelectedItem.Value;
        srch.SearchBy = "";
        DataSet ds = objsrch.itemSearchList(srch);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddl_pname.DataSource = ds;
            ddl_pname.DataTextField = "name";
            ddl_pname.DataBind();
            ddl_pname.Items.Insert(0, new ListItem("Select", "0"));
        }
        else
        {
            ddl_pname.DataSource = null;
            ddl_pname.DataBind();
        }

    }
    protected void ddl_productname_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindSubCategoryName(ddl_subcatl, ddl_productname);
       
    }
    protected void ddl_subcatl_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindproductname();
    }
}
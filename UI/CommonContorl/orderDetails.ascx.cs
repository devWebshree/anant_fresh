using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BAL;
using DAL;
using ClassLibrary;
public partial class control_orderdtl : System.Web.UI.UserControl
{
    modifyData md = new modifyData();

    Stock stk = new Stock();
    public dlStock objstk = new dlStock();

    CallExecutive call = new CallExecutive();
    dlCallExecutive objCall = new dlCallExecutive();
    string ord;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                if (Request.QueryString["oid"] != null)
                {
                    ord = Request.QueryString["oid"].ToString();
                    binddata();
                }
                else
                {
                    Response.Redirect("orderList.aspx");
                }
            }

            
        }
    }

    public void total()
    {
        dlStock objstk = new dlStock();
        try
        {
            double i = Convert.ToDouble(objstk.selectSomeOfPriceByOrder(ord));
            lblAmount.Text = i.ToString();
            lblTotal.Text = i.ToString();
            if (i < 1004.00)
            {
                lbl_shipcharge.Text = "50";
                lblTotal.Text = (Convert.ToInt32(lblTotal.Text) + 50).ToString();
            }
            else
            {
                lbl_shipcharge.Text = "0";
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void binddata()
    {

        DataSet ds2 = objCall.selectOrderedDetails(ord);
        if (ds2.Tables[0].Rows.Count > 0)
        {
            grdViewOrdersOfCustomer.DataSource = ds2;
            grdViewOrdersOfCustomer.DataBind();
        }
        else
        {
            grdViewOrdersOfCustomer.DataSource = null;
            grdViewOrdersOfCustomer.DataBind();
        }

        DataSet ds = md.SelFinalOrdDtl(ord);
        if (ds.Tables[0].Rows.Count > 0)
        {

            dlst.DataSource = ds;
            dlst.DataBind();
        }
        else
        {
            dlst.DataSource = null;
            dlst.DataBind();
        }

        total();
    }
    protected void bcklnk_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["oid"] != null)
        {
            Response.Redirect("genrateInvoice.aspx?orderno=" + Request.QueryString["oid"].ToString() + "");
        }
    }
    public string GetDis(object st)
    {


        if (st.ToString() == "3")
        {
            grdViewOrdersOfCustomer.Enabled = false;
            return "Order Dispatched";
        }
        else if (st.ToString() == "2")
        {
            grdViewOrdersOfCustomer.Enabled = false;
            return "Order Cancelled";
        }
        else if (st.ToString() == "1")
        {
            return "Cancellation Request";
        }
        else
        {
            return "Under Process";
        }

        //if (st.ToString() == "0")
        //{
        //    return "Under Process";
        //}
        //else if (st.ToString() == "1")
        //{
        //    return "Cancel";
        //}
        //else if (st.ToString() == "2")
        //{
        //    return "Cancellation Confirmed";
        //}
        //else if (st.ToString() == "3")
        //{
        //    return "Order Verified";
        //}
        //else if (st.ToString() == "4")
        //{
        //    return "Dispatch Not confirmed";
        //}
        //else if (st.ToString() == "5")
        //{
        //    return "Dispatch confirmed";
        //}
        //else if (st.ToString() == "6")
        //{
        //    return "Order Received";
        //}
        //else if (st.ToString() == "7")
        //{
        //    return "Order Dispatched";
        //}
        //else if (st.ToString() == "8")
        //{
        //    return "Dispatch";
        //}
        //else
        //{
        //    return "Under Process";
        //}
    }


    protected void txt_quantity_TextChanged(object sender, EventArgs e)
    {


   
        var txt = (TextBox)sender;
        var rptChild = txt.NamingContainer;//Child Repeater
        HiddenField pid = (HiddenField)rptChild.FindControl("hf_pid");
        TextBox txtQuantity = (TextBox)rptChild.FindControl("txt_quantity");

        string s = txtQuantity.Text;
        Int32 a;
        if (Int32.TryParse(s, out a))
        {
            stk.NewQuantity = Convert.ToInt32(txtQuantity.Text);
            stk.PurchageId = Convert.ToInt32(pid.Value);
            objstk.updateOrderedProduct(stk);
            // objCall.UpdatePurchaseItemCall(pid.Value, txtQuantity.Text);
        
        }

        binddata();
        
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        var imgbtn = (ImageButton)sender;
        var rptChild = imgbtn.NamingContainer;//Child Repeater
        HiddenField pid = (HiddenField)rptChild.FindControl("hf_pid");
        stk.NewQuantity = 0;
        stk.PurchageId = Convert.ToInt32(pid.Value);
        objstk.updateOrderedProduct(stk);
        binddata();
      

    }
}

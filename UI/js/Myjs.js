﻿

var pageIndex = 0;
var pageCount;
$(window).scroll(function () {
  

    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
        GetRecords();
    }
});
$(function () {
    GetRecords();
});
function GetRecords() {
    pageIndex++;
    if (pageIndex == 1 || pageIndex <= pageCount) {
        $("#loader").show();
        $.ajax({
            type: "POST",
            url: "CS.aspx/GetImages",
            data: '{pageIndex: ' + pageIndex + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: OnSuccess,
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }
}
function OnSuccess(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    pageCount = parseInt(xml.find("PageCount").eq(0).find("PageCount").text());
    var images = xml.find("Images");
    var repeatColumns = parseInt("<%=dlImages.RepeatColumns == 0 ? 1 : dlImages.RepeatColumns %>");
    var rowCount = Math.ceil(images.length / repeatColumns);
    var j = 0;
    images.each(function () {
        var image = $(this);
        var row = $("[id*=dlImages] .item:last").closest("tr");
        if ($(".is_used[value='1']", row).length == repeatColumns) {
            row = $("[id*=dlImages] tr").eq(0).clone();
            $(".is_used", row).val("0");
            $(".image", row).attr("src", "");
            $(".button", row).attr("href", "");
            $(".loader", row).remove();
            $("[id*=dlImages]").append(row);
            j = 0;
        } else {
            row = $("[id*=dlImages] .item:last").closest("tr");
        }
        var cell = $(".item", row).eq(j);
        $(".name", cell).html(image.find("Name").text());
        $(".button", cell).attr("href", image.find("Url").text());
        $(".is_used", cell).attr("value", "1");
        var img = $(".image", cell);
        var loader = $("<img class = 'loader' src = 'loader.gif' />");
        img.after(loader);
        img.hide();
        img.attr("src", image.find("Url").text());
        img.load(function () {
            $(this).parent().find(".loader").remove();
            $(this).fadeIn();
        });
        j++;
    });
    $("[id*=dlImages] .is_used[value='0']").closest(".item").remove();
}
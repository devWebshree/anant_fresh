﻿<%@ Page Title="Refund Policy" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="RefundPolicy.aspx.cs" Inherits="RefundPolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="row">
<h4 class="title1 pd">
Refund Policy
</h4>
<p>
At Anant Fresh, we are very thankful to you and appreciate your purchase with us. Please read the policies, conditions and process time to time as we will update you for important information and guidelines. We make every effort to service the orders placed with us as per specifications and timelines mentioned against each product. 

Anant Fresh guarantees full satisfaction for its products.  However,  in  case  of  any  inconsistency  or  otherwise,  the company  maintains  a  responsive  refund  mechanism.  We provide refund for any purchase within 24 hours and that too without any deductions! Free door step pick up is also arranged for the same.
</p>

<ol class="ol">
<li>On Customers Request</li>
<li>The specific product/model is out of Stock.</li>
<li>The order could not be shipped within the specified timeline.</li>
<li>Any other reason beyond the control of merchants.</li>
<li>Established defect in the product which the seller is not able to either replace or repair.</li>
<li>Correct product has not been delivered.</li>
<li>Shipping address found to be untraceable, wrong, incomplete, locked and customer not available.</li>
<li>Product damaged during transit and delivery.</li>
</ol>
<div class="clear"></div>
<h4 class="title2 m10">Refund Mode & Timelines:</h4>
<p>
<strong>Step 1: </strong>Please contact us via email or call.<br />
<strong>Step 2: </strong>Our team will validate the request by checking the timelines, product type, warranty terms, etc and shall take the request for refund or exchange<br />
<strong>Step 3:</strong> Pack the product properly and label the product with the order number, return address and your address.<br />
<strong>Step 4: </strong>Our merchant will pick up the product which you want to return or replace within 24 hours and there is no any charge.<br />
</p>
<div class="clear"></div>
</div>
</asp:Content>


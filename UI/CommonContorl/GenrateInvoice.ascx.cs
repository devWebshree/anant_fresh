﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using BAL;
using System.IO;
using System.Web.Mail;
using ClassLibrary;
using DAL;

public partial class control_WebUserControl : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    SendMail sm = new SendMail();
    Stock stk = new Stock();
    dlStock objstk = new dlStock();

    CallExecutive call = new CallExecutive();
    dlCallExecutive objCall = new dlCallExecutive();
    ManageData md = new ManageData();
    static string OrderNo;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            if (Request.QueryString["OrderNo"] != null)
            {
                OrderNo = Request.QueryString["OrderNo"].ToString();
                orderDetails();
              //  total();
                bindCostumerdata();
            }
        }
    }


    public void bindCostumerdata() 
    {
        DataSet ds = objCall.selectFromPurchageTable(OrderNo);
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {

                lblOrderDateTime.Text = dr["orderID"].ToString();
                lblOrderNo.Text = dr["OrderDt"].ToString();
                lblAmount.Text = dr["MainAmount"].ToString();
                lblBillTo.Text = dr["BAddress"].ToString();
                lblShippingTo.Text = dr["SAddress"].ToString();
                lblTotal.Text = dr["amount"].ToString();
                lbl_shipcharge.Text = (Convert.ToInt32(dr["Amount"]) - Convert.ToInt32(dr["MainAmount"])).ToString();

            }
        }
    }
  
   public void orderDetails()
   {
       DataSet ds=objCall.selectOrderedDetails(OrderNo);
       if (ds.Tables[0].Rows.Count > 0)
       {
           OrdersOfCustomer.DataSource = ds;
           OrdersOfCustomer.DataBind();
       }
       else
       {
           OrdersOfCustomer.DataSource = null;
           OrdersOfCustomer.DataBind();
       }
   }
   //public void total()
   //{
   //    try
   //    {
   //        double i = Convert.ToDouble(objstk.selectSomeOfPriceByOrder(OrderNo));
   //        lblAmount.Text = i.ToString();
   //        lblTotal.Text = i.ToString();
   //        if (i < 1004.00)
   //        {
   //            lbl_shipcharge.Text = "50";
   //            lblTotal.Text = (Convert.ToInt32(lblTotal.Text) + 50).ToString();
   //        }
   //        else
   //        {
   //            lbl_shipcharge.Text = "0";
   //        }
   //    }
   //    catch (Exception ex)
   //    {
   //    }
   //}
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CallExecutive/callExec.master" AutoEventWireup="true" CodeFile="prodList.aspx.cs" Inherits="CallExecutive_prodList" %>
<%@ Register Src="~/CallExecutive/Control/ProductList.ascx" TagName="ProdList" TagPrefix="uc1" %>
<%@ Register Src="~/CallExecutive/Control/shopctrl.ascx" TagName="viewcart" TagPrefix="uc2" %>
<%@ Register Src="~/CallExecutive/Control/userReg.ascx" TagName="reg" TagPrefix="uc2" %>
<%@ Register Src="~/CallExecutive/Control/UserOrderHistory.ascx" TagName="order" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#prodActive").addClass('active');

    });



</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 


<div style="width: 25%; float:left; ">
<uc2:reg ID="ctrl_reg" runat="server" />
</div>

<div style="width:75%; float:left;">

<uc1:ProdList ID="ctrl_prodList" runat="server" /> 

</div>

<div style="width: 25%; float:left;">
<uc2:viewcart ID="ctrl_cart" runat="server" />
</div>
</asp:Content>


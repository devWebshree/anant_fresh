﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="searchbykeyword.ascx.cs"
    Inherits="control_searchbykeyword" %>
<%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>
<%@ Register Assembly="ASPnetPagerV2netfx2_0" Namespace="CutePager" TagPrefix="cc2" %>



<%@ Register Assembly="msgBox" Namespace="BunnyBear" TagPrefix="cc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc4" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>
<%@ Reference VirtualPath="~/control/chkoutctrl.ascx" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="row">
    <div class="row m10">


        <div class="directory">
            <span>
                <asp:HyperLink ID="hl_cat" runat="server"></asp:HyperLink></span><span runat="server" id="slash">/</span><span>
                    <asp:HyperLink ID="hl_scat" runat="server"></asp:HyperLink></span>
        </div>


        <div class="totalProdct">
            <asp:Label ID="lblcol2" runat="server"></asp:Label>
            <div class="menu" style="float: right;">
                <ul class="parentul">

                    <li><a style="font-family: Verdana; display: block; background: none; font-size: 12px; color: #333; font-weight: normal; border: 0px solid #ddd; padding: -1px 1px;">Sort By :<asp:DropDownList ID="ddlSortProduct" CssClass="ddl width-1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSortProduct_SelectedIndexChanged">
                        <asp:ListItem Value="3">Newest First</asp:ListItem>
                        <asp:ListItem Value="1">Price--Low to High</asp:ListItem>
                        <asp:ListItem Value="2">Price--High to Low</asp:ListItem>
                    </asp:DropDownList>
                    </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="products ">

        <asp:DataList ID="DataList2" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
            BorderWidth="0px" CellPadding="0" Width="100%" OnItemCommand="DataList2_ItemCommand"
            OnItemDataBound="DataList2_ItemDataBound" RepeatLayout="Flow">
            <ItemTemplate>
                <ul class="itemul" title="<%# Eval("name") %>">
                    <li class="border-box">
                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Detail">
                            <div class="catName" style="display: none">
                                <h5>
                                    <span class="left">Code Product</span>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Eval("code") %>'
                                        class="code"></asp:Label>
                                </h5>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="Pitem_img">
                                <div class="itemproduct">
                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/ProductImage/" + Eval("image") %>' />
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="ProductName">
                                <h5>

                                    <asp:Label ID="Label1" runat="server" Text='<%# all.sortStringLength(Eval("name"),25) %>'></asp:Label>
                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("name") %>' Visible="false"></asp:Label>
                                </h5>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="Price clearfix">
                                <span>Price MRP</span> <span>₹&nbsp;</span><asp:Label ID="lblPrice" runat="server" CssClass="cross"
                                    Text='<%# Bind("price") %>'></asp:Label>
                                <span>₹ </span>
                                <asp:Label ID="lblOfferPrice" runat="server" CssClass="new" Text='<%# Bind("offerprice") %>'></asp:Label>
                            </div>
                        </asp:LinkButton>
                        <div class="_button clearfix">
                            <div class="addtowishlist backgroundbg itembttn border-radius">
                                <asp:LinkButton ID="btnWislist" CommandName="wishlist" ValidationGroup="dtle" runat="server">
                                    <asp:Image ID="Image6" ImageUrl="~/images/star_wishlist_icon.png" AlternateText="" runat="server" />Add to Wishlist
                                </asp:LinkButton>
                            </div>
                            <div class="addtocart backgroundbg itembttn border-radius">
                                <asp:LinkButton ID="btnBook" runat="server" CommandName="book" CssClass="button">
                                    <asp:Image ID="Image7" ImageUrl="~/images/cart_btn_icon.png" runat="server" />Add to Cart
                                </asp:LinkButton>
                                </span>
                            </div>
                        </div>
                        <div>
                            <a id="df" runat="server" class="fancybox zoom1" rel="group" href='<%# "~/ProductImage/" + Eval("image") %>'
                                onclick="return hs.expand(this)" visible='<%# setVisiable(Eval("image")) %>'></a>
                        </div>

                        <div class="row">
                            <span class="code_bg">
                                <asp:Label ID="lblid" runat="server" Text='<%# Bind("id") %>' Visible="false"></asp:Label>
                            </span>
                        </div>
                        <span id="hh">Available Quantity
                        <asp:Label ID="lbavlQty" runat="server" Text='<%# getQTY(Eval("id")) %>'></asp:Label></span>
                        <span id="hh">your Quantity
                        <asp:TextBox ID="txtQuantity" runat="server" Width="40">1</asp:TextBox></span>
                        <span id="hh">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtQuantity"
                                CssClass="error" Display="Dynamic" ErrorMessage="Enter Quantity!!" SetFocusOnError="True"
                                ValidationGroup="dtle"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtQuantity"
                                CssClass="error" Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999"
                                MinimumValue="1" SetFocusOnError="True" Type="Integer" ValidationGroup="dtle">                                                                                      
                            </asp:RangeValidator>
                        </span>
                    </li>
                </ul>
            </ItemTemplate>
        </asp:DataList>

    </div>
    <%--<tr><td>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
    <td style="padding-left:10px;"> <asp:Image ID="imglg" runat="server" />
    </td><td class="process"><asp:UpdateProgress ID="UpdateProgress1" runat="server"><ProgressTemplate>
    Processing...<asp:Image ID="Image1" runat="server" ImageUrl="~/images/simple.gif" />
    </ProgressTemplate></asp:UpdateProgress>
    </td><td align="right" style="padding-right:10px;">
       </td></tr></table>
       
       </td>
  </tr>--%>
    <div class="clear"></div>
    <div class="pager m10 clearfix">
        <cc1:CollectionPager ID="CollectionPager1" runat="server" PageSize="16" SliderSize="5"
            QueryStringKey="Page" ResultsFormat="Products {0}-{1} (of {2})" ResultsLocation="None"
            ShowFirstLast="False" ShowLabel="False" BackText="« Back" ControlCssClass=""
            LabelText="Page : -" MaxPages="25" BackNextLinkSeparator="" PageNumbersSeparator=""
            UseSlider="True" BackNextLocation="Split" BackNextDisplay="HyperLinks">
        </cc1:CollectionPager>
    </div>
</div>

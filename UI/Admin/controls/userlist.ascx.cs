﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
 
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
 
using BAL;
using System.Web.Mail;
using System.IO;

public partial class admin_controls_userlist : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    SendMail sm = new SendMail();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
          
            bindUsers();
          
        }

    }
    //protected void rbltop_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (rbltop.SelectedIndex == 0)
    //    {
    //        bindUsers();
    //        pnlregister.Visible = false;
    //        txtsearch.Text = "";

    //        tblusr.Visible = true;
    //    }
    //    else
    //    {
    //        tblusr.Visible = false;

    //        txtsearch.Text = "";
    //        pnlregister.Visible = true;
    //    }
    //}

    public void bindUsers()
    {

        if (HiddenField1.Value == "0")
        {
            DataSet ds;

            ds = ad.selAllUsers();

            GridView1.DataSource = ds;
            GridView1.DataBind();
        }
        else
        {
            DataSet ds;
            ds = ad.selSearchUsersdata(txtsearch.Text);

            if (ds.Tables[0].Rows.Count > 0)
            {

                GridView1.DataSource = ds;
                GridView1.DataBind();

            }
      
        }
      
    }
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {

        HiddenField1.Value = "1";
        bindUsers();
      



    }
    protected void lnkAll_Click(object sender, EventArgs e)
    {
        HiddenField1.Value = "0";
        //rbltop.SelectedIndex = 0;
        bindUsers();
        txtsearch.Text = "";


    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        bindUsers();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow gr = GridView1.Rows[e.RowIndex];
        string username = ((Label)gr.FindControl("lblusr")).Text;
        ad.deleteUser(username);
        bindUsers();

    }
  
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        bindUsers();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        GridView1.EditIndex = -1;
        bindUsers();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow gv = GridView1.Rows[e.RowIndex];

        TextBox txtPassword = ((TextBox)gv.FindControl("txt_pass"));
     TextBox txtName = ((TextBox)gv.FindControl("TextBox1"));
     TextBox txtEmail = ((TextBox)gv.FindControl("TextBox2"));
     TextBox txtPnone = ((TextBox)gv.FindControl("TextBox3"));
     TextBox txtAddress = ((TextBox)gv.FindControl("TextBox4"));
     TextBox txtCity = ((TextBox)gv.FindControl("TextBox5"));
     TextBox txtState = ((TextBox)gv.FindControl("TextBox6"));
     TextBox txtZipCode = ((TextBox)gv.FindControl("TextBox8"));

     Label lblUname = ((Label)gv.FindControl("Label9"));
     Label lblDate = ((Label)gv.FindControl("lblEditregdate"));

     ad.updateuser(lblUname.Text, txtPassword.Text, txtName.Text, txtEmail.Text, txtPnone.Text, txtAddress.Text, txtCity.Text, txtState.Text, txtZipCode.Text, lblDate.Text);
     GridView1.EditIndex = -1;
     bindUsers();
     showmessage("successfully updated");


    }


    protected void imgExel_Click(object sender, EventArgs e)
    {
        //string attachment = "attachment; filename=Export.xls";
        //Response.ClearContent();
        //Response.AddHeader("content-disposition", attachment);
        //Response.ContentType = "application/ms-excel";
        //StringWriter sw = new StringWriter();
        //HtmlTextWriter htw = new HtmlTextWriter(sw);
        //HtmlForm frm = new HtmlForm();
        //GridView1.Parent.Controls.Add(frm);
        //frm.Attributes["runat"] = "server";
        //frm.Controls.Add(GridView1);
        //frm.RenderControl(htw);
        //Response.Write(sw.ToString());
        //Response.End();

        System.Data.DataSet ds = null;
        ds = ad.SelAllUserDtl();
        string flname = null;
        Random rnd = new Random();
        flname = "AF-User-" + rnd.Next(11, 99999).ToString() + ".xls";
        ExportDataSetToExcel(ds, flname);


    }

    public void ExportDataSetToExcel(DataSet ds, string filename)
    {
        HttpResponse response = HttpContext.Current.Response;


        response.Clear();
        response.Charset = "";


        response.ContentType = "application/vnd.ms-excel";
        response.AddHeader("Content-Disposition", "attachment;filename=\"" + filename + "\"");


        using (StringWriter sw = new StringWriter())
        {
            using (HtmlTextWriter htw = new HtmlTextWriter(sw))
            {

                DataGrid dg = new DataGrid();
                dg.DataSource = ds.Tables[0];

                dg.DataBind();
                dg.RenderControl(htw);
                response.Write(sw.ToString());
                response.End();
            }

        }
    }
   
}

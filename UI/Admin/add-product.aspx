﻿<%@ Page Title="AnantFresh:add Product" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="add-product.aspx.cs" Inherits="admin_add_product" %>

<%@ Register src="controls/additem.ascx" tagname="additem" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#addprodActive").addClass('active');  

    });
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div class="row">
 <div class="ProductTitle">
 <h2>ADD PRODUCT</h2>
  </div>
 </div>
    <uc1:additem ID="additem1" runat="server" />
</asp:Content>


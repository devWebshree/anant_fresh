﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BAL;
using System.Data;
using ClassLibrary;
using System.Web.Services;
using System.Web.Script.Services;
using ClassLibrary;
public partial class control_Category : System.Web.UI.UserControl
{

   public Utility all = new Utility();
    modifyData ad = new modifyData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {



            bindCategoryName();
        }
    }



    public void bindCategoryName()
    {
        DataSet ds = ad.CategoryForProduct();
        if (ds.Tables[0].Rows.Count > 0)
        {
            rpt_cat.DataSource = ds;
        
            rpt_cat.DataBind();
        }
        else
        {
            rpt_cat.DataSource = null;
            rpt_cat.DataBind();
        }
        
    }

 

}
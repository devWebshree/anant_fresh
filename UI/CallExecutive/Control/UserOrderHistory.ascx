﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserOrderHistory.ascx.cs"
    Inherits="CallExecutive_Control_UserOrderHistory" %>

    <div class="row m10">
    <div class="registerdiv clearfix" style="width: 93%;">
<asp:DataGrid ID="ordgrd" runat="server" AllowPaging="True" AutoGenerateColumns="False"
    CssClass="table" DataKeyField="pid" OnItemDataBound="ordgrd_ItemDataBound"
    PageSize="100" >
    <AlternatingItemStyle />
    <Columns>


        <asp:TemplateColumn HeaderText="Ord. Date" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
            <ItemTemplate>
                <asp:Label ID="pdt" runat="server" Text='<%# Eval("dop","{0:dd-MMM-yy hh:mm tt}") %>'></asp:Label>
                <br />
                <asp:Label ID="lblOrderNo" runat="server" Text='<%# Eval("orderno") %>'></asp:Label>

               

            </ItemTemplate>
            <HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>

        <asp:TemplateColumn HeaderText="Name" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
            <ItemTemplate>
                <asp:Label ID="lbl_name" runat="server" Text='<%# bind("name") %>'></asp:Label>
            </ItemTemplate>
            <HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>


        <asp:TemplateColumn HeaderText="User" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
            <ItemTemplate>
                <asp:Label ID="urname" runat="server" Text='<%# Eval("username") %>'></asp:Label>
            </ItemTemplate>
            <HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>
       
       
       
       
        <asp:TemplateColumn HeaderText="Order Summary" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
            <ItemTemplate>
                <asp:GridView ID="grdViewOrdersOfCustomer" runat="server" AutoGenerateColumns="false"
                    DataKeyNames="orderno" CssClass="ChildGrid" Width="100%">
                    <Columns>


                        <asp:TemplateField HeaderText="Product" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                            <ItemTemplate>
                                <asp:Label ID="lbl_item" runat="server" Text='<%# bind("Item") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle BackColor="#116591" ForeColor="White" />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                            <ItemTemplate>
                                <asp:Label ID="lbl_quantity" runat="server" Text='<%# bind("Quantity") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle BackColor="#116591" ForeColor="White" />
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="Product Price" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                            <ItemTemplate>
                                <asp:HiddenField ID="hf_pid" runat="server" Value='<%# bind("pid") %>' />
                                <asp:Label ID="lbl_price" runat="server" Text='<%# bind("price") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle BackColor="#116591" ForeColor="White" />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Total Price" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                            <ItemTemplate>
                                <asp:Label ID="lbl_tprice" runat="server" Text='<%# (Convert.ToInt16(Eval("Quantity"))*Convert.ToInt16(Eval("price"))).ToString() %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle BackColor="#116591" ForeColor="White" />
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>
            </ItemTemplate>
            <HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>




        <asp:TemplateColumn HeaderText="Purchage Amount" HeaderStyle-BackColor="#116591"
            HeaderStyle-ForeColor="#ffffff">
            <ItemTemplate>
                <asp:Label ID="lbl_pamount" runat="server" Text='<%# bind("mainamount") %>' Width="50px"></asp:Label>
            </ItemTemplate>
            <HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>



        <asp:TemplateColumn HeaderText="Shipping Amount" HeaderStyle-BackColor="#116591"
            HeaderStyle-ForeColor="#ffffff">
            <ItemTemplate>
                <asp:Label ID="lbl_ship" runat="server" Text='<%# bind("dif") %>' Width="50px"></asp:Label>
            </ItemTemplate>
            <HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>



        <asp:TemplateColumn HeaderText="Total Price" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
            <ItemTemplate>
                <asp:Label ID="lblpr" runat="server" Text='<%# bind("amount") %>' Width="50px"></asp:Label>
            </ItemTemplate>
            <HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>

        <asp:TemplateColumn HeaderText="Order Status" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
            <ItemTemplate>
                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("dispatch") %>' ></asp:Label>
            </ItemTemplate>
            <HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>

        <asp:TemplateColumn HeaderText="Through" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
            <ItemTemplate>
                <asp:Label ID="lblThrough" runat="server" Text='<%# Eval("callexecutive") %>'></asp:Label>
            </ItemTemplate>
            <HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>






    </Columns>
    <HeaderStyle BackColor="#CCCCCC" BorderStyle="None" Font-Bold="True" ForeColor="#666666"
        HorizontalAlign="Center" />
    <ItemStyle HorizontalAlign="Center" BackColor="White" />
    <PagerStyle Mode="NumericPages" NextPageText="Next" PrevPageText="Prev" />
</asp:DataGrid>
</div></div>
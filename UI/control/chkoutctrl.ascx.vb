Imports System.Data.OleDb
Imports BAL
Partial Class chkoutctrl
    Inherits System.Web.UI.UserControl
    Dim username As String
    Protected Sub lnkbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn.Click

        Response.Redirect("shoppingcart.aspx")

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '  If Not IsPostBack Then

        If Session("IsUser") = "0" Then
            Session("IsUser") = extra.BuyerUniqueID()
        End If
        Dim ab As New modifyData
        Dim itm As Integer = 0

        itm = ab.SelPurItem(Session("IsUser"))

        If itm > 0 Then
            lnkbtn.Enabled = True
            chklnk.Enabled = True
        Else
            lnkbtn.Enabled = False
            chklnk.Enabled = False
        End If


        lnkbtn.Text = itm & " Items"


        If HttpContext.Current.User.Identity.Name <> "" Then

            lnkusr.Text = HttpContext.Current.User.Identity.Name
            lnklgns.Text = "Log Out"
            lnkregs.Visible = False
            lval.Visible = False

        Else
            lnkusr.Text = "Guest"
            lnkregs.Visible = True
            lnklgns.Text = "Log in"
            lval.Visible = True
        End If

        '  End If
    End Sub
    Protected Sub chklnk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chklnk.Click
        If HttpContext.Current.User.Identity.Name <> "" Then
            Response.Redirect("cart")
        Else
            Response.Redirect("myaccount.aspx")
        End If
    End Sub

    Protected Sub lnklgns_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnklgns.Click
        If HttpContext.Current.User.Identity.Name <> "" Then
            Response.Redirect("/home")
        Else
            Response.Redirect("/login")
        End If


    End Sub

    Protected Sub lnkregs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkregs.Click
        Response.Redirect("/register")
    End Sub

    Protected Sub lnkusr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkusr.Click
        If HttpContext.Current.User.Identity.Name <> "" Then
            Response.Redirect("~/my-account.aspx")
        Else
            Response.Redirect("/login")
        End If

    End Sub
End Class

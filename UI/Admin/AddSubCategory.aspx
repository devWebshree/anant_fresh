﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="AddSubCategory.aspx.cs" Inherits="Admin_AddSubCategory" %>
<%@ Register Src="~/Admin/controls/addSubCategory.ascx" TagName="scat" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

 <style type="text/css">
 .close {
    display:none;
}
 </style>
<script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#addsubcatActive").addClass('active');

        $('#pnl').hide();
        $('.ProductTitle').on('click', function () {
        
            $(this).children('img').each(function () {
                $(this).toggle(0);
            });


            $('#pnl').slideToggle();
        });

    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:scat ID="Scat1" runat="server" />
</asp:Content>


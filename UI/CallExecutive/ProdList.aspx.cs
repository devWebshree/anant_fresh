﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CallExecutive_prodList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ShopUser"] != "0")
        {
            ctrl_cart.Visible = true;
            ctrl_prodList.Visible = true;
            ctrl_reg.Visible = false;

        }
        else
        {
            ctrl_cart.Visible = false;
            ctrl_prodList.Visible = false;
            ctrl_reg.Visible = true;
        }
    }
}
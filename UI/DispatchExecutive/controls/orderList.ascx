﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="orderList.ascx.cs" Inherits="admin_controls_orderDetail" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc2" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"    TagPrefix="rjs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>




<div class="row m10">
    <div class="registerdiv clearfix" style="width: 93%;">




<div class="row">
<div class="ProductTitle"><h2 class="left">ORDER LIST</h2> 

<asp:LinkButton ID="lb_print" runat="server" OnClientClick="window.print();" CssClass="button right">Print</asp:LinkButton>


<asp:LinkButton ID="LinkButton1" runat="server" 
  onclick="LinkButton1_Click" Visible="False" CssClass="button right">Generate Excel</asp:LinkButton></div>
</div>

<div class="clear"></div>

<div class="clear"></div>
<div class="row m10">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
  
</asp:UpdatePanel>
<div class="menu clearfix m10">
                        <ul class="parentul">
                          <li style="width:300px;">
                          <a> <asp:TextBox ID="txtSrc" placeholder="Order No./ Mobile/ Email/ Name " runat="server" CssClass="inputtxt"></asp:TextBox>
         
          </a>
  
                          </li>



                          <li><a>From :<asp:TextBox ID="txtsdt" runat="server"  Width="100px" ReadOnly="True" CssClass="inputtxt"></asp:TextBox></a></li>
                          <li><a><rjs:PopCalendar ID="PopCalendar2"  runat="server" Control="txtsdt" Format="mm dd yyyy" From-Date="2009-01-01" To-Date="" To-Today="True" /></a></li>
                          <li><a>To :<asp:TextBox ID="txtedt" runat="server" Width="100px" ReadOnly="True" CssClass="inputtxt" ></asp:TextBox></a></li>
                          <li><a><rjs:PopCalendar ID="PopCalendar1" runat="server" Control="txtedt" Format="mm dd yyyy"  From-Date="2009-01-01" To-Date="" To-Today="True" /></a></li>
                          <li><a>Executive :<asp:DropDownList ID="ddlThroughCall" CssClass="ddl width-1" runat="server" AutoPostBack="true" onselectedindexchanged="ddlThroughCall_SelectedIndexChanged" ></asp:DropDownList></a></li>
                          <li><a>Status :<asp:DropDownList ID="ddl_searchOrderStatus" CssClass="ddl width-1" runat="server" AutoPostBack="true" onselectedindexchanged="ddl_searchOrderStatus_SelectedIndexChanged"  >
                             <asp:ListItem Value="">All Order</asp:ListItem>                    
                             <asp:ListItem Value="0" Selected="True">Under Process</asp:ListItem>                    
                   <%--          <asp:ListItem Value="1">Cancellation Request</asp:ListItem>
                             <asp:ListItem Value="2">Cancellation Confirmed</asp:ListItem>--%>
                             <asp:ListItem Value="4">Dispatch In Process</asp:ListItem>
                             <asp:ListItem Value="3">Order Dispatched</asp:ListItem>
                          </asp:DropDownList>
                          </a></li>

                             <li><a>Purchase Type :<asp:DropDownList ID="ddlPurchaseType" CssClass="ddl width-1" runat="server" AutoPostBack="true" onselectedindexchanged="ddlPurchaseType_SelectedIndexChanged"  >               
                             <asp:ListItem Value="0">All</asp:ListItem>                    
                             <asp:ListItem Value="1" Selected="True">Retail</asp:ListItem>
                             <asp:ListItem Value="2">Wholesale</asp:ListItem>
                          </asp:DropDownList>
                          </a></li>

                          <li  id="liSales" runat="server" visible="false"><a>Salesman :<asp:DropDownList ID="ddlSalesman" CssClass="ddl width-1" runat="server" AutoPostBack="true" onselectedindexchanged="ddlSalesman_SelectedIndexChanged" ></asp:DropDownList></a></li>
                          <li><asp:LinkButton ID="lnksearch" runat="server" onclick="lnksearch_Click" Text="Search"  style="text-align:center" /></li>
                          
                          </ul>
</div>


<div class="clear"></div>
</div>

<div class="clear"></div>
<div class="row m20">
<div class="column border-box">
            <asp:DataGrid ID="ordgrd" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" CssClass="table" DataKeyField="pid" 
                OnItemCommand="ordgrd_ItemCommand" OnItemDataBound="ordgrd_ItemDataBound" 
                PageSize="100" onpageindexchanged="ordgrd_PageIndexChanged">
                <AlternatingItemStyle />
                <Columns>

               <asp:TemplateColumn HeaderText="Serial No." HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                        <ItemTemplate>
                          <strong> <%# Container.DataSetIndex + 1 %></strong>

                        </ItemTemplate>
                        </asp:TemplateColumn>


                    <asp:TemplateColumn HeaderText="Ord. Date" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                        <ItemTemplate>
                            <asp:Label ID="pdt" runat="server" Text='<%# Eval("dop","{0:dd-MMM-yy hh:mm tt}") %>'></asp:Label>
                            <br />
                             <asp:HyperLink ID="lnkord" runat="server" 
                                NavigateUrl='<%# "~/DispatchExecutive/orderDetails.aspx?oid=" + Eval("orderno") %>' 
                                Target="_blank" Text='<%# Eval("orderno") %>'></asp:HyperLink>


   
                        </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbladd" runat="server" 
                                Text='<%# selectaddress(Eval("orderno")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>


      <asp:TemplateColumn HeaderText="Name" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
           
            <ItemTemplate>
                <asp:Label ID="lbl_name" runat="server" Text='<%# bind("name") %>' ></asp:Label>
            </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>


                    <asp:TemplateColumn HeaderText="User" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                        <ItemTemplate>
                            <asp:Label ID="urname" runat="server" Text='<%# Eval("username") %>'></asp:Label>
                        </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Order By" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="hh" runat="server" Text='<%# checkorderby(Eval("orderno")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn Visible="false" HeaderText="Order No" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                        <ItemTemplate>
                            <asp:Label ID="lblpid" runat="server" Visible="false" Text='<%# Eval("pid") %>'></asp:Label>
                            <asp:Label ID="itemlbl" runat="server" Visible="false"></asp:Label>
                        </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateColumn>
                    
        <asp:TemplateColumn HeaderText="Order Summary" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
           
            <ItemTemplate>

           <div  style=" overflow:auto; Max-height:200px;">
 <asp:GridView ID="grdViewOrdersOfCustomer" runat="server" AutoGenerateColumns="false" 
                    DataKeyNames="orderno" CssClass="ChildGrid" Width="100%" 
                    onrowdatabound="grdViewOrdersOfCustomer_RowDataBound">
                <columns>


               <asp:TemplateField HeaderText="Serial No.">
                        <ItemTemplate>

                    <strong> <%# Container.DataItemIndex + 1 %></strong>
              

                            </ItemTemplate>
               </asp:TemplateField>



                  <asp:TemplateField  HeaderText="Available Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_iteggmCode" runat="server" Text='<%# objstk.selectQuantity(Eval("itemid").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>



                  <asp:TemplateField  HeaderText="Code" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_itemCode" runat="server" Text='<%# bind("code") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>
                  <asp:TemplateField  HeaderText="Product" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_item" runat="server" Text='<%# bind("Item") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>

             

                  <asp:TemplateField  HeaderText="Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                     
                         


                           <asp:TextBox ValidationGroup="xtx" ID="txt_quantity"  runat="server" MaxLength="5" Width="50px" Text='<%# bind("Quantity") %>'  ontextchanged="txt_quantity_TextChanged" AutoPostBack="true"></asp:TextBox>
                    
                      <asp:RangeValidator ID="Rangevalidator5" runat="server" ControlToValidate="txt_quantity" CssClass="error"
                        Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999" MinimumValue="0"
                        SetFocusOnError="True" Type="Integer" ValidationGroup="xtx"></asp:RangeValidator>

                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>
                  <asp:TemplateField  HeaderText="Product Price  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                        <asp:HiddenField ID="hf_pid" runat="server" Value='<%# bind("pid") %>' />
                       <asp:Label ID="lbl_price" runat="server" Text='<%# bind("price") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>


                  <asp:TemplateField  HeaderText="Total Price  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                           <asp:Label ID="lbl_tprice" runat="server"  Text='<%# (Convert.ToDouble(Eval("Quantity"))*Convert.ToDouble(Eval("price"))).ToString() %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                 </asp:TemplateField>

                  <asp:TemplateField  HeaderText="Delete" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/Redcross.png" OnClientClick="return confirm('Are you sure?')"  
                            runat="server" onclick="ImageButton1_Click" ValidationGroup="Cancle" /> 
                    </ItemTemplate>
                    <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>



           </columns>
</asp:GridView>
</div>
            </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>


        <asp:TemplateColumn HeaderText="Purchase Amount  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
           
            <ItemTemplate>
                <asp:Label ID="lbl_pamount" runat="server" Text='<%# bind("mainamount") %>' Width="50px"></asp:Label>
            </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>


                <asp:TemplateColumn HeaderText="Shipping Amount  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
           
            <ItemTemplate>
                <asp:Label ID="lbl_ship" runat="server" Text='<%# bind("dif") %>' Width="50px"></asp:Label>
            </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText="Total Amount  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
           
            <ItemTemplate>
                <asp:Label ID="lblpr" runat="server" Text='<%# bind("amount") %>' Width="50px"></asp:Label>
            </ItemTemplate>

<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
        </asp:TemplateColumn>


                    <asp:TemplateColumn HeaderText="Remark" Visible="false">
                        <EditItemTemplate>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtRemark" runat="server" Text='<%# Eval("remark") %>' TextMode="MultiLine"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Message for Client" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff"  Visible="false">
                    <ItemTemplate>
                        <asp:TextBox ID="txtMsgcl" runat="server" CssClass="msg_input" TextMode="MultiLine"  Text='<%# Eval("ClMsg") %>'></asp:TextBox>
                  
                    </ItemTemplate>
               
                    
<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
                    
                    </asp:TemplateColumn>

               
                    <asp:TemplateColumn HeaderText="Order Status" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                     <ItemTemplate>
                    
                     <asp:DropDownList ID="ddst" runat="server" CssClass="input"  >
                     <asp:ListItem Value="0">Under Process</asp:ListItem>                    
                  <%--   <asp:ListItem Value="1">Cancellation Request</asp:ListItem>
                     <asp:ListItem Value="2">Cancellation Confirmed</asp:ListItem>--%>
                     <asp:ListItem Value="4">Dispatch In Process</asp:ListItem>
                     <asp:ListItem Value="3">Order Dispatched</asp:ListItem>
                     </asp:DropDownList>

                     <br><br>

<asp:Label ID="lblDispatch2" runat="server" Visible="false" ></asp:Label><asp:Label ID="lblDispatch" runat="server" Text='<%# Eval("Dispatch") %>' Visible="false" ></asp:Label></span>
<asp:TextBox  placeholder="Dispatch Number"  Width="48%"  CssClass="input_2" ID="txtdno" Visible="false" runat="server" Text='<%# Eval("Dispatchno") %>' ></asp:TextBox><!--Used when courier service needed -->                      
<asp:LinkButton ID="lnkdsbt" runat="server" CommandName="UpdtSt" ValidationGroup="stupdt"  OnClientClick="return confirm('Are you sure?')" CssClass="submit">Update  </asp:LinkButton>                  
</ItemTemplate>
<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>

                    </asp:TemplateColumn>



                    <asp:TemplateColumn HeaderText="Action" Visible="false">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkdelorders" runat="server" CommandName="deleteorder" 
                                OnClientClick="return confirm('Do you want to cancel this order?')">Order Cancel</asp:LinkButton>
                            <asp:Label ID="dis" Visible="false" runat="server" Width="50%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                                              


 <asp:TemplateColumn HeaderText="Executive" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                       <asp:Label ID="lblThrough" runat="server" Text='<%# Eval("callexecutive") %>'></asp:Label>
                    </ItemTemplate>    
<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
 </asp:TemplateColumn>

  <asp:TemplateColumn HeaderText="Salesman" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                       <asp:Label ID="lblSalesPersonId" runat="server" Text='<%# findNameBySalesPersonId(Eval("salesname")) %>'></asp:Label>
                    </ItemTemplate>    
<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
 </asp:TemplateColumn>

   <asp:TemplateColumn HeaderText="Genrate Invoice" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>


    <asp:HyperLink ID="LinkGenerateInvoice" runat="server" CssClass="submit" Visible="false"
                                NavigateUrl='<%# "~/dispatchexecutive/genrateInvoice.aspx?OrderNo="+Eval("orderno") %>' 
                                Target="_blank" Text="Genrate Invoice"></asp:HyperLink>
                    
  <br>

    <asp:HyperLink ID="lnkVieword" runat="server" CssClass="submit"
                                NavigateUrl='<%# "~/DispatchExecutive/orderDetails.aspx?oid=" + Eval("orderno") %>' 
                                Target="_blank" Text="View Details"></asp:HyperLink>

                    </ItemTemplate>    
<HeaderStyle BackColor="#116591" ForeColor="White"></HeaderStyle>
 </asp:TemplateColumn>



                </Columns>
                <HeaderStyle BackColor="#CCCCCC" BorderStyle="None" Font-Bold="True" 
                    ForeColor="#666666" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" BackColor="White" />
                <PagerStyle Mode="NumericPages" NextPageText="Next" PrevPageText="Prev" />
            </asp:DataGrid>
</div>
</div>
        
 <asp:Button ID="btnupdateselected"  CssClass="submit" runat="server" 
                Text="Update Selected Rows" Visible="False" onclick="btnupdateselected_Click" />

                </div></div>

         
         




﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Payment.aspx.cs" Inherits="Payment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>



</head>
<body>
    <form id="form1" runat="server">
        <div>

            <input type="hidden" runat="server" id="key" name="key"  />
            <input type="hidden" runat="server" id="hash" name="hash" />
            <input type="hidden" runat="server" id="txnid" name="txnid" />

            <asp:Button ID="btn_pay" runat="server" Text="Pay" OnClick="btn_pay_Click" />
            <asp:RadioButtonList ID="rdobtnPOption" runat="server"></asp:RadioButtonList>
        </div>
    </form>
</body>
</html>

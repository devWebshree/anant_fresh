﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using System.Web.UI.HtmlControls;
using ClassLibrary;
using DAL;
public partial class ProductDetails : System.Web.UI.Page
{
    modifyData md = new modifyData();
    Search srch = new Search();
    dlsearch objsrch = new dlsearch();
    Utility all = new Utility();
    string id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.RouteData.Values["id"] != null)
            {
                id = Page.RouteData.Values["id"].ToString();

                BindData();

            }
            else
            {
                Response.Redirect("/home");
            }
        }
    }



    public void BindData()
    {
        srch.SearchBy = "";
        srch.ProductID = id;
        DataSet ds = new DataSet();
        ds = objsrch.itemSearchList(srch);


        if (ds.Tables[0].Rows.Count > 0)
        {
            dlstProductDeatal.DataSource = ds;
            dlstProductDeatal.DataBind();

            hl_cat.Text = ds.Tables[0].Rows[0]["Category"].ToString();
            hl_cat.NavigateUrl = "/product/" + all.RemoveSpace(ds.Tables[0].Rows[0]["Category"].ToString()) + "/All";

            hl_scat.Text = ds.Tables[0].Rows[0]["Expr6"].ToString();
            hl_scat.NavigateUrl = "/product/" + all.RemoveSpace(ds.Tables[0].Rows[0]["Category"].ToString()) + "/" + all.RemoveSpace(ds.Tables[0].Rows[0]["Expr6"].ToString());


            hf_pname.Text = ds.Tables[0].Rows[0]["name"].ToString();
            hf_pname.NavigateUrl = "/product/" + all.RemoveSpace(ds.Tables[0].Rows[0]["name"].ToString());

            t.InnerHtml= hl_cat.Text + "/" + hl_scat.Text + "/" + hf_pname.Text;

        }
        else
        {
            dlstProductDeatal.DataSource = null;
            dlstProductDeatal.DataBind();

        }

    }
    protected void dlstProductDeatal_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.AlternatingItem | e.Item.ItemType == ListItemType.Item)
        //{

        Label lblimagename = ((Label)e.Item.FindControl("Label2"));
        Label price = ((Label)e.Item.FindControl("lblPrice"));
        Label offerprice = ((Label)e.Item.FindControl("lblOfferPrice"));
        // HtmlAnchor zoom = ((HtmlAnchor)e.Item.FindControl("zoom1"));
        Image imgmain = ((Image)e.Item.FindControl("imgmain"));

        lblimagename.Visible = false;
        if (Convert.ToDecimal(offerprice.Text) == Convert.ToDecimal(0))
        {
            price.Font.Strikeout = false;
            offerprice.Visible = false;

        }
        else
        {
            price.Font.Strikeout = true;
            offerprice.Visible = true;
        }

    }
    protected void dlstProductDeatal_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "continue")
        {
            Response.Redirect("/product");
        }

        if (e.CommandName == "book")
        {

            TextBox txtQuantity = ((TextBox)e.Item.FindControl("txtQuantity"));
            Label lblitemid = ((Label)e.Item.FindControl("lblid"));
            Label lblname = ((Label)e.Item.FindControl("lblname"));
            Label ablStock = ((Label)e.Item.FindControl("lbavlQty"));
            Label lblCode = ((Label)e.Item.FindControl("lblCode"));
            Label price = ((Label)e.Item.FindControl("lblPrice"));
            Label offerprice = ((Label)e.Item.FindControl("lblOfferPrice"));

            if (md.IsPrdctExistCart(lblitemid.Text, Session["IsUser"].ToString(),"1") == false)
            {
                decimal amount;
                if (Convert.ToDecimal(offerprice.Text) == Convert.ToDecimal(0))
                {
                    amount = Convert.ToDecimal(changeAmount(price.Text));
                }
                else
                {
                    amount = Convert.ToDecimal(offerprice.Text);
                }
                md.inserProduct(lblitemid.Text, txtQuantity.Text, Session["IsUser"].ToString(), lblitemid.Text, "", lblname.Text, amount.ToString(),"1");
                // viewitem();
            }
            else
            {
                string pid = md.findPid(lblitemid.Text, Session["IsUser"].ToString());

                if (pid != "0")
                {

                    md.UpdatePurchaseItem(pid, txtQuantity.Text, Session["IsUser"].ToString());
                }
                else
                {

                }
            }

            BindData();
            Response.Redirect("/cart");
        }

        if (e.CommandName == "wishlist")
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                Label lblCode = ((Label)e.Item.FindControl("lblCode"));
                Label lblitemid = ((Label)e.Item.FindControl("lblid"));
                md.InsertInWishlist(lblitemid.Text, Session["IsUser"].ToString());
                showmessage("Successfully Added " + lblCode.Text + " in wishlist");
            }
            else
            {
                Session["URL"] = HttpContext.Current.Request.Url.AbsoluteUri;
                Response.Redirect("/login");

            }

        }
    }
    private void ShowMessageLogout(string str)
    {
        // ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + Message + "')", true); 
        string url = HttpContext.Current.Request.Url.AbsoluteUri;
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "Login", ("alert('" + str + "'); window.parent.location.href='") + Page.ResolveUrl(url) + "'", true);

    }
    public void viewitem()
    {
        chkoutctrl ctrlB = (chkoutctrl)Page.Master.FindControl("chkoutctrl1");
        LinkButton ddl = (LinkButton)ctrlB.FindControl("lnkbtn");
        int itm = md.SelPurItem(Session["IsUser"].ToString());
        ddl.Text = itm.ToString() + " Items";
        ddl.Enabled = true;
    }
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    public string changeAmount(string val)
    {
        if (val != "")
        {
            string[] str = val.Split('$');
            string str1 = str[1];
            return str1;
        }
        else
        {
            return "";
        }

    }
}
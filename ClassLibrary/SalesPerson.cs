﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary
{
    public class SalesPerson
    {

        public int SalesPersonId { get; set; }
        public string Name { get; set; }
        public string Searchtxt { get; set; } 
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public DateTime registerdate { get; set; }
        public int Action { get; set; }

        
    }
}

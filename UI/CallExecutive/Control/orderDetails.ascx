﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="orderDetails.ascx.cs" Inherits="control_orderdtl" %>

<style type="text/css">
.font
{
    font-weight:bold;
}
</style>
<div class="row m10">
    <div class="registerdiv clearfix" style="width: 93%;">


<div class="clear"></div>
<div class="row">
  <h2 class="left"> Order Details</h2>
  <asp:LinkButton ID="bcklnk" runat="server" Visible="false" CssClass="button right" style="width:120px; text-align:center;" CausesValidation="False" OnClick="bcklnk_Click">Print Invoice </asp:LinkButton>
</div>
<div class="line m10"></div>
<div class="clear"></div>

<div class="row m10">
<div class="column border-box">        

<div class="clft left">
<font size=3><b>Order Details :</b></font>
<table class="table">
<tr><td>
<b>Order Date</b></td>
<td><asp:Label ID="lblOrderDateTime" runat="server"></asp:Label>
</td></tr>

<tr><td>
<b>Order No :</b></td><td>
<asp:Label ID="lblOrderNo" runat="server" ></asp:Label>
</td></tr>
<tr><td>
<b>Order Status :</b></td>
<td>
<asp:Label ID="lblOrderStatus" runat="server"></asp:Label><br />
</td></tr>
</table>

</div>


<div class="table">
<asp:Label ID="lblAddress" runat="server" ></asp:Label>
</div>


</div>
</div>
<div class="row m20"><h2>Product Details</h2></div>
<div class="line m10"></div>
<div class="clear"></div>
<div class="column border-box">  
<div class="row ">









<asp:GridView ID="grdViewOrdersOfCustomer" runat="server" AutoGenerateColumns="false" 
                    DataKeyNames="orderno" CssClass="table" Width="100%" 
                    >
                <columns>



             <asp:TemplateField HeaderStyle-Width="7%" HeaderText="Serial No.">
                        <ItemTemplate>

                    <strong> <%# Container.DataItemIndex + 1 %></strong>
              

                            </ItemTemplate>
                  </asp:TemplateField>



                  <asp:TemplateField HeaderStyle-Width="7%"  HeaderText="Code" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_itemCode" runat="server" Text='<%# bind("code") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>


                  <asp:TemplateField HeaderStyle-Width="7%" HeaderText="Available Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_iteggmCode" runat="server" Text='<%# objstk.selectQuantity(Eval("itemid").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>



                  <asp:TemplateField HeaderStyle-Width="51%" HeaderText="Product" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_item" runat="server" Text='<%# bind("Item") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>

         

                  


                  <asp:TemplateField HeaderStyle-Width="7%"  HeaderText="Price  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                        <asp:HiddenField ID="hf_pid" runat="server" Value='<%# bind("pid") %>' />
                       <asp:Label ID="lbl_price" runat="server" Text='<%# bind("price") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>

                  <asp:TemplateField HeaderStyle-Width="7%"  HeaderText="Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
              
                           <asp:TextBox ValidationGroup="xtx" ID="txt_quantity"  runat="server" MaxLength="5" Width="50px" Text='<%# bind("Quantity") %>'  ontextchanged="txt_quantity_TextChanged" AutoPostBack="true"></asp:TextBox>
                      <asp:RangeValidator ID="Rangevalidator5" runat="server" ControlToValidate="txt_quantity" CssClass="error"
                        Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999" MinimumValue="0"
                        SetFocusOnError="True" Type="Integer" ValidationGroup="xtx"></asp:RangeValidator>


                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>

                  <asp:TemplateField  HeaderText="Subtotal  &#8377;" HeaderStyle-Width="7%" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                           <asp:Label ID="lbl_tprice" runat="server"  Text='<%# (Convert.ToDouble(Eval("Quantity"))*Convert.ToDouble(Eval("price"))).ToString() %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                 </asp:TemplateField>

                  <asp:TemplateField  HeaderText="Delete" HeaderStyle-Width="7%" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/Redcross.png" OnClientClick="return confirm('Are you sure?')" 
                            runat="server" onclick="ImageButton1_Click" ValidationGroup="Cancle" /> 
                    </ItemTemplate>
                    <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>



           </columns>
</asp:GridView>
<table class="table">
<tr>
<td colspan="4" class="sp1" style="width:72%; padding:5px; font-family:Verdana;  color:#1176ae; font-weight:bold;font-size:16px; text-align:right; ">Subtotal :</td>
<td class="grdtd grdtdwidth">-</td>
<td class="grdtd grdtdwidth"><asp:Label ID="lblQuantity" Text="0" CssClass="sp2" runat="server" ></asp:Label></td>

<td class="grdtd grdtdwidth"><asp:Label ID="lblAmount"  CssClass="sp2" runat="server" ></asp:Label></td>
<td class="grdtd grdtdwidth">-</td>

</tr>
</table>
</div>
</div>
<div class="clear"></div>













<div class="pricdl">
<ul>

<li>
<span class="sp1">Shipping Cost :</span> <asp:Label ID="lbl_shipcharge" CssClass="sp2" runat="server" ></asp:Label>
</li>
<li>
<span class="sp1">Total :</span> <asp:Label ID="lblTotal" CssClass="sp2" runat="server" ></asp:Label>
</li>
</ul>
</div>




   </div></div>

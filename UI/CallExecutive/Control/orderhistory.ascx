<%@ Control Language="C#" AutoEventWireup="true" CodeFile="orderhistory.ascx.cs" Inherits="control_orderhistory" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl" TagPrefix="rjs" %>

    <div class="registerdiv clearfix" style="width: 93%;">


<div class="row">
 
<div class="title"></div>
<h5>
Search Order History :
<asp:TextBox ID="txt_serch" runat="server" placeholder=" Search by Name Mobile No, Order No"></asp:TextBox> 
Status
                     <asp:DropDownList ID="ddstatus" runat="server" CssClass="input_2"  >
                     <asp:ListItem Selected="True">All</asp:ListItem>                    
                     <asp:ListItem Value="0">Under Process</asp:ListItem>                    
                     <asp:ListItem Value="1">Cancel</asp:ListItem>
                     <asp:ListItem Value="2" >Cancellation Confirmed</asp:ListItem>
                     <asp:ListItem Value="7">Order Dispatched</asp:ListItem>
                     </asp:DropDownList>


<asp:Button ID="btn_srch" 
        runat="server" Text="Search" onclick="btn_srch_Click" />

</h5></div>


 <div class="clear"></div>
 <div class="row m10">
<div class="tab-pane active" id="tab1">

 <asp:DataGrid ID="dgrd" runat="server" AutoGenerateColumns="False" CellPadding="4"
            CssClass="table"    GridLines="None" PageSize="20" 
                onselectedindexchanged="dgrd_SelectedIndexChanged">
            <Columns>
                <asp:TemplateColumn HeaderText="Item">
                    <ItemStyle HorizontalAlign="Center"  />
                    <ItemTemplate>
                        <asp:Label ID="lbsalpr" runat="server" Text='<%# bind("item") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Price">
                    <ItemStyle HorizontalAlign="Center" Width="75px" />
                    <ItemTemplate>
                        <asp:Label ID="lblpr" runat="server" Text='<%# bind("charge") %>' Width="50px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Quantity">
                    <ItemStyle HorizontalAlign="Center"  />
                    <ItemTemplate>
                        <asp:Label ID="txtqty" runat="server" MaxLength="9" Text='<%# bind("quantity")  %>'
                            Width="50px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
              <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />

    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
  <HeaderStyle BackColor="#BDC3C7" Font-Bold="True" Font-Names="verdana" 
        Font-Size="13px" ForeColor="White" HorizontalAlign="Center"  />
    
        </asp:DataGrid>

         
 <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="pid"
              CssClass="table" EmptyDataText="No Data Present" 
                OnRowCommand="GridView2_RowCommand" Width="100%" PageSize="5" 
                AllowPaging="True" 
        OnPageIndexChanging="GridView2_PageIndexChanging1" 
        onrowdatabound="GridView2_RowDataBound">
                <HeaderStyle Height="50px" />
                <Columns>
                <asp:TemplateField HeaderText="OrderNo">
                <ItemTemplate>
                <asp:LinkButton title="Go On Order Details" ID="ord" runat="server" Text='<%#  Eval("Orderno")  %>' 
                        CausesValidation="False" CommandName="dtl" CssClass="my"></asp:LinkButton>
                </ItemTemplate>
                </asp:TemplateField>


               <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                              <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>

               <asp:TemplateField HeaderText="Mobile No.">
                <ItemTemplate>
                              <asp:Label ID="lbl_Mobile" runat="server" Text='<%# Eval("username") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>


                 <asp:TemplateField HeaderText="Purchase Date">
                <ItemTemplate>
                <asp:Label ID="pdt" runat="server" Text='<%# Eval("dop","{0:dd-MMM-yy hh:mm:ss tt}") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>

               <asp:TemplateField HeaderText="Purchase Amount">
                <ItemTemplate>
                              &#8377; <asp:Label ID="lbl_pAmount" runat="server" Text='<%# Eval("mainamount") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>

               <asp:TemplateField HeaderText="Shipping Amount">
                <ItemTemplate>
                              &#8377; <asp:Label ID="lbl_SAmount" runat="server" Text='<%# Eval("dif") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>


               <asp:TemplateField HeaderText="Total Amount">
                <ItemTemplate>
                             &#8377; <asp:Label ID="lbl_TAmount" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Order Status">
                <ItemTemplate>
                     <asp:Label ID="asdtl" runat="server" Text='<%# GetDis(Eval("Dispatch")) %>'></asp:Label>
                     <asp:Label ID="lbl_id" Visible="false" runat="server" Text='<%# Eval("pid") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Order Details">
                <ItemTemplate>
                <asp:LinkButton ID="dtl" runat="server" title="Go On Order Details" Text="Details" CausesValidation="False" 
                        CommandName="dtl" CssClass="my"></asp:LinkButton>
                </ItemTemplate>
                </asp:TemplateField>       

               <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                <asp:HiddenField ID="hf_dispatch"  runat="server" Value='<%# Eval("Dispatch") %>' />
                <asp:LinkButton ID="lb_cancel" runat="server" Text="Cancel Order" CausesValidation="False" 
                        CommandName="can" CssClass="_button1 backgroundbg border-radius"></asp:LinkButton>
                </ItemTemplate>
                </asp:TemplateField>   
               
                </Columns> 
           <HeaderStyle  ForeColor="Black" Height="30px" BorderColor="White" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
   <RowStyle HorizontalAlign="Center" BackColor="#E4E6E8" BorderColor="White" Height="30px" />
   <PagerStyle BackColor="#BDC3C7" ForeColor="White" HorizontalAlign="Center" />
 <HeaderStyle BackColor="#BDC3C7" Font-Bold="True"  
       Font-Size="13px" ForeColor="Black" HorizontalAlign="Center"  />
   <AlternatingRowStyle HorizontalAlign="Center" BackColor="#E4E6E8" ForeColor="Black" />
            </asp:GridView>
                            
</div>
</div>
</div>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="orderdtl.ascx.cs" Inherits="control_orderdtl" %>


<style type="text/css">
    .pricdl {
        padding: 5px;
        width: 310px;
        float: right;
        text-align: right;
    }

        .pricdl ul {
            padding: 0;
            margin: 0;
            list-style: none;
        }

            .pricdl ul li {
                float: left;
                width: 100%;
                margin: 1px;
            }

        .pricdl .sp1 {
            width: 188px;
            float: left;
            background: #ccc;
            color: #1176ae;
            font-family: Verdana;
            font-size: 13px;
            font-weight: bold;
            float: left;
            display: block;
            padding: 5px;
        }

        .pricdl .sp2 {
            width: 100px;
            text-align: left;
            font-weight: bold;
            float: right;
            background: #ccc;
            padding: 5px;
            font-family: Verdana;
            font-size: 13px;
            display: block;
        }
</style>



<div class="row">
</div>
<div class="clear"></div>
<div class="row">
    <h4 class="left">Details</h4>
    <asp:LinkButton ID="bcklnk" runat="server" CssClass="_button1 backgroundbg border-radius right" Style="width: 120px; text-align: center;" CausesValidation="False" OnClick="bcklnk_Click"><< Order History </asp:LinkButton>
</div>
<div class="line m10"></div>
<div class="clear"></div>

<div class="row m10">
    <div class="column border-box">
        <asp:DataList ID="dlst" runat="server" Style="width: 100%;">
            <ItemTemplate>

                <div class="clft left">
                    <h3 style="font-size: 16px; color: #000; font-weight: bold; padding-bottom: 10px;">Order Details</h3>
                    <ul class="lgnul clearfix">
                        <li>
                            <span class="lbl1 mc">Order Date</span>
                            <div class="inputbox">
                                <asp:Label ID="lblodt" runat="server" Text='<%# Eval("DOP","{0:dd-MMM-yy}") %>' CssClass="lbl1"></asp:Label></div>
                            <%# bng(Eval("amount"))%>
                        </li>
                        <li>
                            <span class="lbl1 mc">Order No</span>
                            <div class="inputbox">
                                <asp:Label ID="lblord" runat="server" Text='<%# Eval("OrderNo") %>' CssClass="lbl1"></asp:Label></div>
                        </li>
                        <li>
                            <span class="lbl1 mc">Order Value</span>
                            <div class="inputbox"><span class="left">&#8377;</span>
                                <asp:Label ID="lblordval" runat="server" Text='<%# Eval("amount") %>' CssClass="lbl1"></asp:Label></div>
                        </li>
                        <li>
                            <span class="lbl1 mc">Order Status</span>
                            <div class="inputbox">
                                <asp:Label ID="lbldis" runat="server" Text='<%# GetDis(Eval("Dispatch")) %>' CssClass="lbl1"></asp:Label><br />
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("ClMsg") %>'></asp:Label>
                            </div>
                        </li>

                        <li>
                            <span class="lbl1 mc">PAYMENT METHOD</span>
                            <div class="inputbox">
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("paymentmode") %>' CssClass="lbl1"></asp:Label><br />
                            </div>
                        </li>



                        <li>
                            <span class="lbl1 mc">Transaction Id</span>
                            <div class="inputbox">
                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("TransactionId") %>' CssClass="lbl1"></asp:Label><br />
                            </div>
                        </li>

                    </ul>
                </div>
                <div class="clft right">
                    <asp:Label ID="lbladd" runat="server" Text='<%# Eval("address") %>'></asp:Label>
                </div>

            </ItemTemplate>
        </asp:DataList>
    </div>
</div>
<div class="row m20">
    <h4>Product Details</h4>
</div>
<div class="line m10"></div>
<div class="row ">
    <asp:DataGrid ID="dgrd" runat="server" AutoGenerateColumns="False" CellPadding="4" OnItemDataBound="dgrd_ItemDataBound"
        GridLines="Vertical" PageSize="20" Width="100%" CssClass="table">
        <Columns>
            <asp:TemplateColumn HeaderText="Item(s)" HeaderStyle-CssClass="tblhd">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="lbsalpr" CssClass="left" runat="server" Text='<%# bind("item") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Item Code" HeaderStyle-CssClass="tblhd">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="lbcds" runat="server" Text='<%# bind("code") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Price" HeaderStyle-Width="10%" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                <ItemStyle HorizontalAlign="Center" Width="75px" />
                <ItemTemplate>
                    <asp:Label ID="lblpr" runat="server" Text='<%# bind("price") %>' Width="50px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>


            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Width="15%" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="txtqty" runat="server" MaxLength="9" Text='<%# bind("quantity")  %>'
                        Width="50px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>

            <asp:TemplateColumn HeaderText="Subtotal" HeaderStyle-Width="15%" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                <ItemStyle HorizontalAlign="Center" Width="75px" />
                <ItemTemplate>
                    <asp:Label ID="lblSubPrice" runat="server" Text='<%# (Convert.ToDecimal(Eval("price"))*Convert.ToDecimal(Eval("quantity"))).ToString() %>' Width="50px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>





        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditItemStyle BackColor="#2461BF" />
        <SelectedItemStyle BackColor="#FFDEE9" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <AlternatingItemStyle BackColor="#CCCCCC" />
        <ItemStyle BackColor="White" />
        <HeaderStyle BackColor="#CCCCCC" Font-Bold="True" ForeColor="Black"
            HorizontalAlign="Center" />

    </asp:DataGrid>




    <table class="table">
        <tr>
            <td class="tblhd" style="width: 70%; padding: 5px; font-family: Verdana; color: #1176ae; font-weight: bold; font-size: 16px; text-align: right;">Subtotal :</td>
            <td class="tblhd" style="width: 15%; font-size: 16px; padding: 5px; font-family: Verdana; font-weight: bold; text-align: center;">
                <asp:Label ID="lblQuantity" Text="0" CssClass="sp2" runat="server"></asp:Label></td>
            <td class="tblhd" style="width: 15%; font-size: 16px; padding: 5px; font-family: Verdana; font-weight: bold; text-align: center;">
                <asp:Label ID="lblSubTotal" CssClass="sp2" runat="server"></asp:Label></td>
        </tr>
    </table>


    <div class="pricdl">
        <ul>

            <li>
                <span class="sp1">Shipping Cost :</span>
                <asp:Label ID="lblShipping" CssClass="sp2" runat="server"></asp:Label>
            </li>
            <li>
                <span class="sp1">Total :</span>
                <asp:Label ID="lbltotal" CssClass="sp2" runat="server"></asp:Label>
            </li>
        </ul>
    </div>





</div>

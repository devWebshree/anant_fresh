﻿using System;
using System.Collections;
using System.Configuration;
using System.Data; 
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts; 
using BAL;
using System.IO;
using System.Web.Mail;
using ClassLibrary;
using DAL;
public partial class admin_controls_orderDetail : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    SendMail sm = new SendMail();
    Stock stk = new Stock();
    public  dlStock objstk = new dlStock();

    CallExecutive call = new CallExecutive();
    dlCallExecutive objCall = new dlCallExecutive();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindddlThroughCall();
            string str = Request.QueryString["oid"];
            if (str!=null)
            {

                bindprod(str);
            }
            else
            {
                txtedt.Text = System.DateTime.Now.Date.ToString("MM-dd-yyyy");
                bindord();
            }
            ordgrd.Columns[1].Visible = false;
        }

        
       
    }



    public void bindddlThroughCall()
    {

        DataSet ds = objCall.listAdminRole("CallExecutive");
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlThroughCall.DataSource = ds;
            ddlThroughCall.DataValueField = "aid";
            ddlThroughCall.DataTextField = "uname";
            ddlThroughCall.DataBind();

        }
        else
        {
            ddlThroughCall.DataSource = null;
            ddlThroughCall.DataBind();
        }
        ddlThroughCall.Items.Insert(0, "All");
        ddlThroughCall.Items.Insert(1, "Web");
    }


    void bindord() 
    {
        call.dt1=txtsdt.Text;
        call.dt2 = Convert.ToDateTime(txtedt.Text).AddDays(1).ToString("MM-dd-yyyy");
        call.Status = ddl_searchOrderStatus.SelectedItem.Value; 
        call.Searchtxt = txtSrc.Text;
        if (ddlThroughCall.SelectedIndex != 0)
        {
            call.Through = ddlThroughCall.SelectedItem.Text;
        }
        DataSet DataSetExport = new DataSet();
        DataSet ds = objCall.listFinalOrder(call);
        if (ds.Tables[0].Rows.Count > 0)
        {
            LinkButton1.Visible = true;
            lblNote.Visible = true;
            ordgrd.Visible = true;
            lblMessage.Visible = false;
            DataSetExport = ds;
            ordgrd.DataSource = ds;
            ordgrd.DataBind();


        }
        else
        {
            LinkButton1.Visible = false;
            lblNote.Visible = false;
            ordgrd.Visible = false;
            DataSetExport = null;
            lblMessage.Visible = true;
        }
    }
    public string checkorderby(object order)
    {
        CrmManagement crm = new CrmManagement();
        if (crm.selorderfromcrm(order.ToString()))
        {
            return "CRM";
        }
        else
        {
            return "Self";
        }
        
    }

    public void bindprod(string ord)
    {
        tabsrch.Visible = false;
        LinkButton1.Visible = false;
        btnupdateselected.Visible = false;
        dgrd.DataSource = ad.SelOrd(ord);
        dgrd.DataBind();
        dlst.DataSource = ad.SelFinalOrdDtl(ord);
        dlst.DataBind();
    }
    protected void lnksearch_Click(object sender, EventArgs e)
    {
        bindord();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
       
        System.Data.DataSet ds = null;
        ds = ad.SelAllFinalProducts1();
        string flname = null;
        Random rnd = new Random();
        flname = "AF-" + rnd.Next(11, 99999).ToString() + ".xls";
        ExportDataSetToExcel(ds, flname);

    }
    public void ExportDataSetToExcel(DataSet ds, string filename)
    {
        HttpResponse response = HttpContext.Current.Response;


        response.Clear();
        response.Charset = "";


        response.ContentType = "application/vnd.ms-excel";
        response.AddHeader("Content-Disposition", "attachment;filename=\"" + filename + "\"");


        using (StringWriter sw = new StringWriter())
        {
            using (HtmlTextWriter htw = new HtmlTextWriter(sw))
            {

                DataGrid dg = new DataGrid();
                dg.DataSource = ds.Tables[0];

                dg.DataBind();
                dg.RenderControl(htw);
                response.Write(sw.ToString());
                response.End();
            }

        }
    }

    public string  selectaddress(object orderno)
    {
        return ad.selectaddress(orderno.ToString()).ToString(); ;
    }
    protected void btnupdateselected_Click(object sender, EventArgs e)
    {

    }
    protected void ordgrd_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "deleteorder")
        {
            DropDownList st = e.Item.FindControl("ddst") as DropDownList;
            if (st.SelectedIndex !=3)
            {
                string ord = ((HyperLink)e.Item.FindControl("lnkord")).Text;
                ad.DeleteOrders(ord);
                showmessage("Order " + ord + " Cancelled successfully!");
            }
            else
            {
                string ord = ((HyperLink)e.Item.FindControl("lnkord")).Text;
                showmessage("Order " + ord + " has been dispatched so it can't cancel!");
            }
        }

        if (e.CommandName == "UpdtSt")
        {
            DropDownList st = e.Item.FindControl("ddst") as DropDownList;
            string usr = (e.Item.FindControl("urname") as Label).Text;
            string ord = (e.Item.FindControl("lnkord") as HyperLink).Text;
            string dt = (e.Item.FindControl("pdt") as Label).Text;
            string addr = (e.Item.FindControl("lbladd") as Label).Text;
            string remark = (e.Item.FindControl("txtRemark") as TextBox).Text;
            string pid = (e.Item.FindControl("lblpid") as Label).Text;
            TextBox Clientmsg = ((TextBox)e.Item.FindControl("txtMsgcl"));
            if (st.SelectedValue == "Order Dispatched")
            {
                modifyData md = new modifyData();
                md.UpdtFnlDtl(pid, st.SelectedIndex.ToString(), "", remark, Clientmsg.Text);
                if (st.SelectedValue == "Order Dispatched")
                {
                    sm.sendOredrDetails(usr, ord, "");
                }
                showmessage("Status Updated");
            }
            else if (st.SelectedValue == "Cancellation Confirmed")
            {

                string ord2 = ((HyperLink)e.Item.FindControl("lnkord")).Text;
               // ad.DeleteOrders(ord2);
                modifyData md = new modifyData();
                md.UpdtFnlDtl(pid, st.SelectedIndex.ToString(), "", remark, Clientmsg.Text);
                 orderCancleConfirmed(ord2);
                showmessage("Order " + ord2 + " Cancelled successfully!");
            }
            else if (st.SelectedValue == "Cancellation Request")
            {
                modifyData md = new modifyData();
                md.UpdtFnlDtl(pid, st.SelectedIndex.ToString(), "", remark, Clientmsg.Text);
                showmessage("Status Updated");

            }

            bindord();

        }

    }
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    protected void ordgrd_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem | e.Item.ItemType == ListItemType.Item)
        {
            Label itm =   ((Label)e.Item.FindControl("itemlbl"));   //
            string ord = ((HyperLink)e.Item.FindControl("lnkord")).Text;  //  orderno

            itm.Text = ad.SelOrdProduct(ord);//

            DropDownList drpstatus = ((DropDownList)e.Item.FindControl("ddst"));
            Label lbldispatch = ((Label)e.Item.FindControl("lblDispatch"));
            TextBox txtdispatchno = ((TextBox)e.Item.FindControl("txtdno"));
            LinkButton lnkdsbt = ((LinkButton)e.Item.FindControl("lnkdsbt"));
            TextBox remark = ((TextBox)e.Item.FindControl("txtRemark"));
            TextBox Clientmsg = ((TextBox)e.Item.FindControl("txtMsgcl"));
            Label lbldispatch2 = ((Label)e.Item.FindControl("lblDispatch2"));
            LinkButton lnkdelorders = ((LinkButton)e.Item.FindControl("lnkdelorders"));
            Label dis = ((Label)e.Item.FindControl("dis"));


            if (Convert.ToInt32(lbldispatch.Text) == 3)
            {
                txtdispatchno.Enabled = false;
                drpstatus.SelectedIndex = Convert.ToInt32(lbldispatch.Text);
                drpstatus.Visible = false;
                lbldispatch2.Text = "Order Dispatched";
                lbldispatch2.Visible = true;
                lnkdelorders.Visible = false;
                lnkdsbt.Visible = false;
                dis.Text = "Dispatched";
                dis.Visible = true;

            }
            else if (Convert.ToInt32(lbldispatch.Text) == 1)
            {
                remark.Text = ad.getreasonofcancellation(ord);
                txtdispatchno.Visible = false;
                drpstatus.SelectedIndex = Convert.ToInt32(lbldispatch.Text);
                txtdispatchno.Visible = true;
            }
            else if (Convert.ToInt32(lbldispatch.Text) == 2)
            {
                txtdispatchno.Enabled = false;
                remark.Text = ad.getreasonofcancellation(ord);
                txtdispatchno.Visible = false;
                txtdispatchno.Text = ad.getreasonofcancellation(ord);
                drpstatus.SelectedIndex = Convert.ToInt32(lbldispatch.Text);
                drpstatus.Visible = false;
                lbldispatch2.Text = "Order Cancelled";
                lbldispatch2.Visible = true;
                lnkdelorders.Visible = false;
                lnkdsbt.Visible = false;
                dis.Text = "Cancelled";
                dis.Visible = true;

            }
            else
            {
                drpstatus.SelectedIndex = Convert.ToInt32(lbldispatch.Text);
            }
            txtdispatchno.Visible = false;// used when courier service needed



            //bind nested gridview lbladd
           // Label orderId = ((Label)e.Item.FindControl("lbladd"));
            string customerID = ordgrd.DataKeys[e.Item.ItemIndex].ToString();
            GridView grdViewOrdersOfCustomer = ((GridView)e.Item.FindControl("grdViewOrdersOfCustomer"));

            int Enable = Convert.ToInt32(lbldispatch.Text);
            if (Enable == 3 || Enable == 2)
            {
                grdViewOrdersOfCustomer.Enabled = false;
            }
            grdViewOrdersOfCustomer.DataSource = objCall.selectOrderedDetails(ord);
            grdViewOrdersOfCustomer.DataBind();


        }
       
    }
    protected void ordgrd_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
         ordgrd.CurrentPageIndex = e.NewPageIndex;
    }
    protected void ordgrd_ItemCreated(object sender, DataGridItemEventArgs e)
    {

    }
    protected void ddl_searchOrderStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindord();
    }
    protected void grdViewOrdersOfCustomer_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //GridViewRow row = grdViewOrdersOfCustomer.Rows[e.RowIndex]; lbl_itemCode

                Label itemCode = (Label)e.Row.FindControl("lbl_itemCode");
                Label item = (Label)e.Row.FindControl("lbl_item");
                TextBox quantity = (TextBox)e.Row.FindControl("txt_quantity");
                Label price = (Label)e.Row.FindControl("lbl_price");
                HiddenField pid = (HiddenField)e.Row.FindControl("hf_pid");

            }

        }
        catch (Exception ex) { }
    }
    protected void txt_quantity_TextChanged(object sender, EventArgs e)
    {

        var txt = (TextBox)sender;
        var rptChild = txt.NamingContainer;//Child Repeater
        HiddenField pid = (HiddenField)rptChild.FindControl("hf_pid");
        TextBox txtQuantity = (TextBox)rptChild.FindControl("txt_quantity");

        string s = txtQuantity.Text;
        Int32 a;
        if (Int32.TryParse(s, out a))
        {
            stk.NewQuantity = Convert.ToInt32(txtQuantity.Text);
            stk.PurchageId = Convert.ToInt32(pid.Value);
            objstk.updateOrderedProduct(stk);
            // objCall.UpdatePurchaseItemCall(pid.Value, txtQuantity.Text);
            bindord();
        }
        else
        {
            bindord();

        }
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        var imgbtn = (ImageButton)sender;
        var rptChild = imgbtn.NamingContainer;//Child Repeater
        HiddenField pid = (HiddenField)rptChild.FindControl("hf_pid");
        stk.NewQuantity = 0;
        stk.PurchageId = Convert.ToInt32(pid.Value);
        objstk.updateOrderedProduct(stk);
        bindord();

    }

    //public void total()
    //{
    //    try
    //    {
    //        double i = Convert.ToDouble(ad.selectsmprice(Session["ShopUserId"].ToString()));
    //        lblAmount.Text = i.ToString();
    //        lblTotal.Text = i.ToString();
    //        if (i < 1004.00)
    //        {
    //            pnl_cont.Visible = true;
    //            ltrl_more.Visible = false;
    //            lbl_lessprice.Text = (1004 - Convert.ToInt32(lblTotal.Text)).ToString();
    //            lbl_shipcharge.Text = "50";
    //            lblTotal.Text = (Convert.ToInt32(lblTotal.Text) + 50).ToString();
    //        }
    //        else
    //        {
    //            pnl_cont.Visible = false;
    //            ltrl_more.Visible = true;
    //            lbl_shipcharge.Text = "0";

    //        }

    //    }catch{}
    //}


    public void orderCancleConfirmed(string OrderId)
    {
            DataSet ds = objCall.selectOrderedDetails(OrderId);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //stk.NewQuantity = 0;
                //stk.PurchageId = Convert.ToInt32(ds.Tables[0].Rows[i]["pid"]);
                //objstk.updateOrderedProduct(stk);
                stk.ItemId = Convert.ToInt32(ds.Tables[0].Rows[i]["ItemId"]);
                stk.AddQuantity = Convert.ToInt32(ds.Tables[0].Rows[i]["quantity"]);
                objstk.updateAddItemQuantity(stk);
           
            }
    }



    protected void ddlThroughCall_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindord();
    }
}

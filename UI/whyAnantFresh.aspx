﻿<%@ Page Title="Why AnantFresh" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true"
    CodeFile="whyAnantFresh.aspx.cs" Inherits="whyAnantFresh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <h3 class="title1">
            Anant Fresh – Serving Satisfaction And Earning Trust</h3>
        <div class="clear">
        </div>
        <p class="m5">
            Anant Fresh Private Limited is a distinguished retail sector brand that has assumed
            prominence within a short span of time on account of its quality and responsive
            services. The company was established in the JAN 2014 and since then, has developed
            a resurgent footprint along with a heavy base of satisfied customers to its credit!
            The company is a part of a diversified and well established conglomerate that includes
            four companies – Anant Fresh Private limited, EventBizz entertainment Private Limited,
            Anant Premier Inn Private Limited and Anant Crushers pvt. Ltd.<br />
            <br />
            Anant Fresh presently runs a ‘Mega Grocery Store’ at Kundli, District Sonepat, Haryana.
            The store has emerged as a synonym of success in the last one and half year of existence
            and leads others as a model to be emulated! Under the inspiring leadership of Mr.
            Manoj Kumar Khatri , Anant Fresh has manifested a sudden popular resonance and the
            company has finalized plans to start another mega retail store in Kundali, Sonipat
            .</p>
        <h4 class="title2 pd">
            Our Vision
        </h4>
        <div class="clear">
        </div>
        <p class="m5">
            Anant Fresh will be serving quality and satisfaction and plans to achieve a pan
            India footprint by next 5 years while Haryana will be covered in the next one year.
            Anant Fresh is no where behind in its CSR initiatives and runs an active NGO named
            <br />
            <span class="textbold italic">– Anant Social Link Foundation. The NGO has been serving the
                poorer sections through viable employment and educational inclusion initiatives!</span>
        </p>
        <h4 class="title2 pd">
            Why Anant Fresh
        </h4>
        <div class="clear">
        </div>
        <p class="m5">
            <span class="textbold">Fresh has emerged as a prime shopping destination for the customers
                due to: </span>
        </p>
          <h4 class="title2 pd ">A vibrant array: </h4>
        <p>
          

            Anant Fresh grocery retail store delivers the most extensive products ranges and
            sources latest products from the most trusted brands of the world. Our products
            portfolio include –
        </p>
        <div class="clear">
        </div>
        <ol class="ol m10">
            <li class="italic">High quality dry cereals, pulses</li>
            <li class="italic">Full range FMCG (fast moving consumer goods)</li>
            <li class="italic">Packaged foods and beverages</li>
            <li class="italic">Fine personal care products and much more selective products!</li>
        </ol>
        <div class="clear">
        </div>

        <h4 class="title2 pd ">Real savings experience: </h4>
        <p class="m5">
       
            Anant Fresh retail stores offer true shopping economies to the customers and it
            is manifested in the fast growing consumer base of the company! Anant Fresh has
            particularly specialized in the development of robust and reliable backward linkages
            with the producers and manufacturers and sources its requirements in bulk directly
            from the factories and fields. This has ensured smart economical shopping!
        </p>
         <h4 class="title2 pd ">A trusted online shopping destination:</h4>
        <p>
          
            we offer lively and wholesome online shopping experience at our store through safe
            and secure payments gateways. Navigate through and search for your products and
            we will deliver it at your doorstep same day within 2 hours (T&C).</p>
             <h4 class="title2 pd ">Free shipping:</h4>
        <p>
             
            <br />
            we provide free shipping of all types of goods from our store for the purchases
            of Rs. 1004 or more! (A nominal charge of Rs. 50 is charged for purchases less than
            the above amount).
        </p>
        <h4 class="title2 pd ">Our refund policy: </h4>
        <p>
              
            <br />
            Anant Fresh guarantees full satisfaction for its products to its customers. However,
            in case of any inconsistency or otherwise, the company maintains a responsive refund
            mechanism. We provide refund for any purchase within 24 hours and that too without
            any deductions! Free door step pick up is also arranged for the returned product.</p>
            <h4 class="title2 pd ">Quality certifications:</h4>
        <p>
              
            Anant Fresh carries relevant warehousing and retail quality certifications from
            national and global agencies and our stores/ warehouses are strictly regulated for
            quality checks by the experts.</p>
       <h4 class="title2 pd ">Smart inventory management:</h4>
        <p>
       
            Anant Fresh runs on efficient and frontline inventory management software and protocols
            so that hassle free shopping ambience is offered to you! We ensure that our valued
            customers always get the required product at our store.</p>
                          <h4 class="title2 pd ">Responsive staff:</h4>
        <p>

            Anant Fresh stores runs through responsive and assistive staff that are well trained
            in the conduct of retail operations.
            
            <br /><br />
             Anant Fresh with its motto of ‘Use Quality’
            is serving the society and economy with passion and has been successful in ensuring
            quality and savings not only for the customers but also for the primary producers
            and manufacturers through efficient business bonding and linkages.
            <br /><br />
             We aim to serve
            with ever resurgent ambitions to emerge as an icon of satisfaction! Come be a part!
        </p>
      
       
    </div>
</asp:Content>

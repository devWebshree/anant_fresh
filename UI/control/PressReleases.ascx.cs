﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using ClassLibrary;
using System.Data;
public partial class control_PressReleases : System.Web.UI.UserControl
{

    PressRelease blpress = new PressRelease();
    dlPressRelease dlpress = new dlPressRelease();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            getPressRelease();
        }
    }

    public void getPressRelease()
    {
        blpress.SearchText = "";
       DataSet ds= dlpress.getPressRelease(blpress);
       if (ds.Tables[0].Rows.Count > 0)
       {
           rptPress.DataSource = ds;
           rptPress.DataBind();
       }
       else    
       {
           rptPress.DataSource = null;
           rptPress.DataBind();
       }
    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data.Common;
using ClassLibrary;

namespace DAL
{
   public class dlSalesPerson
    {

       Database db = GetDatabase(AppDatabaseName);
       private static string AppDatabaseName;
       private static Database GetDatabase(string dbName)
       {
           try
           {
               Database db = null;
               if (dbName == null)
               {
                   return db = DatabaseFactory.CreateDatabase();
               }
               else
               {
                   return db = DatabaseFactory.CreateDatabase(dbName);
               }
               return db;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       static dlSalesPerson()
       {
           AppDatabaseName = ConfigurationSettings.AppSettings["AppDatabaseName"];
       }

       public bool insertSalesPerson(SalesPerson obj)    
        {
            try
            {
                DbCommand cmd = db.GetStoredProcCommand("spSalesPerson");
                db.AddInParameter(cmd, "@Name", DbType.String, obj.Name);
                db.AddInParameter(cmd, "@Email", DbType.String, obj.Email);
                db.AddInParameter(cmd, "@Phone", DbType.String, obj.Phone);
                db.AddInParameter(cmd, "@Address", DbType.String, obj.Address);
                db.AddInParameter(cmd, "@Action", DbType.Int32, 1);
                int i = db.ExecuteNonQuery(cmd);

                if (i > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public bool updateSalesPerson(SalesPerson obj)   
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spSalesPerson");
               db.AddInParameter(cmd, "@SalesPersonId", DbType.String, obj.SalesPersonId);
               db.AddInParameter(cmd, "@Name", DbType.String, obj.Name);
               db.AddInParameter(cmd, "@Email", DbType.String, obj.Email);
               db.AddInParameter(cmd, "@Phone", DbType.String, obj.Phone);
               db.AddInParameter(cmd, "@Address", DbType.String, obj.Address);
               db.AddInParameter(cmd, "@Action", DbType.Int32, 2);
               int i = db.ExecuteNonQuery(cmd);

               if (i > 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public DataSet getSalesPerson(SalesPerson obj) 
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spSalesPerson");
               db.AddInParameter(cmd, "@Searchtxt", DbType.String, obj.Searchtxt);
               db.AddInParameter(cmd, "@Name", DbType.String, obj.Name);
               db.AddInParameter(cmd, "@Email", DbType.String, obj.Email);
               db.AddInParameter(cmd, "@Phone", DbType.String, obj.Phone);
               db.AddInParameter(cmd, "@Action", DbType.Int32, 3);
               DataSet ds = db.ExecuteDataSet(cmd);
               return ds;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public DataSet deleteSalesPerson(SalesPerson obj) 
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spSalesPerson");
               db.AddInParameter(cmd, "@SalesPersonId", DbType.String, obj.SalesPersonId);
               db.AddInParameter(cmd, "@Action", DbType.Int32, 4);
               DataSet ds = db.ExecuteDataSet(cmd);
               return ds;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public string findNameBySalesPersonId(SalesPerson obj)  
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spSalesPerson");
               db.AddInParameter(cmd, "@SalesPersonId", DbType.String, obj.SalesPersonId);
               db.AddInParameter(cmd, "@Action", DbType.Int32, 5);
               object ob = db.ExecuteScalar(cmd);
               if (ob == null || ob == System.DBNull.Value || ob.ToString() == "")
               {
                   return "";
               }
               else
               {
                   return ob.ToString();
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

    }
}

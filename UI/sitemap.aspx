﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="sitemap.aspx.cs" Inherits="sitemap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="Contentbox">
<div class="row">
        <h3 class="title1">Sitemap</h3>

<div class="colum2 clearfix ">
<ul class="footerul">
      <li><a href="/home">Home</a></li>
      <li>|</li>
      <li><a href="/whyanantfresh">Why AnantFresh</a></li>
      <li>|</li>
      <li><a href="/refundpolicy">Refund Policy</a></li>
      <li>|</li>
      <li><a href="/termsandconditions">Terms And Conditions</a></li>
      <li>|</li>
      <li><a href="/privacypolicy">Privacy Policy</a></li>
      <li>|</li>
      <li><a href="/contact">Contact Us</a></li>
</ul></div>


<div class="colum3 clearfix ">
<div class="title02">Home</div>
<ul class="footerul">
      <li><a href="/home">Easy to Start Shopping</a></li>
      <li><a href="/home">Choose a Delivery Time</a></li>
      <li><a href="/home">Place a order in minutes</a></li>
      <li><a href="/home">We Deliver to your home</a></li>
</ul></div>



<div class="colum3 clearfix ">
<div class="title02" style=" margin-left:10px;">Contact Us</div>
<ul class="footerul">
      <li><a href="/contact">Contact Us</a></li>
      <li><a href="/contact#Enquiry">Enquiry</a></li>
      <li><a href="/contact#Feedback">Feedback</a></li>
</ul></div>


<div class="colum3 clearfix ">
<div class="title02" style=" margin-left:10px;">Log in</div>
<ul class="footerul">
      <li><a href="/login">Log in</a></li>
      <li><a href="/register">Register</a></li>
</ul></div>

<div class="clear"></div>
<div class="tm10">
<div class="title02">Categories</div>
<div class="colum4 clearfix ">


<asp:Repeater ID="r_cat" runat="server"
        onitemdatabound="r_cat_ItemDataBound">
  <ItemTemplate>
<div class="title03">
    <asp:HyperLink ID="hf_category" NavigateUrl='<%# "/product/"+objAll.RemoveSpace(Eval("Category").ToString()+"/All") %>' runat="server"><%# Eval("Category")%></asp:HyperLink>
    <asp:HiddenField ID="hf_catID" Value='<%# Eval("catid") %>' runat="server" />
 <%--   <asp:HiddenField ID="hf_catname" Value='<%# objAll.RemoveSpace(Eval("Category")).ToString() %>' runat="server" />--%>

</div>
<ul class="footerul">

    <asp:Repeater ID="r_scat" runat="server">
    <ItemTemplate>
    <li>
    <asp:HyperLink ID="hf_subcategory" NavigateUrl='<%# "/product/"+objAll.RemoveSpace(Eval("Category").ToString()) +"/"+objAll.RemoveSpace(Eval("SubCategory").ToString()) %>' runat="server" ><%# Eval("SubCategory")%></asp:HyperLink>
    </li>                                                                
    </ItemTemplate>
     </asp:Repeater>

</ul>
<div class="clear"></div>
  </ItemTemplate>
</asp:Repeater>
</div>
</div>

        <div class="clear"></div>
        </div></div>
</asp:Content>



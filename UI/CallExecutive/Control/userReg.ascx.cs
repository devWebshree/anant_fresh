﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BAL;
using System.Web.Mail;
using ClassLibrary;
using DAL;

public partial class CallExecutive_Control_userReg : System.Web.UI.UserControl
{
    SendMail sm = new SendMail();
    modifyData ad = new modifyData();
    bool ISMobileExist, ISEmailExist;
  
    SalesPerson blsales = new SalesPerson();
    dlSalesPerson objsales = new dlSalesPerson();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindSalesPerson();
            txtcity.Text = "Sonipat";
            txtcoun.Text = "India";
            txtstate.Text = "Delhi";
            txtzip.Text = "110092";
        }
    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        string uName = txtph.Text;
        checkExistWuser();
        checkExistClient();
        retailer();

        if (txtusername.Text == "")
        ISEmailExist = true;
        

        if (ISMobileExist == true && ISEmailExist == true)
        {
            userproperty up = new userproperty();
            up.U_uname = txtph.Text;
            up.U_name = txtname.Text;
            up.U_email = txtusername.Text;
            up.U_ph = txtph.Text;
            up.U_altph = txtaltph.Text;
            up.U_addr = txtaddr.Text;
            up.city = txtcity.Text;
            up.state = txtstate.Text;
            up.country = txtcoun.Text;
            up.zip = txtzip.Text;
            ad.datainsert(up);

            Session["ShopUser"] = txtname.Text + " [" + txtph.Text + "]";
            Session["ShopUserId"] = txtph.Text;

            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
            JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, "Your account has been created successfully! Please check your registered email id", 5000);
        }
        else
        {
            Session["ShopUser"] = txtname.Text + " [" + txtph.Text + "]";
            Session["ShopUserId"] = txtph.Text;  
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
            JqueryNotification.NotificationExtensions.ShowErrorNotification(this, "This email is already registered.", 5000);
        }


    }

    public void reset()
    {
        txtaltph.Text = "";
      //txtcity.Text = "";
        txtname.Text = "";
      //txtcoun.Text = "";
        
        txtaddr.Text = "";
 
    }
    public void bindSalesPerson()
    {
        try
        {
           blsales.Searchtxt = "";
           DataSet ds = objsales.getSalesPerson(blsales);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlsalePerson.DataSource = ds;
                ddlsalePerson.DataTextField = "Name";
                ddlsalePerson.DataValueField = "SalesPersonId";
                ddlsalePerson.DataBind();
            }
            else
            {
                ddlsalePerson.DataSource = null;
                ddlsalePerson.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }


    public void checkExistWuser()
    {
        if (ad.IsMobileAvailable(txtph.Text) == true)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, "This Mobile no is already registered. ", 5000);
            lblMessage.Text = "<span class='error1 left ml5'>This Mobile No. is already registered.</span>";
            lbOrderHistory.Visible = true;
            lblMessage.Visible = true;
            ISMobileExist = false;
            fillData(txtph.Text);
        }
        else
        {
            lbOrderHistory.Visible = false;
            ISMobileExist = true;
            lblMessage.ForeColor = System.Drawing.Color.ForestGreen;
            lblMessage.Text = "<span class='error1 left ml5' style='color:green;'>Mobile no is Available.</span>";
            lblMessage.Visible = true;
         
        }
    }

    protected void txtph_TextChanged(object sender, EventArgs e)
    {
        checkExistWuser();
    }

    public void fillData(string username)
    {
        DataSet ds = ad.registerdataForMobile(username);
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                txtaltph.Text = dr["altPhone"].ToString();
                txtcity.Text = dr["city"].ToString();
                txtname.Text = dr["name"].ToString();
                txtcoun.Text = dr["country"].ToString();
                txtph.Text = dr["phone"].ToString(); 
                txtusername.Text = dr["email"].ToString();
                txtaddr.Text = dr["address"].ToString();
                txtstate.Text = dr["state"].ToString();
                txtzip.Text = dr["zipcode"].ToString();
            }
        }
    }
    protected void lbOrderHistory_Click(object sender, EventArgs e)
    {
        Session["ShopUser"] = txtname.Text + " [" + txtph.Text + "]";
        Session["ShopUserId"] = txtph.Text;
        Session["order"] = txtph.Text;
        retailer(); 
        Response.Redirect("orderDetails.aspx?OrderOF=" + txtph.Text);

    }


    public void checkExistClient()
    {
        if (ad.IsEmailAvailable(txtusername.Text) == true)
        {
            lbOrderHistory.Visible = false;
            lblEmailmsg.ForeColor = System.Drawing.Color.Red;
            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, "This email is already registered.", 5000);
            lblEmailmsg.Text = "<span class='error1 left ml5'>This email is already registered. ";
            lblEmailmsg.Visible = true;
            ISEmailExist = false;
          //  fillData(txtusername.Text);
        }
        else
        {
            lblEmailmsg.ForeColor = System.Drawing.Color.ForestGreen;
            lblEmailmsg.Text = "<span class='error1 left ml5' style='color:green;'>Email is available.</span>";
            lblEmailmsg.Visible = true;
            ISEmailExist = true;
            
        }
    }

    protected void txtusername_TextChanged(object sender, EventArgs e)
    {
        if (txtusername.Text != "")
        {
            checkExistClient();
        }
        else
        {
            ISEmailExist = true;

        }
    }
    protected void rbRetailer_CheckedChanged(object sender, EventArgs e)
    {
        retailer();
    }

    private void retailer()
    {
        if (rbRetailer.Checked)
        {
            Session["UserType"] = "1";
            pnlSales.Visible = false;
        }
        else
        {
            Session["UserType"] = "2";
            pnlSales.Visible = true;
        }
        if (pnlSales.Visible)
        {
            Session["SalesName"] = ddlsalePerson.SelectedItem.Value;
        }
        else
        {
            Session["SalesName"] = "Retail";
        }

    }
    protected void rbWholesale_CheckedChanged(object sender, EventArgs e)
    {
        retailer();        
    }
}

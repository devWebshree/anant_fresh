﻿using System;
using System.Configuration;
using Microsoft.SqlServer.Server;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DlResultDetails
{
  public class ConnectDb
    {
      private static string AppDatabaseName;
        public static Database GetDatabase()
        {
            try
            {
                Database db = null;
                if (AppDatabaseName == null)
                {
                    return db = DatabaseFactory.CreateDatabase();
                }
                else
                {
                    return db = DatabaseFactory.CreateDatabase(AppDatabaseName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static ConnectDb()
        {
            AppDatabaseName = ConfigurationSettings.AppSettings["AppDatabaseName"];
        }
    }  
}

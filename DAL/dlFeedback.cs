﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data.Common;
using ClassLibrary;
namespace DAL
{
    public class dlFeedback
    {
       Database db = GetDatabase(AppDatabaseName);
       private static string AppDatabaseName;
       private static Database GetDatabase(string dbName)
       {
           try
           {
               Database db = null;
               if (dbName == null)
               {
                   return db = DatabaseFactory.CreateDatabase();
               }
               else
               {
                   return db = DatabaseFactory.CreateDatabase(dbName);
               }
               return db;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       static dlFeedback()
       {
           AppDatabaseName = ConfigurationSettings.AppSettings["AppDatabaseName"];
       }


       public bool insertFeedback(Feedback feed)
       {

           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spFeedBack");
               db.AddInParameter(cmd, "@Name", DbType.String, feed.Name);
               db.AddInParameter(cmd, "@EmailID", DbType.String, feed.EmailID);
               db.AddInParameter(cmd, "@PhoneNo", DbType.String, feed.MobileNo);
               db.AddInParameter(cmd, "@ProductName", DbType.String, feed.ProductName);
               db.AddInParameter(cmd, "@ProductSubName", DbType.String, feed.ProductSubName);
               db.AddInParameter(cmd, "@ProductQuality", DbType.String, feed.ProductQuality);
               db.AddInParameter(cmd, "@Comment", DbType.String, feed.Comment);
               int i = db.ExecuteNonQuery(cmd);
               if (i > 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

        
       public DataSet listFeedBack()
       {
           try
           {
               DbCommand cmd = db.GetSqlStringCommand("select * from Feedback");
               DataSet ds = db.ExecuteDataSet(cmd);
               return ds;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
    }



}

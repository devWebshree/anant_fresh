﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="Result.aspx.cs" Inherits="Exam_Result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="Contentbox">

        <style type="text/css">
            .btncv {
                padding: 8px 33px;
            }

            .pl-log-div {
                float: right;
                font-size: 14px;
            }

            .pl-log-img {
                float: left;
                height: 10px;
                width: 10px;
                background-image: url(../images/cras.png);
                margin-right: 5px;
            }

            .pl-log-txt {
                float: left;
                color: Red;
            }

           
            /*.input {
                width: 280px;
            }*/

            .inputbox {
                width: 70%;
                float:none;
            }
            .lbl1 {
                width:35%;
                text-transform:uppercase;
            }
            .lbl2 {
               float: left;
font-size: 12px;
color: #333;
font-weight: bold;
            }
        </style>


        <div class="row">
            <h3 class="title1">AnantFresh Saraswati Award Exam Result</h3>
            <div class="clear">
            </div>

        </div>
        <div class="row m20">
            <div id="ContentPlaceHolder1_login1_pnl_login" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ContentPlaceHolder1_login1_lnklogsubmit')">
                <span style="font-size:14px;">Thanks for participating at <strong>AnantFresh Saraswati Award Exam.</strong> Kindly fill the details in form below to get your result.</span>

                <div class="leftbox" style="margin-top: 30px; width: 40%;">
                    <div class="loginhed">
                        <h2 style="color:#116591; font-size:16px; font-weight:bold;">Get your Result Here...
                        </h2>
                    </div>
                    <ul class="lgnul">
                        <li>
                            <label>
                                <asp:Button ID="btnSearch" CssClass="_button1 backgroundbg border-radius right btncv" runat="server" Text="Search" OnClick="btnSearch_Click" /></label>
                            <div class="inputbox">

                                
                            <asp:TextBox ID="txtName" Visible="false" class="input" placeholder="Enter Your Name." runat="server"></asp:TextBox>

                                 <asp:TextBox ID="txtRollNo" class="input" placeholder="Enter Your Roll No." runat="server"></asp:TextBox>

                            

                            </div>
                        </li>

                        <li class="m10">

                            <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        </li>

                        <li>

                            <span id="pnldetails" runat="server" visible="false">
                                <ul class="lgnul">



                                    <li class="brdr">
                                        <span class="lbl1">Name :
                                        </span>
                                        <asp:Label ID="lblName" CssClass="lbl2" runat="server"></asp:Label>
                                    </li>

                                     <li class="brdr">
                                        <span class="lbl1">Father Name :
                                        </span>
                                      

                                                <asp:Label  CssClass="lbl2" ID="lblFatherName" runat="server"></asp:Label>

                                         
                                    </li>

                                     <li class="brdr">
                                        <span class="lbl1">School Name :
                                        </span>
                                      

                                                <asp:Label  CssClass="lbl2" ID="lblSchoolName" runat="server"></asp:Label>

                                         
                                    </li>

                                    <li class="brdr">
                                        <span class="lbl1">Roll Number :
                                        </span>
                                     
                                                <asp:Label  CssClass="lbl2" ID="lblRollNumber" runat="server"></asp:Label>

                                         
                                    </li>

                                    <li class="brdr">
                                        <span class="lbl1">Class :
                                        </span>
                                      
                                                <asp:Label ID="lblClass"  CssClass="lbl2" runat="server"></asp:Label>

                                    </li>

                                              <li class="brdr">
                                        <span class="lbl1">Marks Obtained :
                                        </span>
                                     
                                                <asp:Label ID="lblMarks"  CssClass="lbl2" runat="server"></asp:Label>

                                         
                                    </li>

                                     <li class="brdr">
                                        <span class="lbl1">Maximum Marks :
                                        </span>
                                       

                                                <asp:Label ID="lblTotalmarks"  CssClass="lbl2" runat="server"></asp:Label>

                                       
                                    </li>

                          


                                </ul>

                            </span>
                        </li>
                    </ul>
                </div>

                <%--<div class="leftbox">
                    <p class="mclear">
                        
                   </p>
                </div>--%>

            </div>
        </div>



    </div>

</asp:Content>


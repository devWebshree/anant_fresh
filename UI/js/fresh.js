﻿$(window).load(function () {

  

    $('.itemproduct img').each(function () {
        var maxWidth = 150; // Max width for the image
        var maxHeight = 150;    // Max height for the image
        var ratio = 0;  // Used for aspect ratio
        var width = $(this).width();    // Current image width
        var height = $(this).height();  // Current image height

        if (width > height) {
            ratio = maxWidth / width;   // get ratio for scaling image
            $(this).css("width", maxWidth); // Set new width
            $(this).css("height", height * ratio);  // Scale height based on ratio
        }
        else {
            ratio = maxHeight / height;
            $(this).css("height", maxHeight);   // Set new height
            $(this).css("width", width * ratio);
        }
        var $img = $(this),
        css = {
            position: 'absolute',
            marginLeft: '-' + (parseInt($img.css('width')) / 2) + 'px',
            left: '50%',
            top: '50%',
            marginTop: '-' + (parseInt($img.css('height')) / 2) + 'px'
        };

        $img.css(css);
    });


    $('.resizeimage img').each(function () {
      
        var maxWidth = $(this).attr('data-maxWidth'); // Max width for the image
        var maxHeight = $(this).attr('data-maxHeight');    // Max height for the image
        var ratio = 0;  // Used for aspect ratio
        var width = $(this).width();    // Current image width
        var height = $(this).height();  // Current image height

        if (width > height) {
            ratio = maxWidth / width;   // get ratio for scaling image
            $(this).css("width", maxWidth); // Set new width
            $(this).css("height", height * ratio);  // Scale height based on ratio
        }
        else {
            ratio = maxHeight / height;
            $(this).css("height", maxHeight);   // Set new height
            $(this).css("width", width * ratio);
        }
        var $img = $(this),
        css = {
            position: 'absolute',
            marginLeft: '-' + (parseInt($img.css('width')) / 2) + 'px',
            left: '50%',
            top: '50%',
            marginTop: '-' + (parseInt($img.css('height')) / 2) + 'px'
        };

        $img.css(css);
    });




    $('.itemproductcall img').each(function () {
        var maxWidth = 100; // Max width for the image
        var maxHeight = 110;    // Max height for the image
        var ratio = 0;  // Used for aspect ratio
        var width = $(this).width();    // Current image width
        var height = $(this).height();  // Current image height

        if (width > height) {
            ratio = maxWidth / width;   // get ratio for scaling image
            $(this).css("width", maxWidth); // Set new width
            $(this).css("height", height * ratio);  // Scale height based on ratio
        }
        else {
            ratio = maxHeight / height;
            $(this).css("height", maxHeight);   // Set new height
            $(this).css("width", width * ratio);
        }
        var $img = $(this),
        css = {
            position: 'absolute',
            marginLeft: '-' + (parseInt($img.css('width')) / 2) + 'px',
            left: '50%',
            top: '50%',
            marginTop: '-' + (parseInt($img.css('height')) / 2) + 'px'
        };

        $img.css(css);
    });



    $('.itemproduct1 img').each(function () {
        var maxWidth = 200; // Max width for the image
        var maxHeight = 200;    // Max height for the image
        var ratio = 0;  // Used for aspect ratio
        var width = $(this).width();    // Current image width
        var height = $(this).height();  // Current image height

        if (width > height) {
            ratio = maxWidth / width;   // get ratio for scaling image
            $(this).css("width", maxWidth); // Set new width
            $(this).css("height", height * ratio);  // Scale height based on ratio

        }
        else {
            ratio = maxHeight / height;
            $(this).css("height", maxHeight);   // Set new height
            $(this).css("width", width * ratio);
        }



        var $img = $(this),
        css = {
            position: 'absolute',
            marginLeft: '-' + (parseInt($img.css('width')) / 2) + 'px',
            left: '50%',
            top: '50%',
            marginTop: '-' + (parseInt($img.css('height')) / 2) + 'px'
        };

        $img.css(css);
    });

});





$(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
        $('#header').addClass("sticky");
        $(".logindropdown").hide();
    }
    else {
        $('#header').removeClass("sticky");
        $(".logindropdown").show();

    }
});

$(document).ready(function () {


    $(".fancybox").fancybox();


    $(":input").focus(function () {
        $(this).addClass("focus");
    });

    $(":input").blur(function () {
        $(this).removeClass("focus");
    });

    $(".navbtn").click(function () {
        $(".logindropdown").toggle();
    });

    $(".logindropdown").click(function () {
        e.stopPropogation();
    });

    $('.bxslider').bxSlider({
        auto: true,
        mode: 'fade'
    }
    );

    $(".presshslider").bxSlider({
        default: 'horizontal'
    });

   


    $('#tab-container').easytabs();

    $('#main-menu a').mouseover(function () {
        var $this = $(this);
        var id = $this.attr('rel');
        var $currentWidget = $('#' + id);
        $currentWidget.show().siblings(".hoverdiv").hide();

    });

    $('._categoryul>li a').mouseover(function () {
        $(this)
        .parents('._categoryul')
        .find('a').css({ 'background': '#fff', 'color': '#116591' })
        .prev().attr('href', '')
        .end().end().end()
        .css({ 'background': '#1f85ba', 'color': '#fff' }).prev().attr('href', '');

    });


    $("._maincategory").mouseleave(function () {
        $(".hoverdiv").hide();
        $("._categoryul li a").css({ 'background': '#fff', 'color': '#000' });
    });


    // Animate the scroll to top start
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.go-top').fadeIn();
        } else {
            $('.go-top').fadeOut();
        }
    });


    $('.go-top').click(function (event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 300);
    })
    // Animate the scroll to top end

    // Animate the toggle bottum top

    $('#pnl').hide();
    $('#tag').on('click', function () {
        $('#pnl').slideToggle();
    });


    // Animate the toggle bottum top
   
});




//-----------------------------------------------------------------for sub menu item
$(document).ready(function () {

    $('._categoryul>li a').mouseover(function () {
    
        $('.inrmenul').empty();
        var id = $(this).attr('data-id');
        var CatName = $(this).attr('data-name');
        var x = $(this).offset();
        var div = '<div class="col mll5"> <ul class="inrmenul">';
        $.ajax({

            type: "POST",
            url: "/index.aspx/getSubCategory",
            data: "{'catId':'" + id + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {

                var x = data.d;
                for (var item in x) {

                    div += '  <li><a href="/product/' + CatName + '/' + ((x[item].SubCategory).replace(/ /g, "-")).replace(/&/g, "All") + '">' + x[item].SubCategory + '</a></li>'
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //alert("Error");
            }
        });
        div += '</ul> </div>';

        // var top = $(this).offset().top;
        $('#innermenu').empty();
        $('#innermenu').append(div);

    });
});


//---------------------------------------------------------------------------------------------------------------


    
              
               
            
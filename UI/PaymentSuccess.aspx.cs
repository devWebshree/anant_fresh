﻿using BAL;
using ClassLibrary;
using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PaymentSuccess : System.Web.UI.Page
{
    modifyData ad = new modifyData();
    SendMail sm = new SendMail();
    Stock stk = new Stock();
    dlStock objstk = new dlStock();

    CallExecutive call = new CallExecutive();
    dlCallExecutive objCall = new dlCallExecutive();
    ManageData md = new ManageData();
    static string OrderNo;
    protected void Page_Load(object sender, EventArgs e)
    {
        isPaymentSuccess();
    }

    private void isPaymentSuccess()
    {
        try
        {

            string[] merc_hash_vars_seq;
            string merc_hash_string = string.Empty;
            string merc_hash = string.Empty;
            string order_id = string.Empty;
            string txnid = string.Empty;
            string fname = string.Empty;
            string emailid = string.Empty;
            string phno = string.Empty;
            string amnt = string.Empty;
            string hash_seq = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

            if (Request.Form["status"] == "success")
            {

                merc_hash_vars_seq = hash_seq.Split('|');
                Array.Reverse(merc_hash_vars_seq);
                merc_hash_string = ConfigurationManager.AppSettings["SALT"] + "|" + Request.Form["status"];
                foreach (string merc_hash_var in merc_hash_vars_seq)
                {
                    merc_hash_string += "|";
                    merc_hash_string = merc_hash_string + (Request.Form[merc_hash_var] != null ? Request.Form[merc_hash_var] : "");
                }
                merc_hash = Generatehash512(merc_hash_string).ToLower();
                if (merc_hash != Request.Form["hash"])
                {
                    Response.Write("Hash value did not matched");

                }
                else
                {
                    txnid = Request.Form["txnid"];
                    order_id = Request.Form["productinfo"];
                    fname = Request.Form["firstname"];
                    emailid = Request.Form["email"];
                    //phno = Request.Form["productinfo"];
                    amnt = Request.Form["amount"];
                    //Response.Write("value matched");
                    //sendorderRequestmail(order_id);

                    sm.sendViewcart(emailid, order_id, DateTime.Now.ToString(), fname);
                    sm.sendmailTOAdmin(emailid, order_id, DateTime.Now.ToString(), "Admin");

                    paymentSuccess(txnid, "Success", order_id);

                    string Message = "<div>Payment Successfully<hr> "+
                     "We've received your order.Your order details are:"+
                     "<br/>Order no. " + order_id +
                     "<br/>Rs. " + amnt +
                     ".<br/>It will be dispatched within 5 - 7 working days."+
                     "<br/><br/>For any query e-mail: support@anantfresh.com:" +
                     "<br/><br/>Thank you! </div>";
                    lblMsg.Text = Message;
                  //  Response.Write(Message);
                }

            }

            else
            {

                //Response.Write("Hash value did not matched");
                //osc_redirect(osc_href_link(FILENAME_CHECKOUT, 'payment' , 'SSL', null, null,true));
                //sendorderRequestmail("arvind@webshree.in", "CUR-4462714", "", "Arvind");
                //Response.Redirect(Session["path"].ToString());

            }
        }

        catch (Exception ex)
        {
            Response.Write("<span style='color:red'>" + ex.Message + " " + ex.Source + "</span>");
        }
    }

    public void sendorderRequestmail(string OrderNo)
    {
        string shippingAddress = "", Discount = "0", Amount = "0", TotalQunatity="0",ShippingCharge="0",OrderDate,EmailId,Name;
        string Product = "";
        int Quantity = 0;
        DataSet ds_tblFinalOrder = objCall.selectFromPurchageTable(OrderNo);
        if (ds_tblFinalOrder.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds_tblFinalOrder.Tables[0].Rows)
            {
                OrderDate = dr["OrderDt"].ToString();


                Name = ds_tblFinalOrder.Tables[1].Rows[0]["name"].ToString();
                EmailId = ds_tblFinalOrder.Tables[1].Rows[0]["Email"].ToString();
                //lblPhone3.Text = ds.Tables[1].Rows[0]["phone"].ToString();
              //  lblAddress2.Text = ds.Tables[1].Rows[0]["address1"].ToString() + "," + ds.Tables[1].Rows[0]["city"].ToString() + "," + ds.Tables[1].Rows[0]["state"].ToString() + "," + ds.Tables[1].Rows[0]["zipcode"].ToString();
               // lblAmount.Text = dr["MainAmount"].ToString();
              //  lblBillTo.Text = dr["BAddress"].ToString();
    
                ShippingCharge = (Convert.ToDouble(dr["Amount"]) - Convert.ToDouble(dr["MainAmount"])).ToString();
                shippingAddress = dr["SAddress"].ToString();
                Amount = dr["Amount"].ToString();
                Discount = dr["Discount"].ToString();
                TotalQunatity = objstk.getTotalQuantityByOrderId(OrderNo);
            }
        }
      
        DataSet ds_tblPurchase = objCall.selectOrderedDetails(OrderNo);
        if (ds_tblPurchase.Tables[0].Rows.Count > 0)
        {
            Product += "<table width='100%'>" +
                    "<tr>" +
                               "<th style='padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial; background-color: rgb(238, 238, 238);'  width='70%'> Product</th>" +
                               "<th style='padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial; background-color: rgb(238, 238, 238);'> Quantity</th>" +
                               "<th style='padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial; background-color: rgb(238, 238, 238);'>Unit price</th>" +
                               "<th style='padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial; background-color: rgb(238, 238, 238);'>Subtotal</th>" +
                   "</tr>";

            foreach (DataRow dr in ds_tblPurchase.Tables[0].Rows)
            {
                Quantity++;
                Product += "<tr>" +
                            "<td style='color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding: 5px 10px; background-color: rgb(255, 255, 255);'>"
                               + dr["item"].ToString() +
                            "</td>" +

                            "<td style='color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding: 5px 10px; text-align: center; background-color: rgb(255, 255, 255);'>"
                               + dr["Quantity"].ToString() +
                            "</td>" +

                            "<td style='color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding: 5px 10px; text-align: right; background-color: rgb(255, 255, 255);'>"
                               + dr["price"].ToString() +
                            "</td>" +
                           "<td style='color: rgb(0, 0, 0); font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: Arial; padding: 5px 10px; text-align: right; white-space: nowrap; background-color: rgb(255, 255, 255);'>"
                              + " <b>" + (Convert.ToDouble(dr["Quantity"]) * Convert.ToDouble(dr["price"])).ToString() + "</b>" +
                            "</td>" +
                       "</tr>";
            }

            Product += "</table>";
        }
        string Mode;
        //if (Session["PaymentMode"] != "0")
        //{
        //    Mode = Session["PaymentMode"].ToString();
        //}
        //else
        //{
        //    Mode = "Credit Card/Debit Card";

        //}
       // sm.orderRequest(EmailId, OrderNo, Amount, Discount, shippingAddress, Product, Name, Mode, Quantity.ToString());

    }

    public void paymentSuccess(string TransactionId, string PaymentStatus, string OrderId) 
    {
        CallExecutive call = new CallExecutive();
        dlCallExecutive objCall = new dlCallExecutive();
        objCall.updatePaymentOption(TransactionId, PaymentStatus, OrderId);
    }
    public string Generatehash512(string text)
    {

        byte[] message = Encoding.UTF8.GetBytes(text);

        UnicodeEncoding UE = new UnicodeEncoding();
        byte[] hashValue;
        SHA512Managed hashString = new SHA512Managed();
        string hex = "";
        hashValue = hashString.ComputeHash(message);
        foreach (byte x in hashValue)
        {
            hex += String.Format("{0:x2}", x);
        }
        return hex;

    }
}
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BAL;

public partial class controls_updtprfl : System.Web.UI.UserControl
{
    modifyData reg = new modifyData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                binddata();
            }
            else
            {
                Response.Redirect("/login/Profile");
            }
        }
    }
    public void binddata()
    {
        DataSet ds = reg.registerdata(HttpContext.Current.User.Identity.Name.ToString());
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataList1.DataSource = ds;
            DataList1.DataBind();
        }
        else 
        {
            DataList1.DataSource = null;
            DataList1.DataBind();
        }

    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string uName = HttpContext.Current.User.Identity.Name;

        if (e.CommandName == "Update")
        {
           
            //cmbmonth.Text + "/" + cmbdate.SelectedIndex + "/" + cmbyear.Text;
            string id = DataList1.DataKeys[e.Item.ItemIndex].ToString();
            //string uname = ((TextBox)e.Item.FindControl("txtusername")).Text;
            //string password = ((TextBox)e.Item.FindControl("txtnme")).Text;
            string name = ((TextBox)e.Item.FindControl("txtname")).Text;
            string pno = ((TextBox)e.Item.FindControl("txtpno")).Text;
            string email = ((TextBox)e.Item.FindControl("txtemail")).Text;


            string addr= ((TextBox)e.Item.FindControl("txtaddr1")).Text;
            string City = ((TextBox)e.Item.FindControl("txtcity")).Text;

            string state = ((TextBox)e.Item.FindControl("txtstate")).Text;
            string zcd= ((TextBox)e.Item.FindControl("txtzip")).Text;
          

            //reg.profileupdate(id, name,pno,email,addr, City, state, zcd);
            reg.profileupdate(name,email,pno,addr,City,state,zcd,id);
            DataList1.EditItemIndex = -1;
            binddata();

            JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, " Your Profile is Successfully Updated ", 1000);

          
        }
    }
    protected void DataList1_EditCommand(object source, DataListCommandEventArgs e)
    {
        DataList1.EditItemIndex = e.Item.ItemIndex;
        binddata();
    }
    protected void DataList1_CancelCommand(object source, DataListCommandEventArgs e)
    {
        DataList1.EditItemIndex = -1;

        binddata();
    }
    protected void mylnk_Click(object sender, EventArgs e)
    {
        Response.Redirect("reg-details.aspx");
    }
    protected void lnkord_Click(object sender, EventArgs e)
    {
        Response.Redirect("my-account.aspx");
    }
    protected void chglnk_Click(object sender, EventArgs e)
    {
        Response.Redirect("/changepassword");
    }
}

﻿using System;
using System.IO;
using System.Web;
using System.Web.Mail;
using System.Data;
using ClassLibrary;



public class SendMail
{
    static string fromAddress = "adminmail@webshree.com";// Gmail Address from where you send the mail
    static string fromPassword = "okmijn#215"; //Password of your gmail address
    static string smtpServer = "mail.webshree.com";
    static string visibleName = "\"AnantFresh.com\"" + "info@anantfresh.com";
    static string AdminEmail = "pooja@webshree.com";//arvind@webshree.in";//
    static string time;
    DateTime dt = new DateTime();
    public SendMail()
    {

        dt = DateTime.Now;
        time = dt.ToString("dd,MMM yyyy hh:mm tt");
    }
    public void sendMail(string EmailId, string subject, string myString)
    {
        var toAddress = EmailId;
        int cdoBasic = 1;
        int cdoSendUsingPort = 2;
        System.Web.Mail.MailMessage obj = new System.Web.Mail.MailMessage();

        obj.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", smtpServer);
        obj.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);
        obj.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", cdoSendUsingPort);
        obj.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", cdoBasic);
        obj.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", fromAddress);
        obj.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", fromPassword);
      //  obj.To = toAddress;
      //  obj.Bcc = "puja@webshree.com";
        obj.Bcc = AdminEmail;
        obj.From = visibleName;
        obj.Subject = subject;
        obj.Body = myString;
        obj.BodyFormat = MailFormat.Html;
        try
        {
            SmtpMail.SmtpServer = smtpServer;
            SmtpMail.Send(obj);
        }
        catch (Exception ex) { }
    }
    //contact-us.aspx
    public void sendFeedback(Feedback sm)
    {
        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/html/feedback.htm"));
        string readFile = reader.ReadToEnd();
        string myString = "";
        myString = readFile;
        myString = myString.Replace("$$user$$", sm.Name);
        myString = myString.Replace("$$product$$", sm.ProductName);
        //myString = myString.Replace("$$mobile$$", sm.MobileNo);
        //myString = myString.Replace("$$pname$$", sm.ProductName);
        //myString = myString.Replace("$$pQuility$$", sm.ProductQuality);
        //myString = myString.Replace("$$comment$$", sm.Comment);
        myString = myString.Replace("$$subject$$", "Feedback - We would love to hear from you on your recent order!");
        reader.Dispose();
        try
        {
            sendMail(sm.EmailID, "Your Feedback - We would love to hear from you on your recent order!", myString);
        }
        catch (Exception ex)
        {

        }

    }
    public void sendUserEnquiry(Enquiry sm)
    {

        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/html/enquiry.htm"));
        string readFile = reader.ReadToEnd();
        string myString = "";
        myString = readFile;
        myString = myString.Replace("$$user$$", sm.Name);
        //myString = myString.Replace("$$email$$", sm.EmailID);
        //myString = myString.Replace("$$mobile$$", sm.MobileNo);
        myString = myString.Replace("$$pname$$", sm.ProductName);
        //myString = myString.Replace("$$comment$$", sm.Comment);

        myString = myString.Replace("$$subject$$", "Your Bulk Enquiry On AnantFresh!");
        reader.Dispose();
        try
        {
            sendMail(sm.EmailID, "Your Bulk Enquiry On AnantFresh!", myString);
        }
        catch (Exception ex)
        {

        }
        finally
        {

        }

    }
    public void sendContactMail(Contact ct)
    {
        //for Admin
        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/html/contactus.htm"));
        string readFile = reader.ReadToEnd();
        string myString = "";
        myString = readFile;
        myString = myString.Replace("$$user$$", ct.Name);
        myString = myString.Replace("$$email$$", ct.EmailID);
        myString = myString.Replace("$$mobile$$", ct.MobileNo);
        myString = myString.Replace("$$comment$$", ct.Comment);
        myString = myString.Replace("$$time$$", time);
        reader.Dispose();
        sendMail("info@anantfresh.com", "Admin- Contact us details", myString);
        //for user
        StreamReader reader1 = new StreamReader(HttpContext.Current.Server.MapPath("~/html/userContact.htm"));
        string readFile1 = reader1.ReadToEnd();
        string myString1 = "";
        myString1 = myString1.Replace("$$user$$", ct.Name);
        reader1.Dispose();
        myString1 = readFile1;
        myString1 = myString1.Replace("$$user$$", ct.Name);
        sendMail(ct.EmailID, "AnantFresh- Thankyou For Contacting us", myString1);
       
    }
    //  //contact-us.aspx end
    public void sendViewcart(string email, string ord, string dt, string usr)
    {
        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/html/requestOrder.htm"));
        string readFile = reader.ReadToEnd();
        string myString = "";
        myString = readFile;
        myString = myString.Replace("$$user$$", usr);
        myString = myString.Replace("$$ord$$", ord);
        myString = myString.Replace("$$dt$$", dt);
        myString = myString.Replace("$$subject$$", "Your AnantFresh.com Order request has been Received!");
        reader.Dispose();
        try
        {

            sendMail(email, "Your AnantFresh.com Order request has been Received!", myString);
        }
        catch (Exception ex)
        {
           
        }
    }

    public void orderStatus(string email, string ord, string usr,string sub)
    {
       
        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/html/requestOrder.htm"));
        string readFile = reader.ReadToEnd();
        string myString = "";
        myString = readFile;
        myString = myString.Replace("$$user$$", usr);
        myString = myString.Replace("$$status$$", "dispatched");
        myString = myString.Replace("$$ord$$", ord);
        myString = myString.Replace("$$dt$$", dt.ToString());
        myString = myString.Replace("$$subject$$", sub);
        reader.Dispose();
        try
        {
            sendMail(email, sub, myString);
        }
        catch (Exception ex)
        {

        }

    }

    public void sendmailTOAdmin(string email, string ord, string dt, string usr)
    {

       StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/html/requestOrder.htm"));
        string readFile = reader.ReadToEnd();
        string myString = "";
        myString = readFile;
        myString = myString.Replace("$$user$$", usr);
        myString = myString.Replace("$$ord$$", ord);
        myString = myString.Replace("$$dt$$", dt);
        myString = myString.Replace("$$subject$$", "Request Order from AnantFresh.com-Admin!");
        reader.Dispose();
        try
        {
            sendMail("info@anantfresh.com", "Request Order from AnantFresh.com-Admin!", myString);

        }
        catch (Exception ex)
        {
           
        }

    }
    public void sendRegisteredUser(string name,string Userid, string pass)
    {

        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/html/welcome.htm"));
        string readFile = reader.ReadToEnd();
        string myString = "";
        myString = readFile;
        myString = myString.Replace("$$user$$", name);
        myString = myString.Replace("$$email$$", Userid);
        myString = myString.Replace("$$pass$$", pass);
        myString = myString.Replace("$$subject$$", "Welcome to AnantFresh.com!");
        reader.Dispose();

        try
        {
            sendMail(Userid, "Welcome to AnantFresh.com", myString);

        }
        catch (Exception ex)
        {
           
        }
    }
    public void sendOredrDetails(string Userid, string order,string dtl)
    {
        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/html/orderDispatched.htm"));
        string readFile = reader.ReadToEnd();
        string myString = "";
        myString = readFile;
        myString = myString.Replace("$$ord$$", order);
        myString = myString.Replace("$$dtl$$", dtl);
        myString = myString.Replace("$$status$$", "dispatched");
        myString = myString.Replace("$$subject$$", "Your AnantFresh.com order has been dispatched!");
        reader.Dispose();
        try
        {
            sendMail(Userid, "Your AnantFresh.com order has been dispatched!.", myString);
        }
        catch (Exception ex)
        {

        }
    }
    public void sendMailForget(string email, string user, string pass) 
    {

        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/html/forgetPass.htm"));
        string readFile = reader.ReadToEnd();
        string myString = "";
        myString = readFile;
        myString = myString.Replace("$$user$$", user);
        myString = myString.Replace("$$email$$", email);
        myString = myString.Replace("$$pass$$", pass);
        myString = myString.Replace("$$subject$$", "Your AnantFresh.com Password Information");
        reader.Dispose();
        try
        {
            sendMail(email, "Your AnantFresh.com Password Information!", myString);            
        }
        catch (Exception ex)
        {
            
        }
        finally
        {

        }

    }

}


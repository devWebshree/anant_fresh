﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using ClassLibrary;
using BAL;
using DAL;
using System.Data;
public partial class Admin_OrderLog : System.Web.UI.Page
{
    modifyData ad = new modifyData();
    dlSubCategory objscat = new dlSubCategory();
    SubCategoryCS scat = new SubCategoryCS();
    Utility all = new Utility();

    static DataSet Export;

    Search srch = new Search();
    dlStock objstk = new dlStock();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtedt.Text = System.DateTime.Now.Date.ToString("MM-dd-yyyy");
            bindCategoryName();
            show();
        }
    }


    protected void ddlCategoryName_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindSubCategoryName();
        show();
    }
    protected void ddl_subCat_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }
    public void bindCategoryName()
    {
        DataSet ds = ad.CategoryForProduct();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCategoryName.DataSource = ds;
            ddlCategoryName.DataTextField = "category";
            ddlCategoryName.DataValueField = "catid";
            ddlCategoryName.DataBind();
            ddlCategoryName.Items.Insert(0, "All");
        }
        else
        {
            ddlCategoryName.DataSource = null;
            ddlCategoryName.DataBind();
        }
    }
    public void bindSubCategoryName()
    {

        DataSet ds = objscat.getSubCategoryNameList(ddlCategoryName.SelectedItem.Value);//Convert.ToInt32(ddl_subCat.SelectedIndex)
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddl_subCat.DataSource = ds;
            ddl_subCat.DataTextField = "SubCategory";
            ddl_subCat.DataValueField = "subcatid";
            ddl_subCat.DataBind();
            ddl_subCat.Items.Insert(0, "All");
        }
        else
        {
            ddl_subCat.DataSource = null;
            ddl_subCat.DataBind();
        }

    }

    public void show()
    {
        try
        {
            string scat = null, cat = null ,todate=null;

            if (ddlCategoryName.SelectedItem.Value.ToString() != "All")
            {
                cat = ddlCategoryName.SelectedItem.Value.ToString();
            }
            if (ddl_subCat.SelectedItem.Value.ToString() != "All")
            {
                scat = ddl_subCat.SelectedItem.Value.ToString();
            }

            if (txtsdt.Text != "")
                todate = txtsdt.Text;

            srch.SearchBy = txt_search.Text;
            srch.CatID = cat;
            srch.SubCatID = scat;
            srch.ToDate =Convert.ToDateTime(txtedt.Text).AddDays(1).ToString("MM-dd-yyyy") ;
            srch.FromDate = todate;


            DataSet ds = objstk.getStockLog(srch);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Export = ds;
                gvrStockLog.DataSource = ds;
                gvrStockLog.DataBind();
            }
            else
            {
                Export = null;
                gvrStockLog.DataSource = null;
                gvrStockLog.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btn_srch_Click(object sender, EventArgs e)
    {
        show();
    }


    protected void gvrStockLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvrStockLog.PageIndex = e.NewPageIndex;
        show();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        if (Export != null)
        {
            if (Export.Tables[0].Rows.Count > 0)
            {
                // ds = ad.SelAllFinalProducts1();
                string flname = null;
                Random rnd = new Random();
                flname = "AF-OrderLog" + DateTime.Now.ToString("dd MMM,yyy hh:mm:ss tt") + ".xls";
                ExportDataSetToExcel(Export, flname);
            }
        }
    }
    public void ExportDataSetToExcel(DataSet ds, string filename)
    {
        HttpResponse response = HttpContext.Current.Response;


        response.Clear();
        response.Charset = "";


        response.ContentType = "application/vnd.ms-excel";
        response.AddHeader("Content-Disposition", "attachment;filename=\"" + filename + "\"");


        using (StringWriter sw = new StringWriter())
        {
            using (HtmlTextWriter htw = new HtmlTextWriter(sw))
            {

                DataGrid dg = new DataGrid();
                dg.DataSource = ds.Tables[0];

                dg.DataBind();
                dg.RenderControl(htw);
                response.Write(sw.ToString());
                response.End();
            }

        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="orderDetails.ascx.cs" Inherits="control_orderdtl" %>
<div class="row m10">
    <div class="registerdiv clearfix" style="width: 93%;">


<div class="clear"></div>
<div class="row">
  <h2 class="left"> Order Details</h2>
  <asp:LinkButton ID="bcklnk" runat="server" CssClass="button right" style="width:120px; text-align:center;" CausesValidation="False" OnClick="bcklnk_Click"><< Print Invoice </asp:LinkButton>
</div>
<div class="line m10"></div>
<div class="clear"></div>

<div class="row m10">
<div class="column border-box">        
<asp:DataList ID="dlst" runat="server" CellPadding="4" ForeColor="#333333" CssClass="table" style="width:100%;">
<ItemTemplate>

<div class="clft left">
<table>
<tr><td>
<b>Order Date</b></td>
<td><asp:Label ID="lblodt" runat="server" Text='<%# Eval("DOP","{0:dd-MMM-yy}") %>' CssClass="lbl1"></asp:Label>
</td></tr>

<tr><td>
<b>Order No</b></td><td>
<asp:Label ID="lblord" runat="server" Text='<%# Eval("OrderNo") %>' CssClass="lbl1"></asp:Label>
</td></tr>


<tr><td>
<b>Order Value</b></td><td>
<span class="left">&#8377;</span> <asp:Label ID="lblordval" runat="server" Text='<%# Eval("amount") %>' CssClass="lbl1"></asp:Label>
</td></tr>


<tr><td>
<b>Order Status</b></td>
<td>
<asp:Label ID="lbldis" runat="server" Text='<%# GetDis(Eval("Dispatch")) %>' CssClass="lbl1"></asp:Label><br />
<asp:Label ID="Label1" runat="server" Text='<%# Eval("ClMsg") %>'></asp:Label>
</td></tr>
</table>

</div>


<div class="clft right">
<asp:Label ID="lbladd" runat="server" Text='<%# Eval("address") %>'></asp:Label>
</div>

 </ItemTemplate>
 </asp:DataList>
</div>
</div>
<div class="row m20"><h2>Product Details</h2></div>
<div class="line m10"></div>
<div class="clear"></div>
<div class="column border-box">  
<div class="row ">









<asp:GridView ID="grdViewOrdersOfCustomer" runat="server" AutoGenerateColumns="false" 
                    DataKeyNames="orderno" CssClass="table" Width="100%" 
                    >
                <columns>



             <asp:TemplateField HeaderText="Serial No.">
                        <ItemTemplate>

                    <strong> <%# Container.DataItemIndex + 1 %></strong>
              

                            </ItemTemplate>
                  </asp:TemplateField>



                  <asp:TemplateField  HeaderText="Code" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_itemCode" runat="server" Text='<%# bind("code") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>


                  <asp:TemplateField  HeaderText="Available Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_iteggmCode" runat="server" Text='<%# objstk.selectQuantity(Eval("itemid").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>



                  <asp:TemplateField  HeaderText="Product" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_item" runat="server" Text='<%# bind("Item") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>

                  <%--<asp:TemplateField  HeaderText="Available Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                      <asp:Label ID="lbl_itemQuantity" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>--%>

                  <asp:TemplateField  HeaderText="Quantity" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                       <%--  ReadOnly='<%# Convert.ToInt32(Eval("flagstatus"))!=1 ? true :false  %>'   --%>
                           <asp:TextBox ValidationGroup="xtx" ID="txt_quantity"  runat="server" MaxLength="5" Width="50px" Text='<%# bind("Quantity") %>'  ontextchanged="txt_quantity_TextChanged" AutoPostBack="true"></asp:TextBox>
                      <asp:RangeValidator ID="Rangevalidator5" runat="server" ControlToValidate="txt_quantity" CssClass="error"
                        Display="Dynamic" ErrorMessage="Invalid Quantity!" MaximumValue="9999" MinimumValue="0"
                        SetFocusOnError="True" Type="Integer" ValidationGroup="xtx"></asp:RangeValidator>


                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>


                  <asp:TemplateField  HeaderText="Product Price  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                        <asp:HiddenField ID="hf_pid" runat="server" Value='<%# bind("pid") %>' />
                       <asp:Label ID="lbl_price" runat="server" Text='<%# bind("price") %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>


                  <asp:TemplateField  HeaderText="Total Price  &#8377;" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                           <asp:Label ID="lbl_tprice" runat="server"  Text='<%# (Convert.ToInt16(Eval("Quantity"))*Convert.ToInt16(Eval("price"))).ToString() %>'></asp:Label>
                    </ItemTemplate>
                      <HeaderStyle BackColor="#116591" ForeColor="White" />
                 </asp:TemplateField>

                  <asp:TemplateField  HeaderText="Delete" HeaderStyle-BackColor="#116591" HeaderStyle-ForeColor="#ffffff">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/Redcross.png" 
                            runat="server" onclick="ImageButton1_Click" ValidationGroup="Cancle" /> 
                    </ItemTemplate>
                    <HeaderStyle BackColor="#116591" ForeColor="White" />
                  </asp:TemplateField>



           </columns>
</asp:GridView>












<%--<asp:DataGrid ID="dgrd" runat="server" AutoGenerateColumns="False" CellPadding="4"
    GridLines="Vertical" PageSize="20" Width="100%"  CssClass="table">
    <Columns>
        <asp:TemplateColumn HeaderText="Item(s)" HeaderStyle-CssClass="tblhd">
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:Label ID="lbsalpr" CssClass="left" runat="server" Text='<%# bind("item") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Item Code" HeaderStyle-CssClass="tblhd">
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:Label ID="lbcds" runat="server" Text='<%# bind("code") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Price" HeaderStyle-CssClass="tblhd">
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:Label ID="lblpr" runat="server" Text='<%# "₹ "+Eval("price") %>' Width="50px"></asp:Label>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-CssClass="tblhd">
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:Label ID="txtqty" runat="server" MaxLength="9" Text='<%# bind("quantity")  %>'
                    Width="50px"></asp:Label>
            </ItemTemplate>
        </asp:TemplateColumn>
    </Columns>

   
    <HeaderStyle  ForeColor="Black" Height="30px" BorderColor="White" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
 
   <PagerStyle BackColor="#BDC3C7" ForeColor="White" HorizontalAlign="Center" />
 <HeaderStyle BackColor="#BDC3C7" Font-Bold="True"  
       Font-Size="13px" ForeColor="Black" HorizontalAlign="Center"  />
   
</asp:DataGrid>--%>
</div>
</div>
<div class="clear"></div>
<div class="column border-box">  
<div class="row ">

     <div class="row m20">
             <table Class="table">
                    <tr>
                        <th Class="tblhd">
                           Purchase Amount
                        </th>
                      

                          <th Class="tblhd">
                            Shipping Charges
                        </th>

                        <th Class="tblhd">
                            Total Amount
                        </th>
                    </tr>
                    <tr>
                        <td>
                            &#8377; <asp:Label ID="lblAmount" runat="server" Text="0"></asp:Label>
                        </td>
                     
                         <td>
                           &#8377; <asp:Label ID="lbl_shipcharge" runat="server" Text="0"></asp:Label>.00
                        </td>
                        <td>
                          &#8377; <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label>.00
                        </td>
                    </tr>
                </table>
                </div>

</div></div>



   </div></div>

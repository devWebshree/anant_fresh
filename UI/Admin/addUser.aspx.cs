﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
public partial class Admin_addUser : System.Web.UI.Page
{
    dlCallExecutive objcall = new dlCallExecutive();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindlist();
        }
    }

   
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        string Role = "";
        if (ddl_role.SelectedItem.Value == "1") 
        {
            Role = "dbadmin"; 
        }
        else if (ddl_role.SelectedItem.Value == "2") 
        {
            Role = "CallExecutive";
        }
        else if (ddl_role.SelectedItem.Value == "3") 
        {
            Role = "DispatchExecutive";
        }
        if (Role != "")
        {
            objcall.insertAdminRole(txtname.Text,txtMobile.Text,txt_username.Text, txtpass.Text, Role);
            bindlist();
            txt_username.Text = "";
            txtpass.Text = "";
            showmessage("Account Successfully Created.");
            bindlist();
        }
    }

    public void bindlist()
    {
        try
        {

            System.Data.DataSet ds = objcall.searchRole(txtsearch.Text); 
            if (ds.Tables[0].Rows.Count > 0)
            {
                listAdmin.DataSource = ds;
                listAdmin.DataBind();
            }
            else
            {
                listAdmin.DataSource = null;
                listAdmin.DataBind();
            }

        }
        catch (Exception ex)
        {
            
        }
    }

    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel2, UpdatePanel2.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    protected void OnClick_btnDelete(object sender, EventArgs e)
    {
        var btnDelete = (Button)sender;
        var rptChild = btnDelete.NamingContainer;//Child Repeater
        HiddenField id = (HiddenField)rptChild.FindControl("fhId");
        objcall.deleteAdminRole(id.Value);
        bindlist();
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bindlist();
    }
}
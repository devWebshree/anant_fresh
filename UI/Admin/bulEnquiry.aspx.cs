﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BAL;
using DAL;
using ClassLibrary;
public partial class admin_bulEnquiry : System.Web.UI.Page
{
    dlEnquiry enq = new dlEnquiry();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindfeedback();
        }
    }

    public void bindfeedback()
    {
        DataSet ds;

        ds = enq.listEnquiry();


        if (ds.Tables[0].Rows.Count > 0)
        {
            enquirygrd.DataSource = ds;
            enquirygrd.DataBind();
        }
        else
        {
            enquirygrd.DataSource = null;
            enquirygrd.DataBind();
        }
    }
}
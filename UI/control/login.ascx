﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="login.ascx.cs" Inherits="control_login" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>

<style type="text/css">
.pl-log-div
{
  float:right;
  font-size:14px;
}

.pl-log-img
{
     float:left; 
     height:10px; 
     width:10px; 
     background-image:url(../images/cras.png); 
     margin-right:5px;
}

.pl-log-txt
{
     float:left; 
     color:Red;
}
</style>


<div class="row">
    <div class="loginuser">
    </div>
    <div class="clear">
    </div>
    <h4 class="m10">
        <asp:Label ID="lbl_title" runat="server">Account Login</asp:Label></h4>
</div>
<div class="row m20">
    <asp:Panel ID="pnl_login" runat="server" DefaultButton="lnklogsubmit">
        <div class="leftbox">
            <div class="loginhed">
                <h2>
                    <asp:Image ID="Image1" ImageUrl="~/images/login_icon.png" runat="server" /><span>Existing
                        User</span></h2>
            </div>
            <ul class="lgnul">
                <li>
                    <label for="login-username" class="lbl">
                        User name</label>
                    <div class="inputbox">
                        <asp:TextBox ID="txtusrname" runat="server" TabIndex="1" ValidationGroup="g"  placeholder="Enter Your User name."  CssClass="input"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" ControlToValidate="txtusrname"
                        CssClass="error" Display="Dynamic" ErrorMessage='Use of ^/\<>?"*| is prohibited.'
                        ValidationExpression='^[^/\<>?"*|]{1,100}' ValidationGroup="vglogin"></asp:RegularExpressionValidator>
                    <asp:Label ID="lblmsg" runat="server"></asp:Label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                        ControlToValidate="txtupswrd" Display="Dynamic" ErrorMessage='Use of ^/\<>?"*| is prohibited.'
                        ValidationExpression='^[^/\<>?"*|]{1,100}' ValidationGroup="vglogin"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtusrname"
                        Display="Dynamic" ErrorMessage="Enter User ID!" CssClass="error" SetFocusOnError="True"
                        ValidationGroup="vglogin"></asp:RequiredFieldValidator></div>
                </li>
                <li>
                    <label for="login-password" class="lbl">
                        password</label>
                    <div class="inputbox">
                        <asp:TextBox ID="txtupswrd" runat="server" TabIndex="2"  placeholder="********"  TextMode="Password" CssClass="input"
                            ValidationGroup="g"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtupswrd"
                            Display="Dynamic" ErrorMessage="Enter Password!" ValidationGroup="vglogin" CssClass="error"></asp:RequiredFieldValidator></div>
                </li>
                <li class="m10">
<asp:Panel ID="pnl_wrong" Visible="false" runat="server">
<div class="pl-log-div">
<div class="pl-log-img"></div>
<div class="pl-log-txt">Please enter a valid Email ID & Password ! </div>
</div></asp:Panel>
                    <asp:Button ID="lnklogsubmit" runat="server" CssClass="_button1 backgroundbg border-radius left"
                        Style="margin-left: 130px;" Text="Login" ValidationGroup="vglogin" OnClick="lnklogsubmit_Click" />
                    <asp:Button ID="lbForgrt" runat="server" OnClick="lbForgrt_Click" CssClass="_button1 backgroundbg border-radius right"
                        Text="Forget Password"></asp:Button>
                </li>
            </ul>
        </div>
        <div class="rightbox">
            <div class="loginhed">
                <h2>
                    <asp:Image ID="Image2" ImageUrl="~/images/new-customer_icon.png" runat="server" /><span>New
                        User</span></h2>
            </div>
            <div class="clear"></div>
            <p class="mclear">
                
Create an account! It's quick, free and gives you access to special features.
            </p>
          <div class="register">
              <asp:Button ID="Button1" style=" margin-top:50px;" CssClass="right _button1 backgroundbg border-radius m20" runat="server" Text="Create New Account" 
                  onclick="Button1_Click" />
          
                </div>
        </div>
    </asp:Panel>
</div>

<div class="clear"></div>
<asp:Panel ID="pnl_forget" Visible="false" runat="server">
    <div class="column">
        <div class="center">
            <ul class="lgnul">
                <li>
                    <label for="login-username" class="lbl" style="width:10%;">
                        User name</label>
                    <div class="inputbox">
                    <asp:TextBox ID="txt_username2" placeholder="Enter Your User name." runat="server"
                            TabIndex="1" ValidationGroup="fp" CssClass="input"></asp:TextBox>
                    <asp:Label ID="lblMsg2" runat="server"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_username2"
                        Display="Dynamic" ErrorMessage="Enter User ID!" CssClass="error1" SetFocusOnError="True"
                        ValidationGroup="fp"></asp:RequiredFieldValidator>
                        </div>
                </li>

                <li>
                <a style="width:100px; text-align:center;" Class="_button1 backgroundbg border-radius left" href="/login"><< Back</a> 
                 <asp:LinkButton ID="lb_sendPass" runat="server" style="width:100px; margin-left:5px; text-align:center;" ValidationGroup="fp" OnClick="lb_sendPass_Click"
                        CssClass="_button1 backgroundbg border-radius left">Send Password</asp:LinkButton> 
                </li>
            </ul>
         
        </div>
    </div>
</asp:Panel>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
</asp:UpdatePanel>

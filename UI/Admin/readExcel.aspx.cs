﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using System.Web.Configuration;

public partial class admin_readExcel : System.Web.UI.Page
{
 
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
       


        if (FileUpload1.HasFile)
        {
            try
            {
               
                string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string Extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
                string FolderPath = ConfigurationManager.AppSettings["FolderPath"];
                string FilePath = Server.MapPath(FolderPath + FileName);
                FileUpload1.SaveAs(FilePath);
                string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", FilePath);
                OleDbConnection connection = new OleDbConnection();
                connection.ConnectionString = excelConnectionString;
                OleDbCommand command = new OleDbCommand("select [categoryname],[SubCategory], [code], [name], [image], [price], [offerPrice], [Detail], [IsVisible] from [Sheet1$]", connection);
                connection.Open();
                // Create DbDataReader to Data Worksheet
                DbDataReader dr = command.ExecuteReader();

                // SQL Server Connection String

                string sqlConnectionString =WebConfigurationManager.ConnectionStrings["anantfreshConnectionString"].ToString();
               //string sqlConnectionString = @"Data Source=NAVIN-PC\SQLSERVER2008;Initial Catalog=AnantFresh;User ID=sa;Password=navinsql";
               //Bulk Copy to SQL Server 
                SqlBulkCopy bulkInsert = new SqlBulkCopy(sqlConnectionString);

                bulkInsert.ColumnMappings.Add("categoryname", "categoryname");
                bulkInsert.ColumnMappings.Add("SubCategory", "SubCategory");
                bulkInsert.ColumnMappings.Add("code", "code");
                bulkInsert.ColumnMappings.Add("name", "name");
                bulkInsert.ColumnMappings.Add("image", "image");
                bulkInsert.ColumnMappings.Add("price", "price");
                bulkInsert.ColumnMappings.Add("offerPrice", "offerPrice");
                bulkInsert.ColumnMappings.Add("Detail", "Detail");
                //bulkInsert.ColumnMappings.Add("productprice", "productprice");
                //bulkInsert.ColumnMappings.Add("BestSellers", "BestSellers");
                //bulkInsert.ColumnMappings.Add("DailyDeal", "DailyDeal");
                //bulkInsert.ColumnMappings.Add("IsNew", "IsNew");
                bulkInsert.ColumnMappings.Add("IsVisible", "IsVisible");
                //bulkInsert.ColumnMappings.Add("IsFeatured", "IsFeatured");
                //bulkInsert.ColumnMappings.Add("Size", "Size");
                //bulkInsert.ColumnMappings.Add("Weight", "Weight");
                //bulkInsert.ColumnMappings.Add("ShortDescripn", "ShortDescripn");


                bulkInsert.DestinationTableName = "itemdetailtable";



                bulkInsert.WriteToServer(dr);
                Label1.Text = "Item Uploaded Successfully";
            }
            catch (Exception ex)
            {
                Label1.Text = ex.ToString();// "Somthing Wrong Here !!!";
            }


        }

        
    
    }
}
﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BAL;

public partial class control_shopctrl : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    Double OfferAfter = Convert.ToDouble(ConfigurationManager.AppSettings["OfferAfter"]);
    Double OfferValue = Convert.ToDouble(ConfigurationManager.AppSettings["OfferValue"]); 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddata();
        }


    }
    public void binddata()
    {
        DataSet ds = ad.SelPurProducts(Session["IsUser"].ToString(),"1");
        int i = ds.Tables[0].Rows.Count;
        if (ds.Tables[0].Rows.Count > 0)
        {
            GridView1.DataSource = ds;
            GridView1.DataBind();
            GridView1.Visible = true;
            imgbtn.Visible = true;
            TABLE1.Visible = true;
            TABLE2.Visible = false;
        }
        else
        {
            GridView1.Visible = false;
            imgbtn.Visible = false;
            TABLE1.Visible = false;
            TABLE2.Visible = true;

        }

        lbltitem.Text = i.ToString();
        total();
    }
    public string GetImg(object itemid)
    {
        return ad.getimagename(itemid.ToString());

    }

    protected void imgbtn_Click(object sender, ImageClickEventArgs e)
    {
        if (HttpContext.Current.User.Identity.Name == "")
        {
            Response.Redirect("/login?shop=1");
        }
        else
        {
            Response.Redirect("view-cart.aspx");
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        Label pid = (Label)row.FindControl("lblPid");
        Label name = (Label)row.FindControl("lblitrm");
        ad.DeletePurchaseItem(pid.Text);
        binddata();
        Session["Itemname"] = "Selected item " + name.Text + " is successfully deleted!";
        //JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, Session["Itemname"].ToString(), 1000);
        Session["path"] = HttpContext.Current.Request.Url.AbsoluteUri;
        Response.Redirect(Session["path"].ToString());



    }
    protected void txtqty_TextChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            TextBox txtQuantity = (TextBox)row.FindControl("txtqty");
            Label pid = (Label)row.FindControl("lblPid");
            Label lblitrm = (Label)row.FindControl("lblitemid");
            object obj = lblitrm.Text;
            ad.UpdatePurchaseItem(pid.Text, txtQuantity.Text, Session["IsUser"].ToString());
            binddata();
        }
    }

    protected void lnkshop_Click1(object sender, EventArgs e)
    {
        Response.Redirect("/product");
    }
    protected void imgbtn_Click(object sender, EventArgs e)
    {
        if (HttpContext.Current.User.Identity.Name != "")
        {
            Response.Redirect("/cartCheckOut");
        }
        else
            Response.Redirect("/login/cartCheckOut");
    }

    public void total()
    {
        try
        {
            double i = Convert.ToDouble(ad.selectsmprice(Session["IsUser"].ToString(), "1"));
            lblAmount.Text = i.ToString();
            lblTotal.Text = i.ToString();
            if (i < OfferAfter)
            {
                pnl_cont.Visible = true;
                ltrl_more.Visible = false;
                lbl_lessprice.Text = (OfferAfter - Convert.ToDouble(lblTotal.Text)).ToString();
                lbl_shipcharge.Text = OfferValue.ToString();
                lblTotal.Text = (Convert.ToDouble(lblTotal.Text) + OfferValue).ToString();
            }
            else
            {
                pnl_cont.Visible = false;
                ltrl_more.Visible = true;
                lbl_shipcharge.Text = "0";

            }

        }
        catch (Exception ex)
        {
        }
    }
}

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Category.ascx.cs" Inherits="control_Category" %>
<div class="_categoryName">
    <h2>
        Categories</h2>
</div>
<div class="_maincategory">
    <nav id="main-menu">
        <ul class="_categoryul">
            <asp:Repeater ID="rpt_cat" runat="server">
                <ItemTemplate>
                    <li class="border-box"><a href="/product/<%# all.RemoveSpace(Eval("Category").ToString()) %>/all" data-name='<%# all.RemoveSpace(Eval("Category").ToString()) %>' data-id='<%# Eval("catid") %>'
                        rel="innermenu">
                        <%# Eval("Category") %></a>
            
                        </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </nav>
    <div id="innermenu" class="hoverdiv box_submenu_bottomAlign">
           
    </div>
</div>

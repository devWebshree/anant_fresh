﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="topcontrol.ascx.vb" Inherits="topcontrol" %>
<%@ Register Src="totalitems.ascx" TagName="totalitems" TagPrefix="uc1" %>
<div class="logindropdown">
<ul class="loginul clearfix">
    <li>
    <asp:LinkButton ID="lnkusr" CssClass="backgroundbg border-radius usrdiv" runat="server"> </asp:LinkButton>
    </li>
    <li id="liLogin" runat="server" visible="false">
        <asp:LinkButton ID="lnklgns" Text="Login" CssClass="btn-top1" runat="server" CausesValidation="False"></asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkregs" runat="server" CssClass="btn-top1" CausesValidation="False" Text="Register"></asp:LinkButton>
    </li>
    <li id="liLogout" runat="server" visible="false">
        <asp:LinkButton ID="lnlLogout" CssClass="btn-top1" Text="Logout" runat="server" CausesValidation="False"></asp:LinkButton>
    </li>
</ul>
</div>
<div class="clear">
</div>
<div class="_cart clearfix">
    <div class="wishlist backgroundbg">
        <asp:LinkButton ID="btnWISHLIST" ToolTip="WishList Items" CssClass="" runat="server" CausesValidation="False">
            <asp:Image ID="Image1" ImageUrl="~/images/add_to_wish_icon.png" runat="server" />
        </asp:LinkButton></div>
    <div class="cart backgroundbg border-radius"> 
        <asp:LinkButton ID="btnShopingcart" CssClass="_carta" ToolTip="Cart Items" runat="server">
           <asp:Image ID="Image2" ImageUrl="~/images/cart_icon.png" runat="server" />
                 <uc1:totalitems ID="totalitems1" runat="server" style="color: #333; display: block;
                padding: 5px; width: 100%;" />
        </asp:LinkButton>
         
        
        <%-- <span> <uc1:totalitems ID="totalitems1" runat="server" /></span>--%>
    </div>
</div>

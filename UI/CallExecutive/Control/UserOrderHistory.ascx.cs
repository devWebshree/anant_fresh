﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using BAL;
using System.IO;
using System.Web.Mail;
using ClassLibrary;
using DAL;
public partial class CallExecutive_Control_UserOrderHistory : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    SendMail sm = new SendMail();
    Stock stk = new Stock();
    dlStock objstk = new dlStock();

    CallExecutive call = new CallExecutive();
    dlCallExecutive objCall = new dlCallExecutive();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["order"] != "0")
            {
                bindord();
            }
        }
    }


    void bindord()
    {
        call.dt1 = "";
        call.dt2 = Convert.ToDateTime(DateTime.Now).AddDays(1).ToString("MM-dd-yyyy");
        call.Status = "";
        call.Searchtxt = Session["order"].ToString();
        DataSet DataSetExport = new DataSet();
        DataSet ds = objCall.listFinalOrder(call);
        if (ds.Tables[0].Rows.Count > 0)
        {

            ordgrd.Visible = true;
            ordgrd.DataSource = ds;
            ordgrd.DataBind();
        }
        else
        {
            ordgrd.Visible = false;
        }
    }


    protected void ordgrd_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem | e.Item.ItemType == ListItemType.Item)
        {


            string ord = ((Label)e.Item.FindControl("lblOrderNo")).Text;  //  orderno
            //bind nested gridview lbladd
            Label lblStatus = ((Label)e.Item.FindControl("lblStatus")); 
            string customerID = ordgrd.DataKeys[e.Item.ItemIndex].ToString();
            GridView grdViewOrdersOfCustomer = ((GridView)e.Item.FindControl("grdViewOrdersOfCustomer"));
            grdViewOrdersOfCustomer.DataSource = objCall.selectOrderedDetails(ord);
            grdViewOrdersOfCustomer.DataBind();

            if (lblStatus.Text == "3")
            {
                lblStatus.Text = "Order Dispatched";
            }
            else if (lblStatus.Text == "2")
            {
                lblStatus.Text = "Order Cancelled";
            }
            else if (lblStatus.Text == "1")
            {
                lblStatus.Text = "Cancellation Request";
            }
            else if (lblStatus.Text == "0")
            {
                lblStatus.Text = "Under Process";
            }
        }

    }
}
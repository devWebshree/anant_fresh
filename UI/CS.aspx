﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CS.aspx.cs" Inherits="CS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .loader
        {
            height: 50px;
            width: 100px;
        }
        .item
        {
            width: 202px;
            border: 1px solid #ccc;
            box-shadow: 2px 2px 8px 2px #ccc;
        }
        .item .header
        {
            height: 30px;
            background-color: #9F9F9F;
            color: #fff;
        }
        .item .body
        {
            width: 200px;
            height: 200px;
        }
        .item .image
        {
            height: 200px;
            width: 200px;
        }
        .item .footer
        {
            height: 50px;
        }
        .button, .button:hover
        {
            height: 45px;
            padding: 10px;
            color: White;
            line-height: 23px;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
            border-radius: 4px;
            text-decoration: none;
            background-color: #9F9F9F;
            border: 1px solid #5C5C5C;
        }
    </style>

<script src="js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var pageIndex = 0;
    var pageCount;

    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            GetRecords();
        }
    });
    $(function () {
        GetRecords();
    });
    function GetRecords() {
        pageIndex++;
        if (pageIndex == 1 || pageIndex <= pageCount) {
            $("#loader").show();
            $.ajax({
                type: "POST",
                url: "/CS.aspx/GetImages",                                   //need to chage -------------------
                data: '{pageIndex: ' + pageIndex + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                  //  alert(response.responseText);
                },
                error: function (response) {
                  //  alert(response.responseText);
                }
            });
        }
    }
    function OnSuccess(response) {
        var xmlDoc = $.parseXML(response.d);
        var xml = $(xmlDoc);
        pageCount = parseInt(xml.find("PageCount").eq(0).find("PageCount").text());
        var images = xml.find("Images");
        var repeatColumns = parseInt("<%=dlImages.RepeatColumns == 0 ? 1 : dlImages.RepeatColumns %>");
        var rowCount = Math.ceil(images.length / repeatColumns);
        var j = 0;
        images.each(function () {
            debugger;
            var ds = $(this);
            var row = $("[id*=dlImages] .item:last").closest("tr");              //dlimage is the id of datalist need to chage-------------------
            if ($(".is_used[value='1']", row).length == repeatColumns) {         //.is_used   for set flag      need to chage-------------------

            //for initial set null value item
                row = $("[id*=dlImages] tr").eq(0).clone();
              
                $(".is_used", row).val("0");
                $(".image", row).attr("src", "");
                $(".button", row).attr("href", "");
                $(".loader", row).remove();
                $("[id*=dlImages]").append(row);
                j = 0;
            //for initial set null value item

            } else {

                //for check it last end
                row = $("[id*=dlImages] .item:last").closest("tr");
                //for check it last end
            }
            var cell = $(".item", row).eq(j);
            $(".name", cell).html(ds.find("name").text());
            $(".code", cell).html(ds.find("code").text());
            $(".price", cell).html(ds.find("price").text());
            $(".offerPrice", cell).html(ds.find("offerPrice").text());

            $(".button", cell).attr("href","http://www.anantfresh.com/ProductImage/"+ ds.find("image").text());
            $(".is_used", cell).attr("value", "1");
            var img = $(".image", cell);                                                              //the class of original image -need to chage-------------------
            var loader = $("<img class = 'loader' src = 'loader.gif' />");                            //if u want xhange name of image -need to chage-------------------
            img.after(loader);
            img.hide();
            $(".image", cell).attr("src", "http://www.anantfresh.com/ProductImage/" + ds.find("image").text());
         //   img.attr("src", "http://www.anantfresh.com/ProductImage/" + ds.find("Image").text());
            img.load(function () {
                $(this).parent().find(".loader").remove();
                $(this).fadeIn();
            });
            j++;
        });
        $("[id*=dlImages] .is_used[value='0']").closest(".item").remove();
    }
</script>
</head>
<body style="font-family: Arial; font-size: 10pt">
    <form id="form1" runat="server">

    <asp:DataList ID="dlImages" runat="server" RepeatLayout="Table" RepeatColumns="3"
        CellPadding="2" CellSpacing="20">
        <ItemTemplate>
            <table class="item" cellpadding="0" cellspacing="0" border="0">
                <tr>
                     
                    <td align="center" class="header">
                        <span class="code">
                            <%# Eval("code") %></span>
                    </td>
                </tr>
                 <tr>
                    <td align="center" class="header">
                        <span class="name">
                            <%# Eval("name") %></span>
                    </td>

                </tr>

                <tr>
                    <td align="center" class="body">
                        <img class="image" src='http://www.anantfresh.com/ProductImage/<%# Eval("Image") %>' alt="" />
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        Photo by <a href="http://www.flickr.com/photos/pearlshelf/">Pearl Photo</a>
                    </td>
                </tr>

                <tr>
                    <td class="footer" align="center">
                        <a href='<%# Eval("Image") %>' target="_blank" class="button">View</a>
                        <input type="hidden" class="is_used" value="0" />
                    </td>
                </tr>
                <tr>
                    <td class="footer">
                         <span class="price">
                            <%# Eval("price") %></span>
                    </td>
                </tr>

                <tr>
                    <td class="footer">
                         <span class="offerPrice">
                            <%# Eval("offerPrice") %></span>
                    </td>
                </tr>

            </table>
        </ItemTemplate>
    </asp:DataList>

    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ClassLibrary;
using BAL;
using DAL;
using System.Web.Security;
public partial class DispatchExecutive_dispatch : System.Web.UI.MasterPage
{
    modifyData ad = new modifyData();
    dlSubCategory objscat = new dlSubCategory();
    SubCategoryCS scat = new SubCategoryCS();
    dlsearch objSrch = new dlsearch();
    Search srch = new Search();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                lbl_name.Text = HttpContext.Current.User.Identity.Name;

            }
            else
            {
                //Response.Redirect("~/login");
            }
        }

        if (Session["Exist"] != null)
        {
            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, Session["Exist"].ToString(), 5000);
            Session["Exist"] = null;

        }
    }

  
    protected void lb_logout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Buffer = true;
        Response.Expires = -1500;
        Response.AddHeader("pragma", "noCache");
        Response.CacheControl = "no-cache";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Session.Clear();
        Session["URL"] = "0";
        Session["IsUser"] = "0";
        Session["min"] = "0";
        Session["max"] = "0";
        FormsAuthentication.SignOut();
        Response.Redirect("/login");

    }
}

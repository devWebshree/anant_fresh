﻿using BAL;
using ClassLibrary;
using DAL;
using System;
using System.Data;

public partial class control_WebUserControl : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    SendMail sm = new SendMail();
    Stock stk = new Stock();
    dlStock objstk = new dlStock();

    CallExecutive call = new CallExecutive();
    dlCallExecutive objCall = new dlCallExecutive();
    ManageData md = new ManageData();
    static string OrderNo;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            if (Request.QueryString["OrderNo"] != null)
            {
                OrderNo = Request.QueryString["OrderNo"].ToString();
                orderDetails();
              //  total();
                bindCostumerdata();
            }
        }
    }




    public void bindCostumerdata() 
    {
        DataSet ds = objCall.selectFromPurchageTable(OrderNo);
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                lblOrderDateTime.Text = dr["OrderDt"].ToString(); 
                lblOrderNo.Text = dr["orderID"].ToString();
                lblOrderNo2.Text = dr["orderID"].ToString();
                lblName2.Text = ds.Tables[1].Rows[0]["name"].ToString();
                lblPhone3.Text = ds.Tables[1].Rows[0]["phone"].ToString();
                lblAddress2.Text = ds.Tables[1].Rows[0]["address1"].ToString() + "," + ds.Tables[1].Rows[0]["city"].ToString() + "," + ds.Tables[1].Rows[0]["state"].ToString() + "," + ds.Tables[1].Rows[0]["zipcode"].ToString();
                lblAmount.Text = dr["MainAmount"].ToString();
                lblBillTo.Text = dr["BAddress"].ToString();
                lblShippingTo.Text = dr["SAddress"].ToString();
                lblTotal.Text = dr["amount"].ToString();
                lbl_shipcharge.Text = (Convert.ToDouble(dr["Amount"]) - Convert.ToDouble(dr["MainAmount"])).ToString();
                lblPaymentMethod.Text = dr["paymentmode"].ToString();
                lblTransaction.Text = dr["TransactionId"].ToString();

                if (dr["paymentmode"].ToString() != "Online")
                {
                    Transaction.Visible = false;
                }
                lblQuantity.Text = objstk.getTotalQuantityByOrderId(OrderNo);
            }
        }
    }
  
   public void orderDetails()
   {
       DataSet ds=objCall.selectOrderedDetails(OrderNo);
       if (ds.Tables[0].Rows.Count > 0)
       {
           OrdersOfCustomer.DataSource = ds;
           OrdersOfCustomer.DataBind();
       }
       else
       {
           OrdersOfCustomer.DataSource = null;
           OrdersOfCustomer.DataBind();
       }
   }
  
}
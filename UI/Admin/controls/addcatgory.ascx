﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="addcatgory.ascx.cs" Inherits="admin_controls_addcatgory" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc4" %>
<%@ Register TagPrefix="cc" Namespace="Winthusiasm.HtmlEditor" Assembly="Winthusiasm.HtmlEditor" %>


<style type="text/css">
    .close {
        display: none;
    }
</style>

<div class="row" id="tag">
    <div class="ProductTitle" style="width: 200px">

        <img id="plumin" src="../../images/plus.jpg" class="open" alt="Open" />
        <img src="../../images/minus.jpg" class="close" alt="Close" />

        <span style="float: right; padding: 5px;">
            <h2>ADD CATEGORY</h2>
        </span>
    </div>
</div>

<div class="clear"></div>
<div class="row" id="pnl">
    <div class="column">
        <ul class="productul">
            <li>
                <span class="lbl">Category Name</span>
                <span class="inputbox">
                    <asp:TextBox ID="txtcat" runat="server" CssClass="input" ValidationGroup="cat"
                        MaxLength="70"></asp:TextBox>
                    <cc1:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcat"
                        CssClass="error" Display="Dynamic" ErrorMessage="Enter Category Name!"
                        SetFocusOnError="True" ValidationGroup="addcat"></cc1:RequiredFieldValidator>
                    <cc1:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtcat"
                        CssClass="error" Display="Dynamic" ErrorMessage="Invalid category Name!" SetFocusOnError="True"
                        ValidationExpression="^[^<>%]{0,70}$" ValidationGroup="addcat"></cc1:RegularExpressionValidator></span>
            </li>

            <li style="display: none">
                <span class="lbl">Description</span>
                <span class="inputbox">
                    <%--  <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine"></asp:TextBox>--%>
                    <cc:HtmlEditor ID="txtDesc" runat="server" Height="300px" Width="600" Toolbars="Select#Format,Bold,Italic,Underline:Left,Center,Right,Justify" />
                </span>
            </li>

            <li style="display: none">
                <span class="lbl">Category Image</span>
                <span class="inputbox">
                    <asp:FileUpload ID="flbnr" runat="server" CssClass="" /></span>
            </li>
            <li>
                <asp:Button ID="btnAddCategory" runat="server" OnClick="btnAddCategory_Click"
                    Text="Add Category" ValidationGroup="addcat" CssClass="submit" />
            </li>
        </ul>

    </div>
</div>

<div class="clear"></div>

<div class="row m10">
    <div class="column">
        <div class="scroll">
            <asp:GridView ID="gvrProduct" runat="server" AllowPaging="True"
                AutoGenerateColumns="False" OnPageIndexChanging="gvrProduct_PageIndexChanging"
                OnRowCancelingEdit="gvrProduct_RowCancelingEdit"
                OnRowDeleting="gvrProduct_RowDeleting" OnRowEditing="gvrProduct_RowEditing"
                OnRowUpdating="gvrProduct_RowUpdating" CssClass="table"
                OnRowDataBound="gvrProduct_RowDataBound" DataKeyNames="catid">

                <Columns>



                    <asp:TemplateField HeaderText="Serial No.">
                        <ItemTemplate>

                            <strong><%# Container.DataItemIndex + 1 %></strong>
                            <asp:Label ID="lblCatid" Visible="false" runat="server" Text='<%# Bind("catid")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Category">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtcat1" runat="server" CssClass="input" MaxLength="70"
                                Text='<%# Eval("category") %>' ValidationGroup="cat"></asp:TextBox>
                            <asp:Label ID="lblEditid" runat="server" Text='<%# Bind("catid")%>'
                                Visible="false"></asp:Label>
                            <cc1:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="txtcat1" CssClass="error" Display="Dynamic"
                                ErrorMessage="Enter Category Name!" SetFocusOnError="True"
                                ValidationGroup="addcat">
                            </cc1:RequiredFieldValidator>
                            <cc1:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                ControlToValidate="txtcat1" CssClass="error" Display="Dynamic"
                                ErrorMessage="Invalid Category Name!" SetFocusOnError="True"
                                ValidationExpression="^[^&lt;&gt;%]{0,70}$" ValidationGroup="addcat">
                            </cc1:RegularExpressionValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCategory0" runat="server"
                                Text='<%# Eval("category") %>'></asp:Label>
                            <asp:Label ID="lblid" runat="server" Text='<%# Bind("catid") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Image" Visible="false">
                        <EditItemTemplate>
                            <asp:Image ID="imgEditImage" runat="server"
                                ImageUrl='<%# "~/imgform.aspx?id=" + Eval("image") %>' CssClass="l admin_img" /><br />
                            <asp:FileUpload ID="fileuploadUpdate" runat="server" CssClass="edit_inp m10" />
                            <asp:HiddenField ID="HiddenField1Image" runat="server"
                                Value='<%# Eval("image") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/imgform.aspx?id=" + Eval("image") %>' />
                            <br />
                            <div class="zoomIcn"><a id="df" runat="server" href='<%# "~/CategoryImages/" + Eval("image") %>' class="highslide" onclick="return hs.expand(this)" visible='<%# setVisiable(Eval("image")) %>'>
                                <asp:Image ID="bimg" runat="server" ImageUrl="~/images/newzoom.jpg" ToolTip="Zoom" /></a></div>
                            <div class="highslide-caption" id="Div5">

                                <asp:HiddenField ID="HiddenField2Image" runat="server"
                                    Value='<%# Eval("image") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Description" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDes" runat="server" Text='<%# Eval("Description").ToString().Replace(Environment.NewLine, "<BR>")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <%--      <asp:TextBox ID="txtDes" runat="server" TextMode="MultiLine" Text='<%# Bind("Description") %>'></asp:TextBox>--%>
                            <cc:HtmlEditor ID="txtDes" runat="server" Height="400px" Toolbars="Select#Format,Bold,Italic,Underline:Left,Center,Right,Justify"
                                Text='<%# Bind("Description") %>' Width="600px" />


                        </EditItemTemplate>

                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Sortid">
                        <ItemTemplate>
                            <asp:Label ID="lblSortid" runat="server" Text='<%# Bind("sortid") %>'></asp:Label>
                        </ItemTemplate>

                        <EditItemTemplate>
                            <asp:Label ID="lblPrvsrt" runat="server" Visible="false" Text='<%# Bind("sortid") %>'></asp:Label>
                            <asp:TextBox ID="txtSrt" runat="server" Text='<%# Bind("sortid") %>' Width="50px"></asp:TextBox>

                        </EditItemTemplate>
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True"
                                CommandName="Update" Text="Update" ValidationGroup="e1" CssClass="button"></asp:LinkButton>
                            <div class="clear"></div>
                            <br />
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False"
                                CommandName="Cancel" Text="Cancel" CssClass="button m10" Style="top: 40px;"></asp:LinkButton>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False"
                                CommandName="Edit" Text="Edit" CssClass="button"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False"
                                CommandName="Delete" OnClientClick="return confirm('Are you sure?')"
                                Text="Delete" CssClass="button"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Visible">
                        <ItemTemplate>
                            <asp:Label ID="lblVisi" Visible="false" runat="server" Text='<%# Bind("IsVisible") %>'></asp:Label>
                            <asp:CheckBox ID="chkHide" runat="server" AutoPostBack="true"
                                OnCheckedChanged="chkHide_CheckedChanged" />
                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>
                <HeaderStyle ForeColor="Black" Height="30px" BorderColor="White" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <RowStyle HorizontalAlign="Center" BackColor="#E4E6E8" BorderColor="White" Height="30px" />
                <PagerStyle BackColor="#BDC3C7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#BDC3C7" Font-Bold="True"
                    Font-Size="13px" ForeColor="Black" HorizontalAlign="Center" />
                <AlternatingRowStyle HorizontalAlign="Center" BackColor="#E4E6E8" ForeColor="Black" />
            </asp:GridView>
        </div>
    </div>
</div>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
</asp:UpdatePanel>

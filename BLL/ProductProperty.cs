﻿using System;
using System.Collections.Generic;

using System.Text;

namespace BAL
{
    public class ProductProperty
    {

        public string p_id, p_subcategoryId, p_categoryname, p_code, p_name, p_image, P_Detail, P_BestSellers, P_DailyDeal, P_isNew, P_IsVisible, P_IsFeatured, P_Size, P_Weight, P_Submitby, p_ShortDescripn;
        public Double P_price, P_offerprice, p_buyingprice, p_wholesaleprice, P_productprice;


      //  string ,string , 



        public Double buyingprice
        {
            get
            {
                return p_buyingprice;
            }
            set
            {
                p_buyingprice = value;
            }
        }

        public Double wholesaleprice 
        {
            get
            {
                return p_wholesaleprice;
            }
            set
            {
                p_wholesaleprice = value;
            }
        }

        public Double price
        {
            get
            {
                return P_price;
            }
            set
            {
                P_price = value;
            }
        }
        public Double offerprice
        {
            get
            {
                return P_offerprice;
            }
            set
            {
                P_offerprice = value;
            }
        }

        public Double productprice
        {
            get
            {
                return P_productprice;
            }
            set
            {
                P_productprice = value;
            }
        }



        public string categoryname
        {
            get
            {
                return p_categoryname;
            }
            set
            {
                p_categoryname = value;
            }
        }
        public string subcategoryname
        {
            get
            {
                return p_subcategoryId;
            }
            set
            {
                p_subcategoryId = value;
            }
        }


        public string code
        {
            get
            {
                return p_code;
            }
            set
            {
                p_code = value;
            }
        }
        public string name
        {
            get
            {
                return p_name;
            }
            set
            {
                p_name = value;
            }
        }
        public string image
        {
            get
            {
                return p_image;
            }
            set
            {
                p_image = value;
            }
        }
        public string pid
        {
            get
            {
                return p_id;
            }
            set
            {
                p_id = value;
            }
        }

        public string Detail
        {
            get
            {
                return P_Detail;
            }
            set
            {
                P_Detail = value;
            }
        }
        public string BestSellers
        {
            get
            {
                return P_BestSellers;
            }
            set
            {
                P_BestSellers = value;
            }
        }
        public string DailyDeal
        {
            get
            {
                return P_DailyDeal;
            }
            set
            {
                P_DailyDeal = value;
            }
        }
        public string isnew
        {
            get
            {
                return P_isNew;
            }
            set
            {
                P_isNew = value;
            }
        }

        public string IsVisible
        {
            get
            {
                return P_IsVisible;
            }
            set
            {
                P_IsVisible = value;
            }
        }

        public string IsFeatured
        {

            get
            {
                return P_IsFeatured;
            }
            set
            {
                P_IsFeatured = value;

            }
        }




        public string Size
        {

            get
            {
                return P_Size;
            }
            set
            {
                P_Size = value;

            }
        }

        public string Weight
        {

            get
            {
                return P_Weight;
            }
            set
            {
                P_Weight = value;

            }
        }

        public string Submitby
        {

            get
            {
                return P_Submitby;
            }
            set
            {
                P_Submitby = value;

            }
        }

        public string ShortDescripn
        {

            get
            {
                return p_ShortDescripn;
            }
            set
            {
                p_ShortDescripn = value;

            }
        }


        

    }
    
}

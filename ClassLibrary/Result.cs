﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary
{
    public class Result
    {
        public string RollNumber { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public int Marks { get; set; }
     
        public bool Active { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
    }
}

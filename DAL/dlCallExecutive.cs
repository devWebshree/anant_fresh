﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using ClassLibrary;

namespace DAL
{
   public class dlCallExecutive
    {
        Database db = GetDatabase(AppDatabaseName);
       private static string AppDatabaseName;
       private static Database GetDatabase(string dbName)
       {
           try
           {
               Database db = null;
               if (dbName == null)
               {
                   return db = DatabaseFactory.CreateDatabase();
               }
               else
               {
                   return db = DatabaseFactory.CreateDatabase(dbName);
               }
               return db;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       static dlCallExecutive()
       {
           AppDatabaseName = ConfigurationSettings.AppSettings["AppDatabaseName"];
       }

       public DataSet getOrderList(CallExecutive obj)  
       {

           try
           {
               DbCommand cmd = db.GetStoredProcCommand("getOrderCallExeList");
               db.AddInParameter(cmd, "@Searchtxt", DbType.String, obj.Searchtxt);
               db.AddInParameter(cmd, "@Status", DbType.String, obj.Status);
               db.AddInParameter(cmd, "@CallExecutive", DbType.String, obj.UserId);
               return db.ExecuteDataSet(cmd);
           }
           catch (Exception ex)
           {
               return null;
           }
       }


       public DataSet listFinalOrder(CallExecutive obj) 
       {

           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spListFinalOrder");
               db.AddInParameter(cmd, "@dt1", DbType.String, obj.dt1);
               db.AddInParameter(cmd, "@Through", DbType.String, obj.Through);
               db.AddInParameter(cmd, "@dt2", DbType.String, obj.dt2);
               db.AddInParameter(cmd, "@srchtxt", DbType.String, obj.Searchtxt);//
               db.AddInParameter(cmd, "@dispatch", DbType.String, obj.Status);
               db.AddInParameter(cmd, "@SalesName", DbType.String, obj.SalesName);
               db.AddInParameter(cmd, "@PurchageType", DbType.String, obj.PurchageType);
               return db.ExecuteDataSet(cmd);
           }
           catch (Exception ex)
           {
               return null;
           }
       }

  

       public DataSet ListDispatchFinalOrder(CallExecutive obj)
       {

           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spListDispatchFinalOrder");
               db.AddInParameter(cmd, "@dt1", DbType.String, obj.dt1);
               db.AddInParameter(cmd, "@Through", DbType.String, obj.Through);
               db.AddInParameter(cmd, "@dt2", DbType.String, obj.dt2);
               db.AddInParameter(cmd, "@srchtxt", DbType.String, obj.Searchtxt);
               db.AddInParameter(cmd, "@dispatch", DbType.String, obj.Status);
               db.AddInParameter(cmd, "@SalesName", DbType.String, obj.SalesName);
               db.AddInParameter(cmd, "@PurchageType", DbType.String, obj.PurchageType);

               return db.ExecuteDataSet(cmd);
           }
           catch (Exception ex)
           {
               return null;
           }
       }


       public void insertAdminRole(string name , string mobile,string UserName,string Password, string Role)  
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spRole");
               db.AddInParameter(cmd, "@Name", DbType.String, name);
               db.AddInParameter(cmd, "@mobileno", DbType.String, mobile);
               db.AddInParameter(cmd, "@UsreName", DbType.String, UserName);
               db.AddInParameter(cmd, "@Password", DbType.String, Password);
               db.AddInParameter(cmd, "@Role", DbType.String, Role);
               db.AddInParameter(cmd, "@Action", DbType.String, "Insert");
               db.ExecuteNonQuery(cmd);
           }
           catch {}
       }

       public void updatePaymentOption(string TransactionId, string PaymentStatus, string OrderId) 
       {
           try
           {
               DbCommand cmd = db.GetSqlStringCommand("update Checkout set TransactionId=@TransactionId , PaymentStatus=@PaymentStatus where OrderId=@OrderId");

               db.AddInParameter(cmd, "@TransactionId", DbType.String, TransactionId);
               db.AddInParameter(cmd, "@PaymentStatus", DbType.String, PaymentStatus);
               db.AddInParameter(cmd, "@OrderId", DbType.String, OrderId);
               db.ExecuteNonQuery(cmd);
           }
           catch { }
       }


       public void deleteAdminRole(string AdminId) 
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spRole");
               db.AddInParameter(cmd, "@aid", DbType.String, AdminId);
               db.AddInParameter(cmd, "@Action", DbType.String, "delete");
               db.ExecuteNonQuery(cmd);
           }
           catch { }
       }

       public DataSet listAdminRole(string Role) 
       {

           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spRole");
               db.AddInParameter(cmd, "@Role", DbType.String, Role);
               db.AddInParameter(cmd, "@Action", DbType.String, "List"); 
               return db.ExecuteDataSet(cmd);
           }
           catch (Exception ex)
           {
               return null;
           }
       }


       public DataSet searchRole(string SearchText)
       { 

           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spRole");
               db.AddInParameter(cmd, "@SearchText", DbType.String, SearchText);
               db.AddInParameter(cmd, "@Action", DbType.String, "getsrch");
               return db.ExecuteDataSet(cmd);
           }
           catch (Exception ex)
           {
               return null;
           }
       }


       public void UpdatePurchaseItemCall(string pid, string qty)
       {
           string str = "update purchasetable set quantity=@qty where (pid=@pid)";
           DbCommand cmd = db.GetSqlStringCommand(str);
           db.AddInParameter(cmd, "@pid", DbType.String, pid);
           db.AddInParameter(cmd, "@qty", DbType.String, qty);
           db.ExecuteNonQuery(cmd);


       }

       public DataSet selectOrderedDetails(string ord) 
       {
           DbCommand cmd = db.GetSqlStringCommand("SELECT pur.quantity,pur.flagstatus,Pur.itemid, pur.pid,pur.price, pur.catid, pur.buyer, pur.item, pur.Orderno,itm.code FROM itemdetailtable AS itm INNER JOIN PurChaseTable AS pur ON itm.id = pur.catid AND pur.Orderno = @ord");
           db.AddInParameter(cmd, "@ord", DbType.String, ord);
           return db.ExecuteDataSet(cmd);
       }

       public void deletePurchaseItemCall(string pid)
       {
           DbCommand cmd = db.GetSqlStringCommand("delete from purchasetable where pid=@pid");
           db.AddInParameter(cmd, "@pid", DbType.String, pid);
           db.ExecuteNonQuery(cmd).ToString();
       }


       public DataSet selectFromPurchageTable(string ord)
       {
           DbCommand cmd = db.GetSqlStringCommand("SELECT * FROM Checkout where orderID = @ord; SELECT * FROM finalordertable fot left join UserTable ut on fot.username=ut.UserName  where fot.OrderNo = @ord; ");
           db.AddInParameter(cmd, "@ord", DbType.String, ord);
           return db.ExecuteDataSet(cmd);

       }



    }
}

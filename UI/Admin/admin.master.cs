﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using WebshreeCommonLibrary;
using DAL;

public partial class admin_admin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                lblCounter.Text ="Total website visitor :"+ getViewCounter().ToString();
            }
            else
            {
                Response.Redirect("/login");
            }
        }
    }

    public int getViewCounter()
    {
        int count = 0;
        try
        {
            dalResult dlViews = new dalResult();
            TransportationPacket Packet = new TransportationPacket();
            Packet = dlViews.getCounter(Packet);
            if (Packet.MessageStatus.Equals(EStatus.Success))
            {
                count =Convert.ToInt32(Packet.MessagePacket);
            }

        }
        catch (Exception ex)
        {
            count = 0;
        }
        return count + 1000;

    }

    protected void lb_logout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Buffer = true;
        Response.Expires = -1500;
        Response.AddHeader("pragma", "noCache");
        Response.CacheControl = "no-cache";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Session.Clear();
        Session["URL"] = "0";
        Session["IsUser"] = "0";
        Session["min"] = "0";
        Session["max"] = "0";
        FormsAuthentication.SignOut();
        Response.Redirect("/login");
        
    }
}

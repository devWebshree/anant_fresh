Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.IO

Imports System.Drawing.Imaging


Partial Class imgform
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strImageID, img, vwimg, itmid As String
        Dim obj = ""

        itmid = Request.QueryString("itmid")
        strImageID = Request.QueryString("img")
        img = Request.QueryString("imgid")
        vwimg = Request.QueryString("id")

        If vwimg <> "" Then
            bindImg(vwimg, itmid)
        Else


            If strImageID <> "" Then
                bindImg(strImageID, itmid)
            Else
                If img <> "" Then
                    bindImg(img, itmid)
                Else
                    bindImg(0, itmid)
                End If
            End If
        End If

    End Sub

    Sub bindImg(ByVal obj As Object, ByVal itmid As String)
        Dim stream As New MemoryStream
        Dim img As String
        If obj Is Nothing Or IsDBNull(obj) Or obj = "0" Then
            Dim a As System.Drawing.Image
            Dim rnd As New Random
            Dim cnt As Integer = rnd.Next(1, 10)
            a = Drawing.Image.FromFile(Server.MapPath("images/noimage.jpg")) '~/readrealimage.aspx?imageid={0}
            Dim thumb As System.Drawing.Image
            Dim hig As Integer = a.Height
            Dim wid As Integer = a.Width
            GetImageHeightWidth(wid, hig)
            thumb = a.GetThumbnailImage(wid, hig, Nothing, Nothing)
            Dim bitmap As New Drawing.Bitmap(thumb)
            Response.ContentType = "image/gif"
            bitmap.Save(Response.OutputStream, ImageFormat.Jpeg)
            stream.Close()
            obj = ""
        Else

            If itmid = 1 Then
                img = "~/ProductImage/" & obj
            Else
                img = "~/CategoryImages/" & obj
            End If


            Dim a As System.Drawing.Image
            Dim rnd As New Random
            'Dim cnt As Integer = rnd.Next(1, 10)
            a = Drawing.Image.FromFile(Server.MapPath(img)) '~/readrealimage.aspx?imageid={0}
            Dim thumb As System.Drawing.Image
            Dim hig As Integer = a.Height
            Dim wid As Integer = a.Width

            Dim abc As String = Request.QueryString("imgid")
            If Request.QueryString("pd") <> "" Then
                If (Request.QueryString("pd") = "2") Then
                    GetImgHtWhforProdetail200(wid, hig)   'for 200*200
                Else
                    GetImgHtWhforProdetail(wid, hig)
                End If
            Else
                If abc <> "" Then
                    GetImageBigHeightWidth(wid, hig)
                Else
                    If Request.QueryString("id") <> "" Then
                        GetImgHtWh(wid, hig)
                    Else
                        GetImageHeightWidth(wid, hig)
                    End If
                End If
            End If


                thumb = a.GetThumbnailImage(wid, hig, Nothing, Nothing)

                Dim bitmap As New Drawing.Bitmap(thumb)
                Response.ContentType = "image/jpeg"
                bitmap.Save(Response.OutputStream, ImageFormat.Jpeg)
                a.Dispose()
                thumb.Dispose()
                stream.Flush()
                bitmap.Dispose()
                bitmap.Dispose()
                stream.Dispose()
                stream.Close()
        End If
    End Sub
    Private Sub GetImageHeightWidth(ByRef wid As Integer, ByRef hig As Integer)
        Dim int As Decimal
        If wid > 65 Or hig > 85 Then
            If wid > hig Or wid = hig Then
                While wid > 65
                    int = 65 / wid * 100
                    wid = wid * int / 100
                    hig = hig * int / 100
                End While

            ElseIf hig > wid Or wid = hig Then
                While hig > 85
                    int = 85 / hig * 100
                    wid = wid * int / 100
                    hig = hig * int / 100
                End While
            End If

        End If
    End Sub
    Private Sub GetImageBigHeightWidth(ByRef wid As Integer, ByRef hig As Integer)
        'Dim int As Decimal
        'If wid > 275 Or hig > 275 Then
        '    If wid > hig Or wid = hig Then
        '        While wid > 275
        '            int = 275 / wid * 100
        '            wid = wid * int / 100
        '            hig = hig * int / 100
        '        End While

        '    ElseIf hig > wid Or wid = hig Then
        '        While hig > 275
        '            int = 275 / hig * 100
        '            wid = wid * int / 100
        '            hig = hig * int / 100
        '        End While
        '    End If

        'End If   
        Dim int As Decimal
        If wid > 175 Or hig > 285 Then
            '  If wid > hig Or wid = hig Then
            While wid > 175
                int = 175 / wid * 100
                wid = wid * int / 100
                '  hig = hig * int / 100
            End While

            'ElseIf hig > wid Or wid = hig Then
            While hig > 285
                int = 285 / hig * 100
                ' wid = wid * int / 100
                hig = hig * int / 100
            End While


        End If
    End Sub
    Private Sub GetImgHtWh(ByRef wid As Integer, ByRef hig As Integer)
        Dim int As Decimal
        If wid > 94 Or hig > 155 Then

            While wid > 94
                int = 94 / wid * 100
                wid = wid * int / 100
                hig = hig * int / 100
            End While
            While hig > 155
                int = 155 / hig * 100
                wid = wid * int / 100
                hig = hig * int / 100
            End While


        End If
    End Sub
    Private Sub GetImgHtWhforProdetail(ByRef wid As Integer, ByRef hig As Integer)
        Dim int As Decimal
        If wid > 500 Or hig > 382 Then

            While wid > 500
                int = 500 / wid * 500
                wid = wid * int / 500
                hig = hig * int / 500
            End While
            While hig > 382
                int = 382 / hig * 382
                wid = wid * int / 382
                hig = hig * int / 382
            End While


        End If
    End Sub
    Private Sub GetImgHtWhforProdetail200(ByRef wid As Integer, ByRef hig As Integer)
        Dim int As Decimal
        If wid > 200 Or hig > 200 Then

            While wid > 200
                int = 200 / wid * 200
                wid = wid * int / 200
                hig = hig * int / 200
            End While
            While hig > 200
                int = 200 / hig * 200
                wid = wid * int / 200
                hig = hig * int / 200
            End While


        End If
    End Sub
End Class

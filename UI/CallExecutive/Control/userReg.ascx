﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="userReg.ascx.cs" Inherits="CallExecutive_Control_userReg" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc2" %>
<div class="row m10">
    <div class="registerdiv clearfix" style="width: 93%;">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>


        <p class="mclear">
           Please enter your details:  <span style="color: rgb(255, 0, 0); font-size: 11px;">* All Fields are mandatory</span></p>
            <div class="center">
        <ul class="lgnul m20">


         <li>
           <label class="lbl" for="input">* Purchase Type</label>
                <div class="inputbox">


              
              Retail <asp:RadioButton ID="rbRetailer" Checked="true" ValidationGroup="admingvreg" 
                        GroupName="m" runat="server" AutoPostBack="true" 
                        oncheckedchanged="rbRetailer_CheckedChanged" />
              Wholesale <asp:RadioButton ID="rbWholesale" runat="server" 
                        ValidationGroup="admingvreg" GroupName="m"   AutoPostBack="true" 
                        oncheckedchanged="rbWholesale_CheckedChanged" />
                </div>
          </li>

          <asp:Panel ID="pnlSales" runat="server" Visible="false">
        <li>
           <label class="lbl" for="input"> Sales Person</label>
                <div class="inputbox">
             <asp:DropDownList ID="ddlsalePerson"  CssClass="input" runat="server" ></asp:DropDownList>
                </div>
          </li>
          </asp:Panel>











        <li>
    
               
                    <label class="lbl" for="input">* Mobile No</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtph" runat="server" CssClass="input" AutoPostBack="true"
                        ontextchanged="txtph_TextChanged"></asp:TextBox>&nbsp;
                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtph"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Phone No." ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>
                    <cc2:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtph"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Invalid Phone No." ValidationExpression="^[0-9 -]{10,14}$"
                        ValidationGroup="admingvreg"></cc2:RegularExpressionValidator>

                </div>
                   <asp:Label ID="lblMessage"  style="float:left; width:100%; font:12px;" runat="server"  Visible="False"></asp:Label>
            <asp:LinkButton ID="lbOrderHistory" Visible="false" OnClick="lbOrderHistory_Click" runat="server">View Order Details</asp:LinkButton>
            </li>








        



                        <li>
            <span class="lbl">Email </span>
                <div class="inputbox">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtusername" AutoPostBack="true" runat="server" CssClass="input" 
                                ontextchanged="txtusername_TextChanged"></asp:TextBox>
                            <cc2:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtusername"
                                CssClass="error1" Display="Dynamic" ErrorMessage="Please Enter a valid Email Id," ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="admingvreg">                    
                            </cc2:RegularExpressionValidator>
                            
                            <asp:Label ID="lblEmailmsg" style="float:left; width:100%; font:12px;" runat="server"  Visible="False"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </li>

         
           
            <li>
                <label for="input" class="lbl">* Full Name</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtname" runat="server" CssClass="input"></asp:TextBox>&nbsp;
                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtname"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter your Name."
                        ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>
                    <cc2:RegularExpressionValidator ID="Reglidator2" runat="server" ControlToValidate="txtname"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Name should contains only Alphabets."
                        ValidationExpression="^[a-zA-Z'.\s]{1,50}$" ValidationGroup="admingvreg"></cc2:RegularExpressionValidator>
                </div>
            </li>
           






            <li>
                <label class="lbl" for="input">Alternate Mobile Number</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtaltph" runat="server" CssClass="input"></asp:TextBox>&nbsp;
                    <cc2:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtaltph"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Invalid Phone No." ValidationExpression="^[0-9 -]{10,14}$"
                        ValidationGroup="admingvreg"></cc2:RegularExpressionValidator>
                </div>
            </li>
            <li>
                <label class="lbl" for="textarea">* Address</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtaddr" runat="server" CssClass="textarea" Height="100px" TextMode="MultiLine"></asp:TextBox>&nbsp;
                    <cc2:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtaddr"
                        Display="Dynamic" ErrorMessage="Please delete any of these characters(,<>#%;'`)."
                        ValidationExpression="^[^&lt;&gt;(){}?&amp;*~`!#$%^=+|\\:'\;]{0,550}$" ValidationGroup="admingvreg"></cc2:RegularExpressionValidator>
                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtaddr"
                        Display="Dynamic" ErrorMessage="Enter Address!" ValidationGroup="admingvreg"
                        CssClass="error1"></cc2:RequiredFieldValidator>
                </div>
            </li>
            <li>
                <label for="input" class="lbl">* City</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtcity" runat="server" CssClass="input"></asp:TextBox>&nbsp;
                    <cc2:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" ControlToValidate="txtcity"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter your City." ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>
                </div>
            </li>
            <li>
                <label class="lbl" for="input">* State</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtstate" runat="server" CssClass="input" MaxLength="200"></asp:TextBox>
                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtstate"
                        Display="Dynamic" ErrorMessage="Enter State!" SetFocusOnError="True" ValidationGroup="admingvreg"
                        CssClass="error1"></cc2:RequiredFieldValidator>
                    <cc2:RegularExpressionValidator ID="RegularExpionValidator7" runat="server" ControlToValidate="txtstate"
                        Display="Dynamic" ErrorMessage="Invalid State!" ValidationExpression="^[^<>%]{0,200}$"
                        ValidationGroup="admingvreg"></cc2:RegularExpressionValidator>
                </div>
            </li>
            <li>
                <label class="lbl" for="input">* Country</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtcoun" runat="server" CssClass="input" MaxLength="200"></asp:TextBox>
                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcoun"
                        Display="Dynamic" ErrorMessage="Enter Country!" SetFocusOnError="True" ValidationGroup="admingvreg"
                        CssClass="error1"></cc2:RequiredFieldValidator>
                    <cc2:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" ControlToValidate="txtcoun"
                        Display="Dynamic" ErrorMessage="Invalid Country!" ValidationExpression="^[^<>%]{0,50}$"
                        ValidationGroup="admingvreg"></cc2:RegularExpressionValidator>
                </div>
            </li>
            <li>
                <label class="lbl" for="input">* Pin Code</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtzip" MaxLength="6" runat="server" CssClass="input"></asp:TextBox>&nbsp;
                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtzip"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Zipcode." ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>
                           <cc2:RegularExpressionValidator ID="rfv_postal" runat="server"
                                        ControlToValidate="txtzip" CssClass="error1" Display="Dynamic" ErrorMessage="Invalid Postal Code!"
                                        ValidationExpression="^[0-9 -]{0,50}$" ValidationGroup="admingvreg"></cc2:RegularExpressionValidator>
                </div>
            </li>
            <li>
                <asp:Button ID="btnRegister" runat="server" CssClass="_button1 backgroundbg border-radius right m20" OnClick="btnRegister_Click"
                    Text="Go for Shopping" ValidationGroup="admingvreg" />
            </li>
        </ul>
</div>

  </ContentTemplate>
                    </asp:UpdatePanel></div></div>
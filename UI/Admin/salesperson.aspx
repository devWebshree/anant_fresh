﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="salesperson.aspx.cs" Inherits="Admin_salesperson" %>
<%@ Register TagName="sale" Src="~/Admin/controls/salepersonlist.ascx" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#addsalesperson").addClass('active');

        $('#pnl').hide();
        $('.ProductTitle').on('click', function () {

            $(this).children('img').each(function () {
                $(this).toggle(0);
            });

            $('#pnl').slideToggle();
        });

    });
</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:sale ID="sale1" runat="server" />
</asp:Content>


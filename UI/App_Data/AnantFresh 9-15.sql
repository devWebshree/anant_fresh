USE [AnantFresh]
GO
/****** Object:  Table [dbo].[finalordertable]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[finalordertable](
	[pid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OrderNo] [nvarchar](50) NULL,
	[username] [nvarchar](50) NULL,
	[DOP] [datetime] NULL,
	[address] [text] NULL,
	[amount] [numeric](18, 2) NULL,
	[Dispatch] [int] NULL,
	[remark] [nvarchar](50) NULL,
	[dispatchno] [nvarchar](50) NULL,
	[Discount] [nvarchar](50) NULL,
	[UsedLoyalty] [nvarchar](50) NULL,
	[MainAmount] [numeric](18, 2) NULL,
	[ClMsg] [nvarchar](2000) NULL,
	[CallExecutive] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[FeedBackID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[EmailID] [nvarchar](50) NULL,
	[PhoneNo] [nvarchar](50) NULL,
	[ProductName] [nvarchar](50) NULL,
	[ProductSubName] [nvarchar](50) NULL,
	[ProductQuality] [nvarchar](50) NULL,
	[Comment] [nvarchar](max) NULL,
	[FeedDate] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[Del] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FeedBackID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Enquiry]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enquiry](
	[EnquiryID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[EmailID] [nvarchar](50) NULL,
	[PhoneNo] [nvarchar](50) NULL,
	[ProductName] [nvarchar](50) NULL,
	[Comment] [nvarchar](max) NULL,
	[EnqDate] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[Del] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EnquiryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[crmlogin]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[crmlogin](
	[crmid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[crm_purchase]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[crm_purchase](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[orderno] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[crm_cancel]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[crm_cancel](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[req_id] [varchar](50) NULL,
	[orderno] [varchar](50) NULL,
	[user_id] [varchar](50) NULL,
	[dateofcancelreq] [datetime] NULL,
	[reason_cancel] [varchar](400) NULL,
	[status] [int] NULL,
	[crm_incharge_id] [varchar](50) NULL,
	[date_of_cancel] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Checkout]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Checkout](
	[Cid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[orderID] [nvarchar](50) NULL,
	[uname] [nvarchar](50) NULL,
	[BAddress] [text] NULL,
	[SAddress] [text] NULL,
	[Discount] [nvarchar](50) NULL,
	[Promo] [nvarchar](50) NULL,
	[paymentmode] [nvarchar](50) NULL,
	[UsedLoyalty] [nvarchar](50) NULL,
	[EarnLoyalty] [int] NULL,
	[amount] [numeric](18, 2) NULL,
	[MainAmount] [numeric](18, 2) NULL,
	[OrderDt] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryTable]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryTable](
	[catid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](100) NULL,
	[sortid] [numeric](18, 0) NOT NULL,
	[image] [nvarchar](50) NULL,
	[IsVisible] [bit] NULL,
	[CatInsDt] [datetime] NULL,
	[Description] [nvarchar](2000) NULL,
	[Submitby] [nvarchar](50) NULL,
 CONSTRAINT [PK_CategoryTable] PRIMARY KEY CLUSTERED 
(
	[catid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdminTable]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminTable](
	[aid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[uname] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[role] [nvarchar](50) NULL,
 CONSTRAINT [PK_AdminTable] PRIMARY KEY CLUSTERED 
(
	[aid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PurChaseTable]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurChaseTable](
	[pid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[catid] [nvarchar](50) NULL,
	[quantity] [numeric](18, 0) NULL,
	[buyer] [nvarchar](200) NULL,
	[itemid] [nvarchar](200) NULL,
	[Orderno] [nvarchar](100) NULL,
	[flagstatus] [nvarchar](200) NULL,
	[item] [nvarchar](50) NULL,
	[price] [numeric](18, 2) NULL,
	[Submitdt] [datetime] NULL,
 CONSTRAINT [PK_PurChaseTable] PRIMARY KEY CLUSTERED 
(
	[pid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[promotable]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[promotable](
	[Pid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Promocode] [nvarchar](50) NULL,
	[PurchaseAbove] [numeric](18, 2) NULL,
	[Discount] [numeric](18, 2) NULL,
	[ExpireDate] [datetime] NULL,
	[Msg] [nvarchar](500) NULL,
	[senddate] [datetime] NULL,
	[Ispracent] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStausText]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStausText](
	[oid] [int] IDENTITY(1,1) NOT NULL,
	[StutasName] [nvarchar](500) NULL,
	[StutasIndex] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoyaltyPercentage]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoyaltyPercentage](
	[lid] [int] IDENTITY(1,1) NOT NULL,
	[percentage] [decimal](18, 1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoyaltyHistory]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoyaltyHistory](
	[lid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Loyalty] [int] NULL,
	[ExpiryDate] [datetime] NULL,
	[IssueDate] [datetime] NULL,
	[Userid] [nvarchar](50) NULL,
	[status] [nvarchar](50) NULL,
	[Orderid] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoyaltyHistory] PRIMARY KEY CLUSTERED 
(
	[lid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[itemdetailtable]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemdetailtable](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[categoryname] [varchar](50) NULL,
	[SubCategory] [nvarchar](50) NULL,
	[code] [varchar](50) NULL,
	[name] [varchar](50) NULL,
	[image] [varchar](50) NULL,
	[price] [numeric](18, 2) NULL,
	[offerPrice] [numeric](18, 2) NULL,
	[Detail] [text] NULL,
	[productprice] [numeric](18, 2) NULL,
	[BestSellers] [bit] NULL,
	[DailyDeal] [bit] NULL,
	[IsNew] [bit] NULL,
	[IsVisible] [bit] NULL,
	[IsFeatured] [bit] NULL,
	[Size] [nvarchar](50) NULL,
	[Weight] [nvarchar](50) NULL,
	[ShortDescripn] [nvarchar](200) NULL,
	[Submitby] [nvarchar](50) NULL,
	[ProInsDt] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Wishlist]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wishlist](
	[wid] [int] IDENTITY(1,1) NOT NULL,
	[Itemid] [int] NULL,
	[Userid] [nvarchar](500) NULL,
	[DOA] [datetime] NULL,
	[status] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserTable]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTable](
	[uid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](200) NULL,
	[Password] [nvarchar](50) NULL,
	[Name] [nvarchar](200) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](2000) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Zipcode] [nvarchar](50) NULL,
	[Altphone] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[registerdate] [datetime] NULL,
 CONSTRAINT [PK_UserTable] PRIMARY KEY CLUSTERED 
(
	[uid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLoyaltyPoint]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLoyaltyPoint](
	[lid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Userid] [nvarchar](50) NULL,
	[Point] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblGroupProducts]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGroupProducts](
	[gid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[productID] [nvarchar](50) NULL,
	[groupProductID] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCategory]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategory](
	[SubCatID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CategoryID] [nvarchar](100) NULL,
	[SubCategory] [nvarchar](100) NULL,
	[SortId] [numeric](18, 0) NOT NULL,
	[Image] [nvarchar](50) NULL,
	[IsVisible] [bit] NULL,
	[SubCatInsDt] [datetime] NULL,
	[Description] [nvarchar](2000) NULL,
	[Submitby] [nvarchar](50) NULL,
 CONSTRAINT [PK_SubCategoryTable] PRIMARY KEY CLUSTERED 
(
	[SubCatID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StockTable]    Script Date: 09/15/2014 15:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StockTable](
	[sid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[itemid] [nvarchar](50) NULL,
	[qty] [numeric](18, 0) NULL,
	[AlertOnQuantity] [int] NULL,
	[date] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[split]    Script Date: 09/15/2014 15:19:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[split](
    @delimited NVARCHAR(MAX),
    @delimiter NVARCHAR(100)
) RETURNS @t TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
    DECLARE @xml XML
    SET @xml = N'<t>' + REPLACE(@delimited,@delimiter,'</t><t>') + '</t>'
    INSERT INTO @t(val)
    SELECT  r.value('.','varchar(MAX)') as item
    FROM  @xml.nodes('/t') as records(r)
    RETURN
END
GO
/****** Object:  StoredProcedure [dbo].[spListFinalOrder]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spListFinalOrder]
@dt1 nvarchar (50)=null,
@dt2 nvarchar (50)=null,
@srchtxt nvarchar (50)=null,
@dispatch nvarchar (50)=null
as

begin
if(@dispatch='')
set @dispatch=null
end

begin
SELECT ut.Name, ft.*, (ft.amount-ft.MainAmount) as dif FROM finalordertable ft left join UserTable ut on ft.username= ut.UserName 
where  
dop between isnull(@dt1,dop) and isnull(@dt2,dop)    and
 dispatch=isnull(@dispatch,dispatch)
and (ft.username LIKE '%' + @srchtxt + '%' OR  OrderNo LIKE '%' + @srchtxt + '%' or ut.Name LIKE '%' + @srchtxt + '%' ) order by dop desc
end
GO
/****** Object:  StoredProcedure [dbo].[spInsKeyPurChaseTable]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInsKeyPurChaseTable]/*	Created by: 	<<Company Name>>, <<Developers Name>>	Date: 		zaterdag, 24 dec 2011	Purpose:	<<invullen>>	Input:		<<invullen>>	Output:		<<invullen>>*/	@catid                                    NVARCHAR(50)         = NULL,   	@quantity                                 NUMERIC(18,0)        = NULL,   	@buyer                                    NVARCHAR(200)        = NULL,   	@itemid                                   NVARCHAR(200)        = NULL,   	@Orderno                                  NVARCHAR(100)        = NULL,@item                                  NVARCHAR(50)        = NULL,@price                                 int        = NULL        		AS 	DECLARE @ProcName 	VARCHAR(50)	DECLARE @Message  	VARCHAR(255)	DECLARE @Testlevel	INT /*_____________________________________________________________________________  Set @Testlevel, @ProcName, en Startingmessage  _____________________________________________________________________________*/ 	/*FILL OUT THIS SELECTSTATEMENT CORRECTLY !	  SELECT @Testlevel = <<Fieldname>> FROM <<SettingsTable>> 	  WHERE <<Keyvalue>> = 'SPTest'	  IF @@ERROR <> 0	  	GOTO ErrorHandling*/ 	SELECT @ProcName = OBJECT_NAME(@@PROCID) 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Start '				+ CONVERT(VARCHAR, GETDATE(), 113)		PRINT @Message	END /*_____________________________________________________________________________  Procedure starts here  _____________________________________________________________________________*/ 		INSERT INTO [PurChaseTable](			[catid],			[quantity],			[buyer],			[itemid],			[Orderno],[item],[price]			)		SELECT			@catid,			@quantity,			@buyer,			@itemid,			@Orderno,@item,@price					IF @@ERROR <> 0			GOTO ErrorHandling /*_____________________________________________________________________________  Closing  ___________________________________________________________________________*/ 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Einde '				+ CONVERT(VARCHAR, GETDATE(), 113) 		PRINT @Message	END 	RETURN 0 /*_____________________________________________________________________________  ErrorHandling  _____________________________________________________________________________*/ 	ErrorHandling:		SELECT @Message = '*** ' + @ProcName + ' - Error '				+ CONVERT(VARCHAR, GETDATE(), 113) 		IF @Testlevel > 0			PRINT @Message 		IF @@TRANCOUNT > 0			ROLLBACK TRAN 		RETURN 1
GO
/****** Object:  StoredProcedure [dbo].[spInsKeyLoyaltyHistory]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInsKeyLoyaltyHistory]/*	Created by: 	<<Company Name>>, <<Developers Name>>	Date: 		donderdag, 20 jun 2013	Purpose:	<<invullen>>	Input:		<<invullen>>	Output:		<<invullen>>*/	@Loyalty                                  INT                  = NULL,   	@ExpiryDate                               DATETIME             = NULL,   	@IssueDate                                DATETIME             = NULL,   	@Userid                                   NVARCHAR(50)         = NULL,	@status                                   NVARCHAR(50)         = NULL,	@Orderid                                   NVARCHAR(50)         = NULL     	AS 	DECLARE @ProcName 	VARCHAR(50)	DECLARE @Message  	VARCHAR(255)	DECLARE @Testlevel	INT /*_____________________________________________________________________________  Set @Testlevel, @ProcName, en Startingmessage  _____________________________________________________________________________*/ 	/*FILL OUT THIS SELECTSTATEMENT CORRECTLY !	  SELECT @Testlevel = <<Fieldname>> FROM <<SettingsTable>> 	  WHERE <<Keyvalue>> = 'SPTest'	  IF @@ERROR <> 0	  	GOTO ErrorHandling*/ 	SELECT @ProcName = OBJECT_NAME(@@PROCID) 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Start '				+ CONVERT(VARCHAR, GETDATE(), 113)		PRINT @Message	END /*_____________________________________________________________________________  Procedure starts here  _____________________________________________________________________________*/ 		INSERT INTO [LoyaltyHistory](			[Loyalty],			[ExpiryDate],			[IssueDate],			[Userid],[status],[Orderid])		SELECT			@Loyalty,			@ExpiryDate,			@IssueDate,			@Userid,@status,@Orderid		IF @@ERROR <> 0			GOTO ErrorHandling /*_____________________________________________________________________________  Closing  ___________________________________________________________________________*/ 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Einde '				+ CONVERT(VARCHAR, GETDATE(), 113) 		PRINT @Message	END 	RETURN 0 /*_____________________________________________________________________________  ErrorHandling  _____________________________________________________________________________*/ 	ErrorHandling:		SELECT @Message = '*** ' + @ProcName + ' - Error '				+ CONVERT(VARCHAR, GETDATE(), 113) 		IF @Testlevel > 0			PRINT @Message 		IF @@TRANCOUNT > 0			ROLLBACK TRAN 		RETURN 1
GO
/****** Object:  StoredProcedure [dbo].[spInsKeyCategoryTable]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInsKeyCategoryTable]/*	Created by: 	<<Company Name>>, <<Developers Name>>	Date: 		woensdag, 21 dec 2011	Purpose:	<<invullen>>	Input:		<<invullen>>	Output:		<<invullen>>*/	@Category                                 NVARCHAR(100)        = NULL,   	@sortid                                   NUMERIC(18,0)        ,         	@image                                    NVARCHAR(50)         = NULL,  	@Description                              NVARCHAR(2000)         = NULL,  	@Submitby                                   NVARCHAR(50)         = NULL     	AS 	DECLARE @ProcName 	VARCHAR(50)	DECLARE @Message  	VARCHAR(255)	DECLARE @Testlevel	INT /*_____________________________________________________________________________  Set @Testlevel, @ProcName, en Startingmessage  _____________________________________________________________________________*/ 	/*FILL OUT THIS SELECTSTATEMENT CORRECTLY !	  SELECT @Testlevel = <<Fieldname>> FROM <<SettingsTable>> 	  WHERE <<Keyvalue>> = 'SPTest'	  IF @@ERROR <> 0	  	GOTO ErrorHandling*/ 	SELECT @ProcName = OBJECT_NAME(@@PROCID) 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Start '				+ CONVERT(VARCHAR, GETDATE(), 113)		PRINT @Message	END /*_____________________________________________________________________________  Procedure starts here  _____________________________________________________________________________*/ 		INSERT INTO [CategoryTable](			[Category],			[sortid],			[image],			[Description],			[Submitby])		SELECT			@Category,			@sortid,					@image,			@Description,			@Submitby		IF @@ERROR <> 0			GOTO ErrorHandling /*_____________________________________________________________________________  Closing  ___________________________________________________________________________*/ 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Einde '				+ CONVERT(VARCHAR, GETDATE(), 113) 		PRINT @Message	END 	RETURN 0 /*_____________________________________________________________________________  ErrorHandling  _____________________________________________________________________________*/ 	ErrorHandling:		SELECT @Message = '*** ' + @ProcName + ' - Error '				+ CONVERT(VARCHAR, GETDATE(), 113) 		IF @Testlevel > 0			PRINT @Message 		IF @@TRANCOUNT > 0			ROLLBACK TRAN 		RETURN 1
GO
/****** Object:  StoredProcedure [dbo].[SpgetTopProducts]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpgetTopProducts]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select  * FROM  dbo.itemdetailtable as tableNew   WHERE     (IsNew = '1') AND (IsVisible <> '0' )ORDER BY id DESC
select  * FROM  dbo.itemdetailtable  as tableDaily WHERE     (DailyDeal = '1') AND (IsVisible <> '0') ORDER BY id DESC
select  * FROM  dbo.itemdetailtable   as tableBest  WHERE   (BestSellers = '1') AND (IsVisible <> '0') ORDER BY id DESC
select  * FROM  dbo.itemdetailtable   as tableFeatured  WHERE   (IsFeatured = '1') AND (IsVisible <> '0') ORDER BY id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[SpGetIndex]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetIndex] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select top(5) * FROM  dbo.itemdetailtable as tableNew   WHERE     (IsNew = '1') AND (IsVisible <> '0' )ORDER BY id DESC
select top(5) * FROM  dbo.itemdetailtable  as tableDaily WHERE     (DailyDeal = '1') AND (IsVisible <> '0') ORDER BY id DESC
select top(3) * FROM  dbo.itemdetailtable   as tableBest  WHERE   (BestSellers = '1') AND (IsVisible <> '0') ORDER BY id DESC
select top(5) * FROM  dbo.itemdetailtable   as tableFeatured  WHERE   (IsFeatured = '1') AND (IsVisible <> '0') ORDER BY id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[spFeedBack]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spFeedBack]

	@Name				nvarchar(50)=null,  
	@EmailID			nvarchar(50)=null,  
	@PhoneNo			nvarchar(50)=null,  
	@ProductName		nvarchar(50)=null,  
	@ProductSubName	nvarchar(50)=null,  
	@ProductQuality		nvarchar(50)=null,  
	@Comment			nvarchar(Max)=null 

as 
begin
insert into Feedback(Name,EmailID,PhoneNo,ProductName,ProductSubName,ProductQuality,Comment) values
(@Name,@EmailID,@PhoneNo,@ProductName,@ProductSubName,@ProductQuality,@Comment)
end
GO
/****** Object:  StoredProcedure [dbo].[spEnquiry]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spEnquiry]

	@Name				nvarchar(50)=null,  
	@EmailID			nvarchar(50)=null,  
	@PhoneNo	     nvarchar(50)=null,  
	@ProductName		nvarchar(50)=null,  	
	@Comment			nvarchar(Max)=null 

as 
begin
insert into Enquiry(Name,EmailID,PhoneNo,ProductName,Comment) values
(@Name,@EmailID,@PhoneNo,@ProductName,@Comment)
end
GO
/****** Object:  StoredProcedure [dbo].[spUpdUserTable]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdUserTable]/*	Created by: 	<<Company Name>>, <<Developers Name>>	Date: 		vrijdag, 23 dec 2011	Purpose:	<<invullen>>	Input:		<<invullen>>	Output:		<<invullen>>*/		@UserName                                 NVARCHAR(200)        = NULL,   	@Password                                 NVARCHAR(50)         = NULL,   	@Name                                     NVARCHAR(200)        = NULL,   	@Email                                    NVARCHAR(200)        = NULL,   	@Phone                                    NVARCHAR(50)         = NULL,   	@Address                                  NVARCHAR(200)        = NULL,   	@City                                     NVARCHAR(50)         = NULL,   	@State                                    NVARCHAR(50)         = NULL,   	@Zipcode                                  NVARCHAR(50)         = NULL,   	@registerdate                             DATETIME             = NULL     	AS 	DECLARE @ProcName 	VARCHAR(50)	DECLARE @Message  	VARCHAR(255)	DECLARE @Testlevel	INT /*_____________________________________________________________________________  Set @Testlevel, @ProcName, en Startingmessage  _____________________________________________________________________________*/ 	/*FILL OUT THIS SELECTSTATEMENT CORRECTLY !	  SELECT @Testlevel = <<Fieldname>> FROM <<SettingsTable>> 	  WHERE <<Keyvalue>> = 'SPTest'	  IF @@ERROR <> 0	  	GOTO ErrorHandling*/ 	SELECT @ProcName = OBJECT_NAME(@@PROCID) 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Start '				+ CONVERT(VARCHAR, GETDATE(), 113)		PRINT @Message	END /*_____________________________________________________________________________  Procedure starts here  _____________________________________________________________________________*/ 		UPDATE 	[UserTable]		SET								[Password] = @Password, 				[Name] = @Name, 				[Email] = @Email, 				[Phone] = @Phone, 				[Address] = @Address, 				[City] = @City, 				[State] = @State, 				[Zipcode] = @Zipcode, 				[registerdate] = @registerdate		WHERE				[UserName] = @UserName		IF @@ERROR <> 0			GOTO ErrorHandling /*_____________________________________________________________________________  Closing  ___________________________________________________________________________*/ 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Einde '				+ CONVERT(VARCHAR, GETDATE(), 113) 		PRINT @Message	END 	RETURN 0 /*_____________________________________________________________________________  ErrorHandling  _____________________________________________________________________________*/ 	ErrorHandling:		SELECT @Message = '*** ' + @ProcName + ' - Error '				+ CONVERT(VARCHAR, GETDATE(), 113) 		IF @Testlevel > 0			PRINT @Message 		IF @@TRANCOUNT > 0			ROLLBACK TRAN 		RETURN 1
GO
/****** Object:  StoredProcedure [dbo].[spUpdCategoryTable]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdCategoryTable]/*	Created by: 	<<Company Name>>, <<Developers Name>>	Date: 		donderdag, 22 dec 2011	Purpose:	<<invullen>>	Input:		<<invullen>>	Output:		<<invullen>>*/	@catid                                    NUMERIC(18,0)        ,         	@Category                                 NVARCHAR(100)        = NULL,   	@sortid                                   NUMERIC(18,0)        ,        	@image                                    NVARCHAR(50)         = NULL,	@Description                              NVARCHAR(2000)         = NULL     	AS 	DECLARE @ProcName 	VARCHAR(50)	DECLARE @Message  	VARCHAR(255)	DECLARE @Testlevel	INT /*_____________________________________________________________________________  Set @Testlevel, @ProcName, en Startingmessage  _____________________________________________________________________________*/ 	/*FILL OUT THIS SELECTSTATEMENT CORRECTLY !	  SELECT @Testlevel = <<Fieldname>> FROM <<SettingsTable>> 	  WHERE <<Keyvalue>> = 'SPTest'	  IF @@ERROR <> 0	  	GOTO ErrorHandling*/ 	SELECT @ProcName = OBJECT_NAME(@@PROCID) 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Start '				+ CONVERT(VARCHAR, GETDATE(), 113)		PRINT @Message	END /*_____________________________________________________________________________  Procedure starts here  _____________________________________________________________________________*/ 		UPDATE 	[CategoryTable]		SET				[Category] = @Category, 				[sortid] = @sortid, 								[image] = @image,				[Description]=@Description		WHERE				[catid] = @catid		IF @@ERROR <> 0			GOTO ErrorHandling /*_____________________________________________________________________________  Closing  ___________________________________________________________________________*/ 	IF @Testlevel > 0	BEGIN		SELECT @Message = '*** ' + @ProcName + ' - Einde '				+ CONVERT(VARCHAR, GETDATE(), 113) 		PRINT @Message	END 	RETURN 0 /*_____________________________________________________________________________  ErrorHandling  _____________________________________________________________________________*/ 	ErrorHandling:		SELECT @Message = '*** ' + @ProcName + ' - Error '				+ CONVERT(VARCHAR, GETDATE(), 113) 		IF @Testlevel > 0			PRINT @Message 		IF @@TRANCOUNT > 0			ROLLBACK TRAN 		RETURN 1
GO
/****** Object:  StoredProcedure [dbo].[SpUpdateCategoryVisibility]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpUpdateCategoryVisibility]
	-- Add the parameters for the stored procedure here
	
    @IsVisible  varchar(15),
    @catid  int

AS
	DECLARE @intErrorCode INT
BEGIN TRAN
    UPDATE CategoryTable
    SET IsVisible = @IsVisible
    WHERE catid = @catid

    SELECT @intErrorCode = @@ERROR
    IF (@intErrorCode <> 0) GOTO PROBLEM

    UPDATE itemdetailtable
    SET itemdetailtable.IsVisible = @IsVisible
    WHERE itemdetailtable.categoryname = @catid

    SELECT @intErrorCode = @@ERROR
    IF (@intErrorCode <> 0) GOTO PROBLEM
COMMIT TRAN

PROBLEM:
IF (@intErrorCode <> 0) BEGIN
PRINT 'Unexpected error occurred!'
    ROLLBACK TRAN
END
GO
/****** Object:  StoredProcedure [dbo].[spSubCategory]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spSubCategory]

@SubCatID numeric(18, 0)= NULL,
@CategoryID nvarchar(100)= NULL,
@SubCategory nvarchar(100) =NULL,
@SortId numeric(18, 0)= NULL,
@Image nvarchar(50)= NULL,
@IsVisible bit =NULL,
@SubCatInsDt datetime =NULL,
@Description nvarchar(2000)= NULL,
@Submitby nvarchar(50)= NULL,
@Action nvarchar(50)= NULL
as
begin

if(@Action='Insert')
begin
insert into SubCategory (CategoryID,SubCategory,Image,Description,Submitby)values (@CategoryID,@SubCategory,@Image,@Description,@Submitby)
end

if(@Action='Update')
begin

Update  SubCategory set SortId=@SortId,SubCategory=@SubCategory,Image=@Image,Description=@Description where SubCatID=@SubCatID

end
end
GO
/****** Object:  StoredProcedure [dbo].[spStock]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spStock]
@ItemId numeric(18,0)=null,
@AvailableQuantity int =0,
@AlertOnQuantity int=null,
@Action nvarchar(50)=null,
@AddQuantity int=null,
@ReduceQuantity int=null
as
begin

if(@Action='insert')
begin
insert into StockTable(itemid,qty,AlertOnQuantity) values(@ItemId,@AvailableQuantity,@AlertOnQuantity)
end

if(@Action='updateAddItemQuantity')
begin
select @AvailableQuantity=qty from StockTable where itemid=@ItemId
set @AvailableQuantity=@AvailableQuantity+@AddQuantity
update StockTable set qty=@AvailableQuantity where itemid=@ItemId 
end


if(@Action='updateReduceItemQuantity')
begin
select @AvailableQuantity=qty from StockTable where itemid=@ItemId

	if((@AvailableQuantity-@ReduceQuantity)<=0)
	begin
		set @AvailableQuantity=0
	end
	else
	begin
		set @AvailableQuantity=@AvailableQuantity-@ReduceQuantity
	end
update StockTable set qty=@AvailableQuantity where itemid=@ItemId 
end

if(@Action='updateItemWithStock')
begin
update StockTable set qty=@AvailableQuantity,AlertOnQuantity=@AlertOnQuantity where itemid=@ItemId 
end


if(@Action='update')
begin
declare 
@PurchageQuantityOfProduct int,
@PriceOfProduct int,
@TotalPriceOfProduct int,
@PurchageId int,
@Amount int,
@Quantity int,
@MainAmount int


select @PriceOfProduct=price from PurChaseTable where pid=@PurchageId
select @PurchageQuantityOfProduct=quantity from PurChaseTable where pid=@PurchageId
set @TotalPriceOfProduct=@PurchageQuantityOfProduct*@PriceOfProduct
update PurChaseTable set quantity=@Quantity where pid=@PurchageId
update StockTable set qty=@AvailableQuantity,AlertOnQuantity=@AlertOnQuantity where itemid=@ItemId  
end

end
GO
/****** Object:  StoredProcedure [dbo].[spSelectGroupproduct]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelectGroupproduct]
	-- Add the parameters for the stored procedure here
	@productID   int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     dbo.tblGroupProducts.gid, dbo.tblGroupProducts.productID, dbo.tblGroupProducts.groupProductID, dbo.itemdetailtable.categoryname, dbo.itemdetailtable.code, 
                      dbo.itemdetailtable.name, dbo.itemdetailtable.image, dbo.itemdetailtable.price, dbo.itemdetailtable.offerPrice, dbo.itemdetailtable.Detail, 
                      dbo.itemdetailtable.productprice, dbo.itemdetailtable.IsVisible, dbo.itemdetailtable.id
FROM         dbo.tblGroupProducts INNER JOIN
                      dbo.itemdetailtable ON dbo.tblGroupProducts.groupProductID = dbo.itemdetailtable.id
WHERE     (dbo.tblGroupProducts.productID = @productID) AND (dbo.itemdetailtable.IsVisible = 'True')
END
GO
/****** Object:  StoredProcedure [dbo].[spSearch]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spSearch]
@SearchBy nvarchar(50) =null,
@CatID nvarchar(50)=null,
@SubCatID nvarchar(50)=null,
@Action nvarchar(50)=null,
@ProductID nvarchar(50)=null,
@RealPrice nvarchar(50)=null,
@OfferPrice nvarchar(50)=null,
@IDs nvarchar(max)=null,
@IsVisible bit =null,
@Visibilty bit =0
as 
begin

	if(@CatID='' or @CatID='0')
	begin
				set @CatID=null
	end

	if(@ProductID='' or @ProductID='0')
	begin
				set @ProductID=null
	end

	if(@SubCatID='' or @SubCatID='0')
	begin
				set @SubCatID=null
	end
	if(@Action='Search')
	begin
	
	
				SELECT st.qty,st.AlertOnQuantity,ct.Category, sc.SubCategory AS Expr6, it.*
				FROM itemdetailtable it INNER JOIN
				CategoryTable ct ON it.categoryname = ct.catid INNER JOIN
				SubCategory sc ON it.SubCategory = sc.SubCatID 
				left join StockTable st on it.id=st.itemid 
				WHERE 
				it.categoryname=isnull(@CatID,it.categoryname)
				and
				it.id=isnull(@ProductID,it.id)
				and
				it.SubCategory=isnull(@SubCatID,it.SubCategory)
				and
				(
				sc.SubCategory LIKE '%' + @SearchBy + '%'
				OR ct.Category LIKE '%' + @SearchBy + '%'
				OR it.code        LIKE '%' + @SearchBy + '%'
				OR it.Detail      LIKE '%' + @SearchBy + '%'
				OR it.name        LIKE '%' + @SearchBy + '%' 
				)
				AND (it.IsVisible <> @Visibilty)  ORDER BY NEWID()

	end
	
	
	
	
	if(@Action='getItemAlertList')
	begin
	
	
				SELECT st.qty,st.AlertOnQuantity,ct.Category, sc.SubCategory AS Expr6, it.*
				FROM itemdetailtable it INNER JOIN
				CategoryTable ct ON it.categoryname = ct.catid INNER JOIN
				SubCategory sc ON it.SubCategory = sc.SubCatID 
				left join StockTable st on it.id=st.itemid 
				WHERE 
				it.categoryname=isnull(@CatID,it.categoryname)
				and
				it.id=isnull(@ProductID,it.id)
				and
				it.SubCategory=isnull(@SubCatID,it.SubCategory)
				and
			    st.qty<=st.AlertOnQuantity
				and
				(
				sc.SubCategory LIKE '%' + @SearchBy + '%'
				OR ct.Category LIKE '%' + @SearchBy + '%'
				OR it.code        LIKE '%' + @SearchBy + '%'
				OR it.Detail      LIKE '%' + @SearchBy + '%'
				OR it.name        LIKE '%' + @SearchBy + '%' 
				)
				AND (it.IsVisible <> @Visibilty)  ORDER BY NEWID()

	end
	
	
	if(@Action='Delete')
	begin 
	delete from itemdetailtable  where id in (select convert(int,val) from split(@IDs,','))
	end
	
	if(@Action='IsVisible')
	begin 
	update itemdetailtable set IsVisible=@IsVisible  where id in (select convert(int,val) from split(@IDs,','))
	end
	
	if(@Action='UpdatePrice')
	begin 
	update itemdetailtable set price=@RealPrice ,  offerPrice=@OfferPrice  where id =@IDs
	end

	
end

--exec spSearch @Action='IsVisible',@IsVisible=0,@IDs="1,2"
GO
/****** Object:  StoredProcedure [dbo].[spRole]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spRole]
@UsreName nvarchar (50)=null,
@Password nvarchar (50)=null,
@Role nvarchar (50)=null,
@Action nvarchar (50)=null
as
begin

if(@Action='Insert')
begin
insert into AdminTable (uname,password,role) values (@UsreName,@Password,@Role)
end

if(@Action='List')
begin
select * from AdminTable   order by aid Desc
end

end
GO
/****** Object:  StoredProcedure [dbo].[getOrderCallExeList]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getOrderCallExeList]

@Searchtxt nvarchar (50)= null,
@CallExecutive nvarchar (50)= null,
@Status nvarchar (50)= null
as 
begin
SELECT ut.Name, ut.Phone, ft.OrderNo,ft.pid,ft.Dispatch, ft.username, ft.DOP, ft.amount, ft.MainAmount,(ft.amount-ft.MainAmount) as dif
FROM finalordertable ft INNER JOIN UserTable ut ON ft.username = ut.UserName 
where ft.CallExecutive=@CallExecutive and ft.Dispatch=isnull(@Status,Dispatch) and( ft.username like '%'+@Searchtxt+'%' or ut.Name like '%'+@Searchtxt+'%' or ft.OrderNo like '%'+@Searchtxt+'%')
end
GO
/****** Object:  StoredProcedure [dbo].[GetIteamPageWise]    Script Date: 09/15/2014 15:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetIteamPageWise] 
--[dbo].[GetImagesPageWise] 2, 10, null
	@PageIndex INT 
   ,@PageSize INT 
   ,@PageCount INT OUTPUT
AS
BEGIN	
	SET NOCOUNT ON;

    SELECT ROW_NUMBER() OVER
            (
                  ORDER BY [Id] ASC
            )AS RowNumber
      ,itemdetailtable.*
    INTO #Results
    FROM itemdetailtable

	DECLARE @RecordCount INT
    SELECT @RecordCount = COUNT(*) FROM #Results

	SET @PageCount = CEILING(CAST(@RecordCount AS DECIMAL(10, 2)) / CAST(@PageSize AS DECIMAL(10, 2)))
    PRINT       @PageCount
           
    SELECT * FROM #Results
    WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
    DROP TABLE #Results
END
GO
/****** Object:  Default [DF_finalordertable_Dispatch]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[finalordertable] ADD  CONSTRAINT [DF_finalordertable_Dispatch]  DEFAULT ((0)) FOR [Dispatch]
GO
/****** Object:  Default [DF_finalordertable_Discount]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[finalordertable] ADD  CONSTRAINT [DF_finalordertable_Discount]  DEFAULT ((0)) FOR [Discount]
GO
/****** Object:  Default [DF_finalordertable_UsedLoyalty]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[finalordertable] ADD  CONSTRAINT [DF_finalordertable_UsedLoyalty]  DEFAULT ((0)) FOR [UsedLoyalty]
GO
/****** Object:  Default [DF__Feedback__FeedDa__014935CB]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT (getdate()) FOR [FeedDate]
GO
/****** Object:  Default [DF__Feedback__Active__023D5A04]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT ((1)) FOR [Active]
GO
/****** Object:  Default [DF__Feedback__Del__03317E3D]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT ((0)) FOR [Del]
GO
/****** Object:  Default [DF__Enquiry__EnqDate__07F6335A]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[Enquiry] ADD  DEFAULT (getdate()) FOR [EnqDate]
GO
/****** Object:  Default [DF__Enquiry__Active__08EA5793]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[Enquiry] ADD  DEFAULT ((1)) FOR [Active]
GO
/****** Object:  Default [DF__Enquiry__Del__09DE7BCC]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[Enquiry] ADD  DEFAULT ((0)) FOR [Del]
GO
/****** Object:  Default [DF_Checkout_amount]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[Checkout] ADD  CONSTRAINT [DF_Checkout_amount]  DEFAULT ((0)) FOR [amount]
GO
/****** Object:  Default [DF_Checkout_OrderDt]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[Checkout] ADD  CONSTRAINT [DF_Checkout_OrderDt]  DEFAULT (getdate()) FOR [OrderDt]
GO
/****** Object:  Default [DF_CategoryTable_sortid]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[CategoryTable] ADD  CONSTRAINT [DF_CategoryTable_sortid]  DEFAULT ((0)) FOR [sortid]
GO
/****** Object:  Default [DF_CategoryTable_IsVisible]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[CategoryTable] ADD  CONSTRAINT [DF_CategoryTable_IsVisible]  DEFAULT ((1)) FOR [IsVisible]
GO
/****** Object:  Default [DF_CategoryTable_CatInsDt]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[CategoryTable] ADD  CONSTRAINT [DF_CategoryTable_CatInsDt]  DEFAULT (getdate()) FOR [CatInsDt]
GO
/****** Object:  Default [DF_PurChaseTable_flagstatus]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[PurChaseTable] ADD  CONSTRAINT [DF_PurChaseTable_flagstatus]  DEFAULT ((0)) FOR [flagstatus]
GO
/****** Object:  Default [DF_PurChaseTable_Submitdt]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[PurChaseTable] ADD  CONSTRAINT [DF_PurChaseTable_Submitdt]  DEFAULT (getdate()) FOR [Submitdt]
GO
/****** Object:  Default [DF_itemdetailtable_offerPrice]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_offerPrice]  DEFAULT ((0)) FOR [offerPrice]
GO
/****** Object:  Default [DF_itemdetailtable_BestSellers]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_BestSellers]  DEFAULT ((0)) FOR [BestSellers]
GO
/****** Object:  Default [DF_itemdetailtable_DailyDeal]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_DailyDeal]  DEFAULT ((0)) FOR [DailyDeal]
GO
/****** Object:  Default [DF_itemdetailtable_IsNew]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_IsNew]  DEFAULT ((0)) FOR [IsNew]
GO
/****** Object:  Default [DF_itemdetailtable_IsVisible]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_IsVisible]  DEFAULT ((0)) FOR [IsVisible]
GO
/****** Object:  Default [DF_itemdetailtable_IsFeatured]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_IsFeatured]  DEFAULT ((0)) FOR [IsFeatured]
GO
/****** Object:  Default [DF_itemdetailtable_ProInsDt]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[itemdetailtable] ADD  CONSTRAINT [DF_itemdetailtable_ProInsDt]  DEFAULT (getdate()) FOR [ProInsDt]
GO
/****** Object:  Default [DF_Wishlist_status]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[Wishlist] ADD  CONSTRAINT [DF_Wishlist_status]  DEFAULT ((0)) FOR [status]
GO
/****** Object:  Default [DF__SubCatego__SortI__619B8048]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[SubCategory] ADD  DEFAULT ((0)) FOR [SortId]
GO
/****** Object:  Default [DF__SubCatego__IsVis__628FA481]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[SubCategory] ADD  DEFAULT ((1)) FOR [IsVisible]
GO
/****** Object:  Default [DF__SubCatego__SubCa__6383C8BA]    Script Date: 09/15/2014 15:19:36 ******/
ALTER TABLE [dbo].[SubCategory] ADD  DEFAULT (getdate()) FOR [SubCatInsDt]
GO

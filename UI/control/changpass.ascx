﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="changpass.ascx.cs" Inherits="control_changpass" %>
<%@ Register Assembly="msgBox" Namespace="BunnyBear" TagPrefix="cc2" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>

<%@ Register src="MenuMyAccount.ascx" tagname="MenuMyAccount" tagprefix="uc1" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
</asp:UpdatePanel>
  <div class="row">
    <uc1:MenuMyAccount ID="MenuMyAccount2" runat="server" />
 </div>

 <div class="row">
 <div class="title"><h4 class="left">Change Password </h4> <span class="right">Password should contains only alphanumeric characters. </span></div>
 </div>

<div id="row m10">
 <div class="column border-box">
 <div class="center">
 <ul class="lgnul">
 <li>
 <span class="lbl">Old Password</span>
 <div class="inputbox">
 <asp:TextBox ID="txtoldpass" runat="server" Columns="22" CssClass="input" 
                            TextMode="Password"></asp:TextBox>
                        <cc1:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="txtoldpass" CssClass="error1" Display="Dynamic" 
                            ErrorMessage="Enter Old Password!" SetFocusOnError="True" 
                            ValidationGroup="chgps">
                        </cc1:RequiredFieldValidator>
                        <cc1:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ControlToValidate="txtoldpass" CssClass="error" Display="Dynamic" 
                            ErrorMessage="Invalid Old Password!" SetFocusOnError="True" 
                            ValidationExpression="^[^&lt;&gt;%]{2,100}$" ValidationGroup="chgps">
                        </cc1:RegularExpressionValidator>
 </div>

 </li>

 <li>
 <span class="lbl">New Password</span>
 <div class="inputbox">
  <asp:TextBox ID="txtnwpass" runat="server" Columns="22" CssClass="input" 
                            TextMode="Password"></asp:TextBox>
                        <cc1:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                            ControlToValidate="txtnwpass" CssClass="error1" Display="Dynamic" 
                            ErrorMessage="Enter New Password! " SetFocusOnError="True" 
                            ValidationGroup="chgps">
                        </cc1:RequiredFieldValidator>
                        <cc1:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                            ControlToValidate="txtnwpass" Display="Dynamic" 
                            ErrorMessage="Invalid Password or less than 2 char!" SetFocusOnError="True" 
                            ValidationExpression="^[a-zA-Z0-9 \s]{2,20}$" ValidationGroup="chgps">
                        </cc1:RegularExpressionValidator>
 </div>
 </li>

  <li>
 <span class="lbl">
 Confirm Password
 </span>
 <div class="inputbox">
 <asp:TextBox ID="txtcnfpass" runat="server" Columns="22" CssClass="input" 
                            TextMode="Password"></asp:TextBox>

<cc1:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                            ControlToValidate="txtcnfpass" CssClass="error1" Display="Dynamic" 
                            ErrorMessage="!" SetFocusOnError="True" 
                            ValidationGroup="chgps">
                        </cc1:RequiredFieldValidator>

                        <cc1:CompareValidator CssClass="error1" ID="CompareValidator1" runat="server" 
                            ControlToCompare="txtnwpass" ControlToValidate="txtcnfpass" Display="Dynamic" 
                            ErrorMessage="Password Mismatch!" SetFocusOnError="True" 
                            ValidationGroup="chgps">
                        </cc1:CompareValidator>
 </div>
 </li>

  <li>
 <asp:LinkButton ID="chdlnbk" runat="server" Width="80px" style="text-align:center;" onclick="chdlnbk_Click" 
                                        ValidationGroup="chgps" BorderWidth="1px" CssClass="_button1 backgroundbg border-radius">Change</asp:LinkButton>
                                         <cc2:msgBox ID="msg" runat="server" />
 </li>
 </ul>
 </div>
 </div>
</div>
 <div class="clear"></div>



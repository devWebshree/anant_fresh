﻿using ClassLibrary;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User : System.Web.UI.MasterPage
{
    PressRelease blpress = new PressRelease();
    dlPressRelease dlpress = new dlPressRelease();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            showAndHideCatSubCat();
            getPressRelease();

        }
        Session["IsUser"] = extra.BuyerUniqueID();
        if (Session["Itemname"] != null)
        {
            JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, Session["Itemname"].ToString(), 5000);
            Session["Itemname"] = null;
        }
        if (Session["Exist"] != null)
        {
            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, Session["Exist"].ToString(), 5000);
                Session["Exist"] = null;

        }
    }

   
    public void getPressRelease()
    {
        blpress.SearchText = "";
        DataSet ds = dlpress.getPressRelease(blpress);
        if (ds.Tables[0].Rows.Count > 0)
        {
            rptPress.DataSource = ds;
            rptPress.DataBind();
        }
        else
        {
            rptPress.DataSource = null;
            rptPress.DataBind();
        }
    }

    public void showAndHideCatSubCat()
    {
        try
        {
            if (Page.RouteData.Values["catname"] != null && Page.RouteData.Values["scatname"] != null)
            {
                cat.Visible = false;
                scat1.Visible = true;
            }
            else
            {
                cat.Visible = true;
                scat1.Visible = false;
            }

        }
        catch (Exception ex) { }
    }
}
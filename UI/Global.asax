﻿<%@ Application Language="C#" %>
<%@ Import Namespace ="System.Web.Routing" %>
<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        WebshreeCommonLibrary.HandleExceptions.FilePath = AppDomain.CurrentDomain.BaseDirectory + "/ProductImage/ExceptionDetails.txt";
        
        
        RouteTable.Routes.MapPageRoute("StoreRoute1", "home", "~/index.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute3", "contact", "~/contact.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute4", "register", "~/register.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute5", "login", "~/login.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute12", "login/{ReturnUrl}", "~/login.aspx");

        RouteTable.Routes.MapPageRoute("StoreRoute6", "product/{srch}", "~/product.aspx");
        
        RouteTable.Routes.MapPageRoute("StoreRoute2", "product/{catname}/{scatname}", "~/product.aspx");
        
        RouteTable.Routes.MapPageRoute("StoreRoute8", "product", "~/product.aspx");

        RouteTable.Routes.MapPageRoute("StoreRoute7", "ProductDetails/{id}/", "~/ProductDetails.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute9", "wishlist", "~/wishlist.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute10", "cart", "~/cart.aspx");

        RouteTable.Routes.MapPageRoute("StoreRoute11", "OrderHistory", "~/OrderHistory.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute13", "Profile", "~/UpdateProfile.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute14", "changepassword", "~/changepasswd.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute15", "cartCheckOut", "~/cartCheckOut.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute16", "OrderDetails/{ord}", "~/orderdetails.aspx");

        RouteTable.Routes.MapPageRoute("StoreRoute17", "apperror", "~/apperror.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute18", "RefundPolicy", "~/RefundPolicy.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute19", "whyAnantFresh", "~/whyAnantFresh.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute20", "OrderComplete", "~/Ordermessage.aspx");

        RouteTable.Routes.MapPageRoute("StoreRoute21", "PrivacyPolicy", "~/PrivacyPolicy.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute22", "termsAndConditions", "~/termsAndConditions.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute23", "sitemap", "~/sitemap.aspx");

        RouteTable.Routes.MapPageRoute("StoreRoute24", "pressrelease", "~/pressrelease.aspx");

        RouteTable.Routes.MapPageRoute("StoreRoute25", "shippingpolicy", "~/ShippingPolicy.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute26", "cancellationpolicy", "~/CancellationPolicy.aspx");
        RouteTable.Routes.MapPageRoute("StoreRoute27", "result", "~/exam/result.aspx");
                        
        
       // ManageData.cs
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        // Code that runs when a new session is started
        Session["URL"] = "0";
        Session["IsUser"] = "0";
        Session["min"] = "0";
        Session["max"] = "0";
        Session["myacount"] = "0";
        Session["srch"] = "0";
        Session["Top"] = "0";
        Session["ShopUser"] = "0";
        Session["ShopUserId"] = "0";
        Session["order"] = "0";
        Session["SalesName"] = "0";
        Session["UserType"] = "0";

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

  

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {
        if (HttpContext.Current.User != null)
        {
            if (Request.IsAuthenticated == true)
            {                          
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(Context.Request.Cookies[FormsAuthentication.FormsCookieName].Value);                          
                string[] roles = new string[1] { ticket.UserData };
                FormsIdentity id = new FormsIdentity(ticket);
                Context.User = new System.Security.Principal.GenericPrincipal(id, roles);        
            }
        }         
 
    }
       
  
       
</script>

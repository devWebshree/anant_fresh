﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data.Common;
using ClassLibrary;

namespace DAL
{
    public class dlEnquiry
    {
       Database db = GetDatabase(AppDatabaseName);
       private static string AppDatabaseName;
       private static Database GetDatabase(string dbName)
       {
           try
           {
               Database db = null;
               if (dbName == null)
               {
                   return db = DatabaseFactory.CreateDatabase();
               }
               else
               {
                   return db = DatabaseFactory.CreateDatabase(dbName);
               }
               return db;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       static dlEnquiry()
       {
           AppDatabaseName = ConfigurationSettings.AppSettings["AppDatabaseName"];
       }

       public bool insertEnquiry(Enquiry enq)
       {

           //try
           //{
               DbCommand cmd = db.GetStoredProcCommand("spEnquiry");
               db.AddInParameter(cmd, "@Name", DbType.String, enq.Name);
               db.AddInParameter(cmd, "@EmailID", DbType.String, enq.EmailID);
               db.AddInParameter(cmd, "@PhoneNo", DbType.String, enq.MobileNo);
               db.AddInParameter(cmd, "@ProductName", DbType.String, enq.ProductName);
               db.AddInParameter(cmd, "@Comment", DbType.String, enq.Comment);
               int i = db.ExecuteNonQuery(cmd);
               if (i >0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           //}
           //catch (Exception ex)
           //{
           //    throw ex;
           //}
       }

       public string getCatID(string itemid)
       {

           try
           {
               DbCommand cmd = db.GetSqlStringCommand("select distinct c.CatID from tblProductInCategory c left join tblProducts p on c.ProdUniqueID=p.ProdUniqueID where p.ProductID=@PID");
               db.AddInParameter(cmd, "@PID", DbType.String, itemid);
               object CatID = db.ExecuteScalar(cmd);
               if (CatID == null || System.DBNull.Value == CatID)
               {
                   return "0";
               }
               else
               {
                   return CatID.ToString();
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }


       }

       public DataSet listEnquiry()
       {
           try
           {
               DbCommand cmd = db.GetSqlStringCommand("select * from Enquiry");
               DataSet ds = db.ExecuteDataSet(cmd);
               return ds;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BAL;
using System.Web.Mail;

public partial class control_register : System.Web.UI.UserControl
{
    SendMail sm = new SendMail();
    modifyData ad = new modifyData();
   static  bool ISEmailExist, ISMobileExist;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        string uName = txtusername.Text;

        if (Page.IsValid)
        {
            checkExistClient();
            if (ISEmailExist==true && ISMobileExist==true)
            {
                userproperty up = new userproperty();
                up.U_uname = txtusername.Text;
                up.U_password = txtPassowrd.Text;
                up.U_name = txtname.Text;
                up.U_email = txtusername.Text;
                up.U_ph = txtph.Text;
                up.U_altph = txtaltph.Text;
                up.U_addr = txtaddr.Text;
                up.city = txtcity.Text;
                up.state = txtstate.Text;
                up.country = txtcoun.Text;
                up.zip = txtzip.Text;
                ad.datainsert(up);

                sm.sendRegisteredUser(txtusername.Text.Trim(), txtusername.Text.Trim(), txtPassowrd.Text.Trim());
                JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, "Your account has been created successfully! Please check your registered email id", 5000);

                txtaddr.Text = "";
                txtPassowrd.Text = "";
                txtaltph.Text = "";
                txtcity.Text = "";
                txtname.Text = "";
                txtcoun.Text = "";
                txtph.Text = "";
                txtstate.Text = "";
                txtusername.Text = "";
                txtzip.Text = "";
            }
            else
                JqueryNotification.NotificationExtensions.ShowErrorNotification(this, "This email is already registered.", 5000);
        }
    }



    public void checkExistClient()
    {
        if (ad.IsEmailAvailable(txtusername.Text) == true)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, "This email is already registered.", 5000);
            lblMessage.Text = "<span class='error1 left ml5'>This email is already registered. Want to</span> <a href='/login' class='left error1 ml5 c'>login</a> <span class='left error1 ml5 '>or</span> <a href='/login?forget=true' class='left error1 ml5 c'>recover</a> <span class='left error1'>your password?</span>";
            lblMessage.Visible = true;
            ISEmailExist = false;
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.ForestGreen;
            lblMessage.Text = "<span class='error1 left ml5' style='color:green;'>We will email you a confirmation.</span>";
            lblMessage.Visible = true;
            ISEmailExist = true;
        }
     }

    public void checkMobileAvailable() 
    {

        if (ad.IsMobileAvailable(txtph.Text) == true)
        {
            lblIsMobileExist.ForeColor = System.Drawing.Color.Red;
            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, "This email is already registered.", 5000);
            lblIsMobileExist.Text = "<span class='error1 left ml5'>This mobile is already registered. Want to</span> <a href='/login' class='left error1 ml5 c'>login</a> <span class='left error1 ml5 '>or</span> <a href='/login?forget=true' class='left error1 ml5 c'>recover</a> <span class='left error1'>your password?</span>";
            lblIsMobileExist.Visible = true;
            
            ISMobileExist = false; 
        }
        else
        {
            lblIsMobileExist.ForeColor = System.Drawing.Color.ForestGreen;
            lblIsMobileExist.Text = "<span class='error1 left ml5' style='color:green;'>We will message you a confirmation.</span>";
            lblIsMobileExist.Visible = true;

            ISMobileExist = true;
        }
    }

    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }

    protected void txtusername_TextChanged(object sender, EventArgs e)
    {
        checkExistClient();
    }
    protected void txtph_TextChanged(object sender, EventArgs e)
    {
        checkMobileAvailable();
    }
}

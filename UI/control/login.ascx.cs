﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using BAL;

public partial class control_login : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    CrmManagement cm = new CrmManagement();
    userproperty up = new userproperty();
    SendMail sm = new SendMail();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["forget"] != null)
            {
                showforgetpnl();
            }
        }
    }
    protected void lnklogsubmit_Click(object sender, EventArgs e)
    {
        ValidateUser();
    }  
 
    void ValidateUser()
    {
        if (ad.checkadmin(txtusrname.Text, txtupswrd.Text) == true)
        {
            string findrole = ad.checkrole(txtusrname.Text).ToString();

            if (findrole == "dbadmin")
            {
                FormsAuthentication.Initialize();
                string strrole = null;
                strrole = "dba";
                FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1, txtusrname.Text, DateTime.Now, DateTime.Now.AddMinutes(30), false, strrole, FormsAuthentication.FormsCookiePath);
                Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(fat)));
                Response.Redirect("/admin/items.aspx");
            }

            if (findrole == "CallExecutive")
            {
                FormsAuthentication.Initialize();
                string strrole = null;
                strrole = "call";
                FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1, txtusrname.Text, DateTime.Now, DateTime.Now.AddMinutes(30), false, strrole, FormsAuthentication.FormsCookiePath);
                Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(fat)));
                Response.Redirect("/CallExecutive/ProdList.aspx");
            }

            if (findrole == "DispatchExecutive")
            {
                FormsAuthentication.Initialize();
                string strrole = null;
                strrole = "dispatch";
                FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1, txtusrname.Text, DateTime.Now, DateTime.Now.AddMinutes(30), false, strrole, FormsAuthentication.FormsCookiePath);
                Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(fat)));
                Response.Redirect("/DispatchExecutive/orderList.aspx");
            }
           
        }
        if (ad.checkUser(txtusrname.Text, txtupswrd.Text))
        {

            Object obj = Session["IsUser"];
            if (obj != null)
            {
                ad.UpdateSessuid(txtusrname.Text, obj.ToString());
            }
            FormsAuthentication.Initialize();
            string strrole = null;
            strrole = "user";
            FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1, txtusrname.Text, DateTime.Now, DateTime.Now.AddMinutes(30), false, strrole, FormsAuthentication.FormsCookiePath);
            Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(fat)));

            if (Page.RouteData.Values["ReturnUrl"] != null)
            {
                Response.Redirect("/" + Page.RouteData.Values["ReturnUrl"].ToString());
            }
            else
            {
                Response.Redirect("/product");
            }
        }
        else
        {
            pnl_wrong.Visible = true;
            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, "Your Email id or Password is incorrect!", 1000);
        }
    }

    public void ShowMessage(string str)
    {
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }
   



    protected void lbForgrt_Click(object sender, EventArgs e)
    {
        showforgetpnl();
    }

    public void showforgetpnl()
    {
        lbl_title.Text = "Recover Your Password";
        pnl_forget.Visible = true;
        pnl_login.Visible = false;
    }

    protected void lb_sendPass_Click(object sender, EventArgs e)
    {
        DataSet ds = up.getAccountInfo(txt_username2.Text.Trim());
        if (ds.Tables[0].Rows.Count > 0)
        {
            sm.sendMailForget(ds.Tables[0].Rows[0]["Username"].ToString(), ds.Tables[0].Rows[0]["Name"].ToString(), ds.Tables[0].Rows[0]["Password"].ToString());
            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, "Account credentials has been send in your registered email id.", 2000);
            //lblMsg2.Text = "<br><span style='color:green;'>Account credentials has been send in your registered email id.</span>";
        }
        else
        {
           // lblMsg2.Text = "<br><span style='color:red;'>This email id doesn't exist!</span>";
            JqueryNotification.NotificationExtensions.ShowWarningNotification(this, "This email id doesn't exist!", 1000);

        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/register");
    }
}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ClassLibrary;
using BAL;
using System.Web.Mail;
using DAL;

public partial class control_orderdtl : System.Web.UI.UserControl
{
    modifyData md = new modifyData();
    string ord;
    int quantity;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                if (Page.RouteData.Values["ord"] != null)
                {
                    ord = Page.RouteData.Values["ord"].ToString();
                }
                binddata();
            }
            else
            {
                Response.Redirect("/login/OrderDetails");
            }

            
        }
    }
    public void binddata()
    {

        dgrd.DataSource = md.SelOrd(ord);
        dgrd.DataBind();
        dlst.DataSource = md.SelFinalOrdDtl(ord);
        dlst.DataBind();
    }
    protected void bcklnk_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["loyalty"] != null)
        {
            Response.Redirect("my-loyalty.aspx");
        }
        else
        {
            Response.Redirect("/OrderHistory");
        }
    }
    public static string GetDis(object st)
    {



        if (st.ToString() == "0")
        {
            return "Under Process";

        }
        else if (st.ToString() == "1")
        {
            return "Cancel request";

        }
        else if (st.ToString() == "2")
        {
            return "Cancellation  Confirmed";

        }
        else if (st.ToString() == "3")
        {
            return "Order Dispatched";
        }
        else if (st.ToString() == "4")
        {
            return "Dispatch In Process";
        }

        else
        {
            return "Under Process";
        }


        //if (st.ToString() == "0")
        //{
        //    return "Under Process";


        //}
        //else if (st.ToString() == "1")
        //{
        //    return "Cancel";

        //}
        //else if (st.ToString() == "2")
        //{
        //    return "Cancellation Confirmed";

        //}
        //else if (st.ToString() == "3")
        //{
        //    return "Order Verified";
        //}
        //else if (st.ToString() == "4")
        //{
        //    return "Dispatch Not confirmed";
        //}
        //else if (st.ToString() == "5")
        //{
        //    return "Dispatch confirmed";

        //}
        //else if (st.ToString() == "6")
        //{
        //    return "Order Received";
        //}
        //else if (st.ToString() == "7")
        //{
        //    return "Order Dispatched";
        //}
        //else if (st.ToString() == "8")
        //{
        //    return "Dispatch";

        //}
        //else
        //{
        //    return "Under Process";
        //}
    }

    protected void dgrd_ItemDataBound(object sender, DataGridItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.AlternatingItem | e.Item.ItemType == ListItemType.Item)
        {

            Label Quantity = ((Label)e.Item.FindControl("txtqty"));
            quantity += Convert.ToInt32(Quantity.Text);
        }
    }

    public string bng(object obj)
    {
        decimal Amount;

        Amount = Convert.ToDecimal(obj);
        decimal j = 0;
        if (Amount >= 1004)
        {

            j = 0;
        }
        else
            j = 50;

        lblQuantity.Text = quantity.ToString();
        lblSubTotal.Text = (Amount - j).ToString();
        lblShipping.Text = j.ToString();
        lbltotal.Text = Amount.ToString();
        return "";
    }
}

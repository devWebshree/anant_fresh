﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using BAL;

public partial class control_SubCategory : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    dlSubCategory objscat = new dlSubCategory();
    static string CatId, SCatID;
    public Utility all = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            bindAccordingSerach();
            bindCategoryName();
        }

    }

    public void bindSubCategoryName()
    {
        DataSet ds = objscat.getSubCategoryNameList(CatId);//Convert.ToInt32(ddl_subCat.SelectedIndex)
        if (ds.Tables[0].Rows.Count > 0)
        {
            
            rpt_subcat.DataSource = ds;
            rpt_subcat.DataBind();

        }
        else
        {
            rpt_subcat.DataSource = null;
            rpt_subcat.DataBind();
        }
        
    }
    public void bindCategoryName()
    {
        DataSet ds = ad.CategoryForProduct();
        if (ds.Tables[0].Rows.Count > 0)
        {
            rpt_cat.DataSource = ds;

            rpt_cat.DataBind();
        }
        else
        {
            rpt_cat.DataSource = null;
            rpt_cat.DataBind();
        }

    }

    public void bindAccordingSerach()
    {

        try
        {
            string category, SubCategory;
            if (Page.RouteData.Values["catname"] != null && Page.RouteData.Values["scatname"] != null)
            {

                category = all.RemoveUndeScore(Page.RouteData.Values["catname"].ToString());
                SubCategory = all.RemoveUndeScore(Page.RouteData.Values["scatname"].ToString());
                hf_cat.Text = category;
                hf_cat.NavigateUrl = "/product/" + Page.RouteData.Values["catname"].ToString() + "/All";
                if (SubCategory != "All")
                {
                    SCatID = objscat.getSubCategoryID(SubCategory);
                }
                if (category != "All")
                {
                    CatId = objscat.getCategoryID(category);
                    bindSubCategoryName();
                }
            }
        }
        catch (Exception ex) { }
    }
    

}
﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using BAL;

public partial class control_wishlist : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    public Utility all = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                show();
            }
            else
            {
                Response.Redirect("/login/wishlist");
            }
        }

    }

    public void show()
    {

        DataSet ds = ad.SelectWishListByID(HttpContext.Current.User.Identity.Name);
        if (ds.Tables[0].Rows.Count > 0)
        {
            lblCatName.Text = "Wishlist";

            if (ds.Tables[0].Rows.Count > 16)
            {
                string pg = Request.QueryString["Page"];
                if (!string.IsNullOrEmpty(pg))
                {

                    if (pg != "1")
                    {
                        Int32 cnt = default(Int32);
                        if (ds.Tables[0].Rows.Count > 16 * Convert.ToInt32(pg))
                        {
                            cnt = 16 * Convert.ToInt32(pg);
                        }
                        else
                        {
                            cnt = ds.Tables[0].Rows.Count;
                        }
                        lblcol1.Text = "Showing " + (16 * (Convert.ToInt32(pg) - 1) + 1) + " - " + cnt + " of " + ds.Tables[0].Rows.Count + " Product";
                        lblcol2.Text = "Showing " + (16 * (Convert.ToInt32(pg) - 1) + 1) + " - " + cnt + " of " + ds.Tables[0].Rows.Count + " Product";
                    }
                    else
                    {
                        lblcol1.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product";
                        lblcol2.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product";
                    }
                }
                else
                {
                    lblcol1.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product";
                    lblcol2.Text = "Showing 1 - 16 of " + ds.Tables[0].Rows.Count + " Product";
                }
                CollectionPager1.Visible = true;
                CollectionPager1.DataSource = ds.Tables[0].DefaultView;
                CollectionPager1.BindToControl = DataList2;
                Collectionpager2.Visible = true;
                Collectionpager2.DataSource = ds.Tables[0].DefaultView;
                Collectionpager2.BindToControl = DataList2;
                DataList2.DataSource = CollectionPager1.DataSourcePaged;
            }
            else
            {
                lblcol1.Text = "Showing  " + ds.Tables[0].Rows.Count + " Product";
                lblcol2.Text = "Showing  " + ds.Tables[0].Rows.Count + " Product";
                CollectionPager1.Visible = false;
                Collectionpager2.Visible = false;
                DataList2.DataSource = ds;
                DataList2.DataBind();
            }
            TABLE2.Visible = false;
        }
        else
        {

            DataList2.DataSource = null;
            DataList2.DataBind();
            TABLE2.Visible = true;
            lblCatName.Text = "";
            lblcol2.Text = "";
            lblcol1.Text = "";
        }




    }
    public bool setVisiable(object obj)
    {
        if (obj.ToString() == "")
        {
            return false;

        }
        else
        {
            return true;
        }


    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("categories.aspx");
    }
    public void viewitem()
    {
        try
        {
            control_totalitems ctrlB = (control_totalitems)Page.Master.FindControl("totalitems1");
            LinkButton ddl = (LinkButton)ctrlB.FindControl("lnkbtn");
            int itm = ad.SelPurItem(Session["IsUser"].ToString());
            ddl.Text = itm.ToString() + " Items";
            ddl.Enabled = true;
        }
        catch (Exception ex)
        { }

    }
    public bool setVisiableNewBest(object obj)
    {
        if (obj.ToString() == "False")
        {
            return false;

        }
        else
        {
            return true;
        }


    }
    protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
    {

        if (e.CommandName == "book")
        {

            TextBox txtQuantity = ((TextBox)e.Item.FindControl("txtQuantity"));
            Label lblitemid = ((Label)e.Item.FindControl("lblid"));
            Label lblname = ((Label)e.Item.FindControl("lblname"));
            Label ablStock = ((Label)e.Item.FindControl("lbavlQty"));
            Label lblCode = ((Label)e.Item.FindControl("lblCode"));
            Label price = ((Label)e.Item.FindControl("lblPrice"));
            Label offerprice = ((Label)e.Item.FindControl("lblOfferPrice"));


            if (ad.IsPrdctExistCart(lblitemid.Text, Session["IsUser"].ToString(),"1") == false)
            {
                string amount;
                if (offerprice.Text == "0.00")
                {
                    amount = price.Text;
                }
                else
                {
                    amount = offerprice.Text;
                }
                ad.inserProduct(lblitemid.Text, txtQuantity.Text, Session["IsUser"].ToString(), lblitemid.Text, "", lblname.Text, amount,"1");
                viewitem();
            }
            else
            {
                // ad.UpdatePurchaseItem(lblitemid.Text, txtQuantity.Text, Session["IsUser"].ToString());

                string pid = ad.findPid(lblitemid.Text, Session["IsUser"].ToString());

                if (pid != "0")
                {

                    ad.UpdatePurchaseItem(pid, txtQuantity.Text, Session["IsUser"].ToString());
                }
            }

            show();
            Session["Itemname"] = "Successfully " + lblname.Text + " Added in Cart";
            // JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, Session["Itemname"].ToString(), 1000);
            Session["path"] = HttpContext.Current.Request.Url.AbsoluteUri;
            Response.Redirect(Session["path"].ToString());

        }

        if (e.CommandName == "delete")
        {
            Label lblname = ((Label)e.Item.FindControl("lblname"));
            Label lblitemid = ((Label)e.Item.FindControl("lblWid"));
            ad.Deletewishlist(lblitemid.Text);
            show();
            JqueryNotification.NotificationExtensions.ShowSuccessfulNotification(this, "Item " + lblname.Text + " suceessfully removed from your Wishlist", 1000);

        }

    }
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }

    public string getQTY(object obj)
    {

        return ad.SetQty(obj.ToString());

    }


    protected void lnkbtn_Click(object sender, EventArgs e)
    {
        Response.Redirect("view-cart.aspx");
    }
    protected void lnkChangPassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("change-password.aspx");
    }
    protected void DataList2_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem | e.Item.ItemType == ListItemType.Item)
        {
            Label price = ((Label)e.Item.FindControl("lblPrice"));
            Label offerprice = ((Label)e.Item.FindControl("lblOfferPrice"));
            if (offerprice.Text == "0" || offerprice.Text == "0.00")
            {
                price.Font.Strikeout = false;
                offerprice.Visible = false;

            }
            else
            {
                price.Font.Strikeout = true;
                offerprice.Visible = true;
            }
        }


    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.IO;
using ClassLibrary;
using BAL;
using DAL;
  
public partial class Admin_controls_addSubCategory : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    dlSubCategory objscat = new dlSubCategory();
    SubCategoryCS scat = new SubCategoryCS();
   
    string userid = HttpContext.Current.User.Identity.Name;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            bindCategoryName(ddl_cat);
            bindCategoryName(ddlCat2);
            show();

        }
    }

    protected void btnAddCategory_Click(object sender, EventArgs e)
    {

        try
        {
            scat.CatID = ddl_cat.SelectedItem.Value.ToString();
            scat.SubCategory = txt_scat.Text.Trim();
            scat.Description = txtDesc.Text;
            scat.Submitby = userid;
            scat.Image = bindimg(flbnr);

            bool check = objscat.InsertSubcategory(scat);

            if (check == true)
            {

                showmessage("Catalogue added successfully");
                cleardata();
                show();
            }
            else
            {

                showmessage("Catalogue already Exist!");
            }

        }
        catch(Exception ex)
        { 

        }

    }

    void cleardata()
    {
        ddl_cat.SelectedIndex = 0;
        txt_scat.Text = "";
        txtDesc.Text = "";


    }

    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }


    public string bindimg(FileUpload fl)
    {

        if (fl.HasFile)
        {
            string Type = Path.GetExtension(fl.PostedFile.FileName);


            if (Type.ToLower() == ".jpg" | Type.ToLower() == ".gif" | Type.ToLower() == ".jpeg")
            {
                string pt = null;
                string cpath = null;
                // Dim filmid As String = Request.QueryString("fid")
                Random r = new Random();
                string RandNo = Convert.ToString(r.Next(1000, 1000000));

                pt = Server.MapPath("~/CategoryImages/");
                string filename = null;

                filename = Path.GetFileName(fl.PostedFile.FileName);

                cpath = pt + RandNo + filename;
                if (File.Exists(cpath))
                {
                    File.Delete(cpath);
                }
                //File.Copy(file2.PostedFile.FileName, cpath)


                fl.PostedFile.SaveAs(cpath);

                return RandNo + filename;
            }
            else
            {
                // ShowMessage("Banner logo Images Should be in Jpg,Gif format")
                return "";
            }
        }
        else
        {
            return "";
        }
    }
    public void show()
    {
        try
        {
            string id = "0";
            if (ddlCat2.SelectedItem.Value != "Select")
            {
                id = ddlCat2.SelectedItem.Value;
            }
            scat.CatID = id;
            DataSet ds = objscat.getSubCategoryList(scat);
            if (ds.Tables[0].Rows.Count > 0)
            {
                scat.CatID = ddl_cat.SelectedItem.Value;
                gvrProduct.DataSource = ds;
                gvrProduct.DataBind();
            }
            else
            {
                gvrProduct.DataSource = null;
                gvrProduct.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }

    public string setimage(object obj)
    {
        if (obj != null)
        {
            return "~/ItemImages/" + obj.ToString();
            // string str = Server.MapPath("~/ItemImages/" + obj.ToString());


            //return Server.MapPath("~/ItemImages/" +obj.ToString());
        }
        else
        {

        }
        return obj.ToString();

    }
    protected void gvrProduct_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvrProduct.EditIndex = e.NewEditIndex;
        show();
    }
    protected void gvrProduct_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvrProduct.EditIndex = -1;
        show();
    }

    protected void gvrProduct_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gv = gvrProduct.Rows[e.RowIndex];
            HiddenField hdnimage = ((HiddenField)gv.FindControl("HiddenField1Image"));
            Label lblid = ((Label)gv.FindControl("lblEditid"));
            TextBox txtcat = ((TextBox)gv.FindControl("txtcat1"));
            FileUpload imgupload = ((FileUpload)gv.FindControl("fileuploadUpdate"));
            Label prevsrt = ((Label)gv.FindControl("lblPrvsrt"));
            TextBox srt = ((TextBox)gv.FindControl("txtSrt"));
            //TextBox des = ((TextBox)gv.FindControl("txtDes"));


            Winthusiasm.HtmlEditor.HtmlEditor des = ((Winthusiasm.HtmlEditor.HtmlEditor)gv.FindControl("txtDes"));

            string imagename;
            string sm1 = bindimg(imgupload);
            if (sm1 == "")
            {
                imagename = hdnimage.Value;
            }
            else
            {
                imagename = sm1;
            }



            scat.SubCatID = lblid.Text;
            scat.SubCategory = txtcat.Text;
            scat.Description = des.Text;
            scat.Image = imagename;
            scat.SortID = srt.Text;

            string srtid = srt.Text;
            if (srt.Text != prevsrt.Text)
            {
                srtid = UpdateSortid(srt.Text);
            }

            bool check = objscat.updateSubCategory(scat);

            if (check == true)
            {

                gvrProduct.EditIndex = -1;
                show();
                showmessage("successfully updated");



            }
            else
            {


                showmessage("Catalogue already Exist!");
            }

        }
        catch (Exception ex)
        {

        }

    }

    public string UpdateSortid(string srt)
    {
        try
        {
            if (objscat.chkSubCatSortid(srt) == false)
            {
                System.Data.DataSet ds = objscat.SelSubCatbysrtid(srt);
                int i = 0;
                int sn = 0;
                int srtid = 0;
                string itmid = null;
                i = ds.Tables[0].Rows.Count;
                if (i > 0)
                {
                    for (sn = 0; sn <= i - 1; sn++)
                    {
                        itmid = ds.Tables[0].Rows[sn]["SubCatID"].ToString();
                        srtid = Convert.ToInt32(ds.Tables[0].Rows[sn]["sortid"]);
                        objscat.UpdateSortid((srtid + 1).ToString(), itmid);
                    }
                }
            }
            return srt;
        }
        catch (Exception ex)
        { return srt; }
    }
    protected void gvrProduct_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow rw = gvrProduct.Rows[e.RowIndex];
        Label lbid = ((Label)rw.FindControl("lblid"));
        objscat.DelecteSubCategory(lbid.Text);
        show();

    }
    protected void gvrProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvrProduct.PageIndex = e.NewPageIndex;
        show();
    }

    public bool setVisiable(object obj)
    {
        if (obj.ToString() == "")
        {
            return false;

        }
        else
        {
            return true;
        }


    }


    protected void gvrProduct_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
            {
                int id = Convert.ToInt32(gvrProduct.DataKeys[e.Row.RowIndex].Value);

                Label lblgn = (Label)e.Row.FindControl("lblVisi");
                CheckBox chk = (CheckBox)e.Row.FindControl("chkHide");

                if (lblgn.Text == "True")
                {
                    chk.Checked = true;

                }
                else
                {
                    chk.Checked = false;
                }

            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void chkHide_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox chk = (CheckBox)sender;
            GridViewRow gr = (GridViewRow)chk.Parent.Parent;
            Label lblgn = (Label)gr.FindControl("lblVisi");
            string id = gvrProduct.DataKeys[gr.RowIndex].Value.ToString();

            if (lblgn.Text == "True")
            {
                objscat.updSubCatVisibility("False", id);
            }
            else
            {
                objscat.updSubCatVisibility("True", id);
            }

            showmessage("successfully updated");

            show();
        }
        catch (Exception ex)
        {

        }
    }

    public void bindCategoryName(DropDownList cat)
    {
        try
        {
            DataSet ds = ad.CategoryForProduct();
            if (ds.Tables[0].Rows.Count > 0)
            {
                cat.DataSource = ds;
                cat.DataTextField = "Category";
                cat.DataValueField = "catid";
                cat.DataBind();
                cat.Items.Insert(0, "Select");
            }
            else
            {
                cat.DataSource = null;
                cat.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void ddl_cat_SelectedIndexChanged(object sender, EventArgs e)
    {
        show();
    }
}

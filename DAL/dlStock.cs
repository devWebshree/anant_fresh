﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data.Common;
using ClassLibrary;

namespace DAL
{
    public class dlStock
    {
       Database db = GetDatabase(AppDatabaseName);
       private static string AppDatabaseName;
       private static Database GetDatabase(string dbName)
       {
           try
           {
               Database db = null;
               if (dbName == null)
               {
                   return db = DatabaseFactory.CreateDatabase();
               }
               else
               {
                   return db = DatabaseFactory.CreateDatabase(dbName);
               }
               return db;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       static dlStock()
       {
           AppDatabaseName = ConfigurationSettings.AppSettings["AppDatabaseName"];
       }

       public bool insertItemQuantity(Stock obj)    
        {
            try
            {
                DbCommand cmd = db.GetStoredProcCommand("spStock");
                db.AddInParameter(cmd, "@ItemId", DbType.String, obj.ItemId);
                db.AddInParameter(cmd, "@AvailableQuantity", DbType.String, obj.AvailableQuantity);
                db.AddInParameter(cmd, "@AlertOnQuantity", DbType.String, obj.AlertOnQuantity);
                db.AddInParameter(cmd, "@Action", DbType.String, "insert");
                int i = db.ExecuteNonQuery(cmd);

                if (i > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public bool updateItemWithStock(Stock obj)  
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spStock");
               db.AddInParameter(cmd, "@ItemId", DbType.String, obj.ItemId);
               db.AddInParameter(cmd, "@AvailableQuantity", DbType.String, obj.AvailableQuantity);
               db.AddInParameter(cmd, "@AlertOnQuantity", DbType.String, obj.AlertOnQuantity);
               db.AddInParameter(cmd, "@Action", DbType.String, "updateItemWithStock");
               int i = db.ExecuteNonQuery(cmd);

               if (i > 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }




       public bool updateReduceItemQuantity(Stock obj)
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spStock");
               db.AddInParameter(cmd, "@ItemId", DbType.String, obj.ItemId);
               db.AddInParameter(cmd, "@ReduceQuantity", DbType.String, obj.ReduceQuantity);
               db.AddInParameter(cmd, "@Action", DbType.String, "updateReduceItemQuantity");
               int i = db.ExecuteNonQuery(cmd);

               if (i > 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }



       public DataSet getStockLog(Search obj) 
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("StockLog");
               db.AddInParameter(cmd, "@ProductID", DbType.String, obj.ProductID); 
               db.AddInParameter(cmd, "@SearchBy", DbType.String, obj.SearchBy);
               db.AddInParameter(cmd, "@SubCatID", DbType.String, obj.SubCatID);
               db.AddInParameter(cmd, "@CatID", DbType.String, obj.CatID);
               db.AddInParameter(cmd, "@ToDate", DbType.String, obj.ToDate);
               db.AddInParameter(cmd, "@FromDate", DbType.String, obj.FromDate);
  
               return db.ExecuteDataSet(cmd);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public bool updateOrderedProduct(Stock obj)
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spUpdateOrderedProduct");
               db.AddInParameter(cmd, "@NewQuantity", DbType.Int32, obj.NewQuantity);
               db.AddInParameter(cmd, "@PurchageId", DbType.Int32, obj.PurchageId);
               int i = db.ExecuteNonQuery(cmd);

               if (i > 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }



       public bool updateAddItemQuantity(Stock obj) 
       {
           try
           {
               DbCommand cmd = db.GetStoredProcCommand("spStock");
               db.AddInParameter(cmd, "@ItemId", DbType.String, obj.ItemId);
               db.AddInParameter(cmd, "@AddQuantity", DbType.String, obj.AddQuantity);
               db.AddInParameter(cmd, "@Action", DbType.String, "updateAddItemQuantity");
               int i = db.ExecuteNonQuery(cmd);

               if (i > 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


       public string selectSomeOfPriceByOrder(string orderno)  
       {
           try
           {
               DbCommand cmd = db.GetSqlStringCommand("SELECT SUM(price * quantity) AS total FROM PurChaseTable WHERE  orderno=@orderno");
               db.AddInParameter(cmd, "@orderno", DbType.String, orderno);
               return db.ExecuteScalar(cmd).ToString();
           }
           catch (Exception ex)
           { return ""; }

       }

       public string getTotalQuantityByOrderId(string orderno)  
       {
           try
           {
               DbCommand cmd = db.GetSqlStringCommand("SELECT SUM(quantity) AS TotalQuantity FROM PurChaseTable WHERE  orderno=@orderno");
               db.AddInParameter(cmd, "@orderno", DbType.String, orderno);
               return db.ExecuteScalar(cmd).ToString();
           }
           catch (Exception ex)
           { return ""; }

       }

       public string selectQuantity(string itemid)
       {
           try
           {
           DbCommand cmd = db.GetSqlStringCommand("SELECT qty FROM StockTable WHERE  itemid=@itemid");
           db.AddInParameter(cmd, "@itemid", DbType.String, itemid);
           return db.ExecuteScalar(cmd).ToString();
           }
           catch (Exception ex)
           { return ""; }


       }


       public int getScopIdentityOfItem() 
       {
           DbCommand cmd = db.GetSqlStringCommand("SELECT MAX(Id) FROM itemdetailtable");
           return Convert.ToInt32(db.ExecuteScalar(cmd));
       }
    }
}

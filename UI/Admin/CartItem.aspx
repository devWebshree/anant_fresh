﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="CartItem.aspx.cs" Inherits="Admin_CartItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:GridView id="GridView1" CssClass="table" runat="server" onrowdeleting="GridView1_RowDeleting" PageSize="20" DataKeyNames="pid" AutoGenerateColumns="False">
    <Columns>
        <asp:TemplateField HeaderText="Item">
            <ItemStyle Font-Bold="False" HorizontalAlign="Center"  />
            <ItemTemplate>
                <asp:Label ID="lblitemid" runat="server" Text='<%# bind("itemid") %>' 
                    Visible="False"></asp:Label>
                      <asp:Label ID="lblPid" runat="server" Text='<%# bind("pid") %>' 
                    Visible="False"></asp:Label>
                <table class="intble">
                    <tr>
                        <td align="center" width="90">
                            <asp:Image ID="sd" runat="server" alt="" 
                              ImageUrl='<%# GetImg(Eval("itemid")) %>'  />
                        </td>
                        <td align="left">
                            <asp:Label ID="lblitrm" runat="server" Text='<%# bind("item") %>'></asp:Label>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Amount">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# "$"+Eval("price") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# "$"+Eval("price") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Quantity">
            <ItemStyle Font-Bold="False" Font-Size="10px" HorizontalAlign="Center" 
                Width="150px"  />
            <ItemTemplate>
                <asp:TextBox ID="txtqty" runat="server" AutoPostBack="true" CssClass="txt_1" 
                    MaxLength="9" OnTextChanged="txtqty_TextChanged" 
                    Text='<%# bind("quantity")  %>' ></asp:TextBox>
                <asp:Label ID="lblstock" runat="server" Visible="false" CssClass="stk" Text='<%# GetStock(Eval("itemid"),Eval("quantity")) %>' 
                   ></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
               
                    <asp:LinkButton ID="dellnk" runat="server" CausesValidation="false" 
                        CommandName="Delete" OnClientClick="return confirm('Do You Want To Delete?')" 
                        text="Delete" CssClass="button"></asp:LinkButton>
              
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" Width="75px"  />
        </asp:TemplateField>
    </Columns>
     <HeaderStyle  ForeColor="Black" Height="30px" BorderColor="White" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
   <RowStyle HorizontalAlign="Center" BackColor="#E4E6E8" BorderColor="White" Height="30px" />
   <PagerStyle BackColor="#BDC3C7" ForeColor="White" HorizontalAlign="Center" />
 <HeaderStyle BackColor="#BDC3C7" Font-Bold="True"  
       Font-Size="13px" ForeColor="Black" HorizontalAlign="Center"  />
   <AlternatingRowStyle HorizontalAlign="Center" BackColor="#E4E6E8" ForeColor="Black" />
</asp:GridView>
</asp:Content>


﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BAL;
using ClassLibrary;
using DAL;
public partial class admin_controls_userlist : System.Web.UI.UserControl
{
    modifyData ad = new modifyData();
    SendMail sm = new SendMail();

    SalesPerson blsales = new SalesPerson();
    dlSalesPerson objsales = new dlSalesPerson();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindSalesPerson();
        }
    }


    public void bindSalesPerson()
    {

        try
        {
            blsales.Searchtxt = txtsearch.Text;
            DataSet ds = objsales.getSalesPerson(blsales);

            if (ds.Tables[0].Rows.Count > 0)
            {
                grdSalesPerson.DataSource = ds;
                grdSalesPerson.DataBind();
            }
            else
            {
                grdSalesPerson.DataSource = null;
                grdSalesPerson.DataBind();
            }
        }
        catch( Exception ex)
        {

        }
      
    }


    protected void btnRegister_Click(object sender, EventArgs e)
    {
        blsales.Name = txtname.Text;
        blsales.Phone = txtMobile.Text;
        blsales.Email = txtEmail.Text;
        blsales.Address = txtAddress.Text;
        if (objsales.insertSalesPerson(blsales))
        {
           txtname.Text="";
           txtMobile.Text = "";
           txtEmail.Text = "";
           txtAddress.Text = "";
           bindSalesPerson();
           showmessage("Account Successfully Created.");
        }
        else
        {
            showmessage("Somthing Wrong.");
        }
    }
    public void showmessage(string str)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "clientScript", " alert('" + str + "')", true);
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bindSalesPerson();
    }
 

    protected void grdSalesPerson_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdSalesPerson.PageIndex = e.NewPageIndex;
        bindSalesPerson();
    }
    protected void grdSalesPerson_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow gr = grdSalesPerson.Rows[e.RowIndex];
        int SalesPersonId =Convert.ToInt32( ((Label)gr.FindControl("lblSalesPersonId")).Text);
        blsales.SalesPersonId = SalesPersonId;
        objsales.deleteSalesPerson(blsales);
        bindSalesPerson();

    }
  
    protected void grdSalesPerson_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdSalesPerson.EditIndex = e.NewEditIndex;
        bindSalesPerson();
    }
    protected void grdSalesPerson_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        grdSalesPerson.EditIndex = -1;
        bindSalesPerson();
    }
    protected void grdSalesPerson_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
     GridViewRow gv = grdSalesPerson.Rows[e.RowIndex];
     TextBox txtName = ((TextBox)gv.FindControl("txtName"));
     TextBox txtEmail = ((TextBox)gv.FindControl("txtEmail"));
     TextBox txtPnone = ((TextBox)gv.FindControl("txtPhone"));//
     TextBox txtAddress = ((TextBox)gv.FindControl("txtAddress"));
     Label SalesPersonId = ((Label)gv.FindControl("lblSalesPersonId")); 



     blsales.Name = txtName.Text;
     blsales.Email = txtEmail.Text;
     blsales.Phone = txtPnone.Text;
     blsales.Address = txtAddress.Text;
     blsales.SalesPersonId =Convert.ToInt32(SalesPersonId.Text);
     objsales.updateSalesPerson(blsales);
     grdSalesPerson.EditIndex = -1;
     bindSalesPerson();
     showmessage("successfully updated");
     
    }
}

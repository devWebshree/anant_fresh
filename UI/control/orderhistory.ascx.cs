using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BAL;

public partial class control_orderhistory : System.Web.UI.UserControl
{
    modifyData md = new modifyData();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                bindOrderHistory();
            }
            else
            {
                Response.Redirect("/login");
            }
        }
    }


    public void bindOrderHistory()
    {
        DataSet ds = md.SelFinalProducts(HttpContext.Current.User.Identity.Name);
        if (ds.Tables[0].Rows.Count > 0)
        {
            GridView2.DataSource = ds;
            GridView2.DataBind();
        }
        else
        {
            GridView2.DataSource = null;
            GridView2.DataBind();
        }
    }
    public static string GetDis(object st)
    {

        if (st.ToString() == "0")
        {
            return "Under Process";

        }
        else if (st.ToString() == "1")
        {
            return "Cancel request";

        }
        else if (st.ToString() == "2")
        {
            return "Cancellation  Confirmed";

        }
        else if (st.ToString() == "3")
        {
            return "Order Dispatched";
        }
        else if (st.ToString() == "4")
        {
            return "Dispatch In Process";
        }
        
        else
        {
            return "Under Process";
        }
    }

    protected void lnkclose_Click(object sender, EventArgs e)
    {

    }
    protected void lnkord_Click(object sender, EventArgs e)
    {
        Response.Redirect("my-account.aspx");
    }
    protected void mylnk_Click(object sender, EventArgs e)
    {
        Response.Redirect("reg-details.aspx");
    }
    protected void chglnk_Click(object sender, EventArgs e)
    {
        Response.Redirect("/changepassword");
    }
    protected void GridView2_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
         GridView2.PageIndex = e.NewPageIndex;
        GridView2.DataSource = md.SelFinalProducts(HttpContext.Current.User.Identity.Name);
        GridView2.DataBind();
    }
    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "dtl")
            {
                GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                string lnlorder = ((LinkButton)row.FindControl("ord")).Text;
                Response.Redirect("/OrderDetails/" + lnlorder);
            }
            if (e.CommandName == "can")
            {
                GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                string orderId = ((Label)row.FindControl("lbl_id")).Text;
                string lnlorder = ((LinkButton)row.FindControl("ord")).Text;
                md.UpdtFnlDtl(orderId, "1", "", "", "");
               // SendMail sm = new SendMail();
               // sm.orderStatus(HttpContext.Current.User.Identity.Name, lnlorder, HttpContext.Current.User.Identity.Name, "Your AnantFresh.com Order Cancellation Request has been Received!");
                bindOrderHistory();
            }
        }
        catch (Exception ex) { }
    }
    protected void dgrd_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hf_status = (HiddenField)e.Row.FindControl("hf_dispatch");
                LinkButton link = (LinkButton)e.Row.FindControl("lb_cancel");
                if (hf_status.Value=="0")
                {
                  
                    link.Enabled = true;
                }
                else
                {
                    link.CssClass = "_butt_cancle";
                    link.ToolTip = "You can't cancel this order";
                    link.Enabled = false;
                }
            }
        }
        catch (Exception ex) { }
    }
}

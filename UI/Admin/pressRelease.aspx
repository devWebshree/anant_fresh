﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="pressRelease.aspx.cs" Inherits="Admin_addUser" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script src="../js/jquery-1.11.0.js" type="text/javascript"></script>

  
<script type="text/javascript">

    $(document).ready(function () {
        $("#pressrelease").addClass('active');

        $('#pnl').hide();
        $('.ProductTitle').on('click', function () {

            $(this).children('img').each(function () {
                $(this).toggle(0);
            });

            $('#pnl').slideToggle();
        });

    });
</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 <style type="text/css">
 .close {
    display:none;
}
 </style>

  <div class="row" id="tag">
 <div class="ProductTitle" style="width:200px">

  <img id="plumin" src="../images/plus.jpg" class="open" alt="Open"  />
      <img src="../images/minus.jpg" class="close" alt="Close" />

  <span style="float:right; padding:5px;"><h2>ADD Press Release </h2></span>  
 </div>
 </div>

 <div class="clear"></div>
 <div class="row">
 <div class="column"  id="pnl">
  <ul class="productul">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
     </ContentTemplate>
                    </asp:UpdatePanel>
            <div class="center">
        <ul class="lgnul m20">


         <li>
                <label for="input" class="lbl"> Name</label>
                <div class="inputbox">
                    <asp:TextBox ID="txtname" runat="server" CssClass="input"></asp:TextBox>&nbsp;

                    <cc2:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtname"
                        CssClass="error1" Display="Dynamic" ErrorMessage="Enter Name."
                        ValidationGroup="admingvreg"></cc2:RequiredFieldValidator>
                </div>
        </li>

        

                <li>

                  <br />
                <label for="input" class="lbl"> Image</label>
                <div class="inputbox">
                    <asp:FileUpload ID="fuImage" runat="server" />
                </div>
       </li>

      



            <li>
              <div class="inputbox">
                           <br />
             <br />
                <asp:Button ID="btnRegister" runat="server"  Width="150px" Height="30px"
                    CssClass="submit right" Text="Submit" 
                    ValidationGroup="admingvreg" onclick="btnRegister_Click" />
                    </div>
            </li>
        </ul>
</div>

 
   </div>                 


 <div class="clear"></div>

                        <div class="menu clearfix m10">
                        <ul class="parentul">
                          <li style="width:800px;">
<a>
<asp:TextBox ID="txtsearch" placeholder="Search with Name/Role/mobile/Username/" CssClass="inputtxt" runat="server"></asp:TextBox>   

                                    </a></li>
                                <li><asp:LinkButton ID="btnsearch" runat="server" OnClick="btnsearch_Click"  Text="Search" ValidationGroup="grpvalusrsrch" /></li>
                        </ul>
                        </div>


 <div class="clear"></div>


 <div class="column m10"  id="Div1">

                    
              <asp:GridView ID="grdPress" CssClass="table" runat="server" AutoGenerateColumns="false">
              <Columns>

                        <asp:TemplateField HeaderText="Serial No.">
                        <ItemTemplate>

                    <strong> <%# Container.DataItemIndex + 1 %></strong>
              

                            </ItemTemplate>
                  </asp:TemplateField>


              <asp:TemplateField HeaderText="Name">
              <ItemTemplate>
              <%# Eval("name")%>
              <asp:HiddenField ID="fhId" runat="server" Value='<%# Eval("id") %>' />
              </ItemTemplate>
              </asp:TemplateField>

            
              <asp:TemplateField HeaderText="Image">
              <ItemTemplate>
            <div class="">
              <img src="../ProductImage/PressRelease/<%# Eval("imagepath") %>" Width="200" Height="150"/>
            </div>
              </ItemTemplate>
              </asp:TemplateField>


          


              <asp:TemplateField HeaderText="Delete">
              <ItemTemplate>
             <asp:Button ID="btnDelete" runat="server"  Text="Delete" OnClientClick="return confirm('Are you sure?')" CssClass="button" OnClick="OnClick_btnDelete"/>            
              </ItemTemplate>
              </asp:TemplateField>


              </Columns>
              </asp:GridView>      
                    
                    
                    
                    
                    </div></div>

</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using WebshreeCommonLibrary;
using System.Data.Common;
using DlResultDetails;
using ClassLibrary;


namespace DAL
{
    public class dalResult
    {
        Result obj;
        Database db = ConnectDb.GetDatabase();

        public TransportationPacket getResult(TransportationPacket packet)
        {
            try
            {
                DataSet ds = new DataSet();
                obj = (Result)packet.MessagePacket;
                DbCommand cmd = db.GetStoredProcCommand("spExamResult");


                db.AddInParameter(cmd, "@Name", DbType.String, obj.Name);
                db.AddInParameter(cmd, "@RollNumber", DbType.String, obj.RollNumber);

               //db.AddInParameter(cmd, "@Class", DbType.String, obj.Class);

                db.AddInParameter(cmd, "@action", DbType.String, "getlist");
                ds = (DataSet)db.ExecuteDataSet(cmd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    packet.MessageResultset = (DataSet)ds;
                    packet.MessageStatus = EStatus.Success;
                }
                else
                {
                    packet.MessageStatus = EStatus.Failure;
                }
            }
            catch (Exception ex)
            {
                packet.MessageResultset = EStatus.Exception;
                HandleExceptions.ExceptionLogging(ex.Source, ex.Message, true);
            }
            return packet;
        }

        public TransportationPacket getCounter(TransportationPacket packet)  
        {
            try
            {
               
                DbCommand cmd = db.GetStoredProcCommand("spNoOfView");
               
                db.AddInParameter(cmd, "@Action", DbType.String, "getlist");
                object ReturnValue = db.ExecuteScalar(cmd);
                if (ReturnValue != DBNull.Value && ReturnValue != null && ReturnValue != "")
                {
                    packet.MessageStatus = EStatus.Success;
                    packet.MessagePacket = ReturnValue;
                }
                else
                {
                    packet.MessageStatus = EStatus.Failure;
                }

            }
            catch (Exception ex)
            {
                packet.MessageResultset = EStatus.Exception;
                HandleExceptions.ExceptionLogging(ex.Source, ex.Message, true);
            }
            return packet;
        }
        public TransportationPacket insertViewCounter(TransportationPacket packet)
        {
            try
            {
                DbCommand cmd = db.GetStoredProcCommand("spNoOfView");
                db.AddInParameter(cmd, "@action", DbType.String, "insert");
                int ReturnValue = db.ExecuteNonQuery(cmd);
                if (ReturnValue > 0)
                {
                    packet.MessageStatus = EStatus.Success;
                }
                else
                {
                    packet.MessageStatus = EStatus.Failure;
                }
            }
            catch (Exception ex)
            {
                packet.MessageResultset = EStatus.Exception;
                HandleExceptions.ExceptionLogging(ex.Source, ex.Message, true);
            }
            return packet;
        }
    }
}

﻿<%@ Page Title="Order-Detail" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="orderList.aspx.cs" Inherits="admin_order_details" %>


<%@ Register src="controls/orderList.ascx" tagname="orderList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


<script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#orderActive").addClass('active');

    });
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:orderList ID="orderList" runat="server" />
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true"
    CodeFile="PrivacyPolicy.aspx.cs" Inherits="PrivacyPolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <h3 class="title1 pd ">
            Privacy Policy and Security at AnantFresh.com</h3>
        <div class="clear">
        </div>
        <p class="m5">
            At AnantFresh, do not publish, sell or rent your personal information to third parties
            for their marketing purposes without your explicit consent. Please read this privacy
            policy to learn more about the ways in which we use and protect your personal information.
            This Privacy Policy describes the information, as part of the normal operation of
            our services; we collect from you and what may happen to that information. By accepting
            the Privacy Policy and the User Agreement in registration, you expressly consent
            to our use and disclosure of your personal information in accordance with this Privacy
            Policy. This Privacy Policy is incorporated into and subject to the terms of the
            User Agreement. This Privacy Policy is effective upon acceptance in registration
            for new registering users.</p>
        <h4 class="title2 pd">
            Note:</h4>
        <p class="m5">
            Our privacy policy is subject to change at any time without notice. To make sure
            you are aware of any changes, please review this policy periodically.
            <br />
            By visiting this website you agree to be bound by the terms and conditions of this
            Privacy Policy. If you do not agree please do not use or access our Site.
        </p>
        <h4 class="title2 pd">
            Your Privacy</h4>
        <p class="m5">
            At AnantFresh.com, we are extremely proud of our commitment to protect your privacy.
            We value your trust in us. We will work hard to earn your confidence so that you
            can enthusiastically use our products and recommend us to friends and family. Please
            read the following policy to understand how your personal information will be treated
            as you make full use of our Site.
        </p>
        <h4 class="title2 pd">
            Registration</h4>
        <p class="m5">
            When you use our Site, we collect and store your personal information from you.
            Our primary goal in doing so is to provide a safe, efficient, smooth and customized
            experience. This allows us to provide services and features that most likely meet
            your needs, and to customize our Site to make your experience safer and easier.
            Importantly, we only collect personal information about you that we consider necessary
            for achieving this purpose.
            <br />
            In general, you can browse the Site without telling us who you are or revealing
            any personal information about yourself. Once you give us your personal information,
            you are not anonymous to us. To fully use our Site, you will need to register using
            our online registration form, where you may be required to provide us with your
            name, date of birth, contact number, email id, user id, password, residence / place
            of business information, billing information, shipping information.Where possible,
            we indicate which fields are required and which fields are optional. You always
            have the option to not provide information.
        </p>
        <h4 class="title2 pd">
            Cookies
        </h4>
        <p class="m5">
            We use data collection devices such as "cookies" on certain pages of the Site to
            help analyze our web page flow, measure promotional effectiveness, and promote trust
            and safety. "Cookies" are small files placed on your hard drive that assist us in
            providing our services. We offer certain features that are only available through
            the use of a "cookie". We also use cookies to allow you to enter your password less
            frequently during a session. Cookies can also help us provide information that is
            targeted to your interests. Most cookies are "session cookies," meaning that they
            are automatically deleted from your hard drive at the end of a session. You are
            always free to decline our cookies if your browser permits, although in that case
            you may not be able to use certain features on the Site and you may be required
            to reenter your password more frequently during a session.
        </p>
        <h4 class="title2 pd">
            Security</h4>
        <p class="m5">
            AnantFresh.com has attempted to take all reasonable steps to have physical, and
            procedural safeguards in place to protect personal information gathered through
            this website. This includes compliance with related federal regulations and may
            include use of industry-standard Secure Socket Layer (SSL) encryption to prevent
            any loss, misuse, unauthorized access, disclosure, or modification of personal information.
            Unfortunately, no data transmission over the Internet can be guaranteed to be 100%
            secure. As a result, while we strive to protect your personal information, we cannot
            guarantee or warrant the security of any information you transmit from our web sites,
            and you do so at your own risk.</p>
    </div>
</asp:Content>

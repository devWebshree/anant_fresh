﻿<%@ Page Title="Add Category" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="addcategory.aspx.cs" Inherits="admin_addcategory" %>

<%@ Register src="controls/addcatgory.ascx" tagname="addcatgory" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script src="../js/jquery-1.11.0.js" type="text/javascript"></script>
  
<script type="text/javascript">

    $(document).ready(function () {
        $("#addcatActive").addClass('active');

        $('#pnl').hide();
        $('.ProductTitle').on('click', function () {
//            debugger;
//            var flag = $('#plumin').attr("data-flag");
//         
//            if (flag = 1) {
//                $('#plumin').attr("src", "../../images/plus.jpg");
//                $('#plumin').attr("data-flag", "2");
//            }
//            else {
//                $('#plumin').attr("src", "../../images/minus.jpg");
//                $('#plumin').attr("data-flag", "1");
//            }
          
                $(this).children('img').each(function () {
                    $(this).toggle(0);
                });
           

            $('#pnl').slideToggle();
        });

    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:addcatgory ID="addcatgory1" runat="server" />
</asp:Content>

